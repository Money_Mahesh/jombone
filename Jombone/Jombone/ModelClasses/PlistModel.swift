//
//  PlistModel.swift
//  Jombone
//
//  Created by Money Mahesh on 04/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation

class PlistModel: CheckBoxCardDataModel {
    var valueCode: String!
    var displayValue: String!
    var state: Bool!
    
    init(data: Any) {
       
        if let dict = data as? NSDictionary,
            let valueCode = dict.value(forKey: "valueCode") as? String,
            let displayValue = dict.value(forKey: "displayValue") as? String,
            let state = dict.value(forKey: "state") as? Bool {
            
            self.valueCode = valueCode
            self.displayValue = displayValue
            self.state = state
        }
    }
    
    class func createSearchParameter(_ plistName : String, rootName : String, indexArray : [Int]) -> [PlistModel]! {
        let plistModelObjArray: [PlistModel] = PlistUtiliy.parsePlistAndReturnPlistModel(plistName)
        
        var selectedPlistModelObjArray = [PlistModel]()
        for currentIndex in indexArray {
            selectedPlistModelObjArray.append(plistModelObjArray[currentIndex])
        }
        return selectedPlistModelObjArray
    }
    
    //MARK: - Default Constructor
    init() {
        displayValue = ""
        valueCode = ""
    }
}
