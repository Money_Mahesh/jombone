//
//  JobHeaderView.swift
//  Jombone
//
//  Created by dev139 on 25/02/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import UIKit

protocol JobDetailHeaderProtocol: JobCardViewProtocol {
    var endDate : String? {get set}
}

class JobHeaderView: CustomView, JobAction, CompanyAction {
    
    @IBOutlet weak var imgViewForCompanylogo: UIImageView?
    @IBOutlet weak var lblForPostionName: UILabel?
    @IBOutlet weak var lblForCompanyName: UILabel?
    @IBOutlet weak var lblForAddress: UILabel?
    
    @IBOutlet weak var lblForPayRate: UILabel?
    @IBOutlet weak var lblForShiftTime: UILabel?
    @IBOutlet weak var lblForShiftEndTime: UILabel?
    @IBOutlet weak var lblForJobType: UILabel?
    
    @IBOutlet weak var lblForStartDate: UILabel?
    @IBOutlet weak var lblForEndDate: UILabel?
    
    @IBOutlet weak var btnForIsLiked: UIButton?
    @IBOutlet weak var btnForIsFollowed: UIButton?
    
    var dataModel: JobDetailHeaderProtocol?
    weak var jobDelegate: JobAction?
    weak var companyDelegate: CompanyAction?
    
    override func xibLoaded(data: Any) {
        
        jobDelegate = self
        companyDelegate = self
        dataModel = (data as? JobModel)
        
        lblForPostionName?.text = dataModel?.jobTitle
        lblForCompanyName?.text = dataModel?.employerName
        lblForAddress?.text = dataModel?.location
        lblForPayRate?.text = String(dataModel?.rate ?? 0) + "/" + (dataModel?.payType ?? "")
        lblForShiftTime?.text = dataModel?.shiftTimeFrom
        lblForShiftEndTime?.text = dataModel?.shiftTimeTo
        lblForJobType?.text = dataModel?.jobType
        lblForStartDate?.text = dataModel?.startDate
        lblForEndDate?.text = dataModel?.endDate
        
        //imgViewForCompanylogo?.backgroundColor = UIColor(hexFromString: "#DCDCDC")
        if let _imageUrlStr = dataModel?.logoPath,
            let imageUrl = URL(string: (_imageUrlStr)) {
            
            let url_request = URLRequest(url: imageUrl)
            self.imgViewForCompanylogo?.setImageWithURLAlamofire(url_request, placeholderImageName: "", success: {
                [weak self] (request: URLRequest?, image: UIImage?) -> Void in
                self?.imgViewForCompanylogo?.backgroundColor = UIColor.clear
                self?.imgViewForCompanylogo?.image = image
                }
                ,failure: {
                    [weak self] (request:URLRequest?, error:Error?) -> Void in
                    self?.imgViewForCompanylogo?.image = UIImage(named: "company_detail_placeholder")
                    print("Not able to load profile pic" + (error?.localizedDescription ?? ""))
            })
        }
        else {
            imgViewForCompanylogo?.image = UIImage(named: "company_detail_placeholder")
        }
        
        setFollowStatus()
        setLikeStatus()
    }
    
    
    private func setFollowStatus() {
        let isFollowed = dataModel?.isFollowed ?? false
        isFollowed ? btnForIsFollowed?.setTitleForAllState(title: "Followed") : btnForIsFollowed?.setTitleForAllState(title: "Follow")
        btnForIsFollowed?.tintColor = UIColor(white: 1, alpha: (isFollowed ? 1.0 : 0.6))
    }
    
    private func setLikeStatus() {
        let isFavorite = dataModel?.isFavorite ?? false
        isFavorite ? btnForIsLiked?.setTitleForAllState(title: "Liked") : btnForIsLiked?.setTitleForAllState(title: "Like")
        btnForIsLiked?.tintColor = UIColor(white: 1, alpha: (isFavorite ? 1.0 : 0.6))
    }
    
    func dataUpdated() {
        NotificationCenter.default.post(name: NSNotification.Name.ReloadJobList, object: nil)
    }
    
    //MARK:- IBActions
    @IBAction func btnActionForLike(_ sender: UIButton) {
        guard let jobId = dataModel?.jobId, let isFavorite = dataModel?.isFavorite else {
            return
        }
        
        jobDelegate?.like_DislikeJob(isLiked: !isFavorite, jobEncId: jobId, completion: { (status: Bool) in
            if status {
                self.dataModel?.isFavorite = !(isFavorite)
                self.setLikeStatus()
                self.dataUpdated()
            }
        })
    }
    
    @IBAction func btnActionForShare(_ sender: UIButton) {
        
        guard let shareUrl = dataModel?.jobURL else {
            return
        }
        jobDelegate?.share(shareDetail: [shareUrl])
    }
    
    @IBAction func btnActionForIsFollowed(_ sender: UIButton) {
        
        guard let companyId = dataModel?.companyId, let isFollowed = dataModel?.isFollowed else {
            return
        }
        companyDelegate?.follow_UnFollowCompany(isFollowed: !isFollowed, companyEncId: companyId, completion: { (status: Bool) in
            if status {
                self.dataModel?.isFollowed = !(isFollowed)
                self.setFollowStatus()
                self.dataUpdated()
            }
        })
    }
    
    @IBAction func companyLogoClicked(_ sender: UIButton) {
        
        guard let companyId = dataModel?.companyId else {
            return
        }
        companyDelegate?.companyDetail(empEncId: companyId, completion: { (companyDetails) in
            CompanyDetailViewController.show(CompanyDetailViewModel(companyDetail: companyDetails), followedAction: {[weak self] (status) in
                self?.dataModel?.isFollowed = status
                self?.setFollowStatus()
                self?.dataUpdated()
            })
        })
        
    }
    
}
