//
//  DocumentCardView.swift
//  Jombone
//
//  Created by dev139 on 25/02/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import UIKit

struct DocumentCardModel {
    var title: String!
    var downloadLink: String!
}

class DocumentCardView: CustomView, JobAction {
    
    @IBOutlet weak var lblForTitle : UILabel!
    @IBOutlet weak var imgViewForDoc : UIImageView!
    
    var documentCardModel: DocumentCardModel?
    weak var jobDelegate: JobAction?
    var downloadDelegate = DownloadUploadAction()

    override func xibLoaded(data: Any) {
        
        self.jobDelegate = self
        let documentCardModel = data as? DocumentCardModel
        self.documentCardModel = documentCardModel
        lblForTitle.text = documentCardModel?.title
    }

    @IBAction func btnActionForDownlaod(_ sender: UIButton) {
        
        guard let urlStr = documentCardModel?.downloadLink, let fileName = documentCardModel?.title else {
            return
        }
        
        downloadDelegate.download(fileName: fileName, urlStr: urlStr)
    }
}
