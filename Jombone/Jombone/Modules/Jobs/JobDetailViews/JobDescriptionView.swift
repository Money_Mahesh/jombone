//
//  JobDescriptionView.swift
//  Jombone
//
//  Created by dev139 on 25/02/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import UIKit

struct JobDescriptionModel {
    var title: String!
    var desc: String!
    var isHtml: Bool = false
}

class JobDescriptionView: CustomView {

    @IBOutlet weak var lblForTitle : UILabel!
    @IBOutlet weak var lblForDescription : UILabel!
    
    override func xibLoaded(data: Any) {
        
        let jobDescriptionModel = data as? JobDescriptionModel
        lblForTitle.text = jobDescriptionModel?.title
        
        if jobDescriptionModel?.isHtml == true {
            lblForDescription.attributedText = jobDescriptionModel?.desc.htmlToAttributedString
        }
        else {
            lblForDescription.text = jobDescriptionModel?.desc
        }
    }
}
