//
//  JobsDetailViewModel.swift
//  Jombone
//
//  Created by Money Mahesh on 12/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation

class JobsDetailViewModel: NSObject {
    
    var data: JobModel!
    
    init(jobDetail: JobModel) {
        self.data = jobDetail
    }
}
