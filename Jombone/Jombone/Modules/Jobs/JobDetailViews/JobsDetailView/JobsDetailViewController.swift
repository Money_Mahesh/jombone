//
//  JobsDetailViewController.swift
//  Jombone
//
//  Created by dev139 on 25/02/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import UIKit

class JobsDetailViewController: WhiteStatusBarViewController, JobAction {
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var btnForApply: UIButton?

    var viewModel: JobsDetailViewModel!
    weak var jobDelegate: JobAction?
    
    static func show(_ viewModel: JobsDetailViewModel, parentController: UIViewController? = nil) {
        let viewController = JOBS_STORYBOARD.instantiateViewController(withIdentifier: "JobsDetailViewController") as! JobsDetailViewController
        
        viewController.viewModel = viewModel
        (parentController?.navigationController ??
            APPLICATION_INSTANCE.visibleViewController()?.navigationController)?.pushViewController(viewController, animated: true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationBarWithTitle("Job Detail", andLeftButton: .back, andRightButton: .none, withBg: .defaultColor)
        
        jobDelegate = self
        addJobDetailSections()
        setApplyStatus()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.tabBarController?.tabBar.isHidden = true
    }
    
    private func addJobDetailSections() {
        stackView.addArrangedSubview(JobHeaderView(frame: CGRect.zero, data: (viewModel.data)!))
        
        if let jobDesc = viewModel.data.jd, jobDesc.count > 0 {
            stackView.addArrangedSubview(JobDescriptionView(frame: CGRect.zero, data: (JobDescriptionModel(title: "Job Description", desc: jobDesc, isHtml: true))))
        }
        
        if let specialInstruction = viewModel.data.specialInstruction, specialInstruction.count > 0 {
            stackView.addArrangedSubview(JobDescriptionView(frame: CGRect.zero, data: (JobDescriptionModel(title: "Special Instructions", desc: specialInstruction, isHtml: false))))
        }
        
        if let jdDocName = viewModel.data.jdDocName, let jdDocPath = viewModel.data.jdDocPath {
            
            stackView.addArrangedSubview(DocumentCardView(frame: CGRect.zero, data: (DocumentCardModel(title: jdDocName, downloadLink: jdDocPath))))
        }
    }
    
    private func setApplyStatus() {
        let isApplied = viewModel.data?.isApplied ?? false
        let title = isApplied ? "APPLIED" : "APPLY"
        btnForApply?.backgroundColor = UIColor(red: 40.0/255.0, green: 191.0/255.0, blue: 97.0/255.0, alpha: (isApplied ? 0.56 : 1.0))
        btnForApply?.setTitleForAllState(title: title)
    }
    
    func dataUpdated() {
        NotificationCenter.default.post(name: NSNotification.Name.ReloadJobList, object: nil)
    }
    
    //MARK:- IBActions
    @IBAction func btnActionForApply(_ sender: UIButton) {
        
        guard let jobId = viewModel.data?.jobId, let isApplied = viewModel?.data?.isApplied, !isApplied else {
            return
        }
        jobDelegate?.applyJob(jobEncId: jobId, completion: { (status: Bool) in
            if status {
                self.viewModel?.data?.isApplied = !(isApplied)
                self.setApplyStatus()
                self.dataUpdated()
            }
        })
    }
}
