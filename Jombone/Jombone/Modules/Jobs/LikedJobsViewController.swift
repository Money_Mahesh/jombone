//
//  LikedJobsViewController.swift
//  Jombone
//
//  Created by dev139 on 06/05/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import UIKit

class LikedJobsViewController: UIViewController {
    
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var noResultView: UIView?
    
    var viewModel = JobsViewModel()
    
    var tableWidget: GenericTableViewComponent<JobCardViewProtocol, JobCardView>?
    
    static func instance() -> LikedJobsViewController{
        
        let instance = JOBS_STORYBOARD.instantiateViewController(withIdentifier: "LikedJobsViewController") as! LikedJobsViewController
        return instance
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationBarWithTitle("Liked Jobs", andLeftButton: .back, andRightButton: .none, withBg: .defaultColor)
        // Do any additional setup after loading the view.
        
        self.showLoader()
        self.getLikedJobsList()
    }
    
    
    @objc func reloadList() {
        tableWidget?.reloadTable()
    }
    

    func getLikedJobsList() {
        
        viewModel.hitGetLikedJobsApi(completion: { (message) in
            self.hideLoader()
            self.noResultView?.isHidden = self.viewModel.dataAvailable
        }, success: { (message) in
            self.setUpTable()
        }, failure: { (errorMsg) in
            //            self.showAlert(message: errorMsg)
        })
    }
    
    func setUpTable() {
        
        if let _tableWidget = tableWidget {
            _tableWidget.appendNewPage(objArray: viewModel.cardDataModels.filter({$0.isFavorite == true}))
        }
        else {
            tableWidget = GenericTableViewComponent()
            tableWidget?.isHidden = false
            tableWidget?.genericTableViewModel = GenericTableViewModel(
                cellDataModel: viewModel.cardDataModels,
                tableFooterView: (LoadMoreView(frame: CGRect(x: 0, y: 0, width: WINDOW_WIDTH, height: 40)), false),
                tableType: .expandable,
                totalPages: viewModel.totalPages)
            tableWidget?.tableStyle(backGroundColor: UIColor.white, insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
            tableWidget?.delegate = self
            stackView?.addArrangedSubview(tableWidget!)
        }
    }
    

}


extension LikedJobsViewController: GenericTableViewComponentDelegate {
    
    func genericTableDidSelectRowAt(_ indexPath: IndexPath, dataSource: inout [Any]) -> Bool {
        
        if let jobModel = viewModel.cardDataModels[indexPath.row] as? JobModel {
            JobsDetailViewController.show(JobsDetailViewModel(jobDetail: jobModel), parentController: self)
        }
        return false
    }
    
    func loadMore() {
        self.getLikedJobsList()
    }
}
