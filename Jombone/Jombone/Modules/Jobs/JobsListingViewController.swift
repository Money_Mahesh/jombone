//
//  JobsListingViewController.swift
//  Jombone
//
//  Created by dev139 on 26/09/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import UIKit

class JobsListingViewController: TabBarChildViewController {

    @IBOutlet weak var stackView: UIStackView?
    @IBOutlet weak var noResultView: UIView?
    @IBOutlet weak var floatingBtn: UIButton?

    var viewModel = JobsViewModel()
    
    var tableWidget: GenericTableViewComponent<JobCardViewProtocol, JobCardView>?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "JOBS"
        
        self.addObserver(self, selector: #selector(JobsListingViewController.updatedList), notificationsName: [Notification.Name.UpdateJobList])
        self.addObserver(self, selector: #selector(JobsListingViewController.reloadList), notificationsName: [Notification.Name.ReloadJobList])
        
        self.showLoader()
        self.getJobList()
    }

    @objc override func updatedList() {
        if !self.isViewLoaded {
            return
        }
        viewModel.reset()
        setUpTable()
        
        self.showLoader()
        self.getJobList()
    }
    
    @objc func reloadList() {
        tableWidget?.reloadTable()
    }
    
    func getJobList() {
        
        viewModel.hitGetJobsApi(completion: { (message) in
            self.hideLoader()
            self.noResultView?.isHidden = self.viewModel.dataAvailable
        }, success: { (message) in
            self.setUpTable()
        }, failure: { (errorMsg) in
//            self.showAlert(message: errorMsg)
        })
    }
    
    override func imageBtnAction(sender: UIButton) {
    }
    
    func setUpTable() {
        
        if let _tableWidget = tableWidget {
            _tableWidget.appendNewPage(objArray: viewModel.cardDataModels)
        }
        else {
            tableWidget = GenericTableViewComponent()
            tableWidget?.isHidden = false
            tableWidget?.genericTableViewModel = GenericTableViewModel(
                cellDataModel: viewModel.cardDataModels,
                tableFooterView: (LoadMoreView(frame: CGRect(x: 0, y: 0, width: WINDOW_WIDTH, height: 40)), false),
                tableType: .expandable,
                totalPages: viewModel.totalPages)
            tableWidget?.tableStyle(backGroundColor: UIColor.white, insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
            tableWidget?.delegate = self
            stackView?.addArrangedSubview(tableWidget!)
        }
    }
    
    //MARK:- IBActions
    @IBAction func btnActionForFloatingBtn(_ sender: UIButton) {
        JobFilterViewController.show(nil)
    }
}

extension JobsListingViewController: GenericTableViewComponentDelegate {
    
    func genericTableDidSelectRowAt(_ indexPath: IndexPath, dataSource: inout [Any]) -> Bool {
        
        if let jobModel = viewModel.cardDataModels[indexPath.row] as? JobModel {
            JobsDetailViewController.show(JobsDetailViewModel(jobDetail: jobModel), parentController: self)
        }
        return false
    }
    
    func loadMore() {
        getJobList()
    }
}
