//
//  JobsViewModel.swift
//  Jombone
//
//  Created by dev139 on 26/09/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import UIKit
import Alamofire

class JobsViewModel: NSObject {

    var cardDataModels = [JobCardViewProtocol]()
    var pageNo = 0
    var totalPages = 0
    var dataAvailable: Bool! {
        get {
            return (cardDataModels.count != 0)
        }
    }
    
    func reset() {
        self.pageNo = 0
        cardDataModels.removeAll()
    }
    
    func hitGetJobsApi(completion: ((_ message: String?)->())? = nil, success:  ((_ message: String?)->())? = nil, failure: ((_ message: String?)->())? = nil) {
        
        let jobsServices = JobsServices(requestTag: "JOBS_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        //Completion Block
        jobsServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
           
            if let jobList = (response.0 as? [JobModel]),
                let pagination = (response.1 as? Pagination),
                let totalPages = pagination.totalPages,
                let currentPage = pagination.currentPage,
                self.pageNo == currentPage, statusCode == 1 {
                
                self.pageNo += 1
                self.cardDataModels.append(contentsOf: jobList)
                self.totalPages = totalPages
                
                completion?(message)
                success?(message)
            }
            else {
                completion?(message)
                failure?(message)
            }
        }
        jobsServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            completion?(nil)
            failure?(nil)
        }
       
        let parameters: [String: Any] = [
            "size": String(PAGE_SIZE),
            "page": String(pageNo),
            "searchBeanString": JobFilterManager.sharedInstance.jsonString()
        ]

        jobsServices.getJobs(parameters: parameters, additionalHeaderElements: nil)
    }
    
    func hitGetLikedJobsApi(completion: ((_ message: String?)->())? = nil, success:  ((_ message: String?)->())? = nil, failure: ((_ message: String?)->())? = nil) {
        
        let jobsServices = JobsServices(requestTag: "LIKED_JOBS_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        //Completion Block
        jobsServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            if let jobList = (response.0 as? [JobModel]),
                let pagination = (response.1 as? Pagination),
                let totalPages = pagination.totalPages,
                let currentPage = pagination.currentPage,
                self.pageNo == currentPage, statusCode == 1 {
                
                self.pageNo += 1
                self.cardDataModels.append(contentsOf: jobList)
                self.totalPages = totalPages
                
                completion?(message)
                success?(message)
            }
            else {
                completion?(message)
                failure?(message)
            }
        }
        jobsServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            completion?(nil)
            failure?(nil)
        }
        
        let str = Utility.convertToJson(dictionaryOrArray: ["likedJob" : true])
        let parameters: [String: Any] = [
            "size": String(PAGE_SIZE),
            "page": String(pageNo),
            "searchBeanString": str
        ]
        
        jobsServices.getJobs(parameters: parameters, additionalHeaderElements: nil)
    }
}

