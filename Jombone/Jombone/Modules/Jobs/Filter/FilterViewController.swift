//
//  FilterViewController.swift
//  Jombone
//
//  Created by Money Mahesh on 05/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import UIKit

class FilterViewController: UIViewController {

    var completion: (()->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    @IBAction func btnActionForApply(_ sender: UIButton) {
        
        self.navigationController?.tabBarController?.tabBar.isHidden = false
        self.navigationController?.popViewController(animated: true)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2) {
            self.completion?()
        }
    }
    
    override func backAction(sender: UIButton) {
        super.backAction(sender: sender)
        
        self.navigationController?.tabBarController?.tabBar.isHidden = false
    }
}
