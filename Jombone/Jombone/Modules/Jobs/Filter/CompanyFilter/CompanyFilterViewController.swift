//
//  CompanyFilterViewController.swift
//  Jombone
//
//  Created by Money Mahesh on 04/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import UIKit

class CompanyFilterViewController: FilterViewController {
    
    @IBOutlet weak var viewForCompanyName: CustomLabelView!
    @IBOutlet weak var viewForIndustry: CustomLabelView!
    @IBOutlet weak var viewForLocation: CustomLabelView!
    
    @IBOutlet weak var btnForFollowCompany: UIButton!

    var viewModel = CompanyFilterViewModel()
    
    static func show(_ completion: (()->())?) {
        let viewController = MAIN_STORYBOARD.instantiateViewController(withIdentifier: "CompanyFilterViewController") as! CompanyFilterViewController
        viewController.completion = completion
        
        (APP_DELEGATE_INSTANCE.tabBarController?.selectedViewController as? UINavigationController)?.pushViewController(viewController, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationBarWithTitle("Filter", andLeftButton: .back, andRightButton: .title(title: "Reset"), withBg: .defaultColor)
        populateData()
    }

    func populateData() {
        
        viewForCompanyName.delegate = self
        viewForIndustry.delegate = self
        viewForLocation.delegate = self
        
        viewModel.industry = CompanyFilterManager.sharedInstance.industry
        viewModel.companyName = CompanyFilterManager.sharedInstance.company
        viewModel.location = CompanyFilterManager.sharedInstance.location
        viewModel.followCompany = CompanyFilterManager.sharedInstance.followCompany

        viewForIndustry.text = viewModel.industry?.title
        viewForCompanyName.text = viewModel.companyName?.title
        viewForLocation.text = viewModel.location?.locality
        
        btnForFollowCompany.isSelected = viewModel.followCompany
    }
    
    //MARK:- IBAction
    override func titleBtnAction(sender: UIButton) {
        CompanyFilterManager.sharedInstance.reset()
        populateData()
        NotificationCenter.default.post(name: NSNotification.Name.UpdatedCompanyList, object: nil)
    }
    
    @IBAction override func btnActionForApply(_ sender: UIButton) {
        
        CompanyFilterManager.sharedInstance.followCompany = btnForFollowCompany.isSelected
        CompanyFilterManager.sharedInstance.industry = viewModel.industry
        CompanyFilterManager.sharedInstance.company = viewModel.companyName
        CompanyFilterManager.sharedInstance.location = viewModel.location
        
        CompanyFilterManager.sharedInstance.followCompany = btnForFollowCompany.isSelected
        
        print(CompanyFilterManager.sharedInstance.createUrl())
        super.btnActionForApply(sender)
        NotificationCenter.default.post(name: NSNotification.Name.UpdatedCompanyList, object: nil)
    }
    
    @IBAction func btnActionForCompanyFollow(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
}

extension CompanyFilterViewController: CustomLabelViewProtocol {
    
    func tapped(_ identifier: String) {
        
        print(identifier)
        switch identifier {
        case "Location":
            AutoCompleteViewController.show(viewModel: AutoCompleteViewModel(
                selectedOptions: (self.viewModel.location as? AutoCompleteCardViewModelProtocol) == nil ? nil : [self.viewModel.location as! AutoCompleteCardViewModelProtocol],
                type: AutoComplete.google(GooglePlaceFor: .city),
                isOffline: true, noOfSelection: .single), parentController: self, delegate: self)
            
        case "CompanyName":
            AutoCompleteViewController.show(viewModel: AutoCompleteViewModel(
                selectedOptions: self.viewModel.companyName == nil ? nil : [self.viewModel.companyName!],
                type: .local(LocalFor: LocalFor.employerTitle),
                isOffline: true, noOfSelection: .single), parentController: self, delegate: self)
            
        case "Industry":
            AutoCompleteViewController.show(viewModel: AutoCompleteViewModel(
                selectedOptions: self.viewModel.industry == nil ? nil : [self.viewModel.industry!],
                type: .local(LocalFor: LocalFor.industry),
                isOffline: true, noOfSelection: .single), parentController: self, delegate: self)
            
        default:
            break
        }
    }
    
    func crossBtnTapped(_ identifier: String) {
        
        print(identifier)
        switch identifier {
        case "Location":
            viewForLocation.text = nil
            viewModel.location = nil
            
        case "CompanyName":
            viewForCompanyName.text = nil
            viewModel.companyName = nil

        case "Industry":
            viewForIndustry.text = nil
            viewModel.industry = nil

        default:
            break
        }
    }
}

extension CompanyFilterViewController: AutoCompleteViewDelegate {
    
    func selectedOption(type: AutoComplete, placeDetail: GooglePlaceDetail?) {
        
        switch type {
        case .google(let googlePlaceFor):
            
            switch googlePlaceFor {
            case .city:
                self.viewModel.location = placeDetail
                self.viewForLocation.text = self.viewModel.location?.locality
            default: return
            }
        default:
            break
        }
    }
    
    func selectedOption(type: AutoComplete, optionSelected: Any?) {
        
        switch type {
        case .google( _):
            break
            
        case .local(let localFor):
            
            switch localFor {
            case .employerTitle:
                self.viewModel.companyName = (optionSelected as? [AutoCompleteCardViewModelProtocol])?.first
                self.viewForCompanyName.text = self.viewModel.companyName?.title
                
            case .industry:
                self.viewModel.industry = (optionSelected as? [AutoCompleteCardViewModelProtocol])?.first
                self.viewForIndustry.text = self.viewModel.industry?.title
            default: return
            }
        }
    }
}
