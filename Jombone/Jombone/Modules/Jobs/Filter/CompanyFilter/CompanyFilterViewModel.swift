//
//  CompanyFilterViewModel.swift
//  Jombone
//
//  Created by Money Mahesh on 04/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation

class CompanyFilterViewModel: NSObject {
    
    var companyName: AutoCompleteCardViewModelProtocol?
    var industry: AutoCompleteCardViewModelProtocol?
    var location: GooglePlaceDetail?
    var followCompany: Bool! = false
}
