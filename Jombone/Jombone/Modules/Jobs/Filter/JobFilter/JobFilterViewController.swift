//
//  JobFilterViewController.swift
//  Jombone
//
//  Created by Money Mahesh on 24/02/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import UIKit

class JobFilterViewController: FilterViewController {

    @IBOutlet weak var viewForJobTitle: CustomLabelView!
    @IBOutlet weak var viewForCompanyName: CustomLabelView!
    @IBOutlet weak var viewForLocation: CustomLabelView!
    
    @IBOutlet weak var viewForPayRate: CheckBoxView!
    @IBOutlet weak var viewForShift: CheckBoxView!
    @IBOutlet weak var viewForJobCategory: CheckBoxView!

    var viewModel = JobFilterViewModel()

    static func show(_ completion: (()->())?) {
        let viewController = MAIN_STORYBOARD.instantiateViewController(withIdentifier: "JobFilterViewController") as! JobFilterViewController
        viewController.completion = completion
       
        (APP_DELEGATE_INSTANCE.tabBarController?.selectedViewController as? UINavigationController)?.pushViewController(viewController, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setNavigationBarWithTitle("Filter", andLeftButton: .back, andRightButton: .title(title: "Reset"), withBg: .defaultColor)
        populateData()
    }

    func populateData() {
        
        viewForJobTitle.delegate = self
        viewForCompanyName.delegate = self
        viewForLocation.delegate = self

        viewModel.jobTitle = JobFilterManager.sharedInstance.jobTitle
        viewModel.companyName = JobFilterManager.sharedInstance.company
        viewModel.location = JobFilterManager.sharedInstance.location
        
        viewForJobTitle.text = viewModel.jobTitle?.title
        viewForCompanyName.text = viewModel.companyName?.title
        viewForLocation.text = viewModel.location?.locality

        let payRateArray = PlistUtiliy.parsePlistAndReturnPlistModel("PayRate")
        viewForPayRate.setDetails(data: payRateArray, selectedData: JobFilterManager.sharedInstance.payRate)
        
        let jobShiftArray = PlistUtiliy.parsePlistAndReturnPlistModel("JobShift")
        viewForShift.setDetails(data: jobShiftArray, selectedData: JobFilterManager.sharedInstance.jobShift)
        
        let jobCategoryArray = PlistUtiliy.parsePlistAndReturnPlistModel("JobCategory")
        viewForJobCategory.setDetails(data: jobCategoryArray, selectedData: JobFilterManager.sharedInstance.jobCategory)
    }
    
    //MARK:- IBAction
    override func titleBtnAction(sender: UIButton) {
        JobFilterManager.sharedInstance.reset()
        populateData()
        NotificationCenter.default.post(name: NSNotification.Name.UpdateJobList, object: nil)
    }
    
    @IBAction override func btnActionForApply(_ sender: UIButton) {
        
        JobFilterManager.sharedInstance.jobTitle = viewModel.jobTitle
        JobFilterManager.sharedInstance.company = viewModel.companyName
        JobFilterManager.sharedInstance.location = viewModel.location

        JobFilterManager.sharedInstance.payRate = viewForPayRate.selectedValue as? [PlistModel]
        JobFilterManager.sharedInstance.jobShift = viewForShift.selectedValue as? [PlistModel]
        JobFilterManager.sharedInstance.jobCategory = viewForJobCategory.selectedValue as? [PlistModel]
        
        print(JobFilterManager.sharedInstance.createUrl())
        super.btnActionForApply(sender)
        NotificationCenter.default.post(name: NSNotification.Name.UpdateJobList, object: nil)
    }
}

extension JobFilterViewController: CustomLabelViewProtocol {
    
    func tapped(_ identifier: String) {

        print(identifier)
        switch identifier {
        case "Location":
            AutoCompleteViewController.show(viewModel: AutoCompleteViewModel(
                selectedOptions: (self.viewModel.location as? AutoCompleteCardViewModelProtocol) == nil ? nil : [self.viewModel.location as! AutoCompleteCardViewModelProtocol],
                type: AutoComplete.google(GooglePlaceFor: .city),
                isOffline: true, noOfSelection: .single), parentController: self, delegate: self)

        case "CompanyName":
            AutoCompleteViewController.show(viewModel: AutoCompleteViewModel(
                selectedOptions: self.viewModel.companyName == nil ? nil : [self.viewModel.companyName!],
                type: .local(LocalFor: LocalFor.employerTitle),
                isOffline: true, noOfSelection: .single), parentController: self, delegate: self)

        case "JobTitle":
            AutoCompleteViewController.show(viewModel: AutoCompleteViewModel(
                selectedOptions: self.viewModel.jobTitle == nil ? nil : [self.viewModel.jobTitle!],
                type: .local(LocalFor: LocalFor.jobTitle),
                isOffline: true, noOfSelection: .single), parentController: self, delegate: self)

        default:
            break
        }
    }
    
    func crossBtnTapped(_ identifier: String) {
        
        print(identifier)
        switch identifier {
        case "Location":
            viewForLocation.text = nil
            viewModel.location = nil
            
        case "CompanyName":
            viewForCompanyName.text = nil
            viewModel.companyName = nil

        case "JobTitle":
            viewForJobTitle.text = nil
            viewModel.jobTitle = nil

        default:
            break
        }
    }
}

extension JobFilterViewController: AutoCompleteViewDelegate {
    
    func selectedOption(type: AutoComplete, placeDetail: GooglePlaceDetail?) {
        
        switch type {
        case .google(let googlePlaceFor):
            
            switch googlePlaceFor {
            case .city:
                self.viewModel.location = placeDetail
                self.viewForLocation.text = self.viewModel.location?.locality
            default: return
            }
        default:
            break
        }
    }
    
    func selectedOption(type: AutoComplete, optionSelected: Any?) {
    
        
        switch type {
        case .google( _):
            break
            
        case .local(let localFor):
            
            switch localFor {
            case .employerTitle:
                self.viewModel.companyName = (optionSelected as? [AutoCompleteCardViewModelProtocol])?.first
                self.viewForCompanyName.text = self.viewModel.companyName?.title
                
            case .jobTitle:
                self.viewModel.jobTitle = (optionSelected as? [AutoCompleteCardViewModelProtocol])?.first
                self.viewForJobTitle.text = self.viewModel.jobTitle?.title
            default: return
            }
        }
    }
}
