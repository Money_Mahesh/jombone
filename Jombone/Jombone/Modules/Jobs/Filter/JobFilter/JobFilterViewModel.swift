//
//  JobFilterViewModel.swift
//  Jombone
//
//  Created by Money Mahesh on 24/02/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation

class JobFilterViewModel: NSObject {
    
    var companyName: AutoCompleteCardViewModelProtocol?
    var jobTitle: AutoCompleteCardViewModelProtocol?
    var location: GooglePlaceDetail?
}
