//
//  WorkPopUpCardView.swift
//  Jombone
//
//  Created by Money Mahesh on 23/02/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import UIKit

class WorkPopUpCardView: GenericView<WorkPopUpCardDataModel> {
    
    @IBOutlet weak var imgViewForIcon: UIImageView!
    @IBOutlet weak var lblForTitle: UILabel?
    @IBOutlet weak var viewForCount: UIView?
    @IBOutlet weak var lblForCount: UILabel?
    
    override var viewModel: ViewModel<WorkPopUpCardDataModel>? {
        didSet {
            lblForTitle?.text = viewModel?.data?.name
            
            imgViewForIcon?.image = nil
            if let iconName = viewModel?.data?.icon {
                imgViewForIcon?.image = UIImage(named: iconName)
            }
            
            viewForCount?.isHidden = true
            if let count = viewModel?.data?.count, count > 0 {
                viewForCount?.isHidden = false
                lblForCount?.text = String(count)
            }
        }
    }
}
