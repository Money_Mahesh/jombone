//
//  WorkPopUpCardViewModel.swift
//  Jombone
//
//  Created by Money Mahesh on 23/02/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation

class WorkPopUpCardDataModel {
    var icon: String?
    var name: String?
    var count: Int?
    var isSelected: Bool?
    
    init(icon: String?, name: String?, count: Int? = 0, isSelected: Bool? = false) {
        self.icon = icon
        self.name = name
        self.count = count
        self.isSelected = isSelected
    }
}

class WorkPopUpCardViewModel: ViewModel<WorkPopUpCardDataModel> {
    
    override init(data: WorkPopUpCardDataModel?, otherDetail: Any?) {
        super.init(data: data, otherDetail: otherDetail)
    }
}
