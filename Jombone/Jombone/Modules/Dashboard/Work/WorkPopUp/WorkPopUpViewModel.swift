//
//  WorkPopUpViewModel.swift
//  Jombone
//
//  Created by Money Mahesh on 23/02/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation

class WorkPopUpViewModel: NSObject {
    
    var data: [WorkPopUpCardDataModel] = [
        WorkPopUpCardDataModel(
            icon: "popUpApplicationIcon",
            name: ("Applications" + WorkPopUpViewModel.getStringFormat(UserDataManager.shared.countDetail?.jobAppCount)),
            count: nil,
            isSelected: false),
        WorkPopUpCardDataModel(
            icon: "popUpOfferIcon",
            name: "Offers",
            count: UserDataManager.shared.countDetail?.jobOfferCount,
            isSelected: false),
        WorkPopUpCardDataModel(
            icon: "popUpWorkIcon",
            name: ("Current Employment" + WorkPopUpViewModel.getStringFormat(UserDataManager.shared.countDetail?.currentEmpCount)),
            count: nil,
            isSelected: false),
        WorkPopUpCardDataModel(
            icon: "popUpPastWork",
            name: ("Past Work" + WorkPopUpViewModel.getStringFormat(UserDataManager.shared.countDetail?.pastEmpCount)),
            count: nil,
            isSelected: false),
        WorkPopUpCardDataModel(
            icon: "popUpRejectedIcon",
            name: "Rejected/Withdrawn",
            count: nil,
            isSelected: false)
    ]
    
    class func getStringFormat(_ count: Int?) -> String {
        
        guard let _count = count, _count > 0 else {
            return ""
        }
        return " (" + String(_count) + ")"
    }
}
