//
//  WorkPopUpViewController.swift
//  Jombone
//
//  Created by Money Mahesh on 23/02/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import UIKit

class WorkPopUpViewController: UIViewController {

    @IBOutlet weak var popStackViewContainer: UIView!
    @IBOutlet weak var stackView: UIStackView!
    
    var viewModel = WorkPopUpViewModel()
    var tableWidget: GenericTableViewComponent<WorkPopUpCardDataModel, WorkPopUpCardView>?

    override func viewDidLoad() {
        super.viewDidLoad()

        setUpTable()
        addShadow()
        
        self.addObserver(self, selector: #selector(WorkPopUpViewController.resetCount), notificationsName: [Notification.Name.NotificationCountUpdated])
    }
    
    @objc func resetCount() {
        
        tableWidget?.reloadTable()
    }
    
    func setUpTable() {
        
        let cardDataModels = viewModel.data
        
        tableWidget = GenericTableViewComponent()
        tableWidget?.isHidden = false
        tableWidget?.delegate = self
        tableWidget?.genericTableViewModel = GenericTableViewModel(
            cellDataModel: cardDataModels,
            tableType: .nonScrollable)
        tableWidget?.tableStyle(backGroundColor: UIColor.white, insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))

        stackView.addArrangedSubview(tableWidget!)
    }
    
    func addShadow() {
        popStackViewContainer.clipsToBounds = false
        popStackViewContainer.layer.shadowColor = UIColor(red: 0.78, green: 0.78, blue: 0.78, alpha: 0.75).cgColor
        popStackViewContainer.layer.shadowOpacity = 0.75
        popStackViewContainer.layer.shadowOffset = CGSize(width: 0, height: 20)
    }

    private func dismissPopUp() {
        self.dismiss(animated: false, completion: nil)
    }
    
    fileprivate func moveTo(_ viewController: UIViewController?) {
        
        guard let _viewController = viewController else {
            return
        }
        APP_DELEGATE_INSTANCE.tabBarController?.selectedIndex = 4
        (APP_DELEGATE_INSTANCE.tabBarController?.selectedViewController as? UINavigationController)?.setViewControllers([_viewController], animated: false)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2) {
            APP_DELEGATE_INSTANCE.tabBarController?.tabBar.items?[4].title = "WORK"
        }
    }
    
    //MARK:- IBActions
    @IBAction func backgroundTapAction(_ sender: UITapGestureRecognizer) {
        if popStackViewContainer.frame.contains(sender.location(in: self.view)) {
            return
        }
        dismissPopUp()
    }
}

extension WorkPopUpViewController: GenericTableViewComponentDelegate {
    
    func genericTableDidSelectRowAt(_ indexPath: IndexPath, dataSource: inout [Any]) -> Bool {
        print("IndexPath \(indexPath.row)")
        
        var viewController: UIViewController?
        switch indexPath.row {
        case 0:
            viewController = ApplicationListViewController.instance()
            
        case 1://Not
            viewController = OfferListViewController.instance()
            
        case 2://Not
            viewController = CurrentWorkListViewController.instance()
            
        case 3://Not
            viewController = PastWorkListViewController.instance()

        case 4://Not
            viewController = Reject_WithdrawListViewController.instance()

        default:
            break
        }
        moveTo(viewController)
        dismissPopUp()
        return true
    }
}
