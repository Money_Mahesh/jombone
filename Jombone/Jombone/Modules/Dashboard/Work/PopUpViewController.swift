//
//  PopUpViewController.swift
//  Jombone
//
//  Created by Money Mahesh on 19/06/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import UIKit

class PopUpViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func backAction(sender: UIButton) {
        if let _navigationController = self.navigationController, _navigationController.viewControllers.count > 1 {
            super.backAction(sender: sender)
        }
        else {
            if let tabBarController = APP_DELEGATE_INSTANCE.tabBarController {
                tabBarController.selectedIndex = 0
            }
        }
    }
}
