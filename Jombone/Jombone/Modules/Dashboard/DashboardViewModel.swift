//
//  DashboardViewModel.swift
//  Jombone
//
//  Created by dev139 on 23/09/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import UIKit

class DashboardViewModel: NSObject {
    
    var pageNo = 0
    var totalPages = 0
    var cardDataModels = [JobCardViewProtocol]()
    var dataAvailable: Bool! {
        get {
            return (cardDataModels.count != 0)
        }
    }
    
    func reset() {
        self.pageNo = 0
        cardDataModels.removeAll()
    }
    
    func hitDashboardApi(completion: ((_ message: String?)->())? = nil, success:  ((_ message: String?)->())? = nil, failure: ((_ message: String?)->())? = nil) {
        
        let dashboardServices = DashboardServices(requestTag: "DASHBOARD_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        //Completion Block
        dashboardServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            if let jobList = (response.0 as? [JobModel]),
                let pagination = (response.1 as? Pagination),
                let totalPages = pagination.totalPages,
                let currentPage = pagination.currentPage,
                self.pageNo == currentPage, statusCode == 1 {
                
                self.pageNo += 1
                self.cardDataModels.append(contentsOf: jobList)
                self.totalPages = totalPages
                
                completion?(message)
                success?(message)
            }
            else {
                completion?(message)
                failure?(message)
            }
        }
        dashboardServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            completion?(nil)
            failure?(nil)
        }
        
        let parameters: [String: Any] = [
            "size": String(PAGE_SIZE),
            "page": String(pageNo),
        ]
        dashboardServices.hitDashboardApi(parameters: parameters, additionalHeaderElements: nil)
    }
    
    
    func hitCountApi(completion: ((_ message: String?)->())? = nil, success:  ((_ message: String?)->())? = nil, failure: ((_ statusCode: Int?, _ message: String?)->())? = nil) {
        
        let dashboardServices = DashboardServices(requestTag: "COUNT_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        //Completion Block
        dashboardServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            if let countData = (response.0 as? CountModel) {
                UserDataManager.shared.countDetail = countData
            }
            if statusCode == 1 {
                completion?(message)
                success?(message)
            }
            else {
                completion?(message)
                failure?(statusCode, message)
            }
        }
        dashboardServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            completion?(nil)
            failure?(nil, nil)
        }
        
        dashboardServices.hitCountApi(parameters: nil, additionalHeaderElements: nil)
    }
}
