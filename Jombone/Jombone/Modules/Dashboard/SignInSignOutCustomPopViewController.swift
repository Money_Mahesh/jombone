//
//  SignInSignOutCustomPopViewController.swift
//  Jombone
//
//  Created by Money Mahesh on 03/04/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import UIKit


class SignInSignOutCustomPopViewController: UIViewController, WorkAction {

    @IBOutlet weak var sliddingView: UIView?
    @IBOutlet weak var stackViewForSignIn: UIStackView?
    @IBOutlet weak var stackViewForSignOut: UIStackView?
    @IBOutlet weak var lblForJobTitleSignIn: UILabel?
    @IBOutlet weak var lblForJobTitleSignOut: UILabel?
    
    @IBOutlet weak var constForSliddingViewBottom: NSLayoutConstraint?
    
    weak var workDelegate: WorkAction?
    private var isVisible: Bool = false
    var completion: (()->())?

    static func show(_ completion: (()->())?) {
        let viewController = MAIN_STORYBOARD.instantiateViewController(withIdentifier: "SignInSignOutCustomPopViewController") as! SignInSignOutCustomPopViewController
        viewController.completion = completion
        
        APP_DELEGATE_INSTANCE.tabBarController?.present(viewController, animated: false, completion: {
            viewController.toggleSliddingView()
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.workDelegate = self
        
        self.stackViewForSignIn?.isHidden = !(UserDataManager.shared.countDetail?.floatingIcon?.enableSignIn ?? false)
        self.stackViewForSignOut?.isHidden = !(UserDataManager.shared.countDetail?.floatingIcon?.enableSignOut ?? false)
        self.lblForJobTitleSignIn?.text = UserDataManager.shared.countDetail?.floatingIcon?.jobTitle
        self.lblForJobTitleSignOut?.text = UserDataManager.shared.countDetail?.floatingIcon?.jobTitle
    }
    
    func toggleSliddingView() {
        
        self.isVisible = !self.isVisible
        
        constForSliddingViewBottom?.constant = self.isVisible ? 40 : 300
        UIView.animate(withDuration: 0.5, delay: 0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            self.view.layoutIfNeeded()
        }, completion: { (status: Bool) in
            if !self.isVisible {
                self.dismiss(animated: false, completion: nil)
            }
        })
    }
    
    //MARK:- IBActions
    @IBAction func cancelBtnAction(_ sender: UIButton) {
        toggleSliddingView()
    }
    
    @IBAction func btnActionForSignIn(_ sender: UIButton) {
        
        toggleSliddingView()
        guard let jobEncId = UserDataManager.shared.countDetail?.floatingIcon?.jobEncId else {
                return
        }

        CustomAlertViewController.show(data: CustomAlertDataModel(title: "Sign In", subTitle: "You are about to sign in.\nSign in time cannot be changed.\nPlease confirm.", completion: {

            self.workDelegate?.signIn(jobEncId: jobEncId, completion: { (status, timesheetEncId) in

                if status {
                    let countDetail = UserDataManager.shared.countDetail
                    countDetail?.floatingIcon?.timesheetEncId = timesheetEncId
                    countDetail?.floatingIcon?.enableSignIn = false
                    UserDataManager.shared.countDetail = countDetail
                }
            })
        }))
    }
    
    @IBAction func btnActionForSignOut(_ sender: UIButton) {
        toggleSliddingView()
        guard
            let timesheetEncId = UserDataManager.shared.countDetail?.floatingIcon?.timesheetEncId else {
                return
        }

        CustomAlertViewController.show(data: CustomAlertDataModel(title: "Sign Out", subTitle: "You are about to sign out.\nSign out time cannot be changed.\nPlease confirm.", completion: {

            self.workDelegate?.signOut(timesheetEncId: timesheetEncId, completion: { (status) in

                if status {
                    let countDetail = UserDataManager.shared.countDetail
                    countDetail?.floatingIcon?.enableSignOut = false
                    UserDataManager.shared.countDetail = countDetail
                }
            })
        }))
    }
}
