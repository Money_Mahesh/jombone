//
//  DashboardViewController.swift
//  Jombone
//
//  Created by Money Mahesh on 18/08/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import UIKit

class DashboardViewController: TabBarChildViewController {

    @IBOutlet weak var stackView: UIStackView?
    @IBOutlet weak var noResultView: UIView?
    @IBOutlet weak var floatingBtn: UIButton?

    var viewModel = DashboardViewModel()
    var profileViewModel = ProfileViewModel()
    
    var tableWidget: GenericTableViewComponent<JobCardViewProtocol, JobCardView>?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "HOME"
        
        self.addObserver(self, selector: #selector(DashboardViewController.updatedList), notificationsName: [Notification.Name.UpdateJobList])
        self.addObserver(self, selector: #selector(DashboardViewController.reloadList), notificationsName: [Notification.Name.ReloadJobList])
        self.addObserver(self, selector: #selector(DashboardViewController.setFloatingBtn), notificationsName: [Notification.Name.NotificationCountUpdated])

        self.showLoader()
        profileViewModel.hitGetProfileDetail(completion: { (message) in
            self.getJobList()
        })
        viewModel.hitCountApi()
    }
    
    override func resetView() {
        super.resetView()
        viewModel.hitCountApi()
    }
    
    @objc func reloadList() {
        tableWidget?.reloadTable()
    }
    
    @objc override func updatedList() {
        if !self.isViewLoaded {
            return
        }
        
        viewModel.reset()
        setUpTable()
        
        self.showLoader()
        self.getJobList()
    }
    
    func getJobList(reset: Bool = false){
        
        if reset {
            self.showLoader()
            viewModel.reset()
            setUpTable()
        }
        viewModel.hitDashboardApi(completion: { (message) in
            self.hideLoader()
            self.noResultView?.isHidden = self.viewModel.dataAvailable
        }, success: { (message) in
            self.setUpTable()
        }, failure: { (errorMsg) in
//            self.showAlert(message: errorMsg)
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func imageBtnAction(sender: UIButton) {
    }
    
    func setUpTable() {
        
        if let _tableWidget = tableWidget {
            _tableWidget.appendNewPage(objArray: viewModel.cardDataModels)
        }
        else {
            tableWidget = GenericTableViewComponent()
            tableWidget?.isHidden = false
            tableWidget?.genericTableViewModel = GenericTableViewModel(
                cellDataModel: viewModel.cardDataModels,
                tableFooterView: (LoadMoreView(frame: CGRect(x: 0, y: 0, width: WINDOW_WIDTH, height: 40)), false),
                tableType: .expandable,
                totalPages: viewModel.totalPages)
            tableWidget?.tableStyle(backGroundColor: UIColor.white, insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
            tableWidget?.delegate = self
            stackView?.addArrangedSubview(tableWidget!)
        }
    }
    
    //MARK:- Set Floating Btn
    @objc func setFloatingBtn(notification: NSNotification) {
        
        floatingBtn?.isHidden = !(UserDataManager.shared.countDetail?.floatingIcon?.enableSignIn ?? false
            || (UserDataManager.shared.countDetail?.floatingIcon?.enableSignOut ?? false))
    }
    
    //MARK:- IBActions
    @IBAction func btnActionForFloatingBtn(_ sender: UIButton) {
        SignInSignOutCustomPopViewController.show(nil)
    }
}

extension DashboardViewController: GenericTableViewComponentDelegate {
    
    func genericTableDidSelectRowAt(_ indexPath: IndexPath, dataSource: inout [Any]) -> Bool {
        if let jobModel = viewModel.cardDataModels[indexPath.row] as? JobModel {
            JobsDetailViewController.show(JobsDetailViewModel(jobDetail: jobModel), parentController: self)
        }
        return false
    }
    
    func loadMore() {
        getJobList()
    }
}
