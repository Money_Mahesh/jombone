//
//  SignUpViewController.swift
//  Jombone
//
//  Created by dev139 on 20/08/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import UIKit

class SignUpViewController: WhiteStatusBarViewController {

    @IBOutlet weak var scrollView: UIScrollView!

    @IBOutlet weak var lblForFirstNameError: UILabel!
    @IBOutlet weak var lblForLastNameError: UILabel!
    @IBOutlet weak var lblForPhoneNoError: UILabel!

    @IBOutlet weak var textfieldForFirstName: CustomTextfieldView!
    @IBOutlet weak var textfieldForLastName: CustomTextfieldView!
    @IBOutlet weak var textfieldForEmail: CustomTextfieldView!
    @IBOutlet weak var textfieldForCountryCode: CustomTextfieldView!
    @IBOutlet weak var textfieldForMobileNo: CustomTextfieldView!
    @IBOutlet weak var textfieldForPassword: CustomTextfieldView!
    @IBOutlet var viewModel: SignupViewModel!
    
    @IBOutlet weak var stackViewForTextfield: UIStackView!
    var mobileUnformatted : String = ""

    static func show() {
        
        let viewController = LOGIN_STORYBOARD.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        viewController.viewModel = SignupViewModel()
        APPLICATION_INSTANCE.visibleViewController()?.navigationController?.pushViewController(viewController, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationBarWithTitle("", andLeftButton: .none, andRightButton: .none, withBg: .bgColor(value: UIColor.clear))

        //To shift table under status bar
        if #available(iOS 11.0, *) {
            scrollView.contentInsetAdjustmentBehavior = .never
        } else {
            automaticallyAdjustsScrollViewInsets = false
        }
        setTextFieldPreferences()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setNavigationBarWithTitle("", andLeftButton: .none, andRightButton: .none, withBg: .bgColor(value: UIColor.clear))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setTextFieldPreferences() {
        
        //Set Textfield Prefernces
        textfieldForMobileNo.textfieldViewDelegate = self
        
        textfieldForFirstName.setJomboneFontStyle()
        textfieldForLastName.setJomboneFontStyle()
        textfieldForEmail.setJomboneEmailStyle()
        textfieldForMobileNo.setJomboneMobileStyle()
        textfieldForCountryCode.setJomboneCountryCodeStyle(delegate: self)
        textfieldForPassword.setJombonePasswordStyle()
        
        textfieldForEmail.textfield.autocapitalizationType = .none
        textfieldForEmail.textfield.autocorrectionType = .no
        textfieldForFirstName.textfieldViewDelegate = self
        textfieldForLastName.textfieldViewDelegate = self
    }

    @IBAction func btnActionForSignUp(_ sender: UIButton) {
    
        view.endEditing(true)
        
        viewModel.reset()
        
        let validateName = DataValidationUtility.shared.validateName(firstName: textfieldForFirstName.text,
                                                                 lastName: textfieldForLastName.text)
        lblForFirstNameError.text = validateName.firstName.errorMsg
        viewModel.firstname = validateName.firstName.value
        
        lblForLastNameError.text = validateName.lastName.errorMsg
        viewModel.lastname = validateName.lastName.value
        
        let validatePhoneNo = DataValidationUtility.shared.validatePhoneNo(viewModel.unFormattedMobile)
        lblForPhoneNoError.text = validatePhoneNo.errorMsg
        viewModel.mobileUnformat = validatePhoneNo.value
        viewModel.mobile = viewModel.formattedMobile
        
        viewModel.email = textfieldForEmail.isValidEmail()
        viewModel.password = textfieldForPassword.isValidPassword(customMessage: "Password should contain 1 numeric, 1 special character, 1 character and length should be from 6 to 16 character long")
        
        guard viewModel.unFormattedMobile != "" else {
            return
        }
        
        showLoader()
        viewModel.hitSignUpApi(completion: { (message) in
            self.hideLoader()
        }, success: { (message) in
            self.hideLoader()
            OTPVerificationViewController.show(viewModel: OTPVerificationViewModel(purpose: .email(email: UserDataManager.shared.profileDetail?.personalDetailsBeans?.email)))
            
        }, failure: { (message) in
            self.hideLoader()
            self.showAlert(message: message, withOk: true)
        })
        
    }
    
    @IBAction func btnActionForCondition(_ sender: UIButton) {
        
        self.navigationController?.pushViewController(WebViewViewController.instance(viewModel: WebViewModel(urlStr: BASE_URL.absoluteString + "/jombone/user-agreement", title: "User Agreement")), animated: true)
    }
    
    @IBAction func btnActionForPrivacyPolicy(_ sender: UIButton) {
        
        self.navigationController?.pushViewController(WebViewViewController.instance(viewModel: WebViewModel(urlStr: BASE_URL.absoluteString + "/jombone/privacy", title: "Privacy Policy")), animated: true)

    }
    
    @IBAction func btnActionForCookie(_ sender: UIButton) {
        
        self.navigationController?.pushViewController(WebViewViewController.instance(viewModel: WebViewModel(urlStr: BASE_URL.absoluteString + "/jombone/cookie", title: "Cookie Policy")), animated: true)
    }
    
    //Already have an account button action
    @IBAction func btnActionForLogin(_ sender: UIButton) {
        LoginViewController.showWithAnimation(true)
    }
}

extension SignUpViewController: CustomTextfieldViewDelegate {
    
    func textFieldDidBeginEditing(_ identifier: String, textField: CustomTextfield) {
        
        if identifier == "FirstName" {
            lblForFirstNameError.text = nil
        }
        else if identifier == "LastName" {
            lblForLastNameError.text = nil
        }
        else if identifier == "MobileNo" {
            lblForPhoneNoError.text = nil
            if let text = textField.text, text.contains("(") {
                textField.text = text.toUnPhoneNumber()
            }
        }
    }
    
    func textFieldDidEndEditing(_ identifier: String, textField: CustomTextfield) {
        
        if identifier == "MobileNo" {
            viewModel.unFormattedMobile = textField.text?.toUnPhoneNumber()
            let formattedString = textField.text?.toPhoneNumber()
            textField.text = formattedString
            viewModel.formattedMobile = formattedString
        }
    }
    
    
    func pickerViewSelected(identifier: String, textfield: CustomTextfield, indexes: [Int]) {
        
        if let firstIndex = indexes.first {
            
            viewModel.countryCode = Utility.countryCodeArray[firstIndex]["isdvalueCodes"] ?? CANADA_COUNTRY_CODE
            viewModel.countryIsoCode = Utility.countryCodeArray[firstIndex]["ISO_Code"] ?? CANADA_ISO_COUNTRY_CODE
        }
    }
}

