//
//  SignupViewModel.swift
//  Jombone
//
//  Created by dev139 on 18/09/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import UIKit

class SignupViewModel: NSObject {
    
    var unFormattedMobile : String!
    var formattedMobile : String!
    var countryCode : String = CANADA_COUNTRY_CODE
    var countryIsoCode : String = CANADA_ISO_COUNTRY_CODE

    var firstname: String! {
        didSet {
            if firstname == nil {
                isInputValid = false
            }
        }
    }
    var lastname: String! {
        didSet {
            if lastname == nil {
                isInputValid = false
            }
        }
    }
    var email: String! {
        didSet {
            if email == nil {
                isInputValid = false
            }
        }
    }
    var password: String! {
        didSet {
            if password == nil {
                isInputValid = false
            }
        }
    }
    var mobile : String! {
        didSet {
            if mobile == nil {
                isInputValid = false
            }
        }
    }
    
    var mobileUnformat : String! {
        didSet {
            if mobileUnformat == nil {
                isInputValid = false
            }
        }
    }

    
    private var isInputValid = true
    func reset() {
        isInputValid = true
    }
    
    func hitSignUpApi(completion: ((_ message: String?)->())? = nil, success:  ((_ message: String?)->())? = nil, failure: ((_ message: String?)->())? = nil) {
        
        if !isInputValid {
            completion?(nil)
            return
        }
        
        let autheticationServices = AutheticationServices(requestTag: "SIGNUP_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        //Completion Block
        autheticationServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            if statusCode == 1, let response = response.0 as? String {
                
                UserDataManager.shared.token = response
                UserDataManager.shared.profileDetail = ProfileDetail(
                    personalDetailsBean: PersonalDetailsBean(firstName: self.firstname,
                                                             lastName: self.lastname,
                                                             email: self.email,
                                                             countryCode: self.countryCode,
                                                             phoneNumber: self.mobile,
                                                             countryIsoCode: self.countryIsoCode))
                
                completion?(message)
                success?(message)
            }
            else {
                completion?(message)
                failure?(message)
            }
        }
        autheticationServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            completion?(nil)
            failure?(nil)
        }
        
        let parameters: [String: Any] = [
            "firstName" : firstname,
            "lastName" : lastname,
            "email": email!,
            "password": password!,
            "mobile": mobile,
            "countryCode": countryCode,
            "countryIsoCode":countryIsoCode,
            "mobileUnformat": unFormattedMobile!,
            "deviceId":DEVICE_TOKEN,
            "deviceType":DEVICE_TYPE
        ]
        autheticationServices.hitSignUp(parameters: parameters, additionalHeaderElements: nil)
    }
}
