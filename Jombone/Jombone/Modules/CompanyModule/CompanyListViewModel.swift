//
//  CompanyListViewModel.swift
//  Jombone
//
//  Created by Money Mahesh on 23/02/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation

class CompanyListViewModel: NSObject {
    
    var pageNo = 0
    var totalPages = 0
    var cardDataModels = [CompanyCardViewProtocol]()
    
    var dataAvailable: Bool! {
        get {
            return (cardDataModels.count != 0)
        }
    }
    func reset() {
        self.pageNo = 0
        cardDataModels.removeAll()
    }
    
    func hitGetCompaniesApi(isFollowed : Bool, completion: ((_ message: String?)->())? = nil, success:  ((_ message: String?)->())? = nil, failure: ((_ message: String?)->())? = nil) {
        
        let companyServices = CompanyServices(requestTag: "COMPANY_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        //Completion Block
        companyServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            if let companyList = (response.0 as? [CompanyModel]),
                let pagination = (response.1 as? Pagination),
                let totalPages = pagination.totalPages,
                let currentPage = pagination.currentPage,
                self.pageNo == currentPage, statusCode == 1 {
                
                self.pageNo += 1
                self.cardDataModels.append(contentsOf: companyList)
                self.totalPages = totalPages
                
                completion?(message)
                success?(message)
            }
            else {
                completion?(message)
                failure?(message)
            }
        }
        companyServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            completion?(nil)
            failure?(nil)
        }
        
        let str = Utility.convertToJson(dictionaryOrArray: ["followed" : true])
        
        let parameters: [String: Any] = [
            "size": String(PAGE_SIZE),
            "page": String(pageNo),
            "searchBeanString": isFollowed ? str : CompanyFilterManager.sharedInstance.jsonString()
        ]
        companyServices.getCompanies(parameters: parameters, additionalHeaderElements: nil)
    }
}
