//
//  FollowedCompanyViewController.swift
//  Jombone
//
//  Created by dev139 on 06/05/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import UIKit

class FollowedCompanyViewController: UIViewController {
    
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var noResultView: UIView?
    
    var viewModel = CompanyListViewModel()
    
    var tableWidget: GenericTableViewComponent<CompanyCardViewProtocol, CompanyCardView>?
    
    static func instance() -> FollowedCompanyViewController{
        
        let instance = COMPANY_STORYBOARD.instantiateViewController(withIdentifier: "FollowedCompanyViewController") as! FollowedCompanyViewController
        return instance
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationBarWithTitle("Followed Companies", andLeftButton: .back, andRightButton: .none, withBg: .defaultColor)
        // Do any additional setup after loading the view.
        
        self.showLoader()
        self.getFollowedCompanyList()
    }
    
    @objc func reloadList() {
        tableWidget?.reloadTable()
    }
    
    
    func getFollowedCompanyList() {
        viewModel.hitGetCompaniesApi(isFollowed: true, completion: { (message) in
            self.hideLoader()
            self.noResultView?.isHidden = self.viewModel.dataAvailable
        }, success: { (message) in
            self.setUpTable()
        }, failure: { (errorMsg) in
            //            self.showAlert(message: errorMsg)
        })
    }
    
    func setUpTable() {
        
        if let _tableWidget = tableWidget {
            _tableWidget.appendNewPage(objArray: viewModel.cardDataModels)
        }
        else {
            tableWidget = GenericTableViewComponent()
            tableWidget?.isHidden = false
            tableWidget?.genericTableViewModel = GenericTableViewModel(
                cellDataModel: viewModel.cardDataModels,
                tableFooterView: (LoadMoreView(frame: CGRect(x: 0, y: 0, width: WINDOW_WIDTH, height: 40)), false),
                tableType: .expandable,
                totalPages: viewModel.totalPages)
            tableWidget?.tableStyle(backGroundColor: UIColor.white, insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
            tableWidget?.delegate = self
            stackView?.addArrangedSubview(tableWidget!)
        }
    }
    
    
}


extension FollowedCompanyViewController: GenericTableViewComponentDelegate {
    
    func genericTableDidSelectRowAt(_ indexPath: IndexPath, dataSource: inout [Any]) -> Bool {
        
        if let companyModel = viewModel.cardDataModels[indexPath.row] as? CompanyModel {
            CompanyDetailViewController.show(CompanyDetailViewModel(companyDetail: companyModel), parentController: self)
        }
        return false
    }
    
    func loadMore() {
        getFollowedCompanyList()
    }
}

