//
//  CompanyCardView.swift
//  Jombone
//
//  Created by Money Mahesh on 10/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import UIKit

class CompanyCardView: GenericView<CompanyCardViewProtocol>, CompanyAction, JobAction {
    
    @IBOutlet weak var imgViewForCompanylogo: UIImageView!
    @IBOutlet weak var btnForIsFollowed: UIButton!
    @IBOutlet weak var lblForFollower: UILabel?
    @IBOutlet weak var lblForCompanyName: UILabel?
    @IBOutlet weak var lblForAddress: UILabel?
    
    weak var jobDelegate: JobAction?
    weak var companyDelegate: CompanyAction?
    
    override func xibLoaded() {
        jobDelegate = self
        companyDelegate = self
    }
    
    override var viewModel: ViewModel<CompanyCardViewProtocol>? {
        didSet {
            lblForCompanyName?.text = viewModel?.data?.employerName
            lblForAddress?.text = viewModel?.data?.employerAddress
            lblForFollower?.text = String(viewModel?.data?.employerFollowers ?? 0) + " Followers"

            imgViewForCompanylogo.backgroundColor = UIColor.clear
            if let _imageUrlStr = viewModel?.data?.employerPhoto,
                let imageUrl = URL(string: (_imageUrlStr)) {
                
                let url_request = URLRequest(url: imageUrl)
                self.imgViewForCompanylogo?.setImageWithURLAlamofire(url_request, placeholderImageName: "", success: {
                    [weak self] (request: URLRequest?, image: UIImage?) -> Void in
                    self?.imgViewForCompanylogo.backgroundColor = UIColor.clear
                    self?.imgViewForCompanylogo.image = image
                    }
                    ,failure: {
                        [weak self] (request:URLRequest?, error:Error?) -> Void in
                        self?.imgViewForCompanylogo.image = nil
                        self?.imgViewForCompanylogo.contentMode = .scaleAspectFill
                        self?.imgViewForCompanylogo?.image = UIImage(named: "company_listing_placeholder")
                        print("Not able to load profile pic" + (error?.localizedDescription ?? ""))
                })
            }
            else {
                self.imgViewForCompanylogo.contentMode = .scaleAspectFill
                self.imgViewForCompanylogo?.image = UIImage(named: "company_listing_placeholder")
            }
            
            setFollowStatus()
        }
    }
    
    private func setFollowStatus() {
        let isFollowed = viewModel?.data?.isFollowed ?? false
        isFollowed ? btnForIsFollowed?.setTitleForAllState(title: "Followed") : btnForIsFollowed?.setTitleForAllState(title: "Follow")
        let imageName = isFollowed ? "wifiActivatedIcon" : "wifiIcon"
        btnForIsFollowed.setImageForAllState(image: UIImage(named: imageName))
    }
    
    //MARK:- IBActions
    @IBAction func btnActionForShare(_ sender: UIButton) {
        
        guard let shareUrl = viewModel?.data?.employerShareURL else {
            return
        }
        jobDelegate?.share(shareDetail: [shareUrl])
    }
    
    @IBAction func btnActionForIsFollowed(_ sender: UIButton) {
        
        guard let jobId = viewModel?.data?.employerEncId, let isFollowed = viewModel?.data?.isFollowed else {
            return
        }
        companyDelegate?.follow_UnFollowCompany(isFollowed: !isFollowed, companyEncId: jobId, completion: { (status: Bool) in
            if status {
                self.viewModel?.data?.isFollowed = !(isFollowed)
                self.setFollowStatus()
            }
        })
    }
    
    @IBAction func btnActionForDirection(_ sender: UIButton) {
        
        guard let employerId = viewModel?.data?.employerEncId else {
            return
        }
        self.viewDirectionForJobOrCompany(isJob: false, jobOrEmployerid: employerId)

    }
}
