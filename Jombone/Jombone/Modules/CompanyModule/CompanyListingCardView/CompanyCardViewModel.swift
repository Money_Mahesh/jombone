//
//  JobCardViewModel.swift
//  Jombone
//
//  Created by Money Mahesh on 10/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation

protocol CompanyCardViewProtocol {
    
    var employerAddress: String? {get set}
    var employerEncId: String? {get set}
    var employerFollowers: Int? {get set}
    var employerName: String? {get set}
    var employerPhoto: String? {get set}
    var isFollowed: Bool? {get set}
    var employerShareURL: String? {get set}
}

class ComapnyCardViewModel: ViewModel<CompanyCardViewProtocol> {
 
    override init(data: CompanyCardViewProtocol?, otherDetail: Any?) {
        super.init(data: data, otherDetail: otherDetail)
    }
}
