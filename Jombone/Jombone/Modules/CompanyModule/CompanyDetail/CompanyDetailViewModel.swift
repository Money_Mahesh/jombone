//
//  CompanyDetailViewModel.swift
//  Jombone
//
//  Created by Money Mahesh on 18/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation

protocol CompanyDetailViewProtocol: CompanyCardViewProtocol {
    var aboutUs: String? {get set}
    var employerContact: [EmployerContact]? {get set}
    var employerIndustries : [EmployerIndustry]? {get set}
    var employerLocations : [EmployerLocation]? {get set}
}

class CompanyDetailViewModel: NSObject {
    
    var data: CompanyDetailViewProtocol!
    
    init(companyDetail: CompanyDetailViewProtocol) {
        self.data = companyDetail
    }
}
