//
//  CompanyDetailViewController.swift
//  Jombone
//
//  Created by dev139 on 13/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import UIKit

class CompanyDetailViewController: WhiteStatusBarViewController {

    @IBOutlet weak var stackView: UIStackView!
    var viewModel: CompanyDetailViewModel!
    var followedAction: ((_ status: Bool)->())?
    
    static func show(_ viewModel: CompanyDetailViewModel, parentController: UIViewController? = nil, followedAction: ((_ status: Bool)->())? = nil) {
        let viewController = COMPANY_STORYBOARD.instantiateViewController(withIdentifier: "CompanyDetailViewController") as! CompanyDetailViewController
        
        viewController.viewModel = viewModel
        viewController.followedAction = followedAction
        
        (parentController?.navigationController ??
            APPLICATION_INSTANCE.visibleViewController()?.navigationController)?.pushViewController(viewController, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationBarWithTitle("Company Detail", andLeftButton: .back, andRightButton: .none, withBg: .defaultColor)

        addCompanyDetailSections()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.tabBarController?.tabBar.isHidden = true
    }
    
    private func addCompanyDetailSections() {
        stackView.addArrangedSubview(CompanyDetailHeaderView(frame: CGRect.zero, data: ((viewModel.data)!, followedAction)))
        
        if let aboutUs = viewModel.data.aboutUs, aboutUs.count > 0 {
            stackView.addArrangedSubview(JobDescriptionView(frame: CGRect.zero, data: (JobDescriptionModel(title: "About us", desc: aboutUs, isHtml: false))))
        }
        
        if let employerLocations = viewModel.data.employerLocations, employerLocations.count > 0 {
            stackView.addArrangedSubview(LocationListView(frame: CGRect.zero, data: (LocationListViewModel(title: "Location", data: employerLocations, minNoOfRecords: 3))))
        }
        
        if let employerIndustries = viewModel.data.employerIndustries, employerIndustries.count > 0 {
            stackView.addArrangedSubview(IndustriesCollectionView(frame: CGRect.zero, data: (IndustriesCollectionViewModel(title: "Industries/Keywords", cardModels: employerIndustries))))
        }
    
        if let employerContact = viewModel.data.employerContact, employerContact.count > 0 {
            stackView.addArrangedSubview(KeyContactsCollectionView(frame: CGRect.zero, data: (KeyContactsViewModel(title: "Key Contacts", cardModels: employerContact))))
        }
    }
}
