//
//  LocationListView.swift
//  Jombone
//
//  Created by Money Mahesh on 19/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import UIKit

class LocationListView: CustomView {
    
    @IBOutlet weak private var stackView: UIStackView!
    @IBOutlet weak private var lblForTitle: UILabel!
    @IBOutlet weak private var btnForExpanded: UIButton!

    var tableWidget: GenericTableViewComponent<LocationDetailProtocol, LocationListCardView>?

    var viewModel: LocationListViewModel!
    
    override func xibLoaded(data: Any) {
        viewModel = (data as! LocationListViewModel)
        lblForTitle.text = viewModel.title
        btnForExpanded.isHidden = viewModel.isExpanded
        setUpTable()
        setBtnStatus(isExpanded: viewModel.isExpanded)
    }
    
    func setBtnStatus(isExpanded: Bool) {
        let imageName: String = isExpanded ? "dropUpBlueArrow" : "dropDownBlueArrow"
        btnForExpanded.setImageForAllState(image: UIImage(named: imageName))
        btnForExpanded.setTitleForAllState(title: isExpanded ? "Show Less" : "Show More")
    }
    
    func setUpTable() {
        
        if let _tableWidget = tableWidget {
            _tableWidget.updateTableRecord(objArray: viewModel.data)
        }
        else {
            tableWidget = GenericTableViewComponent()
            tableWidget?.isHidden = false
            tableWidget?.genericTableViewModel = GenericTableViewModel(
                cellDataModel: viewModel.data,
                tableType: .nonScrollable)
            tableWidget?.tableStyle(backGroundColor: UIColor.white, insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
            stackView.addArrangedSubview(tableWidget!)
        }
    }
    
    //MARK:- IBActions
    @IBAction func btnAction(_ sender: UIButton) {
        viewModel.toggle()
        setUpTable()
        setBtnStatus(isExpanded: viewModel.isExpanded)
    }
}
