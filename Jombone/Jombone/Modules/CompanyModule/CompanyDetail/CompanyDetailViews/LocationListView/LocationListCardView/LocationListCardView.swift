//
//  LocationListCardView.swift
//  Jombone
//
//  Created by Money Mahesh on 19/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import UIKit

class LocationListCardView: GenericView<LocationDetailProtocol> {
    
    @IBOutlet weak var lblForTitle: UILabel?
    @IBOutlet weak var lblForSubTitle: UILabel?
    
    override var viewModel: ViewModel<LocationDetailProtocol>? {
        didSet {
            lblForTitle?.text = viewModel?.data?.employerName
            lblForSubTitle?.text = viewModel?.data?.location
        }
    }
}

