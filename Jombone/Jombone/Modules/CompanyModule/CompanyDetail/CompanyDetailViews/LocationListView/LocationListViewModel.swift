//
//  LocationListViewModel.swift
//  Jombone
//
//  Created by Money Mahesh on 19/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation

protocol LocationDetailProtocol {
    var employerName : String? {get set}
    var location : String? {get set}
}

class LocationListViewModel: NSObject {
    
    private var minNoOfRecords: Int!

    private var _data = [LocationDetailProtocol]()
    var data: [LocationDetailProtocol]! {
        set {
            _data = newValue
        }
        get {
            if isExpanded {
                return _data
            }
            else {
                return Array(_data[0..<(min(minNoOfRecords, _data.count))])
            }
        }
    }
    var isExpanded: Bool = false
    var title: String!
    
    init(title: String, data: [LocationDetailProtocol], minNoOfRecords: Int) {
        super.init()
        
        self.title = title
        self.data = data
        self.minNoOfRecords = minNoOfRecords
        
        if minNoOfRecords >= data.count {
            isExpanded = true
        }
    }
    
    func toggle() {
        isExpanded = !isExpanded
    }
}
