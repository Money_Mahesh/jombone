//
//  IndustriesCollectionView.swift
//  Jombone
//
//  Created by dev139 on 13/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import UIKit

class IndustriesCollectionView: CustomView {

    @IBOutlet weak var industriesCollection: UICollectionView!
    @IBOutlet weak var lblForTitle: UILabel!
    
    var viewModel: IndustriesCollectionViewModel!
    
    override func xibLoaded(data: Any) {
        viewModel = (data as! IndustriesCollectionViewModel)
        lblForTitle.text = viewModel.title
        
        self.industriesCollection.register(UINib(nibName: "IndustriesCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "IndustriesCollectionViewCell")
        self.industriesCollection.reloadData()
    }
}

extension IndustriesCollectionView : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.cardModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IndustriesCollectionViewCell", for: indexPath) as! IndustriesCollectionViewCell
        cell.setValues(viewModel: viewModel.cardModels[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 140.0, height: 103.0)
    }
}
