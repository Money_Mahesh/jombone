//
//  IndustriesCollectionViewCell.swift
//  Jombone
//
//  Created by dev139 on 13/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import UIKit

class IndustriesCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var lblForTitle: UILabel?

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setValues(viewModel: IndustriesDetailProtocol?) {
        
        lblForTitle?.text = viewModel?.industryName
        
        imageView?.backgroundColor = UIColor(hexFromString: "#DCDCDC")
        if let _imageUrlStr = viewModel?.industryLogo,
            let imageUrl = URL(string: (_imageUrlStr)) {
            
            let url_request = URLRequest(url: imageUrl)
            self.imageView?.setImageWithURLAlamofire(url_request, placeholderImageName: "", success: {
                [weak self] (request: URLRequest?, image: UIImage?) -> Void in
                self?.imageView?.backgroundColor = UIColor.clear
                self?.imageView?.image = image
                self?.imageView?.contentMode = .scaleAspectFill
                }
                ,failure: {
                    [weak self] (request:URLRequest?, error:Error?) -> Void in
                    self?.imageView?.image = UIImage(named: "IndustryDefaultIcon")
                    self?.imageView?.contentMode = .center
                    print("Not able to load profile pic" + (error?.localizedDescription ?? ""))
            })
        }
        else {
            imageView?.image = UIImage(named: "IndustryDefaultIcon")
            imageView?.contentMode = .center
        }
        
        imageView?.layer.addGradient(colors: [UIColor(red: 0, green: 0, blue: 0, alpha: 0.75).cgColor, UIColor(red: 0, green: 0, blue: 0, alpha: 0).cgColor], locations: [0.0 , 0.7], direction: GradientDirection.downUp)
    }
}
