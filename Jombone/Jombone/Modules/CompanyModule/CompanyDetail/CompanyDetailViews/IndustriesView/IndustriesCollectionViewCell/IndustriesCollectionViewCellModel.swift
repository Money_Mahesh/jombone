//
//  IndustriesCollectionViewCellModel.swift
//  Jombone
//
//  Created by Money Mahesh on 19/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation


protocol IndustriesDetailProtocol {
    var industryLogo : String? {get set}
    var industryName : String? {get set}
}

class IndustriesCollectionViewCellModel: NSObject {
    
    var data: IndustriesDetailProtocol?
    init(data: IndustriesDetailProtocol?) {
        self.data = data
    }
}
