//
//  IndustriesCollectionViewModel.swift
//  Jombone
//
//  Created by Money Mahesh on 19/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation

class IndustriesCollectionViewModel: NSObject {
    
    var cardModels = [IndustriesDetailProtocol]()
    var title: String!
    
    init(title: String, cardModels: [IndustriesDetailProtocol]) {
        self.cardModels = cardModels
        self.title = title
    }
}
