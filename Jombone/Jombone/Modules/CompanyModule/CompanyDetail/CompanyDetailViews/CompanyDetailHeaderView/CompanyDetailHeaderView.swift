//
//  CompanyDetailHeaderView.swift
//  Jombone
//
//  Created by dev139 on 13/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import UIKit

protocol CompanyDetailHeaderProtocol: CompanyCardViewProtocol {
    var employerWebsite : String? {get set}
}

class CompanyDetailHeaderView: CustomView, CompanyAction {

    @IBOutlet weak var imgViewForCompanylogo: UIImageView?
    @IBOutlet weak var lblForCompanyName: UILabel?
    @IBOutlet weak var lblForAddress: UILabel?
    @IBOutlet weak var lblForWebsite: UILabel?
    @IBOutlet weak var btnForShare: UIButton?
    @IBOutlet weak var btnForIsFollowed: UIButton?

    var dataModel: CompanyDetailHeaderProtocol?
    var followedAction: ((_ status: Bool)->())?
    weak var companyDelegate: CompanyAction?
    
    override func xibLoaded(data: Any) {
        
        companyDelegate = self
        dataModel = (data as? (CompanyModel, followedAction: ((_ status: Bool)->())?))?.0
        followedAction = (data as? (CompanyModel, followedAction: ((_ status: Bool)->())?))?.1
        
        lblForCompanyName?.text = dataModel?.employerName
        lblForAddress?.text = dataModel?.employerAddress
        lblForWebsite?.text = dataModel?.employerWebsite
        
        if let _imageUrlStr = dataModel?.employerPhoto,
            let imageUrl = URL(string: (_imageUrlStr)) {
            
            let url_request = URLRequest(url: imageUrl)
            self.imgViewForCompanylogo?.setImageWithURLAlamofire(url_request, placeholderImageName: "", success: {
                [weak self] (request: URLRequest?, image: UIImage?) -> Void in
                self?.imgViewForCompanylogo?.backgroundColor = UIColor.clear
                self?.imgViewForCompanylogo?.image = image
                }
                ,failure: {
                    [weak self] (request:URLRequest?, error:Error?) -> Void in
                    self?.imgViewForCompanylogo?.image = UIImage(named: "company_detail_placeholder")
                    print("Not able to load profile pic" + (error?.localizedDescription ?? ""))
            })
        }
        else {
            imgViewForCompanylogo?.image = UIImage(named: "company_detail_placeholder")
        }
        
        setFollowStatus()
    }
    
    private func setFollowStatus() {
        let isFollowed = dataModel?.isFollowed ?? false
        let imageName = isFollowed ? "wifiActivatedIcon" : "wifiIcon"
         isFollowed ? btnForIsFollowed?.setTitleForAllState(title: "Followed") : btnForIsFollowed?.setTitleForAllState(title: "Follow")
        btnForIsFollowed?.setImageForAllState(image: UIImage(named: imageName))
    }
    
    //MARK:- IBActions
    @IBAction func btnActionForShare(_ sender: UIButton) {
        
        guard let shareUrl = dataModel?.employerShareURL else {
            return
        }
        companyDelegate?.share(shareDetail: [shareUrl])
    }
    
    //MARK:- IBActions
    @IBAction func btnActionForIsFollowed(_ sender: UIButton) {
        
        guard let companyId = dataModel?.employerEncId, let isFollowed = dataModel?.isFollowed else {
            return
        }
        companyDelegate?.follow_UnFollowCompany(isFollowed: !isFollowed, companyEncId: companyId, completion: { (status: Bool) in
            if status {
                self.followedAction?(!(isFollowed))
                self.dataModel?.isFollowed = !(isFollowed)
                self.setFollowStatus()
                NotificationCenter.default.post(name: NSNotification.Name.ReloadCompanyList, object: nil)                
            }
        })
    }
}
