//
//  KeyContactsCollectionViewCellModel.swift
//  Jombone
//
//  Created by Money Mahesh on 18/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation

protocol EmployerContactDetailProtocol {
    var description: String? {get set}
    var name: String? {get set}
    var photo: String? {get set}
    var designation: String? {get set}
}

class KeyContactsCollectionViewCellModel: NSObject {
    
    var data: EmployerContactDetailProtocol?
    init(data: EmployerContactDetailProtocol?) {
        self.data = data
    }
}
