//
//  KeyContactsCollectionViewCell.swift
//  Jombone
//
//  Created by dev139 on 13/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import UIKit

class KeyContactsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imgViewForContact: UIImageView?
    @IBOutlet weak var lblForContactName: UILabel?
    @IBOutlet weak var lblForContactDesignation: UILabel?
    @IBOutlet weak var lblForContactDescription: UILabel?
        
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setValues(viewModel: EmployerContactDetailProtocol?) {
        
        lblForContactName?.text = viewModel?.name
        lblForContactDesignation?.text = viewModel?.designation
        lblForContactDescription?.text = viewModel?.description

        imgViewForContact?.backgroundColor = UIColor(hexFromString: "#DCDCDC")
        if let _imageUrlStr = viewModel?.photo,
            let imageUrl = URL(string: (_imageUrlStr)) {
            
            let url_request = URLRequest(url: imageUrl)
            self.imgViewForContact?.setImageWithURLAlamofire(url_request, placeholderImageName: "", success: {
                [weak self] (request: URLRequest?, image: UIImage?) -> Void in
                self?.imgViewForContact?.backgroundColor = UIColor.clear
                self?.imgViewForContact?.image = image
                }
                ,failure: {
                    [weak self] (request:URLRequest?, error:Error?) -> Void in
                    self?.imgViewForContact?.image = nil
                    print("Not able to load profile pic" + (error?.localizedDescription ?? ""))
            })
        }
        else {
            imgViewForContact?.image = nil
        }
    }
}
