//
//  KeyContactsCollectionView.swift
//  Jombone
//
//  Created by dev139 on 13/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import UIKit

class KeyContactsCollectionView: CustomView {

    @IBOutlet weak var contactCollectionView: UICollectionView!
    @IBOutlet weak var lblForTitle: UILabel!

    var viewModel: KeyContactsViewModel!

    override func xibLoaded(data: Any) {
        viewModel = (data as! KeyContactsViewModel)
        lblForTitle.text = viewModel.title
        
        self.contactCollectionView.register(UINib(nibName: "KeyContactsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "KeyContactsCollectionViewCell")
        self.contactCollectionView.reloadData()
    }
}

extension KeyContactsCollectionView : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.cardModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "KeyContactsCollectionViewCell", for: indexPath) as! KeyContactsCollectionViewCell
        cell.setValues(viewModel: viewModel.cardModels[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 302.0, height: 302.0)
    }
}
