//
//  KeyContactsViewModel.swift
//  Jombone
//
//  Created by Money Mahesh on 18/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation

class KeyContactsViewModel: NSObject {
    
    var cardModels = [EmployerContactDetailProtocol]()
    var title: String!

    init(title: String, cardModels: [EmployerContactDetailProtocol]) {
        self.cardModels = cardModels
        self.title = title
    }
}
