//
//  CompanyListViewController.swift
//  Jombone
//
//  Created by Money Mahesh on 23/02/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import UIKit

class CompanyListViewController: TabBarChildViewController {

    @IBOutlet weak var stackView: UIStackView?
    @IBOutlet weak var noResultView: UIView?
    @IBOutlet weak var floatingBtn: UIButton?

    var viewModel = CompanyListViewModel()
    
    var tableWidget: GenericTableViewComponent<CompanyCardViewProtocol, CompanyCardView>?

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "COMPANIES"

        self.addObserver(self, selector: #selector(CompanyListViewController.updatedList), notificationsName: [Notification.Name.UpdatedCompanyList])
        self.addObserver(self, selector: #selector(CompanyListViewController.reloadList), notificationsName: [Notification.Name.ReloadCompanyList])

        self.showLoader()
        self.getCompanyList()
    }
    
    @objc override func updatedList() {
        
        if !self.isViewLoaded {
           return
        }
        viewModel.reset()
        setUpTable()
        
        self.showLoader()
        self.getCompanyList()
    }
    
    @objc func reloadList() {
        tableWidget?.reloadTable()
    }
    
    func getCompanyList() {
        
        viewModel.hitGetCompaniesApi(isFollowed: false, completion: { (message) in
            self.hideLoader()
            self.noResultView?.isHidden = self.viewModel.dataAvailable
        }, success: { (message) in
            self.setUpTable()
        }, failure: { (errorMsg) in
//            self.showAlert(message: errorMsg)
        })
    }
    
    override func imageBtnAction(sender: UIButton) {
    }
    
    func setUpTable() {
        
        if let _tableWidget = tableWidget {
            _tableWidget.appendNewPage(objArray: viewModel.cardDataModels)
        }
        else {
            tableWidget = GenericTableViewComponent()
            tableWidget?.isHidden = false
            tableWidget?.genericTableViewModel = GenericTableViewModel(
                cellDataModel: viewModel.cardDataModels,
                tableFooterView: (LoadMoreView(frame: CGRect(x: 0, y: 0, width: WINDOW_WIDTH, height: 40)), false),
                tableType: .expandable,
                totalPages: viewModel.totalPages)
            tableWidget?.tableStyle(backGroundColor: UIColor.white, insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
            tableWidget?.delegate = self
            stackView?.addArrangedSubview(tableWidget!)
        }
    }
    
    //MARK:- IBActions
    @IBAction func btnActionForFloatingBtn(_ sender: UIButton) {
        CompanyFilterViewController.show(nil)
    }
}

extension CompanyListViewController: GenericTableViewComponentDelegate {
    
    func genericTableDidSelectRowAt(_ indexPath: IndexPath, dataSource: inout [Any]) -> Bool {
        
        if let companyModel = viewModel.cardDataModels[indexPath.row] as? CompanyModel {
            CompanyDetailViewController.show(CompanyDetailViewModel(companyDetail: companyModel))
        }
        return false
    }
    
    func loadMore() {
        getCompanyList()
    }
}

