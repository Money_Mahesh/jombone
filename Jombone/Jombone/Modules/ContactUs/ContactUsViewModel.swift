//
//  ContactUsViewModel.swift
//  Jombone
//
//  Created by dev139 on 10/05/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import UIKit

class ContactUsModel: Codable {
    var email : String?
    var message : String?
    var mobile : String?
    var name : String?
    var subject : String?
    
    init() {}
}

class ContactUsViewModel: NSObject {
    
    var contactUs = ContactUsModel()
    
    private var isInputValid = true
    @discardableResult func isValid(error: String?) -> String? {
        if !isInputValid {
            return error
        }
        isInputValid = (error == nil)
        return error
    }
    
    func reset() {
        isInputValid = true
    }
    
    func hitContactUsApi(completion: ((_ message: String?)->())? = nil, success:  ((_ message: String?)->())? = nil, failure: ((_ message: String?)->())? = nil) {
        
        guard isInputValid else {
            completion?(nil)
            failure?(nil)
            return
        }
        
        let profileServices = ProfileServices(requestTag: "CONTACT_US_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        //Completion Block
        profileServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            if statusCode == 1 {
                completion?(message)
                success?(message)
            }
            else {
                completion?(message)
                failure?(message)
            }
        }
        profileServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            completion?(nil)
            failure?(nil)
        }
        
        let parameters = [
                            "email" : contactUs.email,
                            "message" : contactUs.message,
                            "mobile" : contactUs.mobile,
                            "name" : contactUs.name,
                            "subject" : contactUs.subject
                        ]
        
        profileServices.contactUsApi(parameters: parameters as [String : Any], additionalHeaderElements: nil)
    }
    

}
