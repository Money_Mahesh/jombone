//
//  ContactUsViewController.swift
//  Jombone
//
//  Created by dev139 on 09/05/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import UIKit

class ContactUsViewController: UIViewController {
    
    @IBOutlet weak var txtFieldForName: CustomTextfieldView!
    @IBOutlet weak var txtFieldForEmail: CustomTextfieldView!
    @IBOutlet weak var txtFieldForMobile: CustomTextfieldView!
    @IBOutlet weak var txtFieldForSubject: CustomTextfieldView!
    @IBOutlet weak var txtFieldForMessage: CustomTextfieldView!
    
    var viewModel = ContactUsViewModel()

    
    static func instance() -> ContactUsViewController{
        let instance = PREFERENCES_STORYBOARD.instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
        return instance
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
         setNavigationBarWithTitle("Contact Us", andLeftButton: .back, andRightButton: .none, withBg: .defaultColor)
        
        //Set Textfield Preferences
        txtFieldForName.borderStyle = .All
        txtFieldForName.setJomboneFontStyle()
        txtFieldForName.textfieldViewDelegate = self
        txtFieldForName.textfield.tintColor = UIColor.black
        
        txtFieldForEmail.borderStyle = .All
        txtFieldForEmail.setJomboneEmailStyle()
        txtFieldForEmail.textfieldViewDelegate = self
        
        txtFieldForMobile.borderStyle = .All
        txtFieldForMobile.setJomboneMobileStyle()
        txtFieldForMobile.textfieldViewDelegate = self
        
        txtFieldForSubject.borderStyle = .All
        txtFieldForSubject.setJomboneFontStyle()
        txtFieldForSubject.textfieldViewDelegate = self
        txtFieldForSubject.textfield.tintColor = UIColor.black
        
        txtFieldForMessage.borderStyle = .All
        txtFieldForMessage.setJomboneFontStyle()
        txtFieldForMessage.textfieldViewDelegate = self
        txtFieldForMessage.textfield.tintColor = UIColor.black
    }
    
    
    @IBAction func sendBtnAction(_ sender: UIButton) {
        
        self.view.endEditing(true)
        viewModel.reset()
        
        viewModel.contactUs.name = txtFieldForName.isValidName("name")
        viewModel.isValid(error: txtFieldForName.errorMsg)
        
        viewModel.contactUs.email = txtFieldForEmail.isValidEmail()
        viewModel.isValid(error: txtFieldForEmail.errorMsg)
        
        let validatePhoneNo = DataValidationUtility.shared.validatePhoneNo(txtFieldForMobile.text?.toUnPhoneNumber())
        txtFieldForMobile.errorMsg = viewModel.isValid(error: validatePhoneNo.errorMsg)
        viewModel.contactUs.mobile = txtFieldForMobile.text

        viewModel.contactUs.subject = txtFieldForSubject.isEmpty("subject")
        viewModel.isValid(error: txtFieldForSubject.errorMsg)
        
        viewModel.contactUs.message = txtFieldForMessage.isEmpty("message")
        viewModel.isValid(error: txtFieldForMessage.errorMsg)
        
        showLoader()
        viewModel.hitContactUsApi(completion: { (message) in
            self.hideLoader()
        }, success: { (message) in
            self.navigationController?.popViewController(animated: true)
            APPLICATION_INSTANCE.visibleViewController()?.showAlert(message: message)
        }, failure: { (message) in
            self.showAlert(message: message)
        })
    }
}


extension ContactUsViewController : CustomTextfieldViewDelegate {
    
    func textFieldDidBeginEditing(_ identifier: String, textField: CustomTextfield) {
        if identifier == "Mobile" {
            if let text = textField.text, text.contains("(") {
                textField.text = text.toUnPhoneNumber()
            }
        }
    }
    
    func textFieldDidEndEditing(_ identifier: String, textField: CustomTextfield) {
        if identifier == "Mobile" {
            let formattedString = textField.text?.toPhoneNumber()
            textField.text = formattedString
        }
    }
}
