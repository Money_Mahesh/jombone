//
//  TabBarViewController.swift
//  Jombone
//
//  Created by Money Mahesh on 16/09/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addObserver(self, selector: #selector(TabBarViewController.resetCount), notificationsName: [Notification.Name.NotificationCountUpdated])
        
        self.tabBar.layer.addShadow(color: UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.1).cgColor, direction: ShadowDirection.up, radius: 6, spread: 4)
        
        APP_DELEGATE_INSTANCE.tabBarController = self
        self.delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        tabBar.tintColor = DEFAULT_NAVIGATION_COLOR
    }
    
    
    
    @objc func resetCount() {
        
        if let tabItems = APP_DELEGATE_INSTANCE.tabBarController?.tabBar.items {
            
            // In this case we want to modify the badge number of the third tab:
            let tabItem = tabItems[3]
            if let notificationCount = UserDataManager.shared.countDetail?.notificationCount, notificationCount > 0 {
                tabItem.badgeValue = String(notificationCount)
            }
            else {
                tabItem.badgeValue = nil
            }
        }
    }
}

extension TabBarViewController: UITabBarControllerDelegate {
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if let navigationController = viewController as? UINavigationController, navigationController.restorationIdentifier == "WorkTab" {
            
            let workPopUpViewController = WORK_STORYBOARD.instantiateViewController(withIdentifier: "WorkPopUpViewController")
            tabBarController.present(workPopUpViewController, animated: false)
            return false
        }
        else if let navigationController = viewController as? UINavigationController,
            let _viewController = navigationController.visibleViewController as? TabBarChildViewController {
            _viewController.updatedList()
        }
        
        return true
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        NotificationCenter.default.post(name: NSNotification.Name.TabSelectionChanged, object: nil)
        tabBar.tintColor = DEFAULT_NAVIGATION_COLOR
    }
}
