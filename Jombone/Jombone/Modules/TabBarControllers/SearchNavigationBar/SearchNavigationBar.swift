//
//  SearchNavigationBar.swift
//  Jombone
//
//  Created by Money Mahesh on 23/02/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import UIKit

protocol SearchNavigationBarProtocol: NSObjectProtocol {
    func cancelButtonTapped()
    func menuButtonTapped()
    func searchTextfieldEditingBegin()
    func searchTextfieldTextChanged(ch: String)
}

extension SearchNavigationBarProtocol {
    func cancelButtonTapped() {}
    func menuButtonTapped() {}
    func searchTextfieldEditingBegin() {}
    func searchTextfieldTextChanged(ch: String) {}
}

@IBDesignable class SearchNavigationBar: CustomView {
    
    @IBOutlet weak var imgViewForProfile: UIImageView!
    @IBOutlet weak var btnForCancel: UIButton!
    @IBOutlet weak var btnForMenu: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    
    weak var delegate: SearchNavigationBarProtocol?
    
    override func xibLoaded() {
        if let textField = searchBar?.value(forKey: "_searchField") as? UITextField {
            textField.clearButtonMode = UITextField.ViewMode.whileEditing
            textField.textColor = UIColor.init(hexFromString: "#424242")
        }
        searchBar.backgroundImage = UIImage()
        
        addObserver(self, selector: #selector(SearchNavigationBar.profileImageUpdated), notificationsName: [Notification.Name.UserProfileUpdated])
        self.profileImageUpdated()
    }
    
    private func toggleSearchIcon(_ show: Bool) {
        
        guard let textField = searchBar?.value(forKey: "_searchField") as? UITextField else {
            return
        }
        
        if show {
            textField.leftViewMode = UITextField.ViewMode.always
            searching(false)
        }
        else {
            textField.leftViewMode = UITextField.ViewMode.never
            searching(true)
        }
    }
    
    private func searching(_ status: Bool) {
        btnForCancel.isHidden = !status
        imgViewForProfile.isHidden = status
        imgViewForProfile.isHidden = status
    }
    
    @objc func profileImageUpdated() {
        
        if let _imageUrlStr = UserDataManager.shared.profileDetail?.personalDetailsBeans?.profilePicPath,
            let imageUrl = URL(string: (_imageUrlStr)) {
            
            let url_request = URLRequest(url: imageUrl)
            self.imgViewForProfile?.setImageWithURLAlamofire(url_request, placeholderImageName: "", success: {
                [weak self] (request: URLRequest?, image: UIImage?) -> Void in
                    self?.imgViewForProfile.image = image
                }
                ,failure: {
                    [weak self] (request:URLRequest?, error:Error?) -> Void in
                    self?.imgViewForProfile?.image = UIImage(named: "profileDefault")
                    print("Not able to load profile pic" + (error?.localizedDescription ?? ""))
            })
        }
        else {
            imgViewForProfile?.image = UIImage(named: "profileDefault")
        }
    }
    
    func resetSearchBar() {
        searchBar.endEditing(true)
        searchBar.text = ""
        toggleSearchIcon(true)
    }
    
    //MARK:- IBActions
    @IBAction func btnActionForCancel() {
        searchBar.endEditing(true)
        searchBar.text = ""
        toggleSearchIcon(true)
        delegate?.cancelButtonTapped()
    }
    
    @IBAction func tapActionForProfileImage(_ sender: UITapGestureRecognizer) {
        APPLICATION_INSTANCE.visibleViewController()?.navigationController?.pushViewController(ProfileViewController.instance(), animated: true)

    }
    
    @IBAction func btnActionForMenu() {
        btnActionForCancel()
        delegate?.menuButtonTapped()
    }
}

extension SearchNavigationBar: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        toggleSearchIcon(false)
        delegate?.searchTextfieldEditingBegin()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        delegate?.searchTextfieldTextChanged(ch: searchText)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        if (searchBar.text ?? "").count == 0 {
            toggleSearchIcon(true)
            delegate?.cancelButtonTapped()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
}
