//
//  SearchDeaultOptionTableViewCell.swift
//  Jombone
//
//  Created by Money Mahesh on 23/02/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import UIKit

class SearchDeaultOptionTableViewCell: UITableViewCell {

    @IBOutlet weak var imgViewForIcon: UIImageView!
    @IBOutlet weak var lblForTitle: UILabel!
    
    var viewModel: SearchDeaultOptionCellModel? {
        didSet {
            imgViewForIcon.image = nil
            if let iconName = viewModel?.data.iconName {
                imgViewForIcon.image = UIImage(named: iconName)
            }
            lblForTitle.text = viewModel?.data.title
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
