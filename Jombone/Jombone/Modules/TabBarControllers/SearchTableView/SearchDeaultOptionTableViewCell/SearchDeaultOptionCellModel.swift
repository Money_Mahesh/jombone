//
//  SearchDeaultOptionCellModel.swift
//  Jombone
//
//  Created by Money Mahesh on 23/02/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation

class SearchDeaultOptionCellModel: NSObject {
    
    struct DataModel {
        var iconName: String?
        var title: String?
    }
    
    var data: DataModel!
    
    init(data: DataModel) {
        self.data = data
    }
}
