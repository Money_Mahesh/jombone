//
//  SearchTableView.swift
//  Jombone
//
//  Created by Money Mahesh on 23/02/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import UIKit

protocol SearchTableViewProtocol: NSObjectProtocol {
    func optionSelected()
    func moveToJobFilter()
    func moveToDetail()
    func moveToCompanyFilter()
}

extension SearchTableViewProtocol {
    func optionSelected() {}
    func moveToJobFilter() {}
    func moveToCompanyFilter() {}
}

class SearchTableView: CustomView, CompanyAction, JobAction {
    
    @IBOutlet weak var tableView: UITableView!
    weak var delegate: SearchTableViewProtocol?
    
    var viewModel = SearchTableViewModel()
    weak var jobActionDelegate: JobAction?
    weak var companyActionDelegate: CompanyAction?
    
    static func add(frame: CGRect, to parentViewController: UIViewController) -> SearchTableView {
        
        let searchView = SearchTableView(frame: frame)
        searchView.registerCell()
        parentViewController.view.addSubview(searchView)
        return searchView
    }
    
    override func xibLoaded() {
        self.registerCell()
        jobActionDelegate = self
        companyActionDelegate = self
    }
    
    private func registerCell() {
        tableView.register(UINib(nibName: "SearchCustomOptionTableViewCell", bundle: nil), forCellReuseIdentifier: "SearchCustomOptionTableViewCell")
        tableView.register(UINib(nibName: "SearchDeaultOptionTableViewCell", bundle: nil), forCellReuseIdentifier: "SearchDeaultOptionTableViewCell")
    }
    
    func show() {
        self.isHidden = false
        searchText(nil)
    }
    
    func hide() {
        self.isHidden = true
        self.view.endEditing(true)
    }
    
    func searchText(_ text: String?) {
        viewModel.fetchData(text: text) {
            self.tableView.reloadData()
        }
    }
}

extension SearchTableView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if viewModel.type == SearchTableViewModel.SearchType.normal {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SearchDeaultOptionTableViewCell", for: indexPath) as! SearchDeaultOptionTableViewCell
            cell.viewModel = viewModel.data[indexPath.row] as? SearchDeaultOptionCellModel
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SearchCustomOptionTableViewCell", for: indexPath) as! SearchCustomOptionTableViewCell
            cell.viewModel = viewModel.data[indexPath.row] as? GlobalSearchModel
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("IndexPath \(indexPath.row)")
        
        if viewModel.type == SearchTableViewModel.SearchType.normal {
            hide()
            
            switch indexPath.row {
            case 0:
                delegate?.moveToJobFilter()
                
            case 1:
                delegate?.moveToCompanyFilter()

            default:
                break
            }
        }
        else {
            if let data = viewModel.data[indexPath.row] as? GlobalSearchModel, let status = data.status {
                if status == 3 {
                    companyActionDelegate?.companyDetail(empEncId: data.encryptedEmployerId, completion: { (data: CompanyModel) in
                       
                       self.delegate?.moveToDetail()
                        CompanyDetailViewController.show(CompanyDetailViewModel(companyDetail: data))

                    })
                }
                else if status == 2 {
                    
                    jobActionDelegate?.jobDetail(jobEncId: data.encryptedJobId, completion: { (data: JobModel) in
                        self.delegate?.moveToDetail()

                        JobsDetailViewController.show(JobsDetailViewModel(jobDetail: data))
                    })
                }
            }
        }
    }
}
