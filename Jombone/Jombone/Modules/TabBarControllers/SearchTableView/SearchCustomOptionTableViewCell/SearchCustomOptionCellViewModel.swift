//
//  SearchCustomOptionCellViewModel.swift
//  Jombone
//
//  Created by Money Mahesh on 23/02/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
protocol GlobalSearchModelProtocol {
    
    var employerId : String? {get set}
    var employerLogoPath : String? {get set}
    var employerName : String? {get set}
    var encryptedEmployerId : String? {get set}
    var encryptedEmployerLogoId : String? {get set}
    var encryptedJobId : String? {get set}
    var encryptedJobTitleId : String? {get set}
    var jobId : String? {get set}
    var jobTitle : String? {get set}
    var jobTitleId : String? {get set}
    var status : Int? {get set}
}

class SearchCustomOptionCellViewModel: NSObject {
    
    var data: GlobalSearchModelProtocol!
    init(data: GlobalSearchModelProtocol) {
        self.data = data
    }
}
