//
//  SearchCustomOptionTableViewCell.swift
//  Jombone
//
//  Created by Money Mahesh on 23/02/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import UIKit

class SearchCustomOptionTableViewCell: UITableViewCell {

    @IBOutlet weak var imgViewForIcon: UIImageView?
    @IBOutlet weak var lblForTitle: UILabel?
    @IBOutlet weak var lblForSubTitle: UILabel?

    var viewModel: GlobalSearchModelProtocol? {
        didSet {
            
            let isEmployer = (viewModel?.status ?? 3) == 3
            let title = isEmployer ? viewModel?.employerName : viewModel?.jobTitle
            let subTitle: String? = isEmployer ? nil : (viewModel?.employerName == nil ? nil : (", " + (viewModel?.employerName ?? "")))

            lblForTitle?.text = title
            lblForSubTitle?.text = subTitle

            imgViewForIcon?.image = nil
            if let _imageUrlStr = viewModel?.employerLogoPath,
                let imageUrl = URL(string: (_imageUrlStr)) {
                
                let url_request = URLRequest(url: imageUrl)
                self.imgViewForIcon?.setImageWithURLAlamofire(url_request, placeholderImageName: "", success: {
                    [weak self] (request: URLRequest?, image: UIImage?) -> Void in
                    self?.imgViewForIcon?.backgroundColor = UIColor.clear
                    self?.imgViewForIcon?.image = image
                    }
                    ,failure: {
                        
                        [weak self] (request:URLRequest?, error:Error?) -> Void in
                        
                        self?.imgViewForIcon?.contentMode = .scaleAspectFill
                        self?.imgViewForIcon?.image = UIImage(named: (isEmployer ? "company_listing_placeholder" : "noJobPlaceholder"))
                        print("Not able to load profile pic" + (error?.localizedDescription ?? ""))
                })
            }
            else {
                self.imgViewForIcon?.contentMode = .scaleAspectFill
                self.imgViewForIcon?.image = UIImage(named: (isEmployer ? "company_listing_placeholder" : "noJobPlaceholder"))
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
