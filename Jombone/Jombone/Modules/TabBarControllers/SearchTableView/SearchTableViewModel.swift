//
//  SearchTableViewModel.swift
//  Jombone
//
//  Created by Money Mahesh on 23/02/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation

class SearchTableViewModel: NSObject {
    enum SearchType {
        case normal
        case textBase
    }
    private var defaultCellDataModel = [
        SearchDeaultOptionCellModel(data: SearchDeaultOptionCellModel.DataModel(iconName: "SearchJob", title: "Search Jobs")),
        SearchDeaultOptionCellModel(data: SearchDeaultOptionCellModel.DataModel(iconName: "SearchCompany", title: "Search Companies"))
    ]
    
    var type: SearchType!
    var data = [Any]()
    
    override init() {}
    
    func fetchData(text: String?, _ completion: @escaping (()->())) {
        
        if let _text = text, _text.count > 2 {
            type = SearchType.textBase
            
            GlobalSearchHandler.instance.search(string: _text, completion: { (message) in
                
                self.data = GlobalSearchHandler.instance.data ?? [GlobalSearchModel]()
                completion()
            })
        }
        else {
            type = SearchType.normal
            data = defaultCellDataModel
            completion()
        }
    }
}
