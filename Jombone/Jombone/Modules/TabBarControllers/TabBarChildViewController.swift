//
//  TabBarChildViewController.swift
//  Jombone
//
//  Created by Money Mahesh on 23/02/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import UIKit

class TabBarChildViewController: UIViewController {
    
    @IBOutlet weak var navigationView: SearchNavigationBar!
    @IBOutlet weak var searchTableView: SearchTableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        searchTableView.delegate = self
        searchTableView.hide()
        
        navigationView.delegate = self
        
        self.addObserver(self, selector: #selector(TabBarChildViewController.resetView), notificationsName: [Notification.Name.TabSelectionChanged])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.tabBarController?.tabBar.isHidden = false
    }
    
    @objc func updatedList() {
    }
}

extension TabBarChildViewController: SearchNavigationBarProtocol {
    
    func menuButtonTapped() {
        sideMenuController?.revealMenu()
    }
    
    func cancelButtonTapped() {
        searchTableView.hide()
    }
    
    func searchTextfieldEditingBegin() {
        searchTableView.show()
    }
    
    func searchTextfieldTextChanged(ch: String) {
        searchTableView.searchText(ch)
    }
}

extension TabBarChildViewController: SearchTableViewProtocol {
    
    @objc func resetView() {
        
        self.navigationView.resetSearchBar()
        self.searchTableView.hide()
        self.searchTableView.searchText(nil)
    }
    
    func moveToJobFilter() {
        JobFilterViewController.show {
            self.resetView()
            APP_DELEGATE_INSTANCE.tabBarController?.selectedIndex = 1
        }
    }
    
    func moveToCompanyFilter() {
        CompanyFilterViewController.show{
            self.resetView()
            APP_DELEGATE_INSTANCE.tabBarController?.selectedIndex = 2
        }
    }
    
    func moveToDetail() {
        self.resetView()
    }
    
    func optionSelected() {
        navigationView.endEditing(true)
    }
}


