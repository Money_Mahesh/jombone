//
//  ChangePasswordViewModel.swift
//  Jombone
//
//  Created by dev139 on 18/09/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import UIKit

enum ChangePurpose {
    case changePassword
    case resetPassword
}

class ChangePasswordViewModel: NSObject {
    
    var purpose: ChangePurpose!
    var oldPassword: String!{
        didSet {
            if oldPassword == nil {
                isInputValid = false
            }
        }
    }
    var newPassword: String!{
        didSet {
            if newPassword == nil {
                isInputValid = false
            }
        }
    }
    var confirmNewPassword: String!{
        didSet {
            if confirmNewPassword == nil {
                isInputValid = false
            }
        }
    }
    
    init(purpose : ChangePurpose) {
        super.init()
        self.purpose = purpose
    }
    
    private var isInputValid = true
    func reset() {
        isInputValid = true
    }
    
    func hitChangePasswordApi(completion: ((_ message: String?)->())? = nil, success:  ((_ message: String?)->())? = nil, failure: (( _ message: String?)->())? = nil) {
        
        if !isInputValid {
            completion?(nil)
            return
        }
        
        let authenticationServices = AutheticationServices(requestTag: "CHANGE_PASSWORD_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        //Completion Block
        authenticationServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            if statusCode == 1 {
                let _ = response.0 as? ChangePasswordReponse
                completion?(message)
                success?(message)
            }
            else {
                completion?(message)
                failure?(message)
            }
        }
        authenticationServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            completion?(nil)
            failure?(nil)
        }
        
        let parameters: [String: Any] = [
            "currentPassword": oldPassword!,
            "newPassword": newPassword!,
            "confirmPassword":confirmNewPassword
        ]
        
        
        authenticationServices.hitChangePassword(parameters: parameters, additionalHeaderElements: nil)
    }
    
    func hitResetPasswordApi(completion: ((_ message: String?)->())? = nil, success:  ((_ message: String?)->())? = nil, failure: ((_ statusCode: Int?, _ message: String?)->())? = nil) {
        
        if !isInputValid {
            completion?(nil)
            return
        }
        
        let authenticationServices = AutheticationServices(requestTag: "RESET_PASSWORD_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        //Completion Block
        authenticationServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            if statusCode == 1 {
                let loginDetail = response.0 as? LoginDetail
                UserDataManager.shared.token = loginDetail?.token
                if let personalDetailsBean = loginDetail?.personalDetailsBean {
                    UserDataManager.shared.profileDetail = ProfileDetail(personalDetailsBean: personalDetailsBean)
                }
                
                completion?(message)
                success?(message)
            }
            else {
                completion?(message)
                failure?(statusCode, message)
            }
        }
        authenticationServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            completion?(nil)
            failure?(nil, nil)
        }
        
        let parameters: [String: Any] = [
            "newPassword": newPassword!,
            "confirmPassword":confirmNewPassword
        ]
        
        authenticationServices.hitResetPassword(parameters: parameters, additionalHeaderElements: nil)
    }
}
