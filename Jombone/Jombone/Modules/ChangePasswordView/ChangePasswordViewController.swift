//
//  ChangePasswordViewController.swift
//  Jombone
//
//  Created by dev139 on 20/08/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import UIKit

class ChangePasswordViewController: WhiteStatusBarViewController {
    
    @IBOutlet weak var textfieldForOldPassword: CustomTextfieldView!
    @IBOutlet weak var textfieldForNewPassword: CustomTextfieldView!
    @IBOutlet weak var textfieldForConfirmPassword: CustomTextfieldView!
    @IBOutlet weak var signUpLoginStackView: UIStackView!
    
    var viewModel: ChangePasswordViewModel!
    
    static func show(viewModel: ChangePasswordViewModel) {
        
        let viewController = LOGIN_STORYBOARD.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
        
        viewController.viewModel = viewModel
        APPLICATION_INSTANCE.visibleViewController()?.navigationController?.pushViewController(viewController, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textfieldForOldPassword.setJombonePasswordStyle()
        textfieldForNewPassword.setJombonePasswordStyle()
        textfieldForConfirmPassword.setJombonePasswordStyle()
        setUpView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpView() {
        
        switch viewModel.purpose! {
        case .resetPassword:
            textfieldForOldPassword.isHidden = true
            setNavigationBarWithTitle("Change Password", andLeftButton: .none, andRightButton: .none, withBg: .bgColor(value: UIColor.clear))
            
       default:
            signUpLoginStackView.isHidden = true
            setNavigationBarWithTitle("Change Password", andLeftButton: .back, andRightButton: .none, withBg: .bgColor(value: UIColor.clear))

            break
        }
    }
    
    //MARK:- IBAction
    @IBAction func btnActionForSave(_ sender: UIButton) {
        
        view.endEditing(true)
        
        viewModel.reset()
        viewModel.newPassword = textfieldForNewPassword.isValidPassword("new password")
        viewModel.confirmNewPassword = textfieldForConfirmPassword.isValidConfirmPassword(password: textfieldForNewPassword.text)
        
        switch viewModel.purpose! {
        case .resetPassword:
            resetPassword()
        default:
            viewModel.oldPassword = textfieldForOldPassword.isValidPassword("old password")
            changePassword()
            break
        }
    }
    
    @IBAction func btnActionForSignUp(_ sender: Any) {
        moveToRootViewController()
    }
    
    @IBAction func btnActionForLogin(_ sender: Any) {
        moveToRootViewController()
        LoginViewController.showWithAnimation(false)
    }
    
    func moveToRootViewController() {
        self.navigationController?.popToRootViewController(animated: false)
    }
    
    func resetPassword() {
        
        showLoader()
        viewModel.hitResetPasswordApi(completion: { (message) in
            self.hideLoader()
        }, success: { (message) in
            UserDataManager.shared.isSignUpCompleted = true
            APP_DELEGATE_INSTANCE.setupInitialViewController()
        }, failure: { (statusCode, errorMessage) in
            if let errorCode = statusCode {
                self.handleError(errorCode: errorCode)
                self.showAlert(message: errorMessage)
            }
        })
    }
    
    func changePassword() {
        
        showLoader()
        viewModel.hitChangePasswordApi(completion: { (message) in
            self.hideLoader()
        }, success: { (message) in
            self.navigationController?.popViewController(animated: true)
            APPLICATION_INSTANCE.visibleViewController()?.showAlert(message: message)
        }, failure: { (errorMessage) in
            self.showAlert(message: errorMessage)
        })
    }
    
    //For forgot password
    func handleError(errorCode: Int) {
        
        moveToRootViewController()
        switch errorCode {
        case 2:
            //Email not verified - open email verify screen
            OTPVerificationViewController.show(viewModel: OTPVerificationViewModel(
                purpose: .email(email: UserDataManager.shared.profileDetail?.personalDetailsBeans?.email)))
            
        case 3:
            //successful login but location not provided - open location screen
            SelectJobLocationViewController.show()
            
        case 4:
            //mobile not verified, open mobile verify screen
            OTPVerificationViewController.show(viewModel: OTPVerificationViewModel(
                purpose: .phoneNo))
            
        default:
            break
        }
    }
}

