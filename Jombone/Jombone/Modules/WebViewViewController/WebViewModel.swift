//
//  WebViewModel.swift
//  Jombone
//
//  Created by Money Mahesh on 23/08/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import Foundation

class WebViewModel: NSObject {
    var url: URL?
    var title: String
    var willPresent: Bool
    
    init(urlStr: String, title: String, willPresent present: Bool = false) {
        self.url = URL(string: urlStr)
        self.title = title
        self.willPresent = present
    }
}
