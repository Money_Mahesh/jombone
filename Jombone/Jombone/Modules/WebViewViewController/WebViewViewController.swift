//
//  WebViewViewController.swift
//  Jombone
//
//  Created by Money Mahesh on 23/08/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import UIKit
import WebKit

class WebViewViewController: WhiteStatusBarViewController {

    var webView: WKWebView!
    var progressBar: UIProgressView!

    var viewModel: WebViewModel!
    var isObserverAdded: Bool = false
    
    static func instance(viewModel: WebViewModel) -> UIViewController {
        
        let instance = WebViewViewController()
        instance.viewModel = viewModel
        if viewModel.willPresent {
            return UINavigationController(rootViewController: instance)
        }
        return instance
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setNavigationBarWithTitle(viewModel.title, andLeftButton: viewModel.willPresent ? .close : .back, andRightButton: .none, withBg: .defaultColor)
        
        if let url = viewModel.url {
            setWebView(url: url)
        }
        else {
            showError(msg: "Invalid Url")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func setWebView(url: URL) {
    
        let webConfiguration = WKWebViewConfiguration()
        let controller = WKUserContentController()
        webConfiguration.userContentController = controller
        
        webView = WKWebView(frame: CGRect(x: 0, y: 0, width: WINDOW_WIDTH, height: WINDOW_HEIGHT), configuration: webConfiguration)
        
        webView.allowsBackForwardNavigationGestures = false
        webView.navigationDelegate = self
        webView.addObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress), options: .new, context: nil)
        isObserverAdded = true
        
        view.addSubview(webView)
        
        progressBar = UIProgressView(frame: CGRect(x: 0, y: 0, width: WINDOW_WIDTH, height: 2))
        progressBar.progressTintColor = UIColor.green
        view.addSubview(progressBar)
        
        //Load Request
        let urlRequest = URLRequest(url: url, timeoutInterval: 0.0)
        webView.load(urlRequest)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if keyPath == "estimatedProgress" {
            progressBar.setProgress(Float(webView.estimatedProgress), animated: true)
            if progressBar.progress == 1.0 {
                progressBar.isHidden = true
            } else {
                progressBar.isHidden = false
            }
        }
    }
    
    func showError(msg: String) {
        let errorLbl = UILabel(frame: CGRect(x: 0, y: 0, width: WINDOW_WIDTH, height: WINDOW_HEIGHT))
        errorLbl.text = msg
        errorLbl.textColor = UIColor.gray
        errorLbl.textAlignment = .center
        errorLbl.backgroundColor = UIColor.white
        view.addSubview(errorLbl)
    }
    
    deinit {
        if isObserverAdded {
            isObserverAdded = false
            webView.removeObserver(self, forKeyPath: "estimatedProgress")
        }
    }
}

extension WebViewViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        showError(msg: error.localizedDescription)
    }
}

