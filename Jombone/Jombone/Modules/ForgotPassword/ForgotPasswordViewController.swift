//
//  ForgotPasswordViewController.swift
//  Jombone
//
//  Created by dev139 on 19/08/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: WhiteStatusBarViewController {
    
    @IBOutlet weak var textfieldForEmail: CustomTextfieldView!
    @IBOutlet var viewModel: ForgotPasswordViewModel!

    static func show() {
        
        let viewController = LOGIN_STORYBOARD.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
        APPLICATION_INSTANCE.visibleViewController()?.navigationController?.pushViewController(viewController, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Set Preference for textfield
        textfieldForEmail.setJomboneEmailStyle()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func btnActionForSubmit(_ sender: UIButton) {
        
        view.endEditing(true)
        
        viewModel.reset()
        viewModel.email = textfieldForEmail.isValidEmail()
    
        showLoader()
        viewModel.hitForgotPasswordApi(completion: { (message) in
            self.hideLoader()
        }, success: { (message) in
            self.hideLoader()
            OTPVerificationViewController.show(viewModel: OTPVerificationViewModel(purpose: .forgotPassword(email: self.viewModel.email)))
            
        }, failure: { (message) in
            self.hideLoader()
            self.showAlert(message: message, withOk: true)
        })
        
    }
}
