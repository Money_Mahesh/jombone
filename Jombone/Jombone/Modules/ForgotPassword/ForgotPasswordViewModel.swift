//
//  ForgotPasswordViewModel.swift
//  Jombone
//
//  Created by Money Mahesh on 27/02/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation

class ForgotPasswordViewModel: NSObject {
    
    var email: String! {
        didSet {
            if email == nil {
                isInputValid = false
            }
        }
    }
    
    private var isInputValid = true
    func reset() {
        isInputValid = true
    }
    
    func hitForgotPasswordApi(completion: ((_ message: String?)->())? = nil, success:  ((_ message: String?)->())? = nil, failure: ((_ message: String?)->())? = nil) {
        
        if !isInputValid {
            completion?(nil)
            return
        }
        
        let autheticationServices = AutheticationServices(requestTag: "FORGOT_PASSWORD_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        //Completion Block
        autheticationServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            if statusCode == 1, let response = response.0 as? String {
                
                UserDataManager.shared.token = response
                completion?(message)
                success?(message)
            }
            else {
                completion?(message)
                failure?(message)
            }
        }
        autheticationServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            completion?(nil)
            failure?(nil)
        }
        
        let parameters: [String: Any] = [
            "email": email!
        ]
        autheticationServices.hitForgotPassword(parameters: parameters, additionalHeaderElements: nil)
    }
}
