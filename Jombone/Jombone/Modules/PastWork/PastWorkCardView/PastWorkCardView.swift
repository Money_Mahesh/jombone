//
//  PastWorkCardView.swift
//  Jombone
//
//  Created by Money Mahesh on 13/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import UIKit

class PastWorkCardView: GenericView<PastWorkCardProtocol>, CompanyAction {
    
    @IBOutlet weak var btnForDownload: UIButton?
    @IBOutlet weak var jobCardBaseViewStack: UIStackView?
    
    var jobCardBaseView: JobCardBaseView!
    weak var companyDelegate: CompanyAction?
    var downloadDelegate = DownloadUploadAction()

    override func xibLoaded() {
        jobCardBaseView = JobCardBaseView()
        jobCardBaseViewStack?.addArrangedSubview(jobCardBaseView)
        companyDelegate = self
    }
    
    override var viewModel: ViewModel<PastWorkCardProtocol>? {
        didSet {
            jobCardBaseView.viewModel = JobCardBaseViewModel(data: viewModel?.data, otherDetail: nil)
        }
    }
    
    //MARK:- IBActions
    @IBAction func btnActionForDownload(_ sender: UIButton) {
        downloadDelegate.download(fileName: viewModel?.data?.jdDocName, urlStr: viewModel?.data?.downloadRoePath)
    }
}

