//
//  PastWorkCardViewModel.swift
//  Jombone
//
//  Created by Money Mahesh on 13/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation

protocol  PastWorkCardProtocol: JobCardViewProtocol {
    var downloadRoePath : String? {get set}
    var jdDocName : String? {get set}
}

class PastWorkCardViewModel: ViewModel<PastWorkCardProtocol> {
    
    override init(data: PastWorkCardProtocol?, otherDetail: Any?) {
        super.init(data: data, otherDetail: otherDetail)
    }
}
