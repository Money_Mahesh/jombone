//
//  PastWorkListViewController.swift
//  Jombone
//
//  Created by Money Mahesh on 23/02/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import UIKit

class PastWorkListViewController: PopUpViewController {
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var noResultView: UIView!
    
    var navBtnType: NavBtnType = .menu
    var viewModel = PastWorkListViewModel()
    
    var tableWidget: GenericTableViewComponent<PastWorkCardProtocol, PastWorkCardView>?
    
    static func instance(navBtnType: NavBtnType = .back) -> PastWorkListViewController {
        
        let viewController = WORK_STORYBOARD.instantiateViewController(withIdentifier: "PastWorkListViewController") as! PastWorkListViewController
        viewController.navBtnType = navBtnType
        viewController.viewModel = PastWorkListViewModel()
        return viewController
    }
    
    static func show(navBtnType: NavBtnType = .menu) {
        
        let viewController = WORK_STORYBOARD.instantiateViewController(withIdentifier: "PastWorkListViewController") as! PastWorkListViewController
        viewController.navBtnType = navBtnType
        viewController.viewModel = PastWorkListViewModel()
        APPLICATION_INSTANCE.visibleViewController()?.navigationController?.pushViewController(viewController, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationBarWithTitle("Past Work", andLeftButton: navBtnType, andRightButton: .none, withBg: .defaultColor)

        self.addObserver(self, selector: #selector(PastWorkListViewController.reloadList), notificationsName: [Notification.Name.ReloadPastWorkList])
        
        self.showLoader()
        self.getPastWorkList()
    }
    
    func updatedList() {
        viewModel.reset()
        setUpTable()
        
        self.showLoader()
        self.getPastWorkList()
    }
    
    @objc func reloadList() {
        tableWidget?.reloadTable()
    }
    
    func getPastWorkList(){
        
        viewModel.hitGetPastWorkListApi(completion: { (message) in
            self.hideLoader()
            self.noResultView.isHidden = self.viewModel.dataAvailable
        }, success: { (message) in
            self.setUpTable()
        }, failure: { (errorMsg) in
//            self.showAlert(message: errorMsg)
        })
    }
    
    func setUpTable() {
        
        if let _tableWidget = tableWidget {
            _tableWidget.appendNewPage(objArray: viewModel.cardDataModels)
        }
        else {
            tableWidget = GenericTableViewComponent()
            tableWidget?.isHidden = false
            tableWidget?.genericTableViewModel = GenericTableViewModel(
                cellDataModel: viewModel.cardDataModels,
                tableFooterView: (LoadMoreView(frame: CGRect(x: 0, y: 0, width: WINDOW_WIDTH, height: 40)), false),
                tableType: .expandable,
                totalPages: viewModel.totalPages)

            tableWidget?.tableStyle(backGroundColor: UIColor.white, insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
            tableWidget?.delegate = self
            stackView.addArrangedSubview(tableWidget!)
        }
    }
}

extension PastWorkListViewController: GenericTableViewComponentDelegate {
    
    func genericTableDidSelectRowAt(_ indexPath: IndexPath, dataSource: inout [Any]) -> Bool {
        
        if let jobModel = viewModel.cardDataModels[indexPath.row] as? JobModel {
            JobsDetailViewController.show(JobsDetailViewModel(jobDetail: jobModel), parentController: self)
        }
        return false
    }
    
    func loadMore() {
        getPastWorkList()
    }
}

