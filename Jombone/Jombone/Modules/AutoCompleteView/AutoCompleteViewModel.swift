//
//  AutoCompleteViewModel.swift
//  Jombone
//
//  Created by Money Mahesh on 20/09/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import Foundation

class AutoCompleteViewModel: NSObject {
    
    var textTyped: String?
    var searchType: AutoComplete!
    var isOffline: Bool! = false
    var noOfSelection: NoOfSelection = .single
    
    var selectedOptions = [AutoCompleteCardViewModelProtocol]()
//    withSelectionStatusLocation
    private var _options = [AutoCompleteCardViewModelProtocol]()
    var options: [AutoCompleteCardViewModelProtocol] {
        get {
            return _options
        }
        set {
            _options = [AutoCompleteCardViewModelProtocol]()
            var updatedNewValue = [AutoCompleteCardViewModelProtocol]()
            for obj in newValue {
                if selectedOptions.filter({return $0.title == obj.title}).count == 0 {
                    updatedNewValue.append(obj)
                }
            }
            _options = updatedNewValue
        }
    }

    init(selectedOptions: [AutoCompleteCardViewModelProtocol]? = nil, type: AutoComplete, isOffline: Bool, noOfSelection: NoOfSelection = .single) {
        super.init()
        self.isOffline = isOffline
        self.searchType = type
        self.noOfSelection = noOfSelection
        self.selectedOptions = selectedOptions ?? [AutoCompleteCardViewModelProtocol]()
        self.options = self.selectedOptions
    }
    
    func getList(completion: ((_ message: String?)->())? = nil, success:  ((_ message: String?)->())? = nil, failure: ((_ message: String?)->())? = nil) {
    
        switch searchType! {
        
        case .local(let localFor):
            hitAutoSuggestApi(searchType: localFor, completion: completion, success: success, failure: failure)
    
        case .google(let googlePlaceFor):
            fetchPlaces(googlePlaceFor, completion: completion, success: success, failure: failure)
            break
        }
    }
    
    private func hitAutoSuggestApi(searchType: LocalFor, completion: ((_ message: String?)->())? = nil, success:  ((_ message: String?)->())? = nil, failure: ((_ message: String?)->())? = nil) {
        
        let autoSuggestServices = AutoSuggestServices(requestTag: "AUTO_SUGGEST", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        //Completion Block
        autoSuggestServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            if statusCode == 1, let content = (response.0 as? AutoSuggestResponse)?.content {
                self.options = content
                completion?(message)
                success?(message)
            }
            else {
                completion?(message)
                failure?(message)
            }
        }
        autoSuggestServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            completion?(nil)
            failure?(nil)
        }
        
        var parameters: [String: Any]?
        guard let textTyped = textTyped else {
            completion?(nil)
            failure?(nil)
            return
        }
        
        switch searchType {
        case .skill:
            parameters = ["skillName": textTyped]
            
        case .industry:
            parameters = ["industryName": textTyped]
            
        case .employerTitle, .jobTitle:
            parameters = ["term": textTyped]
        }
        
        autoSuggestServices.getAutoSuggestList(relativeUrl: searchType.rawValue, parameters: parameters, additionalHeaderElements: nil)
    }
    
    
    //MARK:- Google Place Api Methods
    var placeDetail: GooglePlaceDetail?

    var googleAutoCompleteServices: GoogleAutoCompleteServices?
    private func fetchPlaces(_ placeFor: GooglePlaceFor, completion: ((_ message: String?)->())? = nil, success:  ((_ message: String?)->())? = nil, failure: ((_ message: String?)->())? = nil) {
        
        googleAutoCompleteServices = GoogleAutoCompleteServices(success: { (response) in
            
            if let _response = response as? GooglePlacesResponse, let places = _response.places {
                self.options = places
                success?(nil)
            }
            else {
                failure?(nil)
            }
        }, failure: { (errorMessage) in
            failure?(errorMessage)
        })
    
        googleAutoCompleteServices?.hitFetchPlaces(string: textTyped ?? "", placeFor: placeFor)
    }
    
    func fetchPlaceDetail(placeId: String, title: String?, completion: ((_ message: String?)->())? = nil, success:  ((_ message: String?)->())? = nil, failure: ((_ message: String?)->())? = nil) {
        
        googleAutoCompleteServices = GoogleAutoCompleteServices(success: { (response) in
            
            if let placeDetail = response as? GooglePlaceDetail {
                self.placeDetail = placeDetail
                success?(nil)
            }
            else {
                failure?(nil)
            }
            completion?(nil)
        }, failure: { (errorMessage) in
            failure?(errorMessage)
            completion?(nil)
        })
        
        googleAutoCompleteServices?.hitFetchPlaceDetail(placeId, title: title, searchType: searchType)
    }
    
    func hitAddLocationApi(placeDetail: GooglePlaceDetail, googlePlaceFor: GooglePlaceFor, completion: ((_ message: String?)->())? = nil, success:  ((_ id: String, _ message: String?)->())? = nil, failure: ((_ message: String?)->())? = nil) {
        
        let profileServices = ProfileServices(requestTag: "ADD_LOCATION_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        //Completion Block
        profileServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            if statusCode == 1, let encryptedId = response.0 as? String {
                completion?(message)
                success?(encryptedId, message)
            }
            else {
                completion?(message)
                failure?(message)
            }
        }
        profileServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            completion?(nil)
            failure?(nil)
        }
        
        var latLong = String()
        if let lat = placeDetail.lat, let long = placeDetail.long {
            latLong = String(describing: lat) + "," + String(describing: long)
        }
        
        let parameters: [String: String] = [
            "addressLine1": placeDetail.street_number ?? "",
            "addressLine2": placeDetail.route ?? "",
            "googlePlaceId": placeDetail.place_id ?? "",
            "zipcode": placeDetail.postal_code ?? "",
            "city": placeDetail.locality ?? "",
            "state": placeDetail.administrative_area_level_1 ?? "",
            "country": placeDetail.country ?? "",
            "latLong": latLong,
            "fullAddress": placeDetail.dislayAddress ?? ""
        ]
        
        let methodName = (googlePlaceFor == .address) ? "/profile/save-address" : "/location/add-candidate-preferred-location"
        profileServices.hitAddLocationService(parameters: parameters, method: methodName, additionalHeaderElements: nil)
    }
    
    func hitRemoveLocationApi(encryptedId: String, googlePlaceFor: GooglePlaceFor, completion: ((_ message: String?)->())? = nil, success:  ((_ message: String?)->())? = nil, failure: ((_ message: String?)->())? = nil) {
        
        let profileServices = ProfileServices(requestTag: "REMOVE_LOCATION_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        //Completion Block
        profileServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            if statusCode == 1 {
                completion?(message)
                success?(message)
            }
            else {
                completion?(message)
                failure?(message)
            }
        }
        profileServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            completion?(nil)
            failure?(nil)
        }
        
        var parameters: [String: String]?
        if (googlePlaceFor != .address) {
             parameters = ["preferenceIdEnc": encryptedId]
        }
       
        let methodName = (googlePlaceFor == .address) ? "/profile/save-address" : "/location/remove-candidate-preferred-location"
        profileServices.hitRemoveLocationService(parameters: parameters, method: methodName, additionalHeaderElements: nil)
    }
    
    func hitRemoveIndustrySkillApi(encryptedId: String, completion: ((_ message: String?)->())? = nil, success:  ((_ message: String?)->())? = nil, failure: ((_ message: String?)->())? = nil) {
        
        let profileServices = ProfileServices(requestTag: "REMOVE_INDUSTRY_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        //Completion Block
        profileServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            if statusCode == 1 {
                completion?(message)
                success?(message)
            }
            else {
                completion?(message)
                failure?(message)
            }
        }
        profileServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            completion?(nil)
            failure?(nil)
        }
        
        let parameters: [String: String] = ["preferenceIdEnc": encryptedId]
        let methodName = "/candidate/remove-preference"
        
        profileServices.hitRemoveLocationService(parameters: parameters, method: methodName, additionalHeaderElements: nil)
    }
    
    func hitAddIndustryApi(encIndustryId: String, completion: ((_ message: String?)->())? = nil, success:  ((_ id: String, _ message: String?)->())? = nil, failure: ((_ message: String?)->())? = nil) {
        
        let profileServices = ProfileServices(requestTag: "ADD_INDUSTRY_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        //Completion Block
        profileServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            if statusCode == 1, let encryptedId = response.0 as? String {
                completion?(message)
                success?(encryptedId, message)
            }
            else {
                completion?(message)
                failure?(message)
            }
        }
        profileServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            completion?(nil)
            failure?(nil)
        }
        
        let methodName = "/candidate/set-industry"
        profileServices.hitAddLocationService(parameters: ["encIndustryId": encIndustryId], method: methodName, additionalHeaderElements: nil)
    }
    
    func hitAddSkillApi(encSkillId: String, completion: ((_ message: String?)->())? = nil, success:  ((_ id: String, _ message: String?)->())? = nil, failure: ((_ message: String?)->())? = nil) {
        
        let profileServices = ProfileServices(requestTag: "ADD_INDUSTRY_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        //Completion Block
        profileServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            if statusCode == 1, let encryptedId = response.0 as? String {
                completion?(message)
                success?(encryptedId, message)
            }
            else {
                completion?(message)
                failure?(message)
            }
        }
        profileServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            completion?(nil)
            failure?(nil)
        }
        
        let methodName = "/candidate/set-skill"
        profileServices.hitAddLocationService(parameters: ["encSkillId": encSkillId], method: methodName, additionalHeaderElements: nil)
    }
    
    func hitSetPrimarySkillApi(encSkillId: String, isPrimary: Bool, completion: ((_ message: String?)->())? = nil, success:  ((_ id: String, _ message: String?)->())? = nil, failure: ((_ message: String?)->())? = nil) {
        
        let profileServices = ProfileServices(requestTag: "SET_PRIMARY_SKILL", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        //Completion Block
        profileServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            if statusCode == 1, let encryptedId = response.0 as? String {
                completion?(message)
                success?(encryptedId, message)
            }
            else {
                completion?(message)
                failure?(message)
            }
        }
        profileServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            completion?(nil)
            failure?(nil)
        }
        
        let methodName = "/candidate/set-primary-skill"
        profileServices.hitAddLocationService(parameters: ["encSkillId": encSkillId, "isPrimary": isPrimary], method: methodName, additionalHeaderElements: nil)
    }
}


