//
//  AutoCompleteViewController.swift
//  Jombone
//
//  Created by Money Mahesh on 20/09/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import UIKit

enum GooglePlaceFor {
    case city
    case address
    case preferredJobLocation
}

enum LocalFor: String {
    case industry = "/suggest/suggest-industries"
    case skill = "/suggest/suggest-skills"
    case jobTitle = "/suggest/job-title"
    case employerTitle = "/suggest/employer"
}

enum AutoComplete {
    case google(GooglePlaceFor: GooglePlaceFor)
    case local(LocalFor: LocalFor)
}

enum NoOfSelection {
    case single
    case multiple
}

protocol AutoCompleteViewDelegate: NSObjectProtocol {
    func selectedOption(type: AutoComplete, optionSelected: Any?)
    func selectedOption(type: AutoComplete, placeDetail: GooglePlaceDetail?)
}

extension AutoCompleteViewDelegate {
    func selectedOption(type: AutoComplete, optionSelected: Any?) {}
    func selectedOption(type: AutoComplete, placeDetail: GooglePlaceDetail?) {}
}

class AutoCompleteViewController: WhiteStatusBarViewController {

    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var lblForErrorMsg: UILabel!
    @IBOutlet weak var btnForDone: UIButton!
    @IBOutlet weak var btnForCancel: UIButton!

    weak var delegate: AutoCompleteViewDelegate?
    var viewModel: AutoCompleteViewModel!
    var tableWidget: GenericTableViewComponent<AutoCompleteCardViewModelProtocol, AutoCompleteCardView>?
    
    static func show(viewModel: AutoCompleteViewModel, parentController: UIViewController? = nil, delegate: AutoCompleteViewDelegate?) {
    
        let viewController = MAIN_STORYBOARD.instantiateViewController(withIdentifier: "AutoCompleteViewController") as! AutoCompleteViewController
        viewController.viewModel = viewModel
        viewController.delegate = delegate
        viewController.modalPresentationStyle = .overCurrentContext

        (parentController ?? APPLICATION_INSTANCE.visibleViewController())?.present(viewController, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if viewModel.noOfSelection == .single {
            btnForDone.setTitleForAllState(title: "Cancel")
        }
        else {
            btnForDone.setTitleForAllState(title: "Done")
        }
//        btnForDone.isHidden = (viewModel.noOfSelection == .single)
//        btnForCancel.isHidden = !(viewModel.noOfSelection == .single)
        
        if let textField = searchBar.value(forKey: "_searchField") as? UITextField {
            textField.backgroundColor = UIColor(hexFromString: "EEEEEE", alpha: 0.75)
        }
        setUpTable()
        addShadow()
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .milliseconds(500)) {
            self.searchBar.becomeFirstResponder()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addShadow() {
        topView.clipsToBounds = false
        topView.layer.shadowColor = UIColor(red: 224.0/255.0, green: 224.0/255.0, blue: 224.0/255.0, alpha: 0.75).cgColor
        topView.layer.shadowOpacity = 0.25
        topView.layer.shadowOffset = CGSize(width: 0, height: 20)
    }
    
    func setUpTable() {
        
        tableWidget = GenericTableViewComponent()
        tableWidget?.isHidden = false
        tableWidget?.delegate = self
        
        var genericTableViewCellOtherDetail: GenericTableViewCellOtherDetail?
        switch viewModel.searchType! {
        case .google( _):
            
            
            let style: (style: AutoCompleteCardViewModel.ViewStyle, action: ((_ sender: Any, _ index: IndexPath, _ obj: AutoCompleteCardViewModelProtocol)->())?) = (style: AutoCompleteCardViewModel.ViewStyle.withSelectionStatusLocation, action: nil)
            
            genericTableViewCellOtherDetail = GenericTableViewCellOtherDetail(generic: style, unique: nil)

        default:
            break
        }
        

        tableWidget?.genericTableViewModel = GenericTableViewModel(
            cellDataModel: viewModel.options,
            cellOtherDetail: genericTableViewCellOtherDetail,
            tableType: .expandable)
        
        tableWidget?.tableStyle(backGroundColor: UIColor.clear, insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
        stackView.addArrangedSubview(tableWidget!)
    }
    
    func selectionCompleted() {
        
        switch viewModel.searchType! {
        case .google( _):
            
            let sortedOrdered = viewModel.selectedOptions.sorted { (firstObj, secondObj) -> Bool in
                return firstObj.title < secondObj.title
            }
            
            self.delegate?.selectedOption(type: viewModel.searchType!, optionSelected: sortedOrdered)
            self.delegate?.selectedOption(type: viewModel.searchType!, placeDetail: viewModel.placeDetail)
            self.dismissController()


        default:
            let sortedOrdered = viewModel.selectedOptions.sorted { (firstObj, secondObj) -> Bool in
                return firstObj.title < secondObj.title
            }
            delegate?.selectedOption(type: viewModel.searchType, optionSelected: sortedOrdered)
            dismissController()
        }
    }
    
    func dismissController() {
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    func add(dataSource: [AutoCompleteCardViewModelProtocol], indexPath: IndexPath) {
        
        switch viewModel.searchType! {
        case .google(let googlePlaceFor):
            
            self.showLoader()
            if let placeId = (dataSource as? [GooglePlace])?[indexPath.row].placeId, let title = (dataSource as? [GooglePlace])?[indexPath.row].title {
                addLocation(placeId: placeId, title: title, googlePlaceFor: googlePlaceFor) { ( status, id, preferredLocationTitle) in
                    self.hideLoader()
                    if status {
                        var updatedObject = self.viewModel.options[indexPath.row]
                        updatedObject.encryptedId = id
                        updatedObject.selected = true
                        if let _preferredLocationTitle = preferredLocationTitle {
                            updatedObject.title = _preferredLocationTitle
                        }
                        self.viewModel.options[indexPath.row] = updatedObject
                        self.viewModel.selectedOptions.append(updatedObject)
                        
                        var _dataSource = dataSource
                        _dataSource[indexPath.row].encryptedId = id
                        self.tableWidget?.updateTableRecord(objArray: _dataSource)
                        self.dataAddedSuccesfully()
                    }
                    else {
                        var _dataSource = dataSource
                        _dataSource[indexPath.row].selected = false
                        self.tableWidget?.updateTableRecord(objArray: _dataSource)
                    }
                }
            }
            
        case .local(let localFor) where (localFor == .industry && !viewModel.isOffline):
            
            self.showLoader()
            if let encryptedId = dataSource[indexPath.row].encryptedId {
                
                self.viewModel.hitAddIndustryApi(encIndustryId: encryptedId, completion: { (message) in
                    self.hideLoader()
                }, success: { (id, message) in
                    
                    var updatedObject = self.viewModel.options[indexPath.row]
                    updatedObject.encryptedId = id
                    updatedObject.selected = true
                    self.viewModel.options[indexPath.row] = updatedObject
                    self.viewModel.selectedOptions.append(updatedObject)
                    
                    var _dataSource = dataSource
                    _dataSource[indexPath.row].encryptedId = id
                    self.tableWidget?.updateTableRecord(objArray: _dataSource)
                    self.dataAddedSuccesfully()
                    
                }, failure: { (message) in
                    var _dataSource = dataSource
                    _dataSource[indexPath.row].selected = false
                    self.tableWidget?.updateTableRecord(objArray: _dataSource)
                })
            }
            
        case .local(let localFor) where (localFor == .skill && !viewModel.isOffline):
            
            self.showLoader()
            if let encryptedId = dataSource[indexPath.row].encryptedId {
                
                self.viewModel.hitAddSkillApi(encSkillId: encryptedId, completion: { (message) in
                    self.hideLoader()
                }, success: { (id, message) in
                    
                    var updatedObject = self.viewModel.options[indexPath.row]
                    updatedObject.encryptedId = id
                    updatedObject.selected = true
                    self.viewModel.options[indexPath.row] = updatedObject
                    self.viewModel.selectedOptions.append(updatedObject)
                    
                    var _dataSource = dataSource
                    _dataSource[indexPath.row].encryptedId = id
                    self.tableWidget?.updateTableRecord(objArray: _dataSource)
                    self.dataAddedSuccesfully()
                    
                }, failure: { (message) in
                    var _dataSource = dataSource
                    _dataSource[indexPath.row].selected = false
                    self.tableWidget?.updateTableRecord(objArray: _dataSource)
                })
            }
            
        default:
            viewModel.selectedOptions.append(viewModel.options[indexPath.row])
            self.dataAddedSuccesfully()

            break
        }
    }
    
    func remove(dataSource: [AutoCompleteCardViewModelProtocol], indexPath: IndexPath) {
        
        switch viewModel.searchType! {
        case .google(let googlePlaceFor):
            
            if self.viewModel.isOffline {
                self.viewModel.selectedOptions = self.viewModel.selectedOptions.filter({$0.title != self.viewModel.options[indexPath.row].title})
            }
            else {
                if let encryptedId = (dataSource as? [GooglePlace])?[indexPath.row].encryptedId {
                    removeLocation(encryptedId: encryptedId, googlePlaceFor: googlePlaceFor) { (status) in
                        if status {
                            self.viewModel.selectedOptions = self.viewModel.selectedOptions.filter({$0.title != self.viewModel.options[indexPath.row].title})
                        }
                        else {
                            var _dataSource = dataSource
                            _dataSource[indexPath.row].selected = true
                            self.tableWidget?.updateTableRecord(objArray: _dataSource)
                        }
                    }
                }
            }
            
        case .local( _):
            viewModel.selectedOptions = viewModel.selectedOptions.filter({$0.title != viewModel.options[indexPath.row].title})
            
            break
        }
    }
    
    func addLocation(placeId: String, title: String, googlePlaceFor: GooglePlaceFor, completion: @escaping ((_ status: Bool, _ id: String?, _ preferredLocationTitle: String?)->())) {
        
        viewModel.fetchPlaceDetail(placeId: placeId, title: title, completion: { (message) in
            if let placeDetail = self.viewModel.placeDetail {
                
                if self.viewModel.isOffline {
                    completion(true, nil, nil)
                    return
                }
                else {
                    self.viewModel.hitAddLocationApi(placeDetail: placeDetail, googlePlaceFor: googlePlaceFor, completion: { (message) in
                    }, success: { (id, message) in
                        
                        if googlePlaceFor == .preferredJobLocation {
                            completion(true, id, placeDetail.dislayAddress)
                        }
                        else {
                            completion(true, id, nil)
                        }
                    }, failure: { (message) in
                        APPLICATION_INSTANCE.visibleViewController()?.showAlert(message: message)
                        completion(false, nil, nil)
                    })
                    return
                }
            }
        })
    }
    
    func dataAddedSuccesfully() {
        if viewModel.noOfSelection == .single {
            //Remove previously selected option
            if viewModel.selectedOptions.count > 1 {
                viewModel.selectedOptions.removeFirst()
            }
        }
        selectionCompleted()
    }
    
    func removeLocation(encryptedId: String, googlePlaceFor: GooglePlaceFor, completion: @escaping ((_ status: Bool)->())) {
        
        self.showLoader()
        self.viewModel.hitRemoveLocationApi(encryptedId: encryptedId, googlePlaceFor: googlePlaceFor, completion: { (message) in
            self.hideLoader()
        }, success: { (message) in
            completion(true)
        }, failure: { (message) in
            completion(false)
        })
    }
    
    //MARK:- IBAction
    @IBAction func btnActionForCancel(_ sender: UIButton) {
        dismissController()
    }
    
    @IBAction func btnActionForDone(_ sender: UIButton) {
        
        if sender.titleLabel?.text == "Cancel" {
            dismissController()
        }
        else {
            selectionCompleted()
        }
    }
}

extension AutoCompleteViewController: GenericTableViewComponentDelegate {
    
    func genericTableDidSelectRowAt(_ indexPath: IndexPath, dataSource: inout [Any]) -> Bool {

        var updatedObjs = (dataSource as! [AutoCompleteCardViewModelProtocol])
        let currentState = updatedObjs[indexPath.row].selected!
        updatedObjs[indexPath.row].selected = !currentState
        dataSource = updatedObjs
        
        if !currentState {
            self.add(dataSource: dataSource as! [AutoCompleteCardViewModelProtocol], indexPath: indexPath)
        }
        else {
            self.remove(dataSource: dataSource as! [AutoCompleteCardViewModelProtocol], indexPath: indexPath)
        }
        return true
    }
}

extension AutoCompleteViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        viewModel.textTyped = searchText
        
        self.viewModel.options.removeAll()
        self.tableWidget?.updateTableRecord(objArray: self.viewModel.options)

        viewModel.getList(success: { (message) in
            self.tableWidget?.updateTableRecord(objArray: self.viewModel.options)
            self.lblForErrorMsg.isHidden = (self.viewModel.options.count != 0)
        },failure: { (message) in
            self.lblForErrorMsg.isHidden = false
        })
    }
}
