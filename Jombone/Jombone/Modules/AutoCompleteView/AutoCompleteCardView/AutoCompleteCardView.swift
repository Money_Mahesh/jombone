//
//  AutoCompleteCardView.swift
//  Jombone
//
//  Created by Money Mahesh on 20/09/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import Foundation
import UIKit

extension Notification.Name {
    static let AutoCompleteCardViewCrossPressed = Notification.Name("AutoCompleteCardViewCrossPressed")
}

class AutoCompleteCardView: GenericView<AutoCompleteCardViewModelProtocol> {
    
    @IBOutlet weak var lblForTitle: UILabel!
    @IBOutlet weak var imgForLocIcon: UIImageView!
    @IBOutlet weak var btnForCross: UIButton!
    @IBOutlet weak var cardBackgroundView: UIView!
    @IBOutlet weak var bottomLineView: UIView!
    
    @IBOutlet weak var checkboxBtn: UIButton!
    @IBOutlet weak var constForLeading: NSLayoutConstraint!
    @IBOutlet weak var constForTrailing: NSLayoutConstraint!
    @IBOutlet weak var constForTop: NSLayoutConstraint!
    @IBOutlet weak var constForBottom: NSLayoutConstraint!
    @IBOutlet weak var constForheight: NSLayoutConstraint!

    var btnAction: ((_ sender: Any, _ index: IndexPath, _ obj: AutoCompleteCardViewModelProtocol)->())?
    
    override var viewModel: ViewModel<AutoCompleteCardViewModelProtocol>? {
        didSet {
            
            lblForTitle.text = viewModel?.data?.title
            imgForLocIcon.isHidden = true
            checkboxBtn.isHidden = true
            
            let otherDetail = viewModel?.otherDetail as? (style: AutoCompleteCardViewModel.ViewStyle, action: ((_ sender: Any, _ index: IndexPath, _ obj: AutoCompleteCardViewModelProtocol)->())?)
            let style = otherDetail?.style ?? AutoCompleteCardViewModel.ViewStyle.withSelectionStatus
            
            switch style {
            case .withCrossWhiteTitle:
                lblForTitle.textColor = UIColor.white
                lblForTitle.textColor = UIColor.white
                btnAction = otherDetail?.action
                self.cardBackgroundView.backgroundColor = UIColor.clear
                self.bottomLineView.isHidden = true
              
            case .withCrossGreyTitle:
                lblForTitle.textColor = UIColor(red: 97.0/255.0, green: 97.0/255.0, blue: 97.0/255.0, alpha: 1.0)
                btnAction = otherDetail?.action
                self.cardBackgroundView.backgroundColor = UIColor.clear
                self.bottomLineView.isHidden = true
                self.btnForCross.setImageForAllState(image: UIImage(named: "crossBtnTextfieldIcon"))
                self.constForheight.constant = 30
                self.constForLeading.constant = 0
                
            case .withSelectionStatus:

                lblForTitle.textColor = (viewModel?.data?.selected ?? false) ? UIColor(red: 66/255, green: 66/255, blue: 66/255, alpha: 1.0) : UIColor(hexFromString: "#424242")
                btnForCross.isHidden = true
                
            case .withSelectionStatusLocation:
                imgForLocIcon.isHidden = false
                lblForTitle.textColor = (viewModel?.data?.selected ?? false) ? UIColor(red: 66/255, green: 66/255, blue: 66/255, alpha: 1.0) : UIColor(hexFromString: "#424242")
                btnForCross.isHidden = true
                
            case .withSelectionStatusLocationClearBg:
                imgForLocIcon.isHidden = false
                lblForTitle.textColor = (viewModel?.data?.selected ?? false) ? UIColor(red: 66/255, green: 66/255, blue: 66/255, alpha: 1.0) : UIColor(hexFromString: "#424242")
                btnForCross.isHidden = true
                self.cardBackgroundView.backgroundColor = UIColor.clear
                
                
                
            case .withCheckBoxCrossWhiteTitle:
                self.constForheight.constant = 28
                self.bottomLineView.isHidden = true
                checkboxBtn.isHidden = false
                lblForTitle.textColor = (viewModel?.data?.selected ?? false) ? UIColor(red: 66/255, green: 66/255, blue: 66/255, alpha: 1.0) : UIColor(hexFromString: "#424242")
                btnAction = otherDetail?.action
                checkboxBtn.isSelected = viewModel?.data?.isPrimary ?? false
            }
        }
    }
    
    
    @IBAction func btnActionForCross(_ sender: Any) {
        viewModel?.data?.selected = false
        
        if let data = viewModel?.data {
            btnAction?(sender, index, data)
        }
    }
    
    
    @IBAction func btnActionForCheckBox(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        if let data = viewModel?.data {
            btnAction?(sender, index, data)
        }
    }
}
