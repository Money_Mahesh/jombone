//
//  AutoCompleteCardViewModel.swift
//  Jombone
//
//  Created by Money Mahesh on 20/09/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import Foundation


protocol AutoCompleteCardViewModelProtocol {
    
    var encryptedId: String? {get set}
    var title: String! {get set}
    var selected: Bool! {get set}
    var isPrimary: Bool? {get set}
}

class AutoCompleteCardViewModel: ViewModel<AutoCompleteCardViewModelProtocol> {
    
    enum ViewStyle {
        case withCheckBoxCrossWhiteTitle
        case withCrossWhiteTitle
        case withCrossGreyTitle
        case withSelectionStatus
        case withSelectionStatusLocation
        case withSelectionStatusLocationClearBg
        
    }
    
    var action: (()->())?
}
