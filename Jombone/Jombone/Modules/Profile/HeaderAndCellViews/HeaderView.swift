//
//  HeaderView.swift
//  Jombone
//
//  Created by dev139 on 10/10/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import UIKit

enum ButtonType {
    
    case showBoth
    case showOnlyAdd
    case showOnlyEdit
    case none
}

class HeaderView: UITableViewHeaderFooterView {

    @IBOutlet weak var lblForTitle: UILabel!
    //TO DO : Change the cross image to add image in XIB
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
    }
    
    func setStyle(withButtonType buttonType: ButtonType) {
        
        switch buttonType {
        case .showBoth:
            break
        case .showOnlyAdd:
            self.editButton.isHidden = true
            self.addButton.isHidden = false
            
        case .showOnlyEdit:
            self.editButton.isHidden = false
            self.addButton.isHidden = true
            
        case .none:
            self.editButton.isHidden = true
            self.addButton.isHidden = true
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}
