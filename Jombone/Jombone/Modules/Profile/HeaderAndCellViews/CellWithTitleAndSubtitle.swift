//
//  CellWithTitleAndSubtitle.swift
//  Jombone
//
//  Created by dev139 on 10/10/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import UIKit

class CellWithTitleAndSubtitle: UITableViewCell {

    @IBOutlet weak var lblForTitle: UILabel!
    @IBOutlet weak var lblForDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
