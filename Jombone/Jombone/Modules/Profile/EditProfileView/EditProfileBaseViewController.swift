//
//  EditProfileBaseViewController.swift
//  Jombone
//
//  Created by Money Mahesh on 17/11/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import UIKit

class EditProfileBaseViewController: UIViewController {

    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var lblForTitle: UILabel!
    @IBOutlet weak var btnForSave: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func setTitle() {
    }
    
    //MARK:- IBActions
    @IBAction func btnActionForCross(_ sender: UIButton) {
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnActionForSave(_ sender: UIButton) {
        self.view.endEditing(true)
    }
}
