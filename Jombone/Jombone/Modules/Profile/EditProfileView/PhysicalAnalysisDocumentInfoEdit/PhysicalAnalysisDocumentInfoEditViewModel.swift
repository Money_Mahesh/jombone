//
//  PhysicalAnalysisDocumentInfoEditViewModel.swift
//  Jombone
//
//  Created by Money Mahesh on 23/04/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import UIKit

class PhysicalAnalysisDocumentInfoEditViewModel: NSObject {
    
    var physicalAnalysisDocumentBean: PhysicalAnalysisDocumentBean?
    var fileData: FileDetail?

    override init() {
        self.physicalAnalysisDocumentBean = UserDataManager.shared.profileDetail?.physicalAnalysisDocumentBean
    }
    
    private var isInputValid = true
    @discardableResult func isValid(error: String?) -> String? {
        if !isInputValid {
            return error
        }
        isInputValid = (error == nil)
        return error
    }
    
    func reset() {
        isInputValid = true
    }

    func hitUpdatePhysicalAnalysisDetail(completion: ((_ message: String?)->())? = nil, success:  ((_ message: String?)->())? = nil, failure: ((_ message: String?)->())? = nil) {
        
        guard let _fileData = fileData, isInputValid else {
            completion?(nil)
            failure?(nil)
            return
        }
        
        let profileServices = ProfileServices(requestTag: "UPDATE_PROFILE_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        //Completion Block
        profileServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            if statusCode == 1, let physicalAnalysisDocumentBean = response.0 as? PhysicalAnalysisDocumentBean {
                let profileDetail = UserDataManager.shared.profileDetail
                profileDetail?.physicalAnalysisDocumentBean = physicalAnalysisDocumentBean
                UserDataManager.shared.profileDetail = profileDetail
                completion?(message)
                success?(message)
            }
            else {
                completion?(message)
                failure?(message)
            }
        }
        profileServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            completion?(nil)
            failure?(nil)
        }
        
        let filesDetail: (keysName: [String], filesName: [String], filesData: [Data]) = (keysName: ["physicalAnalysisDoc"], filesName: [_fileData.name], filesData: [_fileData.data])
        
        profileServices.uploadPhysicalDocumentAnalysis(filesDetail: filesDetail, parameters: nil, additionalHeaderElements: nil)
    }
}
