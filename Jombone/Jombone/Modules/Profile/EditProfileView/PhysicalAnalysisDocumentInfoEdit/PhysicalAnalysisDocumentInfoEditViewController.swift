//
//  PhysicalAnalysisDocumentInfoEditViewController.swift
//  Jombone
//
//  Created by Money Mahesh on 23/04/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import UIKit

class PhysicalAnalysisDocumentInfoEditViewController: EditProfileBaseViewController {
    
    @IBOutlet weak var lblForDocumentError: UILabel!
    @IBOutlet weak var lblForDocFileName: UILabel!

    var viewModel = PhysicalAnalysisDocumentInfoEditViewModel()
    var downloadDelegate = DownloadUploadAction()
    
    class func show() {
        
        let viewController = PROFILE_STORYBOARD.instantiateViewController(withIdentifier: "PhysicalAnalysisDocumentInfoEditViewController") as! PhysicalAnalysisDocumentInfoEditViewController
        
        APPLICATION_INSTANCE.visibleViewController()?.present(viewController, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        populateView()
    }
    
    private func populateView() {
        self.lblForDocFileName.text = viewModel.physicalAnalysisDocumentBean?.viewDocumentFileDisplayName ?? "Add Document"
    }
    
    override func btnActionForSave(_ sender: UIButton) {
        super.btnActionForSave(sender)
        
        viewModel.reset()
        
        if viewModel.physicalAnalysisDocumentBean?.viewDocumentFileDisplayName == nil && viewModel.fileData == nil  {
            lblForDocumentError.isHidden = false
            lblForDocumentError.text = ERROR_REQUIRED_FIELD
            viewModel.isValid(error: lblForDocumentError.text)
        }
        
        showLoader()
        viewModel.hitUpdatePhysicalAnalysisDetail(completion: { (message) in
            self.hideLoader()
        }, success: { (message) in
            self.dismiss(animated: true, completion: {
                APPLICATION_INSTANCE.visibleViewController()?.showAlert(message: message)
            })
        }, failure: { (message) in
            self.showAlert(message: message, withOk: true)
        })
    }
    
    @IBAction func btnActionForUpload(_ sender: UIButton) {
        
        lblForDocumentError.isHidden = true
        downloadDelegate.fetchFile { (fileDetail) in

            if let errorMessage = DataValidationUtility.shared.validateSize(5000000, fileDetail: fileDetail, fileTypeAllowed: [
                FileType.pdf,
                FileType.doc,
                FileType.docx,
                FileType.jpg,
                FileType.jpeg,
                FileType.png
                ]).message {
                self.lblForDocumentError.isHidden = false
                self.lblForDocumentError.text = errorMessage
                self.viewModel.physicalAnalysisDocumentBean?.viewDocumentFileDisplayName = nil
                self.lblForDocFileName.text = "Add Document"
                //                self.lblForDocFileName.text = self.viewModel.physicalAnalysisDocumentBean?.viewDocumentFileDisplayName ?? "Add Document"
            }
            else {
                self.lblForDocumentError.text = nil
                self.viewModel.fileData = fileDetail
                self.lblForDocFileName.text = fileDetail.name
            }
        }
    }
}
