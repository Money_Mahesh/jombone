//
//  EducationInfoEditViewModel.swift
//  Jombone
//
//  Created by Money Mahesh on 23/04/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import UIKit

class EducationInfoEditViewModel: NSObject {
    
    var educationDetailsBean: EducationDetailsBean?
    var education: [MasterData]?
    var fileData: FileDetail?

    init(educationDetailsBean: EducationDetailsBean = EducationDetailsBean()) {
        self.educationDetailsBean = educationDetailsBean
    }
    
    private var isInputValid = true
    @discardableResult func isValid(error: String?) -> String? {
        if !isInputValid {
            return error
        }
        isInputValid = (error == nil)
        return error
    }
    
    func reset() {
        isInputValid = true
    }
    
    func hitUpdateEducationDetail(completion: ((_ message: String?)->())? = nil, success:  ((_ message: String?)->())? = nil, failure: ((_ message: String?)->())? = nil) {
        
        guard let parameters = JsonUtility.jsonString(data: educationDetailsBean), isInputValid else {
            completion?(nil)
            failure?(nil)
            return
        }
        
        let profileServices = ProfileServices(requestTag: "UPDATE_PROFILE_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        //Completion Block
        profileServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            if statusCode == 1, let educationDetailsBean = response.0 as? [EducationDetailsBean] {
                let profileDetail = UserDataManager.shared.profileDetail
                profileDetail?.educationBeans = educationDetailsBean
                UserDataManager.shared.profileDetail = profileDetail
                completion?(message)
                success?(message)
            }
            else {
                completion?(message)
                failure?(message)
            }
        }
        profileServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            completion?(nil)
            failure?(nil)
        }
        
        var filesDetail: (keysName: [String], filesName: [String], filesData: [Data])?
        if let _fileData = fileData {
            filesDetail = (keysName: ["educationDoc"], filesName: [_fileData.name], filesData: [_fileData.data])
        }
        
        profileServices.updateEducationDetail(filesDetail: filesDetail, parameters: ["educationBeanString": parameters], additionalHeaderElements: nil)
    }
}
