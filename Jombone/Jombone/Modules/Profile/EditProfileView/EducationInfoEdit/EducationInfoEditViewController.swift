//
//  EducationInfoEditViewController.swift
//  Jombone
//
//  Created by Money Mahesh on 23/04/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import UIKit

class EducationInfoEditViewController: EditProfileBaseViewController {
    
    @IBOutlet weak var textfieldForEducationLevel: CustomTextfieldView!
    @IBOutlet weak var textfieldForYearCompleted: CustomTextfieldView!
    @IBOutlet weak var textfieldForInstituteName: CustomTextfieldView!
    @IBOutlet weak var lblForDocumentError: UILabel!
    @IBOutlet weak var lblForDocFileName: UILabel!

    var viewModel = EducationInfoEditViewModel()
    var downloadDelegate = DownloadUploadAction()
    
    class func show(viewModel: EducationInfoEditViewModel) {
        
        let viewController = PROFILE_STORYBOARD.instantiateViewController(withIdentifier: "EducationInfoEditViewController") as! EducationInfoEditViewController
        viewController.viewModel = viewModel
        
        APPLICATION_INSTANCE.visibleViewController()?.present(viewController, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setTextfieldPreferences()
        self.populateView()
    }
    
    private func setTextfieldPreferences() {
        
        //Set Textfield Preferences
        textfieldForEducationLevel.borderStyle = .All
        textfieldForEducationLevel.setJomboneFontStyle()
        let currentQualificationName = self.viewModel.educationDetailsBean?.qualificationName
        self.textfieldForEducationLevel?.pickerDataSource1(
            identifier: "EducationLevel",
            array: [[String]](),
            currentValues: (currentQualificationName == nil ? nil : [currentQualificationName!]),
            delegate: self)
        setHighestEducationDataSource()

        textfieldForYearCompleted.borderStyle = .All
        textfieldForYearCompleted.setJomboneFontStyle()
        let currentYear = Date().year
        var yearDataSource = Array((currentYear - 50)...currentYear).map { (obj) -> String in
            return String(obj)
        }
        yearDataSource.reverse()
        if self.viewModel.educationDetailsBean?.yearCompleted == nil {
            self.viewModel.educationDetailsBean?.yearCompleted = yearDataSource.first
        }
        let currentYearCompleted = self.viewModel.educationDetailsBean?.yearCompleted

        self.textfieldForYearCompleted?.pickerDataSource1(
            identifier: "YearCompleted",
            array: [yearDataSource],
            currentValues: (currentYearCompleted == nil ? nil : [currentYearCompleted!]),
            delegate: self)
        
        
        textfieldForInstituteName.borderStyle = .All
        textfieldForInstituteName.setJomboneFontStyle()
        textfieldForInstituteName.textfieldViewDelegate = self
    }
    
    private func setHighestEducationDataSource(completion: (()->())? = nil) {
        
        showLoader()
        MasterDataManager.shared.education { (data: [MasterData]?) in
            
            self.hideLoader()
            self.viewModel.education = data
            
            let dataArray = data?.map({ (obj: MasterData) -> String in
                return obj.displayName ?? ""
            })
            
            let _dataArray = dataArray == nil ? [[String]]() : [dataArray!]
            let currentValue = self.viewModel.educationDetailsBean?.qualificationName
            self.textfieldForEducationLevel?.pickerDataSource1(
                identifier: "EducationLevel",
                array: _dataArray,
                currentValues: (currentValue == nil ? nil : [currentValue!]),
                defaultValueIndex: [0],
                delegate: self)
            
            completion?()
        }
    }
    
    private func populateView() {
        
        textfieldForInstituteName.text = viewModel.educationDetailsBean?.instituteName
        self.lblForDocFileName.text = viewModel.educationDetailsBean?.educationDocDisplayName ?? "Education Document"
    }
    
    override func btnActionForSave(_ sender: UIButton) {
        super.btnActionForSave(sender)
        
        viewModel.reset()
        
        viewModel.educationDetailsBean?.instituteName = textfieldForInstituteName.isValidName("Institution name", customMessage: "Only character and spaces allowed")
        viewModel.isValid(error: textfieldForInstituteName.errorMsg)

//        if viewModel.educationDetailsBean?.educationDocDisplayName == nil && viewModel.fileData == nil  {
//            lblForDocumentError.isHidden = false
//            lblForDocumentError.text = ERROR_REQUIRED_FIELD
//            viewModel.isValid(error: lblForDocumentError.text)
//        }
        
        showLoader()
        viewModel.hitUpdateEducationDetail(completion: { (message) in
            self.hideLoader()
        }, success: { (message) in
            self.dismiss(animated: true, completion: {
                APPLICATION_INSTANCE.visibleViewController()?.showAlert(message: message)
            })
        }, failure: { (message) in
            self.showAlert(message: message, withOk: true)
        })
    }
    
    
    @IBAction func btnActionForUpload(_ sender: UIButton) {
        
        lblForDocumentError.isHidden = true
        downloadDelegate.fetchFile { (fileDetail) in
            
            
            self.viewModel.educationDetailsBean?.educationDocDisplayName = nil
            if let errorMessage = DataValidationUtility.shared.validateSize(5000000, fileDetail: fileDetail, fileTypeAllowed: [
                FileType.pdf,
                FileType.doc,
                FileType.docx,
                FileType.jpg,
                FileType.jpeg,
                FileType.png
                ]).message {
                self.lblForDocumentError.isHidden = false
                self.lblForDocumentError.text = errorMessage
                self.viewModel.educationDetailsBean?.educationDocDisplayName = nil
                self.lblForDocFileName.text = "Education Document"

//                self.lblForDocFileName.text = self.viewModel.educationDetailsBean?.educationDocDisplayName ?? "Education Document"
            }
            else {
                self.lblForDocumentError.text = nil
                self.viewModel.fileData = fileDetail
                self.lblForDocFileName.text = fileDetail.name
                self.viewModel.educationDetailsBean?.educationDocDisplayName = fileDetail.name
            }
        }
    }
}

extension EducationInfoEditViewController: CustomTextfieldViewDelegate {
    
    func textFieldDidBeginEditing(_ identifier: String, textField: CustomTextfield) {
        
        if identifier == "EducationLevel" && viewModel.education == nil {
            self.view.endEditing(true)
            setHighestEducationDataSource(completion: {
                self.textfieldForEducationLevel.setAsFirstResponder()
            })
            print(viewModel.education ?? "Data unavailable")
        }
    }
    
    func textFieldDidEndEditing(_ identifier: String, textField: CustomTextfield) {
        
        if identifier == "InstitutionName" {
            viewModel.educationDetailsBean?.instituteName = textField.text
        }
    }
    
    func pickerViewSelected(identifier: String, textfield: CustomTextfield, indexes: [Int]) {
        
        guard let firstIndex = indexes.first else {
            return
        }

        print("identifier \(identifier) indexes\(firstIndex)")
        if identifier == "EducationLevel" {
            
            viewModel.educationDetailsBean?.encQualificationId = viewModel.education?[firstIndex].encryptedId
            viewModel.educationDetailsBean?.qualificationName = viewModel.education?[firstIndex].name
        }
        else if identifier == "YearCompleted" {
            
            viewModel.educationDetailsBean?.yearCompleted = textfield.text
        }
        self.populateView()
    }
}
