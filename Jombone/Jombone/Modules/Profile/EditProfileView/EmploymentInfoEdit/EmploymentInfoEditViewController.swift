//
//  EmploymentInfoEditViewController.swift
//  Jombone
//
//  Created by Money Mahesh on 23/04/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import UIKit

class EmploymentInfoEditViewController: EditProfileBaseViewController {
    
    @IBOutlet weak var textfieldForEmployerName: CustomTextfieldView!
    @IBOutlet weak var textfieldForEmployerAddress: CustomTextfieldView!
    @IBOutlet weak var textfieldForUnit: CustomTextfieldView!
    @IBOutlet weak var textfieldForPositionHeld: CustomTextfieldView!
    @IBOutlet weak var textfieldForStartDate: CustomTextfieldView!
    @IBOutlet weak var textfieldForEndDate: CustomTextfieldView!
    @IBOutlet weak var textfieldForReferenceName: CustomTextfieldView!
    @IBOutlet weak var textfieldForReferenceEmail: CustomTextfieldView!

    @IBOutlet weak var textfieldForCountryCode: CustomTextfieldView!
    @IBOutlet weak var textfieldForMobileNo: CustomTextfieldView!
    @IBOutlet weak var lblForPhoneNoError: UILabel!

    @IBOutlet weak var btnForCurrentEmployment: UIButton!
    @IBOutlet weak var detailStackView: UIStackView!


    var viewModel = EmploymentInfoEditViewModel()
    
    class func show(viewModel: EmploymentInfoEditViewModel) {
        
        let viewController = PROFILE_STORYBOARD.instantiateViewController(withIdentifier: "EmploymentInfoEditViewController") as! EmploymentInfoEditViewController
        
        viewController.viewModel = viewModel
        APPLICATION_INSTANCE.visibleViewController()?.present(viewController, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setTextfieldPreferences()
        self.populateView()
    }
   
    private func setTextfieldPreferences() {
        
        //Set Textfield Preferences
        textfieldForEmployerName.borderStyle = .All
        textfieldForEmployerName.setJomboneFontStyle()

        textfieldForEmployerAddress.borderStyle = .All
        textfieldForEmployerAddress.setJomboneFontStyle()
        textfieldForEmployerAddress.dataSource(identifier: "EmployerAddress", placeholder: "Employer Address", text: nil, delegate: self)
        
        textfieldForUnit.borderStyle = .All
        textfieldForUnit.setJomboneFontStyle()
        
        textfieldForPositionHeld.borderStyle = .All
        textfieldForPositionHeld.setJomboneFontStyle()
        
        textfieldForStartDate.borderStyle = .All
        textfieldForStartDate.setJomboneFontStyle()
        let startDate = Utility.changeStringToDate(dateStr: viewModel.employmentDetails?.fromDate, inputFormat: "yyyy-MM-dd")
        let endDate = Utility.changeStringToDate(dateStr: viewModel.employmentDetails?.toDate, inputFormat: "yyyy-MM-dd")

        textfieldForStartDate.datePickerDataSource(identifier: "StartDate", currentDate: startDate, minDate: nil, maxDate: endDate, mode: UIDatePicker.Mode.date, delegate: self, pickerSeletedTextFormat: { (data: [Any]?, indexes: [Int], inputMode: CustomTextfieldView.InputType) -> (String) in
            return Utility.changeDateToString(date: data?.first as? Date, withFormat: "yyyy-MM-dd")
        })
        
        textfieldForEndDate.borderStyle = .All
        textfieldForEndDate.setJomboneFontStyle()
        textfieldForEndDate.datePickerDataSource(identifier: "EndDate", currentDate: endDate, minDate: startDate, maxDate: Date(), mode: UIDatePicker.Mode.date, delegate: self, pickerSeletedTextFormat: { (data: [Any]?, indexes: [Int], inputMode: CustomTextfieldView.InputType) -> (String) in
            return Utility.changeDateToString(date: data?.first as? Date, withFormat: "yyyy-MM-dd")
            })

        textfieldForReferenceName.borderStyle = .All
        textfieldForReferenceName.setJomboneFontStyle()
        
        textfieldForReferenceEmail.borderStyle = .All
        textfieldForReferenceEmail.setJomboneEmailStyle()

        let referenceCountryCode = self.viewModel.employmentDetails?.referenceCountryCode ?? CANADA_COUNTRY_CODE
        let referenceCountryIsoCode = self.viewModel.employmentDetails?.referenceCountryIsoCode ?? CANADA_ISO_COUNTRY_CODE
        self.viewModel.employmentDetails?.referenceCountryCode = referenceCountryCode
        self.viewModel.employmentDetails?.referenceCountryIsoCode = referenceCountryIsoCode
        
        textfieldForCountryCode.borderStyle = .none
        textfieldForCountryCode.setJomboneFontStyle()
        textfieldForCountryCode.setJomboneCountryCodeStyle(delegate: self, selectedCode: self.viewModel.employmentDetails?.referenceCountryIsoCode)
        
        textfieldForMobileNo.borderStyle = .none
        textfieldForMobileNo.setJomboneMobileStyle()
        textfieldForMobileNo.textfieldViewDelegate = self
    }
    
    func populateView() {
       
        textfieldForEmployerName.text = viewModel.employmentDetails?.employerName
        textfieldForEmployerAddress.text = viewModel.employmentDetails?.employerAddress
        textfieldForUnit.text = viewModel.employmentDetails?.unitNumber
        textfieldForPositionHeld.text = viewModel.employmentDetails?.designation
        textfieldForStartDate.text = viewModel.employmentDetails?.fromDate
        textfieldForEndDate.text = viewModel.employmentDetails?.toDate
        textfieldForReferenceName.text = viewModel.employmentDetails?.referenceName
        textfieldForReferenceEmail.text = viewModel.employmentDetails?.referenceEmail
        textfieldForMobileNo.text = viewModel.employmentDetails?.referenceMobile
        
        btnForCurrentEmployment.isSelected = viewModel.employmentDetails?.isCurrent ?? false
        if btnForCurrentEmployment.isSelected {
            textfieldForEndDate.isHidden = btnForCurrentEmployment.isSelected
        }
    }
    
    @IBAction func btnActionForIsCurrent(_ sender: UIButton) {
        
        btnForCurrentEmployment.isSelected = !sender.isSelected
        textfieldForEndDate.isHidden = btnForCurrentEmployment.isSelected

        viewModel.employmentDetails?.toDate = nil
        viewModel.employmentDetails?.toDateObj = nil
        
//        viewModel.employmentDetails?.referenceName = nil
//        viewModel.employmentDetails?.referenceEmail = nil
//        viewModel.employmentDetails?.referenceMobile = nil
    }
    
    override func btnActionForSave(_ sender: UIButton) {
        super.btnActionForSave(sender)
        
        viewModel.reset()
        
        viewModel.employmentDetails?.employerName = textfieldForEmployerName.isEmpty("Employer name", customMessage: "Please enter Employer name")
        viewModel.isValid(error: textfieldForEmployerName.errorMsg)

        viewModel.employmentDetails?.employerAddress = textfieldForEmployerAddress.isEmpty("Employer address")
        viewModel.isValid(error: textfieldForEmployerAddress.errorMsg)

        viewModel.employmentDetails?.unitNumber = textfieldForUnit.text

        viewModel.employmentDetails?.designation = textfieldForPositionHeld.isEmpty("your Position")
        viewModel.isValid(error: textfieldForPositionHeld.errorMsg)

        viewModel.employmentDetails?.fromDate = textfieldForStartDate.isEmpty("Start Date")
        viewModel.isValid(error: textfieldForStartDate.errorMsg)
        
        viewModel.employmentDetails?.isCurrent = btnForCurrentEmployment.isSelected

        if !btnForCurrentEmployment.isSelected {
            viewModel.employmentDetails?.toDate = textfieldForEndDate.isEmpty("End Date")
            viewModel.isValid(error: textfieldForEndDate.errorMsg)
        }
        
        viewModel.employmentDetails?.referenceName = textfieldForReferenceName.isValidName("Reference Name", customMessage: "Only character and spaces allowed")
        viewModel.isValid(error: textfieldForReferenceName.errorMsg)
        
        viewModel.employmentDetails?.referenceEmail = textfieldForReferenceEmail.isValidEmail("Reference Email")
        viewModel.isValid(error: textfieldForReferenceEmail.errorMsg)
        
        let validatePhoneNo = DataValidationUtility.shared.validatePhoneNo(
            viewModel.employmentDetails?.referenceMobile?.toUnPhoneNumber(),
            fieldName: "Reference Mobile", customMessage: "Please enter valid Reference Mobile")
        lblForPhoneNoError.text = viewModel.isValid(error: validatePhoneNo.errorMsg)
        
        showLoader()
        viewModel.hitUpdateEmploymentDetail(completion: { (message) in
            self.hideLoader()
        }, success: { (message) in
            self.dismiss(animated: true, completion: {
                APPLICATION_INSTANCE.visibleViewController()?.showAlert(message: message)
            })
        }, failure: { (message) in
            self.showAlert(message: message, withOk: true)
        })
    }
}

extension EmploymentInfoEditViewController: CustomTextfieldViewDelegate {
    
    func textFieldDidBeginEditing(_ identifier: String, textField: CustomTextfield) {
        
        if identifier == "EmployerAddress" {
            
            self.view.endEditing(true)
            AutoCompleteViewController.show(viewModel: AutoCompleteViewModel(
                selectedOptions: nil,
                type: AutoComplete.google(GooglePlaceFor: .address),
                isOffline: true, noOfSelection: .single), parentController: self, delegate: self)
        }
        else if identifier == "ReferenceMobile" {
            lblForPhoneNoError.text = nil
            if let text = textField.text, text.contains("(") {
                textField.text = text.toUnPhoneNumber()
            }
        }
    }
    
    func textFieldDidEndEditing(_ identifier: String, textField: CustomTextfield) {
        
        if identifier == "StartDate" {
            
            if !btnForCurrentEmployment.isSelected && textfieldForStartDate.currentDate() > textfieldForEndDate.currentDate() {
                textfieldForEndDate.text = nil
            }
            textfieldForEndDate.setMinDate(date: textfieldForStartDate.currentDate())
            viewModel.employmentDetails?.fromDate = textField.text
        }
        else if identifier == "EndDate" {
            viewModel.employmentDetails?.toDate = textField.text
        }
        else if identifier == "EmployerName" {
            viewModel.employmentDetails?.employerName = textField.text
        }
        else if identifier == "Unit" {
            viewModel.employmentDetails?.unitNumber = textField.text
        }
        else if identifier == "PositionHeld" {
            viewModel.employmentDetails?.designation = textField.text
        }
        else if identifier == "ReferenceName" {
            viewModel.employmentDetails?.referenceName = textField.text
        }
        else if identifier == "ReferenceEmail" {
            viewModel.employmentDetails?.referenceEmail = textField.text
        }
        else if identifier == "ReferenceMobile" {
            let formattedString = textField.text?.toPhoneNumber()
            textField.text = formattedString
            viewModel.employmentDetails?.referenceMobile = formattedString
            viewModel.employmentDetails?.referenceMobileFormatted = formattedString
        }
    }
    
    func pickerViewSelected(identifier: String, textfield: CustomTextfield, indexes: [Int]) {
        
        guard let firstIndex = indexes.first else {
            return
        }

        print("identifier \(identifier) indexes\(firstIndex)")
        if identifier == "ReferenceCountryCode" {
            viewModel.employmentDetails?.referenceCountryCode = Utility.countryCodeArray[firstIndex]["isdvalueCodes"] ?? CANADA_COUNTRY_CODE
            viewModel.employmentDetails?.referenceCountryIsoCode = Utility.countryCodeArray[firstIndex]["ISO_Code"] ?? CANADA_ISO_COUNTRY_CODE
        }
    }
}

extension EmploymentInfoEditViewController: AutoCompleteViewDelegate {
    
    func selectedOption(type: AutoComplete, placeDetail: GooglePlaceDetail?) {
        self.viewModel.employmentDetails?.employerAddress = placeDetail?.dislayAddress
        self.textfieldForEmployerAddress.text = viewModel.employmentDetails?.employerAddress
    }
}
