//
//  WorkStatusInfoEditViewModel.swift
//  Jombone
//
//  Created by Money Mahesh on 23/04/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import UIKit

class WorkStatusInfoEditViewModel: NSObject {
    
    var workPermitBean: WorkPermitBean?
    var fileData: FileDetail?
    var permitType: [MasterData]?

    override init() {
        self.workPermitBean = UserDataManager.shared.profileDetail?.workPermitBean ?? WorkPermitBean()
    }
    
    private var isInputValid = true
    @discardableResult func isValid(error: String?) -> String? {
        if !isInputValid {
            return error
        }
        isInputValid = (error == nil)
        return error
    }
    
    func reset() {
        isInputValid = true
    }
    
    func resetModel() {
        self.workPermitBean = WorkPermitBean()
    }
    
    func hitUpdateWorkPermitDetail(completion: ((_ message: String?)->())? = nil, success:  ((_ message: String?)->())? = nil, failure: ((_ message: String?)->())? = nil) {
        
        guard let parameters = JsonUtility.jsonString(data: workPermitBean), isInputValid else {
            completion?(nil)
            failure?(nil)
            return
        }
        
        let profileServices = ProfileServices(requestTag: "UPDATE_PROFILE_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        //Completion Block
        profileServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            if statusCode == 1, let workPermitBean = response.0 as? WorkPermitBean {
                let profileDetail = UserDataManager.shared.profileDetail
                profileDetail?.workPermitBean = workPermitBean
                UserDataManager.shared.profileDetail = profileDetail
                completion?(message)
                success?(message)
            }
            else {
                completion?(message)
                failure?(message)
            }
        }
        profileServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            completion?(nil)
            failure?(nil)
        }
        
        var filesDetail: (keysName: [String], filesName: [String], filesData: [Data])?
        if let _fileData = fileData {
             filesDetail = (keysName: ["permitDoc"], filesName: [_fileData.name], filesData: [_fileData.data])
        }

        
        profileServices.updateWorkDetail(filesDetail: filesDetail, parameters: ["workPermitString": parameters], additionalHeaderElements: nil)
    }
}
