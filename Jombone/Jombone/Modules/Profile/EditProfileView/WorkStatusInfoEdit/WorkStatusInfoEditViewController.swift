//
//  WorkStatusInfoEditViewController.swift
//  Jombone
//
//  Created by Money Mahesh on 23/04/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import UIKit

class WorkStatusInfoEditViewController: EditProfileBaseViewController {
    
    @IBOutlet weak var textfieldForWorkinCanada: CustomTextfieldView!
    @IBOutlet weak var textfieldForPermitType: CustomTextfieldView!
    @IBOutlet weak var textfieldForPermitExpDate: CustomTextfieldView!
    @IBOutlet weak var lblForDocumentError: UILabel!
    @IBOutlet weak var lblForDocFileName: UILabel!

    @IBOutlet weak var detailStackView: UIStackView!
    let permitDataSource = ["No", "Yes", "I am a PR or Citizen"]
    let permitTypeDataSource = ["Work Permit", "Study Permit"]
    
    var viewModel = WorkStatusInfoEditViewModel()
    var downloadDelegate = DownloadUploadAction()
    var uploadDocumentLabelText = "Upload Work Permit"
    
    class func show() {
        
        let viewController = PROFILE_STORYBOARD.instantiateViewController(withIdentifier: "WorkStatusInfoEditViewController") as! WorkStatusInfoEditViewController
        
        APPLICATION_INSTANCE.visibleViewController()?.present(viewController, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setTextfieldPreferences()
        self.populateView()
    }
    
    private func setTextfieldPreferences() {
        
        //Set Textfield Preferences
        
        if self.viewModel.workPermitBean?.hasWorkPermit == nil {
            self.viewModel.workPermitBean?.hasWorkPermit = 0
        }
        
        textfieldForWorkinCanada.borderStyle = .All
        textfieldForWorkinCanada.setJomboneFontStyle()
        let currentWorkPermitStatus = self.viewModel.workPermitBean?.hasWorkPermitStr
        self.textfieldForWorkinCanada?.pickerDataSource1(
            identifier: "WorkPermitStatus",
            array: [permitDataSource],
            currentValues: (currentWorkPermitStatus == nil ? nil : [currentWorkPermitStatus!]),
            delegate: self)
        
        textfieldForPermitType.borderStyle = .All
        textfieldForPermitType.setJomboneFontStyle()
        let currentPermitType = self.viewModel.workPermitBean?.permitType
        self.textfieldForPermitType?.pickerDataSource1(
            identifier: "PermitType",
            array: [permitTypeDataSource],
            currentValues: (currentPermitType == nil ? nil : [currentPermitType!]),
            delegate: self)
        
        textfieldForPermitExpDate.borderStyle = .All
        textfieldForPermitExpDate.setJomboneFontStyle()
        textfieldForPermitExpDate.datePickerDataSource(identifier: "ExpiryDate", currentDate: Date(), minDate: Date(), maxDate: nil, mode: UIDatePicker.Mode.date, delegate: self, pickerSeletedTextFormat: { (data: [Any]?, indexes: [Int], inputMode: CustomTextfieldView.InputType) -> (String) in
            return Utility.changeDateToString(date: data?.first as? Date, withFormat: "yyyy-MM-dd")
        })
    }
    
    private func populateView() {
        textfieldForWorkinCanada.text = self.viewModel.workPermitBean?.hasWorkPermitStr
        
        detailStackView.isHidden = !(self.viewModel.workPermitBean?.hasWorkPermit != nil && self.viewModel.workPermitBean?.hasWorkPermit == 1)
        textfieldForPermitType.text = self.viewModel.workPermitBean?.permitType
        textfieldForPermitExpDate.text = self.viewModel.workPermitBean?.permitExpiry
        lblForDocFileName.text = self.viewModel.workPermitBean?.permitDocumentDisplayName ?? (((self.viewModel.workPermitBean?.permitType ?? "work permit").lowercased() == "work permit") ? "Upload Work Permit" : "Upload Study Permit")
    }
    
    override func btnActionForSave(_ sender: UIButton) {
        super.btnActionForSave(sender)
        
        viewModel.reset()
        
        if (self.viewModel.workPermitBean?.hasWorkPermit != nil && self.viewModel.workPermitBean?.hasWorkPermit == 1) {
            
            viewModel.workPermitBean?.permitType = textfieldForPermitType.isValidName("Permit Type", customMessage: "Please select Permit Type")
            viewModel.isValid(error: textfieldForPermitType.errorMsg)
            
            viewModel.workPermitBean?.permitExpiry = textfieldForPermitExpDate.isEmpty("Expiry Date")
            viewModel.isValid(error: textfieldForPermitExpDate.errorMsg)
            
            if viewModel.workPermitBean?.permitDocumentPath == nil && viewModel.fileData == nil  {
                lblForDocumentError.isHidden = false
                lblForDocumentError.text = ERROR_REQUIRED_FIELD
                viewModel.isValid(error: lblForDocumentError.text)
            }
        }
        
        showLoader()
        viewModel.hitUpdateWorkPermitDetail(completion: { (message) in
            self.hideLoader()
        }, success: { (message) in
            self.dismiss(animated: true, completion: {
                APPLICATION_INSTANCE.visibleViewController()?.showAlert(message: message)
            })
        }, failure: { (message) in
            self.showAlert(message: message, withOk: true)
        })
    }
    
    
    @IBAction func btnActionForUpload(_ sender: UIButton) {
        
        lblForDocumentError.isHidden = true
        downloadDelegate.fetchFile { (fileDetail) in
            
            self.viewModel.workPermitBean?.permitDocumentPath = nil
            if let errorMessage = DataValidationUtility.shared.validateSize(5000000, fileDetail: fileDetail, fileTypeAllowed: [
                FileType.pdf,
                FileType.doc,
                FileType.docx,
                FileType.jpg,
                FileType.jpeg,
                FileType.png
                ]).message {
                self.lblForDocumentError.isHidden = false
                self.lblForDocumentError.text = errorMessage
                self.viewModel.workPermitBean?.permitDocumentPath = nil 
                self.lblForDocFileName.text = (((self.viewModel.workPermitBean?.permitType ?? "work permit").lowercased() == "work permit") ? "Upload Work Permit" : "Upload Study Permit")
                //                self.lblForDocFileName.text = self.viewModel.workPermitBean?.permitDocumentDisplayName ?? "Upload Work Permit"

            }
            else {
                self.lblForDocumentError.text = nil
                self.viewModel.fileData = fileDetail
                self.lblForDocFileName.text = fileDetail.name
                self.viewModel.workPermitBean?.permitDocumentDisplayName = fileDetail.name
            }
        }
    }
}

extension WorkStatusInfoEditViewController: CustomTextfieldViewDelegate {
    
    func textFieldDidBeginEditing(_ identifier: String, textField: CustomTextfield) {
        
    }
    
    func textFieldDidEndEditing(_ identifier: String, textField: CustomTextfield) {
        
        if identifier == "ExpiryDate" {
            self.viewModel.workPermitBean?.permitExpiry = textField.text
        }
    }
    
    func pickerViewSelected(identifier: String, textfield: CustomTextfield, indexes: [Int]) {
        
        guard let firstIndex = indexes.first else {
            return
        }

        print("identifier \(identifier) indexes\(firstIndex)")
        if identifier == "WorkPermitStatus" {
            if viewModel.workPermitBean?.hasWorkPermit != firstIndex {
                viewModel.resetModel()
            }
            viewModel.workPermitBean?.hasWorkPermit = firstIndex
        }
        else if identifier == "PermitType" {
            self.viewModel.workPermitBean?.permitType = textfield.text
        }
        self.populateView()
    }
}
