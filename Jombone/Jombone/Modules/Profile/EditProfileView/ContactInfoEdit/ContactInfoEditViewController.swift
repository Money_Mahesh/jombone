//
//  ContactInfoEditViewController.swift
//  Jombone
//
//  Created by Money Mahesh on 23/04/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import UIKit

class ContactInfoEditViewController: EditProfileBaseViewController {
    
    @IBOutlet weak var textfieldForName: CustomTextfieldView!
    @IBOutlet weak var textfieldForHomeCountryCode: CustomTextfieldView!
    @IBOutlet weak var textfieldForHomeMobileNo: CustomTextfieldView!
    @IBOutlet weak var lblForHomePhoneNoError: UILabel!

    @IBOutlet weak var textfieldForEmergencyCountryCode: CustomTextfieldView!
    @IBOutlet weak var textfieldForEmergencyMobileNo: CustomTextfieldView!
    @IBOutlet weak var lblForEmergencyPhoneNoError: UILabel!

    var viewModel = ContactInfoEditViewModel()
    
    class func show() {
        
        let viewController = PROFILE_STORYBOARD.instantiateViewController(withIdentifier: "ContactInfoEditViewController") as! ContactInfoEditViewController
        
        APPLICATION_INSTANCE.visibleViewController()?.present(viewController, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setTextfieldPreferences()
        self.populateView()
    }
    
    private func setTextfieldPreferences() {
        
        //Set Textfield Preferences
        textfieldForName.borderStyle = .All
        textfieldForName.setJomboneFontStyle()
        textfieldForName.textfieldViewDelegate = self

        textfieldForHomeCountryCode.borderStyle = .none
        textfieldForHomeCountryCode.setJomboneFontStyle()
        textfieldForHomeCountryCode.setJomboneCountryCodeStyle(delegate: self, selectedCode: self.viewModel.contactDetails?.homePhoneNumberCountryIsoCode)

        textfieldForHomeMobileNo.borderStyle = .none
        textfieldForHomeMobileNo.setJomboneMobileStyle()
        textfieldForHomeMobileNo.textfieldViewDelegate = self

        textfieldForEmergencyCountryCode.borderStyle = .none
        textfieldForEmergencyCountryCode.setJomboneFontStyle()
        textfieldForEmergencyCountryCode.setJomboneCountryCodeStyle(delegate: self, selectedCode: self.viewModel.contactDetails?.emergencyContactNumberCountryIsoCode)

        textfieldForEmergencyMobileNo.borderStyle = .none
        textfieldForEmergencyMobileNo.setJomboneMobileStyle()
        textfieldForEmergencyMobileNo.textfieldViewDelegate = self
    }
    
    func populateView() {
        
        textfieldForName.text = viewModel.contactDetails?.emergencyContactName
        textfieldForHomeMobileNo.text = viewModel.contactDetails?.homePhoneNumber?.toPhoneNumber()
        textfieldForEmergencyMobileNo.text = viewModel.contactDetails?.emergencyContactNumber?.toPhoneNumber()
    }
    
    override func btnActionForSave(_ sender: UIButton) {
        super.btnActionForSave(sender)
        
        viewModel.reset()
        
        viewModel.contactDetails?.emergencyContactName = textfieldForName.text
        
        if let text = textfieldForName.text, text.count > 0 {
            _ = textfieldForName.isValidName(customMessage: "Only character and spaces allowed")
            viewModel.isValid(error: textfieldForName.errorMsg)
        }
        
        if let text = textfieldForEmergencyMobileNo.text, text.count > 0 {
            let validatePhoneNo = DataValidationUtility.shared.validatePhoneNo(
                viewModel.contactDetails?.emergencyContactNumber,
                fieldName: "mobile number", customMessage: "Please enter valid mobile number")
            lblForEmergencyPhoneNoError.text = viewModel.isValid(error: validatePhoneNo.errorMsg)
        }
        
        if let text = textfieldForHomeMobileNo.text, text.count > 0 {
            let validatePhoneNo = DataValidationUtility.shared.validatePhoneNo(
                viewModel.contactDetails?.homePhoneNumber,
                fieldName: "mobile number", customMessage: "Please enter valid mobile number")
            lblForHomePhoneNoError.text = viewModel.isValid(error: validatePhoneNo.errorMsg)
        }
        
        showLoader()
        viewModel.hitUpdateContactDetail(completion: { (message) in
            self.hideLoader()
        }, success: { (message) in
            self.dismiss(animated: true, completion: {
                APPLICATION_INSTANCE.visibleViewController()?.showAlert(message: message)
            })
        }, failure: { (message) in
            self.showAlert(message: message, withOk: true)
        })
    }
}

extension ContactInfoEditViewController: CustomTextfieldViewDelegate {
    
    func textFieldDidBeginEditing(_ identifier: String, textField: CustomTextfield) {
        
        if identifier == "EmergencyContactNumber" {
            lblForEmergencyPhoneNoError.text = nil
        }
        else if identifier == "HomePhoneNumber" {
            lblForHomePhoneNoError.text = nil
        }
        if let text = textField.text, text.contains("(") {
            textField.text = text.toUnPhoneNumber()
        }
    }
    
    func textFieldDidEndEditing(_ identifier: String, textField: CustomTextfield) {
        
        if identifier == "EmergencyContactNumber" {
            viewModel.contactDetails?.emergencyContactNumber = textField.text?.toUnPhoneNumber()
            let formattedString = textField.text?.toPhoneNumber()
            textField.text = formattedString
            viewModel.contactDetails?.emergencyContactNumberFormatted = textField.text!.toPhoneNumber()
        }
        else if identifier == "HomePhoneNumber" {
            viewModel.contactDetails?.homePhoneNumber = textField.text?.toUnPhoneNumber()
            let formattedString = textField.text?.toPhoneNumber()
            textField.text = formattedString
            viewModel.contactDetails?.homePhoneNumberFormatted = textField.text!.toPhoneNumber()
        }
    }
    
    func pickerViewSelected(identifier: String, textfield: CustomTextfield, indexes: [Int]) {
        
        guard let firstIndex = indexes.first else {
            return
        }

        print("identifier \(identifier) indexes\(firstIndex)")
        if identifier == "HomeCountryCode" {
            viewModel.contactDetails?.homePhoneNumberCountryCode = Utility.countryCodeArray[firstIndex]["isdvalueCodes"] ?? CANADA_COUNTRY_CODE
            viewModel.contactDetails?.homePhoneNumberCountryIsoCode = Utility.countryCodeArray[firstIndex]["ISO_Code"] ?? CANADA_ISO_COUNTRY_CODE
        }
        else if identifier == "EmpergencyCountryCode" {
            viewModel.contactDetails?.emergencyContactNumberCountryCode = Utility.countryCodeArray[firstIndex]["isdvalueCodes"] ?? CANADA_COUNTRY_CODE
            viewModel.contactDetails?.emergencyContactNumberCountryIsoCode = Utility.countryCodeArray[firstIndex]["ISO_Code"] ?? CANADA_ISO_COUNTRY_CODE
        }
    }
}
