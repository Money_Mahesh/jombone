//
//  UseCurrentLocationView.swift
//  Jombone
//
//  Created by Money Mahesh on 19/11/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

protocol UseCurrentLocationProtocol: NSObjectProtocol {
    func completion(_ placeDetail: GooglePlaceDetail)
}

class UseCurrentLocationView: CustomView {
    
    @IBOutlet weak var currentLocation: UIImageView!
    
    weak var delegate: UseCurrentLocationProtocol?

    override func xibLoaded(data: Any) {
        delegate = data as? UseCurrentLocationProtocol
    }
    
    @IBAction func tapGestureAction(_ sender: UITapGestureRecognizer) {
        APPLICATION_INSTANCE.visibleViewController()?.view.endEditing(true)
        
        APPLICATION_INSTANCE.visibleViewController()?.showLoader()
        LocationManager.instance.setUp(completion: { (status: Bool, locationDetail: CurrentLocationDetail?, coordinate: CLLocationCoordinate2D?) in
            
            APPLICATION_INSTANCE.visibleViewController()?.hideLoader()
            if status {
                
                if let _coordinate = coordinate {
                    
                    GooglePlaceUtility.getFetchPlaceIdFromLatLng(latitude: _coordinate.latitude, longitude: _coordinate.longitude, completion: nil, success: { (placeDetail: GooglePlaceDetail, message) in
                        self.delegate?.completion(placeDetail)
                    }, failure: nil)
                }
            }
        })
    }
}
