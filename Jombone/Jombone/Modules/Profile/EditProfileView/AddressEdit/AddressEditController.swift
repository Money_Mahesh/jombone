//
//  AddressEditController.swift
//  Jombone
//
//  Created by Money Mahesh on 17/11/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import Foundation
import UIKit

class AddressEditController: EditProfileBaseViewController {
    
    @IBOutlet weak var textfieldForAddress: CustomTextfieldView!
    @IBOutlet weak var textfieldForUnit: CustomTextfieldView!
    @IBOutlet var currentLocationView: UseCurrentLocationView!
    @IBOutlet weak var lblForAddressError: UILabel!

    var viewModel = AddressEditViewModel()

    class func show() {
        
        let addressEditController = PROFILE_STORYBOARD.instantiateViewController(withIdentifier: "AddressEditController") as! AddressEditController

        APPLICATION_INSTANCE.visibleViewController()?.present(addressEditController, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setTextfieldPreferences()
        viewModel.dataUpdated = {
            self.populateData()
        }
    }
    
    func setTextfieldPreferences() {
        
        self.stackView.spacing = 16
        
        textfieldForAddress.borderStyle = .All
        textfieldForAddress.dataSource(identifier: "Address", placeholder: "Address", text: viewModel.addressDetail?.fullAddress, delegate: self)
        
        textfieldForUnit.dataSource(identifier: "Unit", placeholder: "Unit", text: viewModel.addressDetail?.unit, delegate: self)
        textfieldForUnit.borderStyle = .All
        textfieldForUnit.MaxLength = 20
        
        currentLocationView.delegate = self
    }
    
    func populateData() {
        textfieldForAddress.text = viewModel.addressDetail?.fullAddress
        textfieldForUnit.text = viewModel.addressDetail?.unit
    }
    
    override func btnActionForSave(_ sender: UIButton) {
        super.btnActionForSave(sender)
        
        guard let _text = textfieldForAddress.text, _text.count != 0 else {
            lblForAddressError.isHidden = false
            lblForAddressError.text = AppError.empty(field: "Address").message
            return
        }
        
        showLoader()
        viewModel.hitUpdateAddressDetail(completion: { (message) in
            self.hideLoader()
        }, success: { (message) in
            self.dismiss(animated: true, completion: {
                APPLICATION_INSTANCE.visibleViewController()?.showAlert(message: message)
            })
        }, failure: { (message) in
            self.showAlert(message: message, withOk: true)
        })
    }
}

extension AddressEditController: CustomTextfieldViewDelegate {
    
    func textFieldDidBeginEditing(_ identifier: String, textField: CustomTextfield) {
        
        if identifier == "Address" {
            
            self.view.endEditing(true)
            
            lblForAddressError.isHidden = true
            lblForAddressError.text = nil
            AutoCompleteViewController.show(viewModel: AutoCompleteViewModel(
                selectedOptions: nil,
                type: AutoComplete.google(GooglePlaceFor: .address),
                isOffline: true, noOfSelection: .single), parentController: self, delegate: self)
        }
    }
    
    func textFieldDidEndEditing(_ identifier: String, textField: CustomTextfield) {
    
        if identifier == "Unit" {
            viewModel.addressDetail?.unit = textField.text
        }
    }
}

extension AddressEditController: AutoCompleteViewDelegate {
    
    func selectedOption(type: AutoComplete, placeDetail: GooglePlaceDetail?) {
        self.viewModel.updateAddress(placeDetail)
    }
}

extension AddressEditController: UseCurrentLocationProtocol {
    func completion(_ placeDetail: GooglePlaceDetail) {
        lblForAddressError.text = nil
        lblForAddressError.isHidden = true
        self.viewModel.updateAddress(placeDetail)
    }
}
