//
//  AddressEditViewModel.swift
//  Jombone
//
//  Created by Money Mahesh on 17/11/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import Foundation

class AddressEditViewModel: NSObject {
    
    var addressDetail: AddressBean?
    var dataUpdated: (()->())?
    
    override init() {
        self.addressDetail = UserDataManager.shared.profileDetail?.addressBean
    }
    
    func updateAddress(_ placeDetail: GooglePlaceDetail?) {
        
        var latLong: String?
        if let lat = placeDetail? .lat, let long = placeDetail? .long {
            latLong = String(describing: lat) + "," + String(describing: long)
        }
        self.addressDetail?.latLong = latLong
        self.addressDetail?.addressLine1 = placeDetail? .street_number
        self.addressDetail?.addressLine2 = placeDetail? .route
        self.addressDetail?.city = placeDetail? .locality
        self.addressDetail?.completeAddress = placeDetail? .dislayAddress
        self.addressDetail?.fullAddress = placeDetail? .dislayAddress
        self.addressDetail?.googlePlaceId = placeDetail? .place_id
        self.addressDetail?.state = placeDetail? .administrative_area_level_1
        self.addressDetail?.zipcode = placeDetail? .postal_code
        self.addressDetail?.country = placeDetail? .country
        dataUpdated?()
    }
    
    func hitUpdateAddressDetail(completion: ((_ message: String?)->())? = nil, success:  ((_ message: String?)->())? = nil, failure: ((_ message: String?)->())? = nil) {
        
        guard let parameters = JsonUtility.dictionary(data: addressDetail) else {
            completion?(nil)
            failure?(nil)
            return
        }
        
        let profileServices = ProfileServices(requestTag: "UPDATE_PROFILE_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        //Completion Block
        profileServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            if statusCode == 1, let addressDetail = response.0 as? AddressBean {
                let profileDetail = UserDataManager.shared.profileDetail
                profileDetail?.addressBean = addressDetail
                UserDataManager.shared.profileDetail = profileDetail
                completion?(message)
                success?(message)
            }
            else {
                completion?(message)
                failure?(message)
            }
        }
        profileServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            completion?(nil)
            failure?(nil)
        }
        
        profileServices.updateAddress(parameters: parameters, additionalHeaderElements: nil)
    }
}
