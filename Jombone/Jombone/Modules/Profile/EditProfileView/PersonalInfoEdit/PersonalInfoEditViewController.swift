//
//  PersonalInfoEditViewController.swift
//  Jombone
//
//  Created by Money Mahesh on 23/04/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import UIKit
import PhotosUI

class PersonalInfoEditViewController: EditProfileBaseViewController {
    
    @IBOutlet weak var imgForProfilePic: UIImageView!
    @IBOutlet weak var lblForName: UILabel!
    @IBOutlet weak var lblForJomboneId: UILabel!
    
    @IBOutlet weak var lblForNameError: UILabel!
    @IBOutlet weak var lblForPhoneNoError: UILabel!
    
    @IBOutlet weak var textfieldForFirstName: CustomTextfieldView!
    @IBOutlet weak var textfieldForLastName: CustomTextfieldView!
    @IBOutlet weak var textfieldForCountryCode: CustomTextfieldView!
    @IBOutlet weak var textfieldForMobileNo: CustomTextfieldView!
    @IBOutlet weak var textfieldForEmail: CustomTextfieldView!
    @IBOutlet weak var textfieldForAvailableFrom: CustomTextfieldView!
    @IBOutlet weak var textfieldForExperience: CustomTextfieldView!
    @IBOutlet weak var textfieldForHighestEducation: CustomTextfieldView!

    @IBOutlet weak var switchForImmediateAvailable: UISwitch!
    
    private var imagePicker = UIImagePickerController()
    var viewModel = PersonalInfoEditViewModel()
    
    class func show() {
        
        let viewController = PROFILE_STORYBOARD.instantiateViewController(withIdentifier: "PersonalInfoEditViewController") as! PersonalInfoEditViewController
        
        APPLICATION_INSTANCE.visibleViewController()?.present(viewController, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setTextfieldPreferences()
        self.populateView()
        viewModel.dataUpdated = {
            self.populateView()
        }
    }
    
    private func setTextfieldPreferences() {
        
        //Set Textfield Preferences
        textfieldForFirstName.borderStyle = .none
        textfieldForFirstName.setJomboneFontStyle()
        textfieldForFirstName.textfieldViewDelegate = self
        textfieldForFirstName.textfield.tintColor = UIColor.black
        
        textfieldForLastName.borderStyle = .none
        textfieldForLastName.setJomboneFontStyle()
        textfieldForLastName.textfieldViewDelegate = self

        textfieldForCountryCode.borderStyle = .none
        textfieldForCountryCode.setJomboneFontStyle()
        textfieldForCountryCode.setJomboneCountryCodeStyle(delegate: self, selectedCode: self.viewModel.personalDetails?.countryIsoCode)

        textfieldForMobileNo.borderStyle = .none
        textfieldForMobileNo.setJomboneMobileStyle()
        textfieldForMobileNo.textfieldViewDelegate = self

        textfieldForEmail.borderStyle = .All
        textfieldForEmail.setJomboneEmailStyle()

        textfieldForExperience.borderStyle = .All
        textfieldForExperience.setJomboneFontStyle()
        textfieldForExperience.inputType = .Keyboard(type: .numberPad)

        textfieldForAvailableFrom.borderStyle = .All
        textfieldForAvailableFrom.setJomboneFontStyle()
        textfieldForAvailableFrom.datePickerDataSource(identifier: "AvailableFrom", currentDate: nil, minDate: Date(), maxDate: nil, mode: UIDatePicker.Mode.date, delegate: self, pickerSeletedTextFormat: { (data: [Any]?, indexes: [Int], inputMode: CustomTextfieldView.InputType) -> (String) in
            return Utility.changeDateToString(date: data?.first as? Date, withFormat: "yyyy-MM-dd")
        })
        
        
        textfieldForHighestEducation.borderStyle = .All
        textfieldForHighestEducation.setJomboneFontStyle()
        let currentValue = self.viewModel.personalDetails?.highestEducationName
        self.textfieldForHighestEducation?.pickerDataSource1(
            identifier: "HighestEducation",
            array: [[String]](),
            currentValues: (currentValue == nil ? nil : [currentValue!]),
            delegate: self)
        setHighestEducationDataSource()
    }
    
    private func setHighestEducationDataSource(completion: (()->())? = nil) {
    
        showLoader()
        MasterDataManager.shared.education { (data: [MasterData]?) in
            
            self.hideLoader()
            self.viewModel.education = data
            
            let dataArray = data?.map({ (obj: MasterData) -> String in
                return obj.displayName ?? ""
            }) ?? [String]()
            
            let currentValue = self.viewModel.personalDetails?.highestEducationName
            self.textfieldForHighestEducation?.pickerDataSource1(
                identifier: "HighestEducation",
                array: [dataArray],
                currentValues: (currentValue == nil ? nil : [currentValue!]),
                defaultValueIndex: [0],
                delegate: self)
            
            completion?()
        }
    }
    
    @objc func populateView() {
        
        if let imageUrlStr = viewModel.personalDetails?.profilePicPath, let imageUrl = URL(string: imageUrlStr) {
            
            let url_request = URLRequest(url: imageUrl)
            
            imgForProfilePic.setImageWithURLAlamofire(url_request, placeholderImageName: nil, success: {
                [weak imgForProfilePic] (request:URLRequest?, image:UIImage?) -> Void in
                imgForProfilePic?.image = image }
                ,failure: {
                    [weak imgForProfilePic] (request:URLRequest?, error:Error?) -> Void in
                    imgForProfilePic?.image = UIImage(named: "profileDefault")
                    print("Not able to load profile pic" + (error?.localizedDescription ?? ""))
            })
        }
        else {
            imgForProfilePic?.image = UIImage(named: "profileDefault")
        }
        
        lblForName.text = ((viewModel.personalDetails?.firstName ?? "") + " " + (viewModel.personalDetails?.lastName ?? ""))
        lblForJomboneId.text = (viewModel.personalDetails?.jomboneId ?? "")
        
        textfieldForFirstName.text = viewModel.personalDetails?.firstName
        textfieldForLastName.text = viewModel.personalDetails?.lastName
        textfieldForMobileNo.text = viewModel.personalDetails?.phoneNumber
        textfieldForEmail.text = viewModel.personalDetails?.email
        textfieldForAvailableFrom.text = viewModel.personalDetails?.availableFrom
        textfieldForExperience.text = String(viewModel.personalDetails?.exp ?? 0)
        textfieldForHighestEducation.text = viewModel.personalDetails?.highestEducationName

        switchForImmediateAvailable.isOn = viewModel.personalDetails?.availability ?? false
        availbilityUpdated()
        
        let unformattedNo = viewModel.personalDetails?.phoneNumber?.toUnPhoneNumber()
        viewModel.personalDetails?.mobileUnformat = unformattedNo
    }
    
    private func availbilityUpdated() {
        textfieldForAvailableFrom.isHidden = switchForImmediateAvailable.isOn
    }
    
    @IBAction func btnActionForEdit(_ sender: UIButton) {
        selectProfilePic()
    }
    
    @IBAction func immediateAvailable(_ sender: UISwitch) {
        viewModel.personalDetails?.availability = sender.isOn
        availbilityUpdated()
        self.view.endEditing(true)
    }
    
    override func btnActionForSave(_ sender: UIButton) {
        super.btnActionForSave(sender)
        
        viewModel.reset()
        
        let validateName = DataValidationUtility.shared.validateName(
            firstName: textfieldForFirstName.text,
            lastName: textfieldForLastName.text, customMessage: ERROR_ONLY_CHARACTER_ALLOWED)
        lblForNameError.text = viewModel.isValid(
            error: validateName.firstName.errorMsg ?? validateName.lastName.errorMsg)
        viewModel.personalDetails?.firstName = validateName.firstName.value
        viewModel.personalDetails?.lastName = validateName.lastName.value
        
        let validatePhoneNo = DataValidationUtility.shared.validatePhoneNo(viewModel.personalDetails?.mobileUnformat)
        lblForPhoneNoError.text = viewModel.isValid(error: validatePhoneNo.errorMsg)
        viewModel.personalDetails?.phoneNumber = textfieldForMobileNo.text
        
        viewModel.personalDetails?.availability = switchForImmediateAvailable.isOn
        if switchForImmediateAvailable.isOn {
            viewModel.personalDetails?.availableFrom = nil
        }
        else {
            viewModel.personalDetails?.availableFrom = textfieldForAvailableFrom.isEmpty("Availability date")
        }
        
        viewModel.personalDetails?.exp = Int(textfieldForExperience.isNumberWithRange(0, fieldName: "total experience", customMessage: ERROR_ONLY_POSITIVE_NUMBER_ALLOWED) ?? "0")
        viewModel.isValid(error: textfieldForExperience.errorMsg)

        _ = textfieldForHighestEducation.isEmpty("highest education")
        viewModel.isValid(error: textfieldForHighestEducation.errorMsg)

        showLoader()
        viewModel.hitUpdatePersonalDetail(completion: { (message) in
            self.hideLoader()
        }, success: { (message) in
            self.dismiss(animated: true, completion: {
                APPLICATION_INSTANCE.visibleViewController()?.showAlert(message: message)
            })
        }, failure: { (message) in
            self.showAlert(message: message, withOk: true)
        })
    }
    
    func uploadProfilePic(profileImage: UIImage) {
        
        showLoader()
        viewModel.hitUpdateProfilePic(image: profileImage, completion: { (message) in
            self.hideLoader()
            self.showAlert(message: message, withOk: true)
        })
    }
}

extension PersonalInfoEditViewController: CustomTextfieldViewDelegate {
    
    func textFieldDidBeginEditing(_ identifier: String, textField: CustomTextfield) {
        
        if identifier == "HighestEducation" && viewModel.education == nil {
            self.view.endEditing(true)
            setHighestEducationDataSource(completion: {
                self.textfieldForHighestEducation.setAsFirstResponder()
            })
            print(viewModel.education ?? "Data unavailable")
        }
        else if identifier == "FirstName" || identifier == "LastName" {
            lblForNameError.text = nil
        }
        else if identifier == "MobileNo" {
            lblForPhoneNoError.text = nil
            if let text = textField.text, text.contains("(") {
                textField.text = text.toUnPhoneNumber()
            }
        }
    }
    
    func textFieldDidEndEditing(_ identifier: String, textField: CustomTextfield) {
        
        if identifier == "MobileNo" {
            viewModel.personalDetails?.mobileUnformat = textField.text?.toUnPhoneNumber()
            let formattedString = textField.text?.toPhoneNumber()
            textField.text = formattedString
        }
    }
    
    func pickerViewSelected(identifier: String, textfield: CustomTextfield, indexes: [Int]) {
        
        guard let firstIndex = indexes.first else {
            return
        }

        print("identifier \(identifier) indexes\(firstIndex)")
        if identifier == "CountryCode" {
            viewModel.personalDetails?.countryCode = Utility.countryCodeArray[firstIndex]["isdvalueCodes"] ?? CANADA_COUNTRY_CODE
            viewModel.personalDetails?.countryIsoCode = Utility.countryCodeArray[firstIndex]["ISO_Code"] ?? CANADA_ISO_COUNTRY_CODE
        }
        else if identifier == "HighestEducation" {
            viewModel.personalDetails?.highestEducationEncId = viewModel.education?[firstIndex].encryptedId
            viewModel.personalDetails?.highestEducationName = viewModel.education?[firstIndex].name
        }
    }
}

extension PersonalInfoEditViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func selectProfilePic() {
        self.view.endEditing(true)
        
        imagePicker.delegate = self
        
        APPLICATION_INSTANCE.visibleViewController()?.showAlert(message: "Change profile picture", style: UIAlertController.Style.actionSheet, withCancel: true, withCustomAction: [
            UIAlertAction(title: "Camera", style: UIAlertAction.Style.default, handler: { (action) in self.openCamera() }),
            UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default, handler: { (action) in self.openImageGallery() })
            ]
        )
    }
    
    private func openImageGallery() {
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = true
        
        let photoAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        switch photoAuthorizationStatus {
        case .authorized:
            APPLICATION_INSTANCE.visibleViewController()?.present(imagePicker, animated: true, completion: nil)
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization({[weak imagePicker]
                (newStatus) in
                DispatchQueue.main.async {
                    if newStatus ==  PHAuthorizationStatus.authorized {
                        if let _imagePicker = imagePicker {
                            APPLICATION_INSTANCE.visibleViewController()?.present(_imagePicker, animated: true, completion: nil)
                        }
                    }else{
                        print("User denied")
                    }
                }})
            break
        case .restricted:
            APPLICATION_INSTANCE.visibleViewController()?.showAlert(message: "You do not have permissions enabled for this. Please enable it from phone setting.")

            print("restricted")
            break
        case .denied:
            APPLICATION_INSTANCE.visibleViewController()?.showAlert(message: "You do not have permissions enabled for this. Please enable it from phone setting.")
            print("denied")
            break
        }
    }
    
    private func openCamera() {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            imagePicker.cameraOverlayView = UIView(frame: CGRect(x: 0, y: 0, width: 200, height: 40), backgroundColor: UIColor.green)
            imagePicker.cameraOverlayView?.backgroundColor = UIColor.clear
            APPLICATION_INSTANCE.visibleViewController()?.present(imagePicker, animated: true, completion: nil)
        }
        else {
            let alertController = UIAlertController(title: "Oops!!", message: "Camera is not available", preferredStyle: UIAlertController.Style.alert)
            let cancelAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.cancel, handler: nil)
            
            alertController.addAction(cancelAction)
            APPLICATION_INSTANCE.visibleViewController()?.present(alertController, animated: true, completion: nil)
        }
    }
    
    //MARK:- Image Picker Delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        self.imagePicker.dismiss(animated: true, completion: {
            if let pickerImage = (info[UIImagePickerController.InfoKey.editedImage] as? UIImage) {
                
                self.imgForProfilePic.image = pickerImage
                self.uploadProfilePic(profileImage: pickerImage)
            }
            else{
                print("Something went wrong in  video")
            }
        })
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.imagePicker.dismiss(animated: true, completion: nil)
    }
}
