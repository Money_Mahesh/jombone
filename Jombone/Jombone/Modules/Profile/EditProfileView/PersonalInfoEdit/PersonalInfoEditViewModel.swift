//
//  PersonalInfoEditViewModel.swift
//  Jombone
//
//  Created by Money Mahesh on 23/04/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import UIKit

class PersonalInfoEditViewModel: NSObject {
    
    var personalDetails: PersonalDetailsBean?
    var education: [MasterData]?
    var dataUpdated: (()->())?

    override init() {
        self.personalDetails = UserDataManager.shared.profileDetail?.personalDetailsBeans ?? PersonalDetailsBean()
    }
    
    private var isInputValid = true
    @discardableResult func isValid(error: String?) -> String? {
        if !isInputValid {
            return error
        }
        isInputValid = (error == nil)
        return error
    }
    
    func reset() {
        isInputValid = true
    }
    
    func hitUpdatePersonalDetail(completion: ((_ message: String?)->())? = nil, success:  ((_ message: String?)->())? = nil, failure: ((_ message: String?)->())? = nil) {
        
        guard let parameters = JsonUtility.jsonString(data: personalDetails), isInputValid else {
            completion?(nil)
            failure?(nil)
            return
        }
        
        let profileServices = ProfileServices(requestTag: "UPDATE_PROFILE_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        //Completion Block
        profileServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            if statusCode == 1, let personalDetailsBeans = response.0 as? PersonalDetailsBean {
                let profileDetail = UserDataManager.shared.profileDetail
                profileDetail?.personalDetailsBeans = personalDetailsBeans
                UserDataManager.shared.profileDetail = profileDetail
                completion?(message)
                success?(message)
            }
            else {
                completion?(message)
                failure?(message)
            }
        }
        profileServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            completion?(nil)
            failure?(nil)
        }
        
        profileServices.updatePersonalDetail(parameters: ["personalDetailsString": parameters], additionalHeaderElements: nil)
    }
    
    func hitUpdateProfilePic(image: UIImage, completion: ((_ message: String?)->())? = nil, success:  ((_ message: String?)->())? = nil, failure: ((_ message: String?)->())? = nil) {
        
        guard let imgData = image.jpegData(compressionQuality: 0.2) else {
            completion?(nil)
            failure?(nil)
            return
        }
        
        let profileServices = ProfileServices(requestTag: "UPDATE_PROFILE_PIC_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        //Completion Block
        profileServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            if statusCode == 1, let personalDetailsBeans = response.0 as? PersonalDetailsBean {
                let profileDetail = UserDataManager.shared.profileDetail
                profileDetail?.personalDetailsBeans = personalDetailsBeans
                UserDataManager.shared.profileDetail = profileDetail
                completion?(message)
                success?(message)
            }
            else {
                completion?(message)
                failure?(message)
            }
        }
        profileServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            completion?(nil)
            failure?(nil)
        }
        
        profileServices.updateProfilePic(imageData: imgData, parameters: nil, additionalHeaderElements: nil)
    }
}
