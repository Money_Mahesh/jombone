//
//  IDInfoEditViewController.swift
//  Jombone
//
//  Created by Money Mahesh on 23/04/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import UIKit

class IDInfoEditViewController: EditProfileBaseViewController {
    
    @IBOutlet weak var textfieldForGovtIssuedPhotoIdType: CustomTextfieldView!
    @IBOutlet weak var textfieldForGovtIssuedPhotoIdExpiryDate: CustomTextfieldView!
    @IBOutlet weak var textfieldForGovtIssuedPhotoIdNo: CustomTextfieldView!
    @IBOutlet weak var btnForGovtIssuedPhotoIdNoExpiry: UIButton!
    @IBOutlet weak var photoIdDetailStackView: UIStackView!
    @IBOutlet weak var lblForPhotoIdDocumentError: UILabel!
    @IBOutlet weak var lblForPhotoIdDocFileName: UILabel!

    @IBOutlet weak var textfieldForGovtIssuedIdType: CustomTextfieldView!
    @IBOutlet weak var textfieldForGovtIssuedIdExpiryDate: CustomTextfieldView!
    @IBOutlet weak var textfieldForGovtIssuedIdNo: CustomTextfieldView!
    @IBOutlet weak var btnForGovtIssuedIdNoExpiry: UIButton!
    @IBOutlet weak var idDetailStackView: UIStackView!
    @IBOutlet weak var lblForIdDocumentError: UILabel!
    @IBOutlet weak var lblForIdDocFileName: UILabel!


    var viewModel = IDInfoEditViewModel()
    var downloadDelegate = DownloadUploadAction()
    
    class func show(viewModel: IDInfoEditViewModel) {
        
        let viewController = PROFILE_STORYBOARD.instantiateViewController(withIdentifier: "IDInfoEditViewController") as! IDInfoEditViewController
        viewController.viewModel = viewModel
        
        APPLICATION_INSTANCE.visibleViewController()?.present(viewController, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setTextfieldPreferences()
        self.populateView()
    }
    
    private func setTextfieldPreferences() {
        
        //Set Textfield Preferences
        textfieldForGovtIssuedPhotoIdType.borderStyle = .All
        textfieldForGovtIssuedPhotoIdType.setJomboneFontStyle()
        let currentPhotoGovtIdDocument = self.viewModel.documentBeans?.photoGovtIdDocument
        self.textfieldForGovtIssuedPhotoIdType?.pickerDataSource1(
            identifier: "GovtIssuedPhotoID",
            array: [[String]](),
            currentValues: (currentPhotoGovtIdDocument == nil ? nil : [currentPhotoGovtIdDocument!]),
            delegate: self)
        setPhotoIdTypeDataSource()
        
        textfieldForGovtIssuedPhotoIdExpiryDate.borderStyle = .All
        textfieldForGovtIssuedPhotoIdExpiryDate.setJomboneFontStyle()
        textfieldForGovtIssuedPhotoIdExpiryDate.datePickerDataSource(identifier: "PhotoIDExpiryDate", currentDate: Date(), minDate: Date(), maxDate: nil, mode: UIDatePicker.Mode.date, delegate: self, pickerSeletedTextFormat: { (data: [Any]?, indexes: [Int], inputMode: CustomTextfieldView.InputType) -> (String) in
            return Utility.changeDateToString(date: data?.first as? Date, withFormat: "yyyy-MM-dd")
        })
        
        textfieldForGovtIssuedPhotoIdNo.borderStyle = .All
        textfieldForGovtIssuedPhotoIdNo.textfieldViewDelegate = self
        textfieldForGovtIssuedPhotoIdNo.setJomboneFontStyle()
        
        textfieldForGovtIssuedIdType.borderStyle = .All
        textfieldForGovtIssuedIdType.setJomboneFontStyle()
        let currentGovtIdDocument = self.viewModel.documentBeans?.govtIdDocument
        self.textfieldForGovtIssuedIdType?.pickerDataSource1(
            identifier: "GovtIssuedID",
            array: [[String]](),
            currentValues: (currentGovtIdDocument == nil ? nil : [currentGovtIdDocument!]),
            delegate: self)
        setIdTypeDataSource()
        
        textfieldForGovtIssuedIdExpiryDate.borderStyle = .All
        textfieldForGovtIssuedIdExpiryDate.setJomboneFontStyle()
        textfieldForGovtIssuedIdExpiryDate.datePickerDataSource(identifier: "IDExpiryDate", currentDate: Date(), minDate: Date(), maxDate: nil, mode: UIDatePicker.Mode.date, delegate: self, pickerSeletedTextFormat: { (data: [Any]?, indexes: [Int], inputMode: CustomTextfieldView.InputType) -> (String) in
            return Utility.changeDateToString(date: data?.first as? Date, withFormat: "yyyy-MM-dd")
        })
        
        textfieldForGovtIssuedIdNo.borderStyle = .All
        textfieldForGovtIssuedIdNo.textfieldViewDelegate = self
        textfieldForGovtIssuedIdNo.setJomboneFontStyle()
    }
    
    private func setPhotoIdTypeDataSource(completion: (()->())? = nil) {
        
        showLoader()
        MasterDataManager.shared.photoGovtIds { (data: [MasterData]?) in
            
            self.hideLoader()
            self.viewModel.mainPhotoIdType = data
            if let _ = self.viewModel.mainPhotoIdType {
                self.viewModel.mainPhotoIdType!.insert(MasterData(displayName: "None", encryptedId: ""), at: 0)
            }
            self.viewModel.photoIdType = self.viewModel.mainPhotoIdType

            let dataArray = self.viewModel.mainPhotoIdType?.map({ (obj: MasterData) -> String in
                return obj.displayName ?? ""
            })
            let _dataArray = dataArray == nil ? [[String]]() : [dataArray!]

            let currentValue = self.viewModel.documentBeans?.photoGovtIdDocument
            self.textfieldForGovtIssuedPhotoIdType?.pickerDataSource1(
                identifier: "GovtIssuedPhotoID",
                array: _dataArray,
                currentValues: (currentValue == nil ? nil : [currentValue!]),
                delegate: self)
            
            self.populateView()
            completion?()
        }
    }
    
    private func setIdTypeDataSource(completion: (()->())? = nil) {
        
        showLoader()
        MasterDataManager.shared.govtIds { (data: [MasterData]?) in
            
            self.hideLoader()
            self.viewModel.mainIdType = data
            if let _ = self.viewModel.mainIdType {
                self.viewModel.mainIdType!.insert(MasterData(displayName: "None", encryptedId: ""), at: 0)
            }
            self.viewModel.idType = self.viewModel.mainIdType

            let dataArray = self.viewModel.mainIdType?.map({ (obj: MasterData) -> String in
                return obj.displayName ?? ""
            })
            
            let _dataArray = dataArray == nil ? [[String]]() : [dataArray!]
            let currentValue = self.viewModel.documentBeans?.govtIdDocument
            self.textfieldForGovtIssuedIdType?.pickerDataSource1(
                identifier: "GovtIssuedID",
                array: _dataArray,
                currentValues: (currentValue == nil ? nil : [currentValue!]),
                delegate: self)
            
            self.populateView()
            completion?()
        }
    }
    
    private func populateView() {
        
        photoIdDetailStackView.isHidden = viewModel.documentBeans?.viewDocumentTypeName == nil

        if viewModel.documentBeans?.viewDocumentTypeName != nil {
        
            textfieldForGovtIssuedPhotoIdType.text = viewModel.documentBeans?.viewDocumentTypeName
            textfieldForGovtIssuedPhotoIdExpiryDate.text = viewModel.documentBeans?.photoGovtIdExpiryDate ?? Utility.changeDateToString(date: Date(), withFormat: "yyyy-MM-dd")
            textfieldForGovtIssuedPhotoIdNo.text = viewModel.documentBeans?.photoGovtIdNumber == nil ? nil : String(viewModel.documentBeans!.photoGovtIdNumber!)
            btnForGovtIssuedPhotoIdNoExpiry.isSelected = viewModel.documentBeans?.photoGovtIdExpiry ?? false
            
            textfieldForGovtIssuedPhotoIdExpiryDate.isHidden = btnForGovtIssuedPhotoIdNoExpiry.isSelected
        }
        
        idDetailStackView.isHidden = viewModel.documentBeans?.govtIdDocumentTypeName == nil

        if viewModel.documentBeans?.govtIdDocumentTypeName != nil {
            textfieldForGovtIssuedIdType.text = viewModel.documentBeans?.govtIdDocumentTypeName
            textfieldForGovtIssuedIdExpiryDate.text = viewModel.documentBeans?.govtIdExpiryDate ?? Utility.changeDateToString(date: Date(), withFormat: "yyyy-MM-dd")
            textfieldForGovtIssuedIdNo.text = viewModel.documentBeans?.govtIdNumber == nil ? nil : String(viewModel.documentBeans!.govtIdNumber!)
            btnForGovtIssuedIdNoExpiry.isSelected = viewModel.documentBeans?.govtIdExpiry ?? false
            
            textfieldForGovtIssuedIdExpiryDate.isHidden = btnForGovtIssuedIdNoExpiry.isSelected
        }
        
        func getUpdateDataSource(_ currentDataSource: [MasterData]?, currentSelectedElement: String?, removeElement: String?) -> (dataArray: [MasterData]?, dataStringArray: [String], index: Int?) {
            
            var newDataSource = [MasterData]()
            var newSelectedIndex: Int?
            var newDataStringArray: [String] {
                get {
                    return newDataSource.map({ (obj) -> String in
                        return obj.displayName ?? ""
                    })
                }
            }
            
            guard let _currentDataSource = currentDataSource else {
                return (newDataSource, newDataStringArray, newSelectedIndex)
            }
            
            
            for (index, value) in _currentDataSource.enumerated() {
                if value.displayName != (removeElement ?? "") {
                    newDataSource.append(value)
                }
                if value.displayName == (currentSelectedElement ?? "") {
                    newSelectedIndex = index
                }
            }
            return (newDataSource, newDataStringArray, newSelectedIndex)
        }
        
        //To make both option mutually exclusive we remove the option selected in one from other
        let idTypeDataSourceDetail = getUpdateDataSource(viewModel.mainIdType, currentSelectedElement: viewModel.documentBeans?.govtIdDocumentTypeName, removeElement: viewModel.documentBeans?.viewDocumentTypeName)
        viewModel.idType = (idTypeDataSourceDetail.dataArray?.count == 0) ? nil : idTypeDataSourceDetail.dataArray
        textfieldForGovtIssuedIdType.updatePickerDataArray(dataSource: [idTypeDataSourceDetail.dataStringArray], selectedIndex: (idTypeDataSourceDetail.index == nil ? [Int]() : [idTypeDataSourceDetail.index!]))
        
        let photoIdTypeDataSourceDetail = getUpdateDataSource(viewModel.mainPhotoIdType, currentSelectedElement: viewModel.documentBeans?.viewDocumentTypeName, removeElement: viewModel.documentBeans?.govtIdDocumentTypeName)
        viewModel.photoIdType = (photoIdTypeDataSourceDetail.dataArray?.count == 0) ? nil : photoIdTypeDataSourceDetail.dataArray
        textfieldForGovtIssuedPhotoIdType.updatePickerDataArray(dataSource: [photoIdTypeDataSourceDetail.dataStringArray], selectedIndex: (photoIdTypeDataSourceDetail.index == nil ? [Int]() : [photoIdTypeDataSourceDetail.index!]))
    }
    
    override func btnActionForSave(_ sender: UIButton) {
        super.btnActionForSave(sender)
        
        viewModel.reset()
        
        if viewModel.documentBeans?.viewDocumentTypeName != nil {
            
            if !btnForGovtIssuedPhotoIdNoExpiry.isSelected {
                
                viewModel.documentBeans?.photoGovtIdExpiryDate = textfieldForGovtIssuedPhotoIdExpiryDate.isEmpty("Expiry Date")
                viewModel.isValid(error: textfieldForGovtIssuedPhotoIdExpiryDate.errorMsg)
            }
            
            if let no = textfieldForGovtIssuedPhotoIdNo.isEmpty("ID number") {
                viewModel.documentBeans?.photoGovtIdNumber = no
            }
            else {
                viewModel.documentBeans?.photoGovtIdNumber = nil
            }
            viewModel.isValid(error: textfieldForGovtIssuedPhotoIdNo.errorMsg)
            
            lblForPhotoIdDocumentError.isHidden = true
            if viewModel.documentBeans?.photoGovtIdDocument == nil && viewModel.photoIdFileData == nil  {
                lblForPhotoIdDocumentError.isHidden = false
                lblForPhotoIdDocumentError.text = ERROR_REQUIRED_FIELD
                viewModel.isValid(error: lblForPhotoIdDocumentError.text)
            }
        }
        
        if viewModel.documentBeans?.govtIdDocumentTypeName != nil {
            
            if !btnForGovtIssuedIdNoExpiry.isSelected {
                
                viewModel.documentBeans?.govtIdExpiryDate = textfieldForGovtIssuedIdExpiryDate.isEmpty("Expiry Date")
                viewModel.isValid(error: textfieldForGovtIssuedIdExpiryDate.errorMsg)
            }
            
            if let no = textfieldForGovtIssuedIdNo.isEmpty("ID number") {
                viewModel.documentBeans?.govtIdNumber = no
            }
            else {
                viewModel.documentBeans?.govtIdNumber = nil
            }
            viewModel.isValid(error: textfieldForGovtIssuedIdNo.errorMsg)
            
            lblForIdDocumentError.isHidden = true
            if viewModel.documentBeans?.govtIdDocument == nil && viewModel.idFileData == nil  {
                lblForIdDocumentError.isHidden = false
                lblForIdDocumentError.text = ERROR_REQUIRED_FIELD
                viewModel.isValid(error: lblForIdDocumentError.text)
            }
        }
        
        if viewModel.documentBeans?.govtIdDocumentTypeName == nil && viewModel.documentBeans?.viewDocumentTypeName == nil {
            self.showAlert(message: "Please provide at least one id", withOk: true)
            return
        }
        
        showLoader()
        viewModel.hitUpdateIdDetail(completion: { (message) in
            self.hideLoader()
        }, success: { (message) in
            self.dismiss(animated: true, completion: {
//                APPLICATION_INSTANCE.visibleViewController()?.showAlert(message: message)
            })
        }, failure: { (message) in
            self.showAlert(message: message, withOk: true)
        })
    }
    
    @IBAction func btnActionForGovtIdNoExpiry(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        viewModel.documentBeans?.govtIdExpiry = sender.isSelected
        viewModel.documentBeans?.govtIdExpiryDate = nil
        populateView()
    }

    @IBAction func btnActionForGovtPhotoIdNoExpiry(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        viewModel.documentBeans?.photoGovtIdExpiry = sender.isSelected
        viewModel.documentBeans?.photoGovtIdExpiryDate = nil
        populateView()
    }
    
    @IBAction func btnActionForUploadPhotoId(_ sender: UIButton) {
        
        lblForPhotoIdDocumentError.isHidden = true
        downloadDelegate.fetchFile { (fileDetail) in
            
            if let errorMessage = DataValidationUtility.shared.validateSize(5000000, fileDetail: fileDetail, fileTypeAllowed: [
                FileType.pdf,
                FileType.doc,
                FileType.docx,
                FileType.jpeg,
                FileType.jpg,
                FileType.png,
                FileType.tiff
                ]).message {
                self.lblForPhotoIdDocumentError.isHidden = false
                self.lblForPhotoIdDocumentError.text = errorMessage
            }
            else {
                self.lblForPhotoIdDocumentError.text = nil
                self.viewModel.photoIdFileData = fileDetail
                self.lblForPhotoIdDocFileName.text = fileDetail.name
            }
        }
    }
    
    @IBAction func btnActionForUploadId(_ sender: UIButton) {
        
        lblForIdDocumentError.isHidden = true
        
        downloadDelegate.fetchFile { (fileDetail) in
            
            if let errorMessage = DataValidationUtility.shared.validateSize(5000000, fileDetail: fileDetail, fileTypeAllowed: [
                FileType.pdf,
                FileType.doc,
                FileType.docx,
                FileType.jpeg,
                FileType.jpg,
                FileType.png,
                FileType.tiff
                ]).message {
                self.lblForIdDocumentError.isHidden = false
                self.lblForIdDocumentError.text = errorMessage
            }
            else {
                self.lblForIdDocumentError.text = nil
                self.viewModel.idFileData = fileDetail
                self.lblForIdDocFileName.text = fileDetail.name
            }
        }
    }
}

extension IDInfoEditViewController: CustomTextfieldViewDelegate {
    
    func textFieldDidBeginEditing(_ identifier: String, textField: CustomTextfield) {
        
        if identifier == "GovtIssuedPhotoID" && viewModel.photoIdType == nil {
            self.view.endEditing(true)
            setPhotoIdTypeDataSource(completion: {
                self.textfieldForGovtIssuedPhotoIdType.setAsFirstResponder()
            })
        }
        else if identifier == "GovtIssuedID" && viewModel.idType == nil {
            self.view.endEditing(true)
            setIdTypeDataSource(completion: {
                self.textfieldForGovtIssuedIdType.setAsFirstResponder()
            })
        }
    }
    
    func textFieldDidEndEditing(_ identifier: String, textField: CustomTextfield) {
        
        if identifier == "IDExpiryDate" {
            viewModel.documentBeans?.govtIdExpiryDate = textField.text
        }
        else if identifier == "PhotoIDExpiryDate" {
            viewModel.documentBeans?.photoGovtIdExpiryDate = textField.text
        }
        else if identifier == "PhotoIDNumber" {
            viewModel.documentBeans?.photoGovtIdNumber = textField.text
        }
        else if identifier == "IDNumber" {
            viewModel.documentBeans?.govtIdNumber = textField.text
        }
    }

    func pickerViewSelected(identifier: String, textfield: CustomTextfield, indexes: [Int]) {
        
        guard let firstIndex = indexes.first else {
            return
        }
        
        print("identifier \(identifier) indexes\(firstIndex)")
        if identifier == "GovtIssuedPhotoID" {
            self.view.endEditing(true)
            photoIdDetailStackView.isHidden = (firstIndex == 0)
            
            if (firstIndex == 0) {
                viewModel.documentBeans?.viewDocumentTypeName = nil
                viewModel.documentBeans?.photoGovtIdDocument = nil
                viewModel.documentBeans?.photoGovtIdExpiryDate = nil
                viewModel.documentBeans?.photoGovtIdNumber = nil
                viewModel.documentBeans?.photoGovtIdExpiry = nil
            }
            else {
                viewModel.documentBeans?.encPhotoGovtIdDocumentType = viewModel.photoIdType?[firstIndex].encryptedId
                viewModel.documentBeans?.viewDocumentTypeName = viewModel.photoIdType?[firstIndex].displayName
            }
        }
        else if identifier == "GovtIssuedID" {
            self.view.endEditing(true)
            idDetailStackView.isHidden = (firstIndex == 0)
            
            if (firstIndex == 0) {
                viewModel.documentBeans?.govtIdDocumentTypeName = nil
                viewModel.documentBeans?.govtIdDocument = nil
                viewModel.documentBeans?.govtIdExpiryDate = nil
                viewModel.documentBeans?.govtIdNumber = nil
                viewModel.documentBeans?.govtIdExpiry = nil
            }
            else {
                viewModel.documentBeans?.encGovtIdDocumentType = viewModel.idType?[firstIndex].encryptedId
                viewModel.documentBeans?.govtIdDocumentTypeName = viewModel.idType?[firstIndex].displayName
            }
        }
        self.populateView()
    }
}
