//
//  IDInfoEditViewModel.swift
//  Jombone
//
//  Created by Money Mahesh on 23/04/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import UIKit

class IDInfoEditViewModel: NSObject {
    
    var documentBeans: DocumentDetailsBean?
    var photoIdFileData: FileDetail?
    var idFileData: FileDetail?
    var mainPhotoIdType: [MasterData]?
    var mainIdType: [MasterData]?

    var photoIdType: [MasterData]?
    var idType: [MasterData]?
    
    init(documentBeans: DocumentDetailsBean? = DocumentDetailsBean()) {
        self.documentBeans = documentBeans
    }
    
    private var isInputValid = true
    @discardableResult func isValid(error: String?) -> String? {
        if !isInputValid {
            return error
        }
        isInputValid = (error == nil)
        return error
    }
    
    func reset() {
        isInputValid = true
    }
    
    func hitUpdateIdDetail(completion: ((_ message: String?)->())? = nil, success:  ((_ message: String?)->())? = nil, failure: ((_ message: String?)->())? = nil) {
        
        guard let parameters = JsonUtility.jsonString(data: documentBeans), isInputValid else {
            completion?(nil)
            failure?(nil)
            return
        }
        
        let profileServices = ProfileServices(requestTag: "UPDATE_PROFILE_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        //Completion Block
        profileServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            if statusCode == 1, let documentDetailsBeans = response.0 as? [DocumentDetailsBean] {
                let profileDetail = UserDataManager.shared.profileDetail
                profileDetail?.documentBeans = documentDetailsBeans
                UserDataManager.shared.profileDetail = profileDetail
                completion?(message)
                success?(message)
            }
            else {
                completion?(message)
                failure?(message)
            }
        }
        profileServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            completion?(nil)
            failure?(nil)
        }
        
        var filesDetail: (keysName: [String], filesName: [String], filesData: [Data])?
        
        var keysName = [String]()
        var filesName = [String]()
        var filesData = [Data]()
        if let _photoIdFileData = photoIdFileData {
            keysName.append("uploadPhotoGovtId")
            filesName.append(_photoIdFileData.name)
            filesData.append(_photoIdFileData.data)
        }
        if let _idFileData = idFileData {
            keysName.append("uploadGovtId")
            filesName.append(_idFileData.name)
            filesData.append(_idFileData.data)
        }
        
        if keysName.count > 0 {
            filesDetail =
                (keysName: keysName,
                 filesName: filesName,
                 filesData: filesData)
        }
        
        profileServices.updateIdDetail(filesDetail: filesDetail, parameters: ["documentDetailsString": parameters], additionalHeaderElements: nil)
    }
}
