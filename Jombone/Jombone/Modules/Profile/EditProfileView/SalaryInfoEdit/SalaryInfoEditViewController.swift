//
//  SalaryInfoEditViewController.swift
//  Jombone
//
//  Created by Money Mahesh on 23/04/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import UIKit

class SalaryInfoEditViewController: EditProfileBaseViewController {
    
    @IBOutlet weak var textfieldForSalaryDepositType: CustomTextfieldView!
    @IBOutlet weak var textfieldForEmail: CustomTextfieldView!
    @IBOutlet weak var textfieldForConfirmEmail: CustomTextfieldView!
    @IBOutlet weak var textfieldForSin: CustomTextfieldView!

    @IBOutlet weak var textfieldForBranch_TransitNumber: CustomTextfieldView!
    @IBOutlet weak var textfieldForInsititutionNumber: CustomTextfieldView!
    @IBOutlet weak var textfieldForAccountNumber: CustomTextfieldView!
    @IBOutlet weak var lblForVoidCheque_DirectDeposit: UILabel!

    @IBOutlet weak var stackViewForDirectDeposit: UIStackView!
    @IBOutlet weak var stackViewForEmailTransfer: UIStackView!

    @IBOutlet weak var lblForDocumentError: UILabel!
    
    var viewModel = SalaryInfoEditViewModel()
    var downloadDelegate = DownloadUploadAction()

    class func show() {
        
        let viewController = PROFILE_STORYBOARD.instantiateViewController(withIdentifier: "SalaryInfoEditViewController") as! SalaryInfoEditViewController
        
        APPLICATION_INSTANCE.visibleViewController()?.present(viewController, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setTextfieldPreferences()
        self.populateView()
    }
    
    private func setTextfieldPreferences() {
        
        //Set Textfield Preferences
        textfieldForEmail.borderStyle = .All
        textfieldForEmail.setJomboneEmailStyle()
        textfieldForEmail.textfieldViewDelegate = self

        textfieldForConfirmEmail.borderStyle = .All
        textfieldForConfirmEmail.setJomboneEmailStyle()
        textfieldForConfirmEmail.textfieldViewDelegate = self

        textfieldForSin.borderStyle = .All
        textfieldForSin.setJomboneFontStyle()
        textfieldForSin.textfieldViewDelegate = self
        textfieldForSin.inputType = .Keyboard(type: .numberPad)

        textfieldForBranch_TransitNumber.borderStyle = .All
        textfieldForBranch_TransitNumber.setJomboneFontStyle()
        textfieldForBranch_TransitNumber.inputType = .Keyboard(type: .numberPad)
        textfieldForBranch_TransitNumber.textfieldViewDelegate = self

        textfieldForInsititutionNumber.borderStyle = .All
        textfieldForInsititutionNumber.setJomboneFontStyle()
        textfieldForInsititutionNumber.inputType = .Keyboard(type: .numberPad)
        textfieldForInsititutionNumber.textfieldViewDelegate = self

        textfieldForAccountNumber.borderStyle = .All
        textfieldForAccountNumber.setJomboneFontStyle()
        textfieldForAccountNumber.inputType = .Keyboard(type: .numberPad)
        textfieldForAccountNumber.textfieldViewDelegate = self

        textfieldForSalaryDepositType.borderStyle = .All
        textfieldForSalaryDepositType.setJomboneFontStyle()
    
        let currentValue = self.viewModel.bankAccountBean?.transferTypeName
        self.textfieldForSalaryDepositType?.pickerDataSource1(
            identifier: "DepositSalaryBy",
            array: [[String]](),
            currentValues: (currentValue == nil ? nil : [currentValue!]),
            delegate: self)
        setSalaryDepositDataSource()
    }
    
    private func setSalaryDepositDataSource(completion: (()->())? = nil) {
    
        showLoader()
        MasterDataManager.shared.salaryTransferTypes { (data: [MasterData]?) in
            
            self.hideLoader()
            self.viewModel.salaryDepositType = data
            
            let dataArray = data?.map({ (obj: MasterData) -> String in
                return obj.displayName ?? ""
            }) ?? [String]()
            
            let currentValue = self.viewModel.bankAccountBean?.transferTypeName ?? dataArray.first
            self.textfieldForSalaryDepositType?.pickerDataSource1 (
                identifier: "DepositSalaryBy",
                array: [dataArray],
                currentValues: (currentValue == nil ? nil : [currentValue!]),
                defaultValueIndex: [0],
                delegate: self)
            self.populateView()
            completion?()
        }
    }
    
    private func populateView() {
        

        if let transferTypeName = viewModel.bankAccountBean?.transferTypeName, transferTypeName != "Direct Transfer" {
            
            stackViewForDirectDeposit.isHidden = true
            stackViewForEmailTransfer.isHidden = false
            textfieldForEmail.text = viewModel.bankAccountBean?.bankAccountEmail
            textfieldForConfirmEmail.text = viewModel.bankAccountBean?.bankAccountConfirmEmail
        }
        else {
            viewModel.bankAccountBean?.encTransferType = viewModel.salaryDepositType?[0].encryptedId
            viewModel.bankAccountBean?.transferTypeName = viewModel.salaryDepositType?[0].name
            
            stackViewForEmailTransfer.isHidden = true
            stackViewForDirectDeposit.isHidden = false
            textfieldForBranch_TransitNumber.text = viewModel.bankAccountBean?.branch
            textfieldForInsititutionNumber.text = viewModel.bankAccountBean?.institution
            textfieldForAccountNumber.text = viewModel.bankAccountBean?.accountNumber
            lblForVoidCheque_DirectDeposit.text = viewModel.bankAccountBean?.accountDocFileDisplayName ?? "Void cheque/Direct deposit"

        }
        
        textfieldForSin.text = viewModel.bankAccountBean?.sinNumber
    }
    
    override func btnActionForSave(_ sender: UIButton) {
        super.btnActionForSave(sender)
        
        viewModel.reset()
        
        if let transferTypeName = viewModel.bankAccountBean?.transferTypeName, transferTypeName != "Direct Transfer" {
            viewModel.bankAccountBean?.bankAccountEmail =  textfieldForEmail.isValidEmail()
            viewModel.bankAccountBean?.bankAccountConfirmEmail =  textfieldForConfirmEmail.isValidEmail()
            
            if textfieldForConfirmEmail.errorMsg == nil && textfieldForConfirmEmail.text != textfieldForEmail.text {
                textfieldForConfirmEmail.errorMsg = ERROR_CONFIRM_Email
            }
            viewModel.isValid(error: textfieldForConfirmEmail.errorMsg)
            
        }
        else {
            viewModel.bankAccountBean?.branch =  textfieldForBranch_TransitNumber.isOfLength("Branch/Transit Number", lenght: 5, customMessage: "Please enter valid 5 digit Branch/Transit Number")
            viewModel.isValid(error: textfieldForBranch_TransitNumber.errorMsg)
            
            viewModel.bankAccountBean?.accountNumber =  textfieldForAccountNumber.isValidAccountNumber()
            viewModel.isValid(error: textfieldForAccountNumber.errorMsg)

            viewModel.bankAccountBean?.institution =  textfieldForInsititutionNumber.isOfLength("Institution number", lenght: 3, customMessage: "Please enter a valid 3 digit Institution number")
            viewModel.isValid(error: textfieldForInsititutionNumber.errorMsg)
            
            if viewModel.bankAccountBean?.accountDetailsDocumentPath == nil && viewModel.fileData == nil  {
                lblForDocumentError.isHidden = false
                lblForDocumentError.text = ERROR_REQUIRED_FIELD
                viewModel.isValid(error: lblForDocumentError.text)
            }
        }
        
        viewModel.bankAccountBean?.sinNumber = textfieldForSin.isValidSIN()
        if let text = textfieldForSin.text, text.count > 0 {
            viewModel.isValid(error: textfieldForSin.errorMsg)
        }
        
        showLoader()
        viewModel.hitUpdateSalaryDetail(completion: { (message) in
            self.hideLoader()
        }, success: { (message) in
            self.dismiss(animated: true, completion: {
                APPLICATION_INSTANCE.visibleViewController()?.showAlert(message: message)
            })
        }, failure: { (message) in
            self.showAlert(message: message, withOk: true)
        })
    }
    
    
    @IBAction func btnActionForUpload(_ sender: UIButton) {
        
        lblForDocumentError.isHidden = true
        downloadDelegate.fetchFile { (fileDetail) in
            
            self.viewModel.bankAccountBean?.accountDetailsDocumentPath = nil
            if let errorMessage = DataValidationUtility.shared.validateSize(5000000, fileDetail: fileDetail, fileTypeAllowed: [
                FileType.pdf,
                FileType.doc,
                FileType.docx,
                FileType.jpg,
                FileType.jpeg,
                FileType.png
                ]).message {
                self.lblForDocumentError.isHidden = false
                self.lblForDocumentError.text = errorMessage
                self.viewModel.bankAccountBean?.accountDetailsDocumentPath = nil
                self.lblForVoidCheque_DirectDeposit.text = "Void cheque/Direct deposit"

            }
            else {
                self.lblForDocumentError.text = nil
                self.viewModel.fileData = fileDetail
                self.lblForVoidCheque_DirectDeposit.text = fileDetail.name
                self.viewModel.bankAccountBean?.accountDocFileDisplayName = fileDetail.name
            }
        }
    }
}

extension SalaryInfoEditViewController: CustomTextfieldViewDelegate {
    
    func textFieldDidBeginEditing(_ identifier: String, textField: CustomTextfield) {
        
        if identifier == "DepositSalaryBy" && viewModel.salaryDepositType == nil {
            self.view.endEditing(true)
            setSalaryDepositDataSource(completion: {
                self.textfieldForSalaryDepositType.setAsFirstResponder()
            })
            print(viewModel.salaryDepositType ?? "Data unavailable")
        }
    }
    
    func textFieldDidEndEditing(_ identifier: String, textField: CustomTextfield) {
        
        if identifier == "Email" {
            viewModel.bankAccountBean?.bankAccountEmail = textField.text
        }
        else if identifier == "ConfirmEmail" {
            viewModel.bankAccountBean?.bankAccountConfirmEmail = textField.text
        }
        else if identifier == "Sin" {
            viewModel.bankAccountBean?.sinNumber = textField.text
        }
        else if identifier == "BranchTransitNumber" {
            viewModel.bankAccountBean?.branch = textField.text
        }
        else if identifier == "InsititutionNumber" {
            viewModel.bankAccountBean?.institution = textField.text
        }
        else if identifier == "AccountNumber" {
            viewModel.bankAccountBean?.accountNumber = textField.text
        }
    }
    
    func pickerViewSelected(identifier: String, textfield: CustomTextfield, indexes: [Int]) {
        
        guard let firstIndex = indexes.first else {
            return
        }

        if viewModel.bankAccountBean?.transferTypeName != viewModel.salaryDepositType?[firstIndex].name {
            let sinNo = viewModel.bankAccountBean?.sinNumber
            viewModel.resetModel()
            viewModel.bankAccountBean?.sinNumber = sinNo
        }
        print("identifier \(identifier) indexes\(firstIndex)")
        if identifier == "DepositSalaryBy" {
            viewModel.bankAccountBean?.encTransferType = viewModel.salaryDepositType?[firstIndex].encryptedId
            viewModel.bankAccountBean?.transferTypeName = viewModel.salaryDepositType?[firstIndex].name
            
            self.populateView()
        }
    }
}
