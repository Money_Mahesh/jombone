//
//  SalaryInfoEditViewModel.swift
//  Jombone
//
//  Created by Money Mahesh on 23/04/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import UIKit

class SalaryInfoEditViewModel: NSObject {
    
    var bankAccountBean: BankAccountBean?
    var salaryDepositType: [MasterData]?
    var fileData: FileDetail?

    override init() {
        self.bankAccountBean = UserDataManager.shared.profileDetail?.bankAccountBean ?? BankAccountBean()
    }
    
    private var isInputValid = true
    @discardableResult func isValid(error: String?) -> String? {
        if !isInputValid {
            return error
        }
        isInputValid = (error == nil)
        return error
    }
    
    func reset() {
        isInputValid = true
    }
    
    func resetModel() {
        self.bankAccountBean = BankAccountBean()
    }
    
    func hitUpdateSalaryDetail(completion: ((_ message: String?)->())? = nil, success:  ((_ message: String?)->())? = nil, failure: ((_ message: String?)->())? = nil) {
        
        guard let parameters = JsonUtility.jsonString(data: bankAccountBean), isInputValid else {
            completion?(nil)
            failure?(nil)
            return
        }
        
        let profileServices = ProfileServices(requestTag: "UPDATE_PROFILE_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        //Completion Block
        profileServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            if statusCode == 1, let bankAccountBean = response.0 as? BankAccountBean {
                let profileDetail = UserDataManager.shared.profileDetail
                profileDetail?.bankAccountBean = bankAccountBean
                UserDataManager.shared.profileDetail = profileDetail
                completion?(message)
                success?(message)
            }
            else {
                completion?(message)
                failure?(message)
            }
        }
        profileServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            completion?(nil)
            failure?(nil)
        }
        
        var filesDetail: (keysName: [String], filesName: [String], filesData: [Data])?
        if let _fileData = fileData {
            filesDetail = (keysName: ["uploadedFile"], filesName: [_fileData.name], filesData: [_fileData.data])
        }

        profileServices.updateSalaryDetail(filesDetail: filesDetail, parameters: ["bankAccountBeanString": parameters], additionalHeaderElements: nil)

    }
}
