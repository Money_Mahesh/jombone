//
//  AddressView.swift
//  Jombone
//
//  Created by Money Mahesh on 12/11/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import Foundation
import UIKit

class AddressView: NSObject {
    
    var mainView: CommonBaseView!
    var lbl = UILabel()
    
    override init() {
        super.init()
        
        lbl.font = UIFont.rubikRegularFontOfSize(14)
        lbl.textColor = UIColor(hexFromString: "#424242")
        lbl.isHidden = true
        lbl.numberOfLines = 0
        
        mainView = CommonBaseView(frame: CGRect(), data: CommonBaseData(title: "Address Details", btnType: CommonBaseData.BtnType.add(line: false)))
        mainView.btn.addTarget(self,action:#selector(AddressView.editBtnAction(sender:)), for:.touchUpInside)
        
        mainView.stackView.addArrangedSubview(lbl)
        populateView()
        
        addObserver(self, selector: #selector(AddressView.populateView), notificationsName: [Notification.Name.UserProfileUpdated])
    }
    
    @objc func populateView() {
        
        let available = (UserDataManager.shared.profileDetail?.addressBean?.completeAddress ?? "").count > 0
        mainView.populate(btnType: available ? .edit(line: true) : .add(line: false))
        
        lbl.isHidden = !available
        lbl.text = UserDataManager.shared.profileDetail?.addressBean?.completeAddress
    }
    
    @objc func editBtnAction(sender: UIButton) {
        print("AddressEditController ----Edit")

        AddressEditController.show()
//        AutoCompleteViewController.show(viewModel: AutoCompleteViewModel(selectedOptions: nil, type: AutoComplete.google(GooglePlaceFor: GooglePlaceFor.address), noOfSelection: .single), delegate: self)
    }
}

extension AddressView: AutoCompleteViewDelegate {
    func selectedOption(type: AutoComplete, optionSelected: Any?) {
        
        UserDataManager.shared.profileDetail?.addressBean?.completeAddress = (optionSelected as? [AutoCompleteCardViewModelProtocol])?.first?.title
        print((optionSelected as? [AutoCompleteCardViewModelProtocol])?.first?.title)
    }
}
