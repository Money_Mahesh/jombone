//
//  PhysicalDemandAnalysisView.swift
//  Jombone
//
//  Created by Money Mahesh on 10/04/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import UIKit

class PhysicalDemandAnalysisView: NSObject {
    
    var mainView: CommonBaseView!
    var physicalDemandAnalysisDetailView = PhysicalDemandAnalysisDetailView()
    
    override init() {
        super.init()
        
        mainView = CommonBaseView(frame: CGRect(), data: CommonBaseData(title: "Physical Demand Analysis", btnType: CommonBaseData.BtnType.none(line: true)))
        mainView.stackView.addArrangedSubview(physicalDemandAnalysisDetailView)
    }
}
