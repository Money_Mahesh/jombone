//
//  PhysicalDemandAnalysisDetailView.swift
//  Jombone
//
//  Created by Money Mahesh on 15/04/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import UIKit

class PhysicalDemandAnalysisDetailView: CustomView {
    
    @IBOutlet weak var stackViewForDocumentDetail: UIStackView?
    @IBOutlet weak var lblForName: UILabel?

    var viewModel = PhysicalDemandAnalysisDetailViewModel()
    var downloadDelegate = DownloadUploadAction()
    
    override func xibLoaded() {
        addObserver(self, selector: #selector(PhysicalDemandAnalysisDetailView.populateView), notificationsName: [Notification.Name.UserProfileUpdated])
        populateView()
    }
    
    @objc func populateView() {
        
        let detail = UserDataManager.shared.profileDetail?.physicalAnalysisDocumentBean
        
        stackViewForDocumentDetail?.isHidden = true
        if let documentName = detail?.viewDocumentFileDisplayName {
            lblForName?.text = documentName
            stackViewForDocumentDetail?.isHidden = false
        }
    }
    
    //MARK:- IBActions
    @IBAction func btnActionForDelete(_ sender: UIButton) {
        
        APPLICATION_INSTANCE.visibleViewController()?.showLoader()
        viewModel.hitDeleteIdApi(completion: { (message) in
            APPLICATION_INSTANCE.visibleViewController()?.hideLoader()
            APPLICATION_INSTANCE.visibleViewController()?.showAlert(message: message)
        }, success: { (message) in
            self.populateView()
            ProfileViewModel.hitScoreApi()
        })
    }
    
    @IBAction func btnActionForDownloadForm(_ sender: UIButton) {
        
        downloadDelegate.download(
            fileName: UserDataManager.shared.profileDetail?.physicalAnalysisDocumentBean?.physicalAnalysisDownloadFormName,
            urlStr: UserDataManager.shared.profileDetail?.physicalAnalysisDocumentBean?.physicalAnalysisFormPath)
    }
    
    @IBAction func fileLinkTapAction(_ sender: UITapGestureRecognizer) {
        
        if let fileExtension = UserDataManager.shared.profileDetail?.physicalAnalysisDocumentBean?.viewDocumentFileName?.split(separator: ".").last, let fileName = UserDataManager.shared.profileDetail?.physicalAnalysisDocumentBean?.viewDocumentFileDisplayName {
            
            downloadDelegate.download(
                fileName: (fileName + "." + fileExtension),
                urlStr: UserDataManager.shared.profileDetail?.physicalAnalysisDocumentBean?.documentFilePath)
        }
    }
    
    @IBAction func btnActionForUpload(_ sender: UIButton) {
        PhysicalAnalysisDocumentInfoEditViewController.show()
    }
}
