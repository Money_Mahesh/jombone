//
//  PhysicalDemandAnalysisDetailViewModel.swift
//  Jombone
//
//  Created by Money Mahesh on 15/04/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation

class PhysicalDemandAnalysisDetailViewModel: NSObject {
    
    private func deleteFile() {
        
        UserDataManager.shared.profileDetail?.physicalAnalysisDocumentBean?.documentFileEncId = nil
        UserDataManager.shared.profileDetail?.physicalAnalysisDocumentBean?.viewDocumentFileName = nil
        UserDataManager.shared.profileDetail?.physicalAnalysisDocumentBean?.viewDocumentFileDisplayName = nil
        UserDataManager.shared.profileDetail?.physicalAnalysisDocumentBean?.documentFilePath = nil
    }
    
    func hitDeleteIdApi(completion: ((_ message: String?)->())? = nil, success:  ((_ message: String?)->())? = nil, failure: ((_ message: String?)->())? = nil) {
        
        guard let documentEncId = UserDataManager.shared.profileDetail?.physicalAnalysisDocumentBean?.documentEncId else {
            completion?(nil)
            failure?(nil)
            return
        }
        
        let profileServices = ProfileServices(requestTag: "DELETE_ID_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        //Completion Block
        profileServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            if statusCode == 1 {
                
                self.deleteFile()
                completion?(message)
                success?(message)
            }
            else {
                completion?(message)
                failure?(message)
            }
        }
        profileServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            completion?(nil)
            failure?(nil)
        }
        
        let parameters: [String: Any] = [
            "encDocId": documentEncId
        ]
        profileServices.deleteID(parameters: parameters, additionalHeaderElements: nil)
    }
    
    func hitUploadFile(_ uploadFilePath: String, fileName: String, file: Data, keyName: String, parameter: [String : Any]?, completion: ((_ status: Bool, _ message: String?, _ url: URL?)->())?) {
        
        let downloadService = DownloadUploadService(requestTag: "DOWNLOAD_FILE_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        APPLICATION_INSTANCE.visibleViewController()?.showLoader()
        //Completion Block
        downloadService.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            APPLICATION_INSTANCE.visibleViewController()?.hideLoader()
            if let url = response.0 as? URL {
                completion?(true, message, url)
            }
            else {
                APPLICATION_INSTANCE.visibleViewController()?.showAlert(message: message)
                completion?(false , message, nil)
            }
        }
        downloadService.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            completion?(false, nil, nil)
        }
        
//        downloadService.hitUploadFile(uploadFilePath, keyName: keyName, fileData: file, fileName: fileName, parameters: parameter, additionalHeaderElements: nil, completion: nil)
    }
}
