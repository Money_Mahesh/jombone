//
//  EmploymentCardViewModel.swift
//  Jombone
//
//  Created by dev139 on 28/04/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import UIKit


class EmploymentCardViewModel: NSObject {

    func hitDeleteEmploymentApi(encEmploymentId: String?, completion: ((_ message: String?)->())? = nil, success:  ((_ message: String?)->())? = nil, failure: ((_ message: String?)->())? = nil) {
        
        guard let encEmpId = encEmploymentId else {
            completion?(nil)
            failure?(nil)
            return
        }
        
        let profileServices = ProfileServices(requestTag: "DELETE_EDUCATION_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        //Completion Block
        profileServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            if statusCode == 1 {
                
                UserDataManager.shared.profileDetail?.employmentBeans = response.0 as? [EmploymentDetailsBean]
                completion?(message)
                success?(message)
            }
            else {
                completion?(message)
                failure?(message)
            }
        }
        profileServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            completion?(nil)
            failure?(nil)
        }
        
        let parameters: [String: Any] = [
            "encEmpId": encEmpId
        ]
        profileServices.deleteEmployment(parameters: parameters, additionalHeaderElements: nil)
    }
}
