//
//  EmploymentTableViewCell.swift
//  Jombone
//
//  Created by dev139 on 28/04/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import UIKit

protocol ReloadCellProtocol {
    func reloadCell(sender: UIButton, isExpanded: Bool)
    func deleteCell(sender: UIButton)
}


class EmploymentTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblForCompanyName: UILabel?
    @IBOutlet weak var lblForCompanyAddress: UILabel?
    @IBOutlet weak var lblForPosition: UILabel?
    @IBOutlet weak var lblForTenure: UILabel?
    
    @IBOutlet weak var referenceDetailBtn: UIButton?
    @IBOutlet weak var arrowBtn: UIButton?
    
    @IBOutlet weak var lblForRefernceName: UILabel?
    @IBOutlet weak var lblForRefernceEmail: UILabel?
    @IBOutlet weak var lblForRefernceMobile: UILabel?
    @IBOutlet weak var referenceDetailStackView: UIStackView?
    @IBOutlet weak var referenceHeaderStackView: UIStackView?
    @IBOutlet weak var stackViewForBtn: UIStackView?

    var indexPath : IndexPath!
    var delegate: ReloadCellProtocol?
    var employmentData: EmploymentDetailsBean!
    var viewModel = EmploymentCardViewModel()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func bindData(employmentData: EmploymentDetailsBean, indexPath : IndexPath) {
        
        self.indexPath = indexPath
        self.employmentData = employmentData
        lblForCompanyName?.text = employmentData.employerName
        
        if let count = employmentData.unitNumber?.count, count > 0 {
            lblForCompanyAddress?.text =  (employmentData.unitNumber! + ", " +  (employmentData.employerAddress ?? ""))
        } else {
            lblForCompanyAddress?.text =  employmentData.employerAddress
        }
        lblForPosition?.text = employmentData.designation
        
        if let fromDate = employmentData.fromDate {
            lblForTenure?.text = fromDate + (employmentData.toDate != nil ? (" - " + (employmentData.toDate ?? "")) : " - Present")
        }
        
        if let referenceName = employmentData.referenceName {
            lblForRefernceName?.text = referenceName
            lblForRefernceEmail?.text = employmentData.referenceEmail ?? "N/A"

            lblForRefernceMobile?.text = "N/A"
            if let _referenceMobile = employmentData.referenceMobile, let referenceCountryCode = employmentData.referenceCountryCode, _referenceMobile.count > 0, referenceCountryCode.count > 0 {
                
                lblForRefernceMobile?.text = referenceCountryCode + " " + _referenceMobile
            }
            
            referenceDetailBtn?.tag = indexPath.row
            arrowBtn?.tag = indexPath.row
            
            referenceDetailStackView?.isHidden = false
            referenceHeaderStackView?.isHidden = false
            
            toggleCell()
        }
        else {
            referenceDetailStackView?.isHidden = true
            referenceHeaderStackView?.isHidden = true
        }
        
        stackViewForBtn?.isHidden = !(employmentData.job == nil)
    }
    
    func toggleCell() {
        
            if let isExpanded = employmentData.isExpanded, isExpanded {
                arrowBtn?.setImageForAllState(image: UIImage(named: "dropUpBlueArrow"))
                referenceDetailStackView?.isHidden = false
            }
            else {
                arrowBtn?.setImageForAllState(image: UIImage(named: "dropDownBlueArrow"))
                referenceDetailStackView?.isHidden = true
        }
    }
    
    @IBAction func referenceDetailBtnAction(_ sender: UIButton) {
        
        var isCellExpanded = false
        if let isHidden = referenceDetailStackView?.isHidden, isHidden {
            isCellExpanded = true
        }
        delegate?.reloadCell(sender: sender, isExpanded: isCellExpanded)
    }
    
    @IBAction func deleteBtnAction(_ sender: UIButton) {
        
        APPLICATION_INSTANCE.visibleViewController()?.showAlert("Confirm Delete", message: "You are about to delete one record, this procedure is irreversible. Do you want to proceed?", style: .alert, withOk: false, withCancel: false, withAnimation: true, withCustomAction: [UIAlertAction(title: "Cancel", style: .cancel, handler: nil), UIAlertAction(title: "Delete", style: .destructive, handler: { (action) in
            APPLICATION_INSTANCE.visibleViewController()?.showLoader()
            self.viewModel.hitDeleteEmploymentApi(encEmploymentId: self.employmentData.employmentIdEnc, completion: { (message) in
                APPLICATION_INSTANCE.visibleViewController()?.hideLoader()
                APPLICATION_INSTANCE.visibleViewController()?.showAlert(message: message)
            }, success: { (message) in
                self.delegate?.deleteCell(sender: sender)
                ProfileViewModel.hitScoreApi()
            })
        })])
    
    }
    
    @IBAction func editBtnAction(_ sender: UIButton) {
        EmploymentInfoEditViewController.show(viewModel: EmploymentInfoEditViewModel(employmentBeans: employmentData))
    }
}
