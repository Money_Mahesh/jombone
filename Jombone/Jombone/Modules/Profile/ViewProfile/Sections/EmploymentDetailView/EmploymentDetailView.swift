//
//  EmploymentDetailView.swift
//  Jombone
//
//  Created by Money Mahesh on 10/04/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import UIKit

class EmploymentDetailView: NSObject {
    
    var mainView: CommonBaseView!
    var employmentsListView = EmploymentsListView()

    
    override init() {
        super.init()
        
        mainView = CommonBaseView(frame: CGRect(), data: CommonBaseData(title: "Employment Details", btnType: CommonBaseData.BtnType.add(line: false)))
        mainView.btn.addTarget(self, action: #selector(EmploymentDetailView.editBtnAction), for: UIControl.Event.touchUpInside)
        
        addObserver(self, selector: #selector(EmploymentDetailView.populateView), notificationsName: [Notification.Name.UserProfileUpdated])
        
        mainView.stackView.addArrangedSubview(employmentsListView)
        
        populateView()
    }
    
    @objc func populateView() {
        
        let available = (UserDataManager.shared.profileDetail?.employmentBeans?.count ?? 0) != 0
        mainView.populate(btnType: available ? .add(line: true) : .add(line: false))
        employmentsListView.isHidden = !available
        employmentsListView.populateData()
    }
    
    @objc func editBtnAction() {
        print("----Edit")
        EmploymentInfoEditViewController.show(viewModel: EmploymentInfoEditViewModel())
    }
}
