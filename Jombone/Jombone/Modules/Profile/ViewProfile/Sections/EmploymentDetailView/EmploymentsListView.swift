//
//  EmploymentsListView.swift
//  Jombone
//
//  Created by dev139 on 28/04/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import UIKit

class EmploymentsListView: CustomView {

    struct DataStructure {
        var title: String!
        var dataSource = [EmploymentDetailsBean]()
    }
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var constForTableHeight: NSLayoutConstraint!
    
    var dataSource = [DataStructure]()
    
    override func xibLoaded() {
        
        tableView.register(UINib(nibName: "EmploymentTableViewCell", bundle: nil), forCellReuseIdentifier: "EmploymentTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 330
        
        tableView.addObserver(self, forKeyPath: "contentSize", options: [NSKeyValueObservingOptions.new, NSKeyValueObservingOptions.old], context: nil)
        tableView.isScrollEnabled = false
        constForTableHeight.isActive = true
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if let change = change, let oldvalue = change[NSKeyValueChangeKey.oldKey] as? CGSize,
            let newvalue = change[NSKeyValueChangeKey.newKey] as? CGSize,
            let table = tableView, oldvalue != newvalue {
            
            let newHeight = table.contentSize.height
            table.layer.removeAllAnimations()
            constForTableHeight?.constant = newHeight
        }
    }
    
    func populateData() {
        dataSource = [DataStructure]()
        
        let tempArray = UserDataManager.shared.profileDetail?.employmentBeans
        if let currentEmployment = tempArray?.filter({$0.isCurrent == true}), currentEmployment.count > 0 {
            dataSource.append(DataStructure(title: "Current Employment", dataSource: currentEmployment))
        }
        if let pastEmployment = tempArray?.filter({$0.isCurrent == false}), pastEmployment.count > 0 {
            dataSource.append(DataStructure(title: "Previous Employment", dataSource: pastEmployment))
        }
        tableView.reloadData()
    }
}

extension EmploymentsListView : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return dataSource[section].dataSource.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "EmploymentTableViewCell", for: indexPath) as! EmploymentTableViewCell
        cell.bindData(employmentData: dataSource[indexPath.section].dataSource[indexPath.row], indexPath: indexPath)
        cell.delegate = self
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: WINDOW_WIDTH, height: 15.0), backgroundColor: UIColor.white)
        
        let label = UILabel(frame: headerView.frame)
        label.font = UIFont.rubikMediumFontOfSize(14.0)
        label.textColor = UIColor(hexFromString: "424242")
        label.text = dataSource[section].title
        headerView.addSubview(label)
        return headerView
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 15.0
    }
    
    @objc func reloadTable() {
        self.tableView.reloadData()
    }
}

extension EmploymentsListView : ReloadCellProtocol {
    func deleteCell(sender: UIButton) {
        
        let position: CGPoint = sender.convert(CGPoint.zero, to: self.tableView)
        if let indexPath = tableView?.indexPathForRow(at: position) {
            
            if dataSource[indexPath.section].dataSource.count == 1 {
                dataSource.remove(at: 0)
                tableView.deleteSections([indexPath.section], with: UITableView.RowAnimation.fade)
            }
            else {
                dataSource[indexPath.section].dataSource.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.fade)
            }
        }
    }
    
    
    func reloadCell(sender: UIButton, isExpanded: Bool) {
        
        let position: CGPoint = sender.convert(CGPoint.zero, to: self.tableView)
        if let indexPath = tableView?.indexPathForRow(at: position) {
            dataSource[indexPath.section].dataSource[indexPath.row].isExpanded = isExpanded
            self.tableView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.fade)
        }
    }
}
