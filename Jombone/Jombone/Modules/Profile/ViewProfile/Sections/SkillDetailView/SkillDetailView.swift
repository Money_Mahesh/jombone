//
//  SkillDetailView.swift
//  Jombone
//
//  Created by Money Mahesh on 10/04/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import UIKit

class SkillDetailView: NSObject {
    
    var mainView: CommonBaseView!
    var skillTokenView = SkillTokenView()
    
    override init() {
        super.init()
        
        mainView = CommonBaseView(frame: CGRect(), data: CommonBaseData(title: "Skills", btnType: CommonBaseData.BtnType.add(line: false)))
        mainView.btn.addTarget(self, action: #selector(SkillDetailView.editBtnAction), for: UIControl.Event.touchUpInside)
        
        addObserver(self, selector: #selector(SkillDetailView.populateView), notificationsName: [Notification.Name.UserProfileUpdated])
        
        populateView()
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .milliseconds(100)) {
            self.mainView.stackView.addArrangedSubview(self.skillTokenView)
        }
    }
    
    @objc func populateView() {
       
        let detail = UserDataManager.shared.profileDetail?.skill ?? []
        let available = detail.count > 0
        mainView.populate(btnType: available ? .add(line: true) : .add(line: false))
        skillTokenView.isHidden = (UserDataManager.shared.profileDetail?.skill ?? []).count == 0
        skillTokenView.reloadData()
    }

    @objc func editBtnAction() {
        CandidateSkillsViewController.show(viewModel: CandidateSkillsViewModel(selectedOptions:  UserDataManager.shared.profileDetail?.skill))
    }
}
