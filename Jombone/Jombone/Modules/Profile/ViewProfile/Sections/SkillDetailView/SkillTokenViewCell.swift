//
//  SkillTokenViewCell.swift
//  Jombone
//
//  Created by dev139 on 16/04/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import UIKit

class SkillTokenViewCell: UICollectionViewCell {

    @IBOutlet weak var lblForSkillName: UILabel?
    @IBOutlet weak var bgView: UIView?
    
    var autoCompleteViewModel = AutoCompleteViewModel(type: AutoComplete.local(LocalFor: LocalFor.skill), isOffline: false)

    var tokenDetail: AutoSuggestObj? {
        didSet {
            lblForSkillName?.text = tokenDetail?.title
            bgView?.backgroundColor = tokenDetail?.isPrimary ?? false ? UIColor(hexFromString: "28bf61") : UIColor(hexFromString: "9e9e9e")
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func btnActionForCross(_ sender: UIButton) {
        hitRemoveSkillApi()
    }
    
    func hitRemoveSkillApi() {
        
        guard let encryptedId = tokenDetail?.encryptedId else {
            return
        }
        
        APPLICATION_INSTANCE.visibleViewController()?.showLoader()
        autoCompleteViewModel.hitRemoveIndustrySkillApi (
            encryptedId: encryptedId,
            completion: { (message) in
                APPLICATION_INSTANCE.visibleViewController()?.hideLoader()
                APPLICATION_INSTANCE.visibleViewController()?.showAlert(message: message)
        }, success: { (message) in
            
            let profileDetail = UserDataManager.shared.profileDetail
            profileDetail?.skill = (profileDetail?.skill)?.filter({$0.encryptedId != encryptedId})
            UserDataManager.shared.profileDetail = profileDetail
        })
    }
}
