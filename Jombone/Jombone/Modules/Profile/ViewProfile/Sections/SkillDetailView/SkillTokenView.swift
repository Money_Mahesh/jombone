//
//  SkillTokenView.swift
//  Jombone
//
//  Created by dev139 on 16/04/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import UIKit

class SkillTokenView: CustomView {

    @IBOutlet weak var skillCollectionView: UICollectionView!
    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!
    
    override func xibLoaded() {
        
        skillCollectionView.register(UINib(nibName: "SkillTokenViewCell", bundle: nil), forCellWithReuseIdentifier: "SkillTokenViewCell")
        
        skillCollectionView.delegate = self
        skillCollectionView.dataSource = self
    }

    func reloadData() {
        skillCollectionView.reloadData()
    }
}


extension SkillTokenView : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return UserDataManager.shared.profileDetail?.skill?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SkillTokenViewCell", for: indexPath) as! SkillTokenViewCell
        cell.layer.cornerRadius = 20
        cell.tokenDetail = UserDataManager.shared.profileDetail?.skill?[indexPath.row]        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let skillName = UserDataManager.shared.profileDetail?.skill?[indexPath.row].title
        let size = skillName!.size(withAttributes:nil)

        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
            self.collectionViewHeightConstraint.constant = self.collectionViewContentHeight()
        }
        return CGSize(width: size.width + 70.0, height: 40)
    }
    
    func collectionViewContentHeight() -> CGFloat {
        
        skillCollectionView.isScrollEnabled = false
        let height = skillCollectionView.contentSize.height
        skillCollectionView.isScrollEnabled = true
        
        return height
    }

}
