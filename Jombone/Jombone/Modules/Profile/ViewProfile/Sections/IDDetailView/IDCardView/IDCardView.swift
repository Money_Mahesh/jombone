//
//  IDCardView.swift
//  Jombone
//
//  Created by Money Mahesh on 11/04/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import UIKit

class IDCardView: GenericView<IDCardProtocol> {
    
    @IBOutlet weak var lblForType: UILabel?
    @IBOutlet weak var lblForName: UILabel?
    @IBOutlet weak var lblForExpiry: UILabel?
    @IBOutlet weak var lblForFilePath: UILabel?
    
    var downloadDelegate = DownloadUploadAction()

    var apiModel: IDCardViewModel?
    override var viewModel: ViewModel<IDCardProtocol>? {
        didSet {
            apiModel = IDCardViewModel(data: viewModel?.data, otherDetail: nil)
            
            lblForType?.text = nil
            if let documentDomain = viewModel?.data?.documentDomain {
                lblForType?.text = (documentDomain == "IDType1") ? "Govt. Issued Photo ID" : "Govt. Issued ID"
            }
            
            
            lblForName?.text = nil
            if let viewDocumentTypeName = viewModel?.data?.viewDocumentTypeName {
                var title = viewDocumentTypeName
                
                if let viewDocumentIdNumber = viewModel?.data?.viewDocumentIdNumber {
                    title = title + " (" + viewDocumentIdNumber + ")"
                }
                lblForName?.text = title
            }
            
            lblForExpiry?.isHidden = true
            if let viewDocumentExpiryDate = viewModel?.data?.viewDocumentExpiryDate {
                lblForExpiry?.isHidden = false
                lblForExpiry?.text = "Expiry Date: " + viewDocumentExpiryDate
            }
            
            lblForFilePath?.text = viewModel?.data?.viewDocumentTypeDisplayName
        }
    }
    
    @IBAction func btnActionForDelete(_ sender: UIButton) {
        
        guard let documentEncId = viewModel?.data?.documentEncId else {
            return
        }
        
        APPLICATION_INSTANCE.visibleViewController()?.showAlert("Confirm Delete", message: "You are about to delete one record, this procedure is irreversible. Do you want to proceed?", style: .alert, withOk: false, withCancel: false, withAnimation: true, withCustomAction: [UIAlertAction(title: "Cancel", style: .cancel, handler: nil), UIAlertAction(title: "Delete", style: .destructive, handler: { (action) in
            APPLICATION_INSTANCE.visibleViewController()?.showLoader()
            self.apiModel?.hitDeleteIdApi(documentEncId: documentEncId, completion: { (message) in
                APPLICATION_INSTANCE.visibleViewController()?.hideLoader()
                APPLICATION_INSTANCE.visibleViewController()?.showAlert(message: message)
            }, success: { (message) in
                NotificationCenter.default.post(name: NSNotification.Name.IdDeleted, object: nil, userInfo: ["Button" : sender])
                ProfileViewModel.hitScoreApi()
            })
        })])
    }
    
    @IBAction func btnActionForFilePath(_ sender: UIButton) {
        
        if let fileExtension = viewModel?.data?.viewDocumentFileName?.split(separator: ".").last, let fileName = viewModel?.data?.viewDocumentTypeDisplayName {
            
        downloadDelegate.download(fileName: (fileName + "." + fileExtension), urlStr: viewModel?.data?.documentPath)
        }
    }
}
