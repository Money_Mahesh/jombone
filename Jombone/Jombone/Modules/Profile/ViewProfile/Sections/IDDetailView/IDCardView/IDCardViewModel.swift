//
//  IDCardViewModel.swift
//  Jombone
//
//  Created by Money Mahesh on 11/04/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation


protocol IDCardProtocol {
    var documentEncId : String? {get set}
    var documentDomain : String? {get set}
    var viewDocumentTypeName : String? {get set}
    var viewDocumentIdNumber : String? {get set}
    var viewDocumentExpiryDate : String? {get set}
    var viewDocumentTypeDisplayName : String? {get set}
    var viewDocumentFileName : String? {get set}
    var documentPath : String? {get set}
}

class IDCardViewModel: ViewModel<IDCardProtocol> {
    
    func hitDeleteIdApi(documentEncId: String, completion: ((_ message: String?)->())? = nil, success:  ((_ message: String?)->())? = nil, failure: ((_ message: String?)->())? = nil) {
        
        let profileServices = ProfileServices(requestTag: "DELETE_ID_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        //Completion Block
        profileServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            if statusCode == 1 {
                
                completion?(message)
                success?(message)
            }
            else {
                completion?(message)
                failure?(message)
            }
        }
        profileServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            completion?(nil)
            failure?(nil)
        }
        
        let parameters: [String: Any] = [
            "encDocId": documentEncId
            ]
        profileServices.deleteID(parameters: parameters, additionalHeaderElements: nil)
    }
}
