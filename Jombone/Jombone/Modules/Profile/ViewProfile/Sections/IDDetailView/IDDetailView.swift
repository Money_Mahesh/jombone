//
//  IDDetailView.swift
//  Jombone
//
//  Created by Money Mahesh on 10/04/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import UIKit

class IDDetailView: NSObject {
    
    var mainView: CommonBaseView!
    
    var tableWidget: GenericTableViewComponent<IDCardProtocol, IDCardView>?

    override init() {
        super.init()
        
        mainView = CommonBaseView(frame: CGRect(), data: CommonBaseData(title: "ID Details", btnType: CommonBaseData.BtnType.add(line: false)))
        mainView.btn.addTarget(self, action: #selector(IDDetailView.editBtnAction), for: UIControl.Event.touchUpInside)
        
        addObserver(self, selector: #selector(IDDetailView.populateView), notificationsName: [Notification.Name.UserProfileUpdated])
        addObserver(self, selector: #selector(IDDetailView.idDeleted(_:)), notificationsName: [Notification.Name.IdDeleted])

        populateView()
    }
    
    @objc func populateView() {
        
        let detail = UserDataManager.shared.profileDetail?.documentBeans ?? []
        let available = detail.count > 0
        mainView.populate(btnType: available ? .add(line: true) : .add(line: false))
        mainView.stackView.layoutMargins = UIEdgeInsets(top: 22, left: 16, bottom: available ? 0 : 22, right: 16)
        
        if available {
            setUpTable()
        }
        else {
            if let _tableWidget = tableWidget {
                mainView.stackView?.removeArrangedSubview(_tableWidget)
                tableWidget = nil
            }
        }
    }
    
    @objc func idDeleted(_ notification: NSNotification) {
        
        if let detail = notification.userInfo?["Button"] as? UIButton {
            if let indexPath = self.tableWidget?.deleteCellIndex(sender: detail) {
                UserDataManager.shared.profileDetail?.documentBeans?.remove(at: indexPath.row)
                setUpTable()
            }
        }
    }
    
    private func setUpTable() {
        
        if let _tableWidget = tableWidget {
            _tableWidget.appendNewPage(objArray: UserDataManager.shared.profileDetail?.documentBeans)
        }
        else {
            tableWidget = GenericTableViewComponent()
            tableWidget?.isHidden = false
            tableWidget?.genericTableViewModel = GenericTableViewModel(
                cellDataModel: UserDataManager.shared.profileDetail?.documentBeans,
                tableType: .nonScrollable)
            tableWidget?.tableStyle(backGroundColor: UIColor.white, insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
            mainView.stackView?.addArrangedSubview(tableWidget!)
        }
        tableWidget?.isHidden = (UserDataManager.shared.profileDetail?.documentBeans ?? []).count == 0
    }
    
    @objc func editBtnAction() {
        print("----Edit")
        
        IDInfoEditViewController.show(viewModel: IDInfoEditViewModel())
    }
}
