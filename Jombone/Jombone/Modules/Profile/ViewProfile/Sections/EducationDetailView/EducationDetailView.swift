//
//  IDDetailView.swift
//  Jombone
//
//  Created by Money Mahesh on 10/04/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import UIKit

class EducationDetailView: NSObject {
    
    var mainView: CommonBaseView!
    
    var tableWidget: GenericTableViewComponent<EducationCardProtocol, EducationCardView>?

    override init() {
        super.init()
        
        mainView = CommonBaseView(frame: CGRect(), data: CommonBaseData(title: "Education Details", btnType: CommonBaseData.BtnType.add(line: false)))
        mainView.btn.addTarget(self, action: #selector(IDDetailView.editBtnAction), for: UIControl.Event.touchUpInside)
        
        addObserver(self, selector: #selector(IDDetailView.populateView), notificationsName: [Notification.Name.UserProfileUpdated])
        addObserver(self, selector: #selector(EducationDetailView.docDeleted(_:)), notificationsName: [Notification.Name.EducationDeleted])

        populateView()
    }
    
    @objc func populateView() {
        
        let detail = UserDataManager.shared.profileDetail?.educationBeans ?? []
        let available = detail.count > 0
        mainView.populate(btnType: available ? .add(line: true) : .add(line: false))
        mainView.stackView.layoutMargins = UIEdgeInsets(top: 22, left: 16, bottom: available ? 0 : 22, right: 16)
        
        if available {
            setUpTable()
        }
        else {
            if let _tableWidget = tableWidget {
                mainView.stackView?.removeArrangedSubview(_tableWidget)
                tableWidget = nil
            }
        }
    }
    
    @objc func docDeleted(_ notification: NSNotification) {
        
        if let detail = notification.userInfo?["Button"] as? UIButton {
            if let indexPath = self.tableWidget?.deleteCellIndex(sender: detail) {
                UserDataManager.shared.profileDetail?.educationBeans?.remove(at: indexPath.row)
                setUpTable()
            }
        }
    }
    
    private func setUpTable() {
        
        if let _tableWidget = tableWidget {
            _tableWidget.appendNewPage(objArray: UserDataManager.shared.profileDetail?.educationBeans)
        }
        else {
            tableWidget = GenericTableViewComponent()
            tableWidget?.isHidden = false
            tableWidget?.genericTableViewModel = GenericTableViewModel(
                cellDataModel: UserDataManager.shared.profileDetail?.educationBeans,
                tableType: .nonScrollable)
            tableWidget?.tableStyle(backGroundColor: UIColor.white, insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
            mainView.stackView?.addArrangedSubview(tableWidget!)
        }
        tableWidget?.isHidden = (UserDataManager.shared.profileDetail?.educationBeans ?? []).count == 0
    }
    
    @objc func editBtnAction() {
        print("----Edit")
        EducationInfoEditViewController.show(viewModel: EducationInfoEditViewModel())
    }
}
