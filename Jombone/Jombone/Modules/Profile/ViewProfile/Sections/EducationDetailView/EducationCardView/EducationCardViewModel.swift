//
//  IDCardViewModel.swift
//  Jombone
//
//  Created by Money Mahesh on 11/04/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation


protocol EducationCardProtocol {
    var qualificationName : String? {get set}
    var yearCompleted : String? {get set}
    var educationDocDisplayName : String? {get set}
    var educationDocName : String? {get set}
    var educationDocPath : String? {get set}
    var educationIdEnc : String? {get set}
    var instituteName : String? {get set}
}

class EducationCardViewModel: ViewModel<EducationCardProtocol> {
    
    func hitDeleteEducationApi(completion: ((_ message: String?)->())? = nil, success:  ((_ message: String?)->())? = nil, failure: ((_ message: String?)->())? = nil) {
        
        guard let educationIdEnc = data?.educationIdEnc else {
            completion?(nil)
            failure?(nil)
            return
        }
        
        let profileServices = ProfileServices(requestTag: "DELETE_EDUCATION_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        //Completion Block
        profileServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            if statusCode == 1 {
                
                completion?(message)
                success?(message)
            }
            else {
                completion?(message)
                failure?(message)
            }
        }
        profileServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            completion?(nil)
            failure?(nil)
        }
        
        let parameters: [String: Any] = [
            "encEduId": educationIdEnc
            ]
        profileServices.deleteEducation(parameters: parameters, additionalHeaderElements: nil)
    }
    
}
