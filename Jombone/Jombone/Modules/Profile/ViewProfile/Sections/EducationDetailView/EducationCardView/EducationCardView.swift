//
//  IDCardView.swift
//  Jombone
//
//  Created by Money Mahesh on 11/04/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import UIKit

class EducationCardView: GenericView<EducationCardProtocol> {
    
    @IBOutlet weak var lblForType: UILabel?
    @IBOutlet weak var lblForName: UILabel?
    @IBOutlet weak var lblForFilePath: UILabel?
    
    var downloadDelegate = DownloadUploadAction()

    var apiModel: EducationCardViewModel?
    override var viewModel: ViewModel<EducationCardProtocol>? {
        didSet {
            apiModel = EducationCardViewModel(data: viewModel?.data, otherDetail: nil)
            lblForType?.text = viewModel?.data?.qualificationName
            
            if let instituteName = viewModel?.data?.instituteName {
                let yearCompleted = viewModel?.data?.yearCompleted ?? ""
                lblForName?.text = instituteName + (yearCompleted.count > 0 ? ("(" + yearCompleted + ")") : "")
            }
            
            lblForFilePath?.text = viewModel?.data?.educationDocDisplayName
        }
    }
    
    @IBAction func btnActionForDelete(_ sender: UIButton) {
        
        APPLICATION_INSTANCE.visibleViewController()?.showAlert("Confirm Delete", message: "You are about to delete one record, this procedure is irreversible. Do you want to proceed?", style: .alert, withOk: false, withCancel: false, withAnimation: true, withCustomAction: [UIAlertAction(title: "Cancel", style: .cancel, handler: nil), UIAlertAction(title: "Delete", style: .destructive, handler: { (action) in
            APPLICATION_INSTANCE.visibleViewController()?.showLoader()
            self.apiModel?.hitDeleteEducationApi(completion: { (message) in
                APPLICATION_INSTANCE.visibleViewController()?.hideLoader()
                APPLICATION_INSTANCE.visibleViewController()?.showAlert(message: message)
            }, success: { (message) in
                NotificationCenter.default.post(name: NSNotification.Name.EducationDeleted, object: nil, userInfo: ["Button" : sender])
                ProfileViewModel.hitScoreApi()
            })
        })])
        
        
    }
    
    @IBAction func btnActionForEdit(_ sender: UIButton) {
        
        if let data = viewModel?.data {
            EducationInfoEditViewController.show(viewModel: EducationInfoEditViewModel(educationDetailsBean: data as! EducationDetailsBean))
        }
    }
    
    @IBAction func btnActionForFilePath(_ sender: UIButton) {
        
        if let fileExtension = viewModel?.data?.educationDocName?.split(separator: ".").last, let fileName = viewModel?.data?.educationDocDisplayName {
            downloadDelegate.download(fileName: (fileName + "." + fileExtension), urlStr: viewModel?.data?.educationDocPath)
        }
    }
}
