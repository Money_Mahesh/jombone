//
//  BasicInfoView.swift
//  Jombone
//
//  Created by Money Mahesh on 12/11/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import Foundation
import UIKit

class BasicInfoView: CustomView {
    
    @IBOutlet weak var imgForProfilePic: UIImageView!
    @IBOutlet weak var lblForName: UILabel!
    @IBOutlet weak var lblForJomboneId: UILabel!
    @IBOutlet weak var lblForEmailId: UILabel!
    @IBOutlet weak var lblForPhoneNo: UILabel!

    @IBOutlet weak var lblForAvailability: UILabel!
    @IBOutlet weak var lblForTotalExp: UILabel!
    @IBOutlet weak var lblForQualification: UILabel!

    @IBOutlet weak var lblForProfileScore: UILabel!
    @IBOutlet weak var lblForJScore: UILabel!
    @IBOutlet weak var lblForMaxJScore: UILabel!

    @IBOutlet weak var viewForAvaialable: UIView!

    @IBOutlet weak var progressBarForJScore: UIProgressView!
    @IBOutlet weak var progressBarForProfile: UIProgressView!

    override func xibLoaded() {
        addObserver(self, selector: #selector(BasicInfoView.populateView), notificationsName: [Notification.Name.UserProfileUpdated])
        addObserver(self, selector: #selector(BasicInfoView.populateView), notificationsName: [Notification.Name.ScoreUpdated])
    }
    
    @objc func populateView() {
        
        let personalDetailsBeans = UserDataManager.shared.profileDetail?.personalDetailsBeans
        
        if let imageUrlStr = personalDetailsBeans?.profilePicPath, let imageUrl = URL(string: imageUrlStr) {
            
            let url_request = URLRequest(url: imageUrl)
            
            imgForProfilePic.setImageWithURLAlamofire(url_request, placeholderImageName: nil, success: {
                [weak imgForProfilePic] (request:URLRequest?, image:UIImage?) -> Void in
                imgForProfilePic?.image = image }
                ,failure: {
                    [weak imgForProfilePic] (request:URLRequest?, error:Error?) -> Void in
                    imgForProfilePic?.image = UIImage(named: "profileDefault")
                    print("Not able to load profile pic" + (error?.localizedDescription ?? ""))
            })
        }
        else {
            imgForProfilePic?.image = UIImage(named: "profileDefault")
        }
        
        lblForName.text = ((personalDetailsBeans?.firstName ?? "") + " " + (personalDetailsBeans?.lastName ?? ""))
        lblForJomboneId.text = (personalDetailsBeans?.jomboneId ?? "")
        lblForEmailId.text = personalDetailsBeans?.email ?? ""
        lblForPhoneNo.text = ((personalDetailsBeans?.countryCode ?? "") + " " + (personalDetailsBeans?.phoneNumber ?? ""))
        
        lblForTotalExp.text = String(personalDetailsBeans?.exp ?? 0) + " years"
        lblForQualification.text = personalDetailsBeans?.highestEducationName ?? "NA"
        
        let profileScore = personalDetailsBeans?.profileCompleteness ?? 0
        lblForProfileScore.text = String(profileScore) + "%"
        progressBarForProfile.setProgress(Float(profileScore) / (100.0), animated: false)
        progressBarForProfile.progressTintColor = getProfileScoreTintColor(byScore: Float(profileScore))
        
        let jomboneScore = personalDetailsBeans?.jomboneScore ?? 0
        lblForJScore.text = String(jomboneScore)
        progressBarForJScore.setProgress(Float(jomboneScore) / (1000.0), animated: false)
        progressBarForJScore.progressTintColor = getJScoreTintColor(byScore: Float(jomboneScore))

        let isImmediateAvailable = ((personalDetailsBeans?.availability ?? false) == true)
        lblForAvailability.text = ((personalDetailsBeans?.availability ?? false) == true) ? ("Immediate") : (personalDetailsBeans?.availableFrom ?? "NA")
        viewForAvaialable.backgroundColor = isImmediateAvailable ?  UIColor(hexFromString: "#60C659") : UIColor(hexFromString: "#FF0000")
    }
    
    //MARK:- IBAction
    @IBAction func btnActionForEdit(_ sender: UIButton) {
        
        PersonalInfoEditViewController.show()
    }
    
    func getProfileScoreTintColor(byScore : Float) -> UIColor {
        
        var color = UIColor.white
        
        switch byScore {
        case 0.0...10.9:
            color = UIColor(red: 255.0/255.0, green: 0.0, blue: 0.0, alpha: 1.0)
        case 11.0...20.9:
            color = UIColor(red: 255.0/255.0, green: 34.0/255.0, blue: 0.0, alpha: 1.0)
        case 21.0...30.9:
            color = UIColor(red: 255.0/255.0, green: 68.0/255.0, blue: 0.0, alpha: 1.0)
        case 31.0...40.9:
            color = UIColor(red: 255.0/255.0, green: 102.0/255.0, blue: 0.0, alpha: 1.0)
        case 41.0...50.9:
            color = UIColor(red: 255.0/255.0, green: 136.0/255.0, blue: 0.0, alpha: 1.0)
        case 51.0...60.9:
            color = UIColor(red: 255.0/255.0, green: 170.0/255.0, blue: 0.0, alpha: 1.0)
        case 61.0...70.9:
            color = UIColor(red: 198.0/255.0, green: 175.0/255.0, blue: 21.0/255.0, alpha: 1.0)
        case 71.0...80.9:
            color = UIColor(red: 141.0/255.0, green: 180.0/255.0, blue: 42.0/255.0, alpha: 1.0)
        case 81.0...90.9:
            color = UIColor(red: 84.0/255.0, green: 185.0/255.0, blue: 63.0/255.0, alpha: 1.0)
        case 91.0...100.0:
            color = UIColor(red: 27.0/255.0, green: 190.0/255.0, blue: 85.0/255.0, alpha: 1.0)
        default:
            color = UIColor.white
        }
        return color
    }
    
    func getJScoreTintColor(byScore : Float) -> UIColor {
        
        var color = UIColor.white
        
        switch byScore {
        case 0.0...90.9:
            color = UIColor(red: 255.0/255.0, green: 0.0, blue: 0.0, alpha: 1.0)
        case 91.0...180.9:
            color = UIColor(red: 255.0/255.0, green: 34.0/255.0, blue: 0.0, alpha: 1.0)
        case 181.0...270.9:
            color = UIColor(red: 255.0/255.0, green: 68.0/255.0, blue: 0.0, alpha: 1.0)
        case 271.0...360.9:
            color = UIColor(red: 255.0/255.0, green: 102.0/255.0, blue: 0.0, alpha: 1.0)
        case 361.0...450.9:
            color = UIColor(red: 255.0/255.0, green: 136.0/255.0, blue: 0.0, alpha: 1.0)
        case 451.0...540.9:
            color = UIColor(red: 255.0/255.0, green: 170.0/255.0, blue: 0.0, alpha: 1.0)
        case 541.0...630.9:
            color = UIColor(red: 198.0/255.0, green: 175.0/255.0, blue: 21.0/255.0, alpha: 1.0)
        case 631.0...720.9:
            color = UIColor(red: 141.0/255.0, green: 180.0/255.0, blue: 42.0/255.0, alpha: 1.0)
        case 721.0...810.9:
            color = UIColor(red: 84.0/255.0, green: 185.0/255.0, blue: 63.0/255.0, alpha: 1.0)
        case 811.0...900.0:
            color = UIColor(red: 27.0/255.0, green: 190.0/255.0, blue: 85.0/255.0, alpha: 1.0)
        default:
            color = UIColor.white
        }
        return color
    }
    
}
