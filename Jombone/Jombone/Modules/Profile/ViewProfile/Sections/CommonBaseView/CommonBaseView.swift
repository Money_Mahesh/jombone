//
//  CommonBaseView.swift
//  Jombone
//
//  Created by Money Mahesh on 12/11/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import Foundation
import UIKit

struct CommonBaseData {
    
    enum BtnType {
        case edit(line: Bool)
        case add(line: Bool)
        case none(line: Bool)
//        case edit = "edit_gray"
//        case add = "add_gray"
//        case addWithBottomLine = "add_gray "
    }

    var title: String
    var btnType: BtnType
}

class CommonBaseView: CustomView {
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var lblForTitle: UILabel!
    @IBOutlet weak var viewForSeperator: UIView!
    @IBOutlet weak var btn: UIButton!
    
    override func xibLoaded(data: Any) {
        if let _data = data as? CommonBaseData {
            lblForTitle.text = _data.title
            populate(btnType: _data.btnType)
        }
    }
    
    func populate(btnType: CommonBaseData.BtnType) {
        
        switch btnType {
        case .edit(let line):
            viewForSeperator.isHidden = !line
            btn.setImageForAllState(image: UIImage(named: "edit_gray"))

        case .add(let line):
            viewForSeperator.isHidden = !line
            btn.setImageForAllState(image: UIImage(named: "add_gray"))

        case .none(let line):
            viewForSeperator.isHidden = !line
            btn.isHidden = true
        }
    }
}
