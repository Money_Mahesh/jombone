//
//  WorkStatusDetailView.swift
//  Jombone
//
//  Created by Money Mahesh on 10/04/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import UIKit

class WorkStatusDetailView: TilteSubtitleView {
    
    var lblForFilePath: UILabel!
    var downloadDelegate = DownloadUploadAction()

    override init() {
        super.init()
        
        lblForFilePath = setSubTitleLblStyle(UILabel())
        lblForFilePath.isUserInteractionEnabled = true
        lblForFilePath.textColor = UIColor(hexFromString: "#1473e6")
        mainView.stackView.addArrangedSubview(lblForFilePath)
        
        lblForFilePath.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(WorkStatusDetailView.fileLinkAction)))
        
        populateView()
    }
    
    @objc override func populateView() {
        
        mainView.lblForTitle.text = "Work Status"
        let detail = UserDataManager.shared.profileDetail?.workPermitBean?.detail
        let available = detail != nil
        mainView.populate(btnType: available ? .edit(line: true) : .add(line: false))
        
        for index in 0..<3 {
            
            stackViews[index].isHidden = true
            titleLbl[index].isHidden = true
            subTitleLbl[index].isHidden = true
            
            if index < (detail?.count ?? 0) {
                
                stackViews[index].isHidden = false
                titleLbl[index].isHidden = false
                subTitleLbl[index].isHidden = false
                
                titleLbl[index].text = detail?[index].keys.first
                subTitleLbl[index].text = detail?[index].values.first
            }
        }
        
        lblForFilePath?.text = UserDataManager.shared.profileDetail?.workPermitBean?.permitDocumentDisplayName
        lblForFilePath?.isHidden = false
    }
    
    @objc override func editBtnAction() {
        print("----Edit")
        WorkStatusInfoEditViewController.show()
    }
    
    @objc func fileLinkAction() {
        print("----Download")
        
        if let fileExtension = UserDataManager.shared.profileDetail?.workPermitBean?.permitDocumentName?.split(separator: ".").last, let fileName = UserDataManager.shared.profileDetail?.workPermitBean?.permitDocumentDisplayName {
            
            downloadDelegate.download(
                fileName: (fileName + "." + fileExtension),
                urlStr: UserDataManager.shared.profileDetail?.workPermitBean?.permitDocumentPath)

        }
    }
}

