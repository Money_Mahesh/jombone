//
//  ContactDetailView.swift
//  Jombone
//
//  Created by Money Mahesh on 12/11/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import Foundation
import UIKit

class ContactDetailView: TilteSubtitleView {
    
    @objc override func populateView() {
        
        mainView.lblForTitle.text = "Contact Details"
        let detail = UserDataManager.shared.profileDetail?.contactDetailsBean?.detail
        let available = detail != nil
        mainView.populate(btnType: available ? .edit(line: true) : .add(line: false))
        
        for index in 0..<3 {
            
            stackViews[index].isHidden = true
            titleLbl[index].isHidden = true
            subTitleLbl[index].isHidden = true

            if index < (detail?.count ?? 0) {
                
                stackViews[index].isHidden = false
                titleLbl[index].isHidden = false
                subTitleLbl[index].isHidden = false
                
                titleLbl[index].text = detail?[index].keys.first
                subTitleLbl[index].text = detail?[index].values.first
            }
        }
    }
    
    @objc override func editBtnAction() {
        print("----Edit")
        ContactInfoEditViewController.show()
    }
}
