//
//  SalaryDepositDetailView.swift
//  Jombone
//
//  Created by Money Mahesh on 10/04/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import UIKit

class SalaryDepositDetailView: TilteSubtitleView {
    
    @objc override func populateView() {
        
        mainView.lblForTitle.text = "Salary Deposit"
        let detail = UserDataManager.shared.profileDetail?.bankAccountBean?.detail
        let available = detail != nil
        mainView.populate(btnType: available ? .edit(line: true) : .add(line: false))
        
        for index in 0..<5 {
            
            stackViews[index].isHidden = true
            titleLbl[index].isHidden = true
            subTitleLbl[index].isHidden = true
            
            if index < (detail?.count ?? 0) {
                
                stackViews[index].isHidden = false
                titleLbl[index].isHidden = false
                subTitleLbl[index].isHidden = false
                
                titleLbl[index].text = detail?[index].keys.first
                subTitleLbl[index].text = detail?[index].values.first
            }
        }
    }
    
    @objc override func editBtnAction() {
        print("----Edit")
        SalaryInfoEditViewController.show()
    }
}

