//
//  TilteSubtitleView.swift
//  
//
//  Created by Money Mahesh on 15/04/19.
//

import Foundation
import UIKit

class TilteSubtitleView: NSObject {
    
    var mainView: CommonBaseView!
    
    var stackViews = [UIStackView]()
    var titleLbl = [UILabel]()
    var subTitleLbl = [UILabel]()
    
    override init() {
        super.init()
        
        mainView = CommonBaseView(frame: CGRect(), data: CommonBaseData(title: "", btnType: CommonBaseData.BtnType.add(line: false)))
        mainView.btn.addTarget(self, action: #selector(TilteSubtitleView.editBtnAction), for: UIControl.Event.touchUpInside)
        
        for _ in 0..<5 {
            let stackView = UIStackView()
            stackView.axis = .vertical
            stackView.alignment = .fill
            stackView.isHidden = true
            
            stackView.addArrangedSubview(setTitleLblStyle(UILabel()))
            stackView.addArrangedSubview(setSubTitleLblStyle(UILabel()))
            stackViews.append(stackView)
            mainView.stackView.addArrangedSubview(stackView)
        }
        
        addObserver(self, selector: #selector(TilteSubtitleView.populateView), notificationsName: [Notification.Name.UserProfileUpdated])
        
        populateView()
    }
    
    func setTitleLblStyle(_ lbl: UILabel) -> UILabel {
        
        lbl.font = UIFont.rubikRegularFontOfSize(12)
        lbl.textColor = UIColor(hexFromString: "#757575")
        lbl.isHidden = true
        lbl.numberOfLines = 0
        titleLbl.append(lbl)
        return lbl
    }
    
    func setSubTitleLblStyle(_ lbl: UILabel) -> UILabel {
        
        lbl.font = UIFont.rubikRegularFontOfSize(14)
        lbl.textColor = UIColor(hexFromString: "#424242")
        lbl.isHidden = true
        lbl.numberOfLines = 0
        subTitleLbl.append(lbl)
        return lbl
    }
    
    @objc func populateView() {
        
    }
    
    @objc func editBtnAction() {
        print("----Edit")
    }
}
