//
//  ProfileViewModel.swift
//  Jombone
//
//  Created by Money Mahesh on 15/11/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import Foundation

class ProfileViewModel: NSObject {
        
    func hitGetProfileDetail(completion: ((_ message: String?)->())? = nil, success:  ((_ message: String?)->())? = nil, failure: ((_ message: String?)->())? = nil) {
        
        let profileServices = ProfileServices(requestTag: "SIGNUP_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        //Completion Block
        profileServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            if statusCode == 1, let profileDtetail = response.0 as? ProfileDetail {
                UserDataManager.shared.profileDetail = profileDtetail
                
                if let skills = profileDtetail.skill {
                    for index in 0..<skills.count {
                        UserDataManager.shared.profileDetail?.skill?[index].selected = true
                    }
                }
                
                completion?(message)
                success?(message)
            }
            else {
                completion?(message)
                failure?(message)
            }
        }
        profileServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            completion?(nil)
            failure?(nil)
        }

        profileServices.getProfile(parameters: nil, additionalHeaderElements: nil)
    }
    
   static func hitScoreApi(completion: ((_ message: String?)->())? = nil, success:  ((_ message: String?)->())? = nil, failure: ((_ statusCode: Int?, _ message: String?)->())? = nil) {
        
        let profileServices = ProfileServices(requestTag: "SCORE_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        //Completion Block
        profileServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            if let scoreData = (response.0 as? ScoreModel) {
                
                UserDataManager.shared.profileDetail?.personalDetailsBeans?.jomboneScore = scoreData.jomboneScore
                UserDataManager.shared.profileDetail?.personalDetailsBeans?.profileCompleteness = scoreData.profileCompleteness
                NotificationCenter.default.post(name: NSNotification.Name.ScoreUpdated, object: nil)
            }
            if statusCode == 1 {
                completion?(message)
                success?(message)
            }
            else {
                completion?(message)
                failure?(statusCode, message)
            }
        }
        profileServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            completion?(nil)
            failure?(nil, nil)
        }
        
        profileServices.hitJscoreApi(parameters: nil, additionalHeaderElements: nil)
    }
}
