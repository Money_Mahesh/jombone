//
//  ProfileViewController.swift
//  Jombone
//
//  Created by dev139 on 09/10/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import UIKit

class ProfileViewController: WhiteStatusBarViewController {

    @IBOutlet weak var stackView: UIStackView!
    var viewModel = ProfileViewModel()

    var addressSection: AddressView! = AddressView()
    var contactSection: ContactDetailView! = ContactDetailView()
    var iDDetailSection: IDDetailView! = IDDetailView()
    var workStatusDetailSection: WorkStatusDetailView! = WorkStatusDetailView()
    var salaryDepositDetailSection: SalaryDepositDetailView! = SalaryDepositDetailView()
    var skillDetailSection: SkillDetailView! = SkillDetailView()
    var educationDetailSection: EducationDetailView! = EducationDetailView()
    var employmentDetailView: EmploymentDetailView! = EmploymentDetailView()
    var physicalDemandAnalysisSection: PhysicalDemandAnalysisView! = PhysicalDemandAnalysisView()

    static func instance() -> ProfileViewController{
        
        let instance = PROFILE_STORYBOARD.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        return instance
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        setNavigationBarWithTitle("My Profile", andLeftButton: .back, andRightButton: .none, withBg: .defaultColor)
        updateDetail()
        addProfileSection()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addProfileSection() {
        stackView.addArrangedSubview(BasicInfoView())
        stackView.addArrangedSubview(addressSection.mainView)
        stackView.addArrangedSubview(contactSection.mainView)
        stackView.addArrangedSubview(iDDetailSection.mainView)
        stackView.addArrangedSubview(workStatusDetailSection.mainView)
        stackView.addArrangedSubview(salaryDepositDetailSection.mainView)
        stackView.addArrangedSubview(skillDetailSection.mainView)
        stackView.addArrangedSubview(educationDetailSection.mainView)
        stackView.addArrangedSubview(employmentDetailView.mainView)
        stackView.addArrangedSubview(physicalDemandAnalysisSection.mainView)
    }
    
    func updateDetail() {
        showLoader()
        viewModel.hitGetProfileDetail(completion: { (message) in
            self.hideLoader()
        }, failure: { (message) in
            self.showAlert(message: message)
        })
    }
}
