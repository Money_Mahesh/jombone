//
//  TrainingCardViewModel.swift
//  Jombone
//
//  Created by Money Mahesh on 28/08/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import Foundation
import UIKit

enum Image {
    case imageUrlStr(urlStr: String)
    case imageName(name: String)
    case image(image: UIImage)
}

class TrainingCardViewModel: NSObject {
    
    private var image: Image!
    var isLeftSelected: Bool!
    var isRightSelected: Bool!

    init(image: Image, isLeftSelected: Bool, isRightSelected: Bool) {
        self.image = image
        self.isLeftSelected = isLeftSelected
        self.isRightSelected = isRightSelected
    }
    
    func getImage(completion: ((UIImage?)->())) {
    
//        switch image! {
//        case .imageUrlStr(let urlStr):
//            
//            break
//            
//        case .imageName(let name):
//            completion(UIImage(named: name))
//            
//        case .image(let image):
//            completion(image)
//
//        }
        
    }
}
