//
//  TrainingCardView.swift
//  Jombone
//
//  Created by Money Mahesh on 28/08/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import UIKit

class TrainingCardView: CustomView {

    @IBOutlet weak private var rightBtn: UIButton!
    @IBOutlet weak private var leftBtn: UIButton!
    @IBOutlet weak private var imageView: UIImageView!
    @IBOutlet weak private var btnForPlay: UIButton!
    @IBOutlet weak private var constForTop: NSLayoutConstraint!
    @IBOutlet weak private var constForLeft: NSLayoutConstraint!
    @IBOutlet weak private var constForRight: NSLayoutConstraint!
    @IBOutlet weak private var constForBottom: NSLayoutConstraint!

    
    func setStyle(insets: UIEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0), leftBtnTitle: String? = nil, rightBtnTitle: String? = nil) {
        
        constForTop.constant = insets.top
        constForLeft.constant = insets.top
        constForRight.constant = insets.top
        constForBottom.constant = insets.top

    }
    
    @IBAction func btnActionForPlay(_ sender: UIButton) {
        
    }
    
    @IBAction func btnActionForRight(_ sender: UIButton) {
        
    }
    
    @IBAction func btnActionForLeft(_ sender: UIButton) {
        
    }
}
