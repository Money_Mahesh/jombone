//
//  OfferListViewController.swift
//  Jombone
//
//  Created by dev139 on 19/09/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import UIKit

class OfferListViewController: PopUpViewController {
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var noResultView: UIView!
    var viewModel = OfferListViewModel()
    
    var navBtnType: NavBtnType = .menu
    var tableWidget: GenericTableViewComponent<OfferCardViewModelProtocol, OfferCardView>?
    
    static func instance(navBtnType: NavBtnType = .back) -> OfferListViewController {
        
        let viewController = APPLICATION_STORYBOARD.instantiateViewController(withIdentifier: "OfferListViewController") as! OfferListViewController
        viewController.navBtnType = navBtnType
        viewController.viewModel = OfferListViewModel()
        return viewController
    }
    
    static func show(navBtnType: NavBtnType = .menu) {
        
        let viewController = APPLICATION_STORYBOARD.instantiateViewController(withIdentifier: "OfferListViewController") as! OfferListViewController
        viewController.navBtnType = navBtnType
        viewController.viewModel = OfferListViewModel()
        APPLICATION_INSTANCE.visibleViewController()?.navigationController?.pushViewController(viewController, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationBarWithTitle("Offers", andLeftButton: navBtnType, andRightButton: .none, withBg: .defaultColor)
        
        self.addObserver(self, selector: #selector(OfferListViewController.reloadList), notificationsName: [Notification.Name.ReloadOfferList])
        self.addObserver(self, selector: #selector(OfferListViewController.updatedList), notificationsName: [Notification.Name.UpdateOfferList])
        self.addObserver(self, selector: #selector(OfferListViewController.offerRejected(_:)), notificationsName: [Notification.Name.OfferRejected])

        self.showLoader()
        self.getOffersList()
    }
    
    @objc func updatedList() {
        viewModel.reset()
        setUpTable()
        
        self.showLoader()
        self.getOffersList()
    }
    
    @objc func reloadList() {
        tableWidget?.reloadTable()
    }
    
    @objc func offerRejected(_ notification: NSNotification) {
        
        if let detail = notification.userInfo?["Button"] as? UIButton {
            if let indexPath = self.tableWidget?.deleteCellIndex(sender: detail) {
                self.viewModel.cardDataModels.remove(at: indexPath.row)
                self.noResultView.isHidden = self.viewModel.dataAvailable
            }
        }
    }
    
    func getOffersList() {
        
        viewModel.hitGetOffersApi(completion: { (message) in
            self.hideLoader()
            self.noResultView.isHidden = self.viewModel.dataAvailable
        }, success: { (message) in
            self.setUpTable()
        }, failure: { (errorMsg) in
            //            self.showAlert(message: errorMsg)
        })
    }
    
    override func imageBtnAction(sender: UIButton) {
    }
    
    func setUpTable() {
        
        if let _tableWidget = tableWidget {
            _tableWidget.appendNewPage(objArray: viewModel.cardDataModels)
        }
        else {
            tableWidget = GenericTableViewComponent()
            tableWidget?.isHidden = false
            tableWidget?.genericTableViewModel = GenericTableViewModel(
                cellDataModel: viewModel.cardDataModels,
                tableFooterView: (LoadMoreView(frame: CGRect(x: 0, y: 0, width: WINDOW_WIDTH, height: 40)), false),
                tableType: .expandable,
                totalPages: viewModel.totalPages)
            tableWidget?.tableStyle(backGroundColor: UIColor.white, insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
            tableWidget?.delegate = self
            stackView.addArrangedSubview(tableWidget!)
        }
    }
}

extension OfferListViewController: GenericTableViewComponentDelegate {
    
    func genericTableDidSelectRowAt(_ indexPath: IndexPath, dataSource: inout [Any]) -> Bool {
        
        if let jobModel = viewModel.cardDataModels[indexPath.row] as? JobModel {
            JobsDetailViewController.show(JobsDetailViewModel(jobDetail: jobModel), parentController: self)
        }
        return false
    }
    
    func loadMore() {
        getOffersList()
    }
}


