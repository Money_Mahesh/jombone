//
//  OffersViewModel.swift
//  Jombone
//
//  Created by dev139 on 23/09/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import UIKit

class OfferListViewModel: NSObject {
    
    var cardDataModels = [OfferCardViewModelProtocol]()
    var pageNo = 0
    var totalPages = 0
    var dataAvailable: Bool! {
        get {
            return (cardDataModels.count != 0)
        }
    }
    
    func reset() {
        self.pageNo = 0
        cardDataModels.removeAll()
    }
    
    func hitGetOffersApi(completion: ((_ message: String?)->())? = nil, success:  ((_ message: String?)->())? = nil, failure: ((_ message: String?)->())? = nil) {
        
        let offerServices = OfferServices(requestTag: "OFFERS_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        //Completion Block
        offerServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            if let offersList = (response.0 as? [JobModel]),
                let pagination = (response.1 as? Pagination),
                let totalPages = pagination.totalPages,
                let currentPage = pagination.currentPage,
                self.pageNo == currentPage, statusCode == 1 {
                
                self.pageNo += 1
                self.cardDataModels.append(contentsOf: offersList)
                self.totalPages = totalPages
                
                completion?(message)
                success?(message)
            }
            else {
                completion?(message)
                failure?(message)
            }
        }
        offerServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            completion?(nil)
            failure?(nil)
        }
        
        let parameters: [String: Any] = [
            "size": String(50),
            "page": String(pageNo)
        ]
        
        offerServices.getOffersList(parameters: parameters, additionalHeaderElements: nil)
    }
}
