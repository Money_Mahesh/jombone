//
//  OfferCardViewModel.swift
//  Jombone
//
//  Created by Money Mahesh on 13/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation

protocol OfferCardViewModelProtocol: JobCardBaseViewProtocol {
    var jobId: String? {get set}
    var isOfferAccepted : Bool? {get set}
    var isOfferRejected : Bool? {get set}
    var applyDate: String? {get set}
    var appEncId: String? {get set}
    
    var offerAcceptedDate: String? {get set}
    var jobOfferedDate : String? {get set}
}

class OfferCardViewModel: ViewModel<OfferCardViewModelProtocol> {
    
    override init(data: OfferCardViewModelProtocol?, otherDetail: Any?) {
        super.init(data: data, otherDetail: otherDetail)
    }
}
