//
//  OfferCardView.swift
//  Jombone
//
//  Created by Money Mahesh on 13/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import UIKit

class OfferCardView: GenericView<OfferCardViewModelProtocol>, OfferAction {
    
    @IBOutlet weak var btnForAccept: UIButton?
    @IBOutlet weak var btnForReject: UIButton?
    @IBOutlet weak var lblForAppliedDate: UILabel?
    @IBOutlet weak var jobCardBaseViewStack: UIStackView?
    
    @IBOutlet weak var offeredAndAcceptedTitle: UILabel?
    var jobCardBaseView: JobCardBaseView!
    weak var offerActionDelegate: OfferAction?
    
    override func xibLoaded() {
        jobCardBaseView = JobCardBaseView()
        jobCardBaseViewStack?.addArrangedSubview(jobCardBaseView)
        offerActionDelegate = self
    }
    
    override var viewModel: ViewModel<OfferCardViewModelProtocol>? {
        didSet {
            jobCardBaseView.viewModel = JobCardBaseViewModel(data: viewModel?.data, otherDetail: nil)
            
            if let isOfferAccepted = viewModel?.data?.isOfferAccepted, let offerAcceptedDate = viewModel?.data?.offerAcceptedDate, isOfferAccepted {
                offeredAndAcceptedTitle?.text = "Accepted on: "
                lblForAppliedDate?.text = offerAcceptedDate
            } else if let jobOfferedDate = viewModel?.data?.jobOfferedDate {
                offeredAndAcceptedTitle?.text = "Offered on: "
                lblForAppliedDate?.text = jobOfferedDate
            }
            else {
                lblForAppliedDate?.text = nil
            }
            setAcceptStatus()
            setRejectStatus()
        }
    }
    
    private func setRejectStatus() {
        let isOfferRejected = viewModel?.data?.isOfferRejected ?? false
        let title = isOfferRejected ? "REJECTED" : "REJECT"
        btnForReject?.setTitleForAllState(title: title)
        
        if isOfferRejected {
            btnForReject?.setImageForAllState(image: UIImage(named: "tickWithWhiteCircle"))
        }
        else {
            btnForReject?.setImageForAllState(image: nil)
        }
    }
    
    private func setAcceptStatus() {
        let isOfferAccepted = viewModel?.data?.isOfferAccepted ?? false
        let title = isOfferAccepted ? "ACCEPTED" : "ACCEPT"
        btnForAccept?.setTitleForAllState(title: title)

        if isOfferAccepted {
            btnForAccept?.setImageForAllState(image: UIImage(named: "tickWithWhiteCircle"))
        }
        else {
            btnForAccept?.setImageForAllState(image: nil)
        }
    }
    
    //MARK:- IBActions
    @IBAction func btnActionForReject(_ sender: UIButton) {
        
        guard let appEncId = viewModel?.data?.appEncId,
            let isOfferRejected = viewModel?.data?.isOfferRejected,
            !isOfferRejected else {
            return
        }
        
        CustomAlertViewController.show(data: CustomAlertDataModel(title: "Reject Offer", subTitle: "Are you sure you want to reject\nthe job offer?", completion: {
            
            self.offerActionDelegate?.rejectOffer(appEncId: appEncId, completion: { (status: Bool) in
                if status {
                    self.viewModel?.data?.isOfferRejected = true
                    self.viewModel?.data?.isOfferAccepted = false
                    self.setRejectStatus()
                    self.setAcceptStatus()
                    NotificationCenter.default.post(name: NSNotification.Name.OfferRejected, object: nil, userInfo: ["Button" : sender])
                }
            })
        }))
    }
    
    @IBAction func btnActionForAccept(_ sender: UIButton) {
        
        guard let appEncId = viewModel?.data?.appEncId,
            let isOfferAccepted = viewModel?.data?.isOfferAccepted,
            !isOfferAccepted else {
            return
        }
        
        CustomAlertViewController.show(data: CustomAlertDataModel(title: "Accept Offer", subTitle: "Are you sure you want to accept\nthe job offer?", completion: {
            
            self.offerActionDelegate?.acceptOffer(appEncId: appEncId, completion: { (status: Bool) in
                if status {
                    
                    self.viewModel?.data?.isOfferAccepted = true
                    self.viewModel?.data?.isOfferRejected = false
                    self.setRejectStatus()
                    self.setAcceptStatus()
                }
            })
        }))
    }
    
    @IBAction func btnActionForDirection(_ sender: UIButton) {
        guard let jobId = viewModel?.data?.jobId else {
            return
        }
        self.viewDirectionForJobOrCompany(isJob: true, jobOrEmployerid: jobId)
    }
}
