//
//  JobCardBaseView.swift
//  Jombone
//
//  Created by Money Mahesh on 13/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import UIKit

class JobCardBaseView: GenericView<JobCardBaseViewProtocol>, CompanyAction {
    
    @IBOutlet weak var imgViewForCompanylogo: UIImageView!
    @IBOutlet weak var btnForIsFollowed: UIButton!
    @IBOutlet weak var lblForPostionName: UILabel?
    @IBOutlet weak var lblForCompanyName: UILabel?
    @IBOutlet weak var lblForAddress: UILabel?
    @IBOutlet weak var lblForPayRate: UILabel?
    @IBOutlet weak var lblForShiftTime: UILabel?
    @IBOutlet weak var lblForJobType: UILabel?
    @IBOutlet weak var lblForStartDate: UILabel?
    @IBOutlet weak var lblForEndDate: UILabel?
    @IBOutlet weak var lblForShiftEnd: UILabel?
    
    
    weak var companyDelegate: CompanyAction?
    
    override func xibLoaded() {
        companyDelegate = self
    }
    
    override var viewModel: ViewModel<JobCardBaseViewProtocol>? {
        didSet {
            
            lblForPostionName?.text = viewModel?.data?.jobTitle
            lblForCompanyName?.text = viewModel?.data?.employerName
            lblForAddress?.text = viewModel?.data?.truncateLocation
            lblForPayRate?.text =  viewModel?.data?.payRateString
            lblForShiftTime?.text = viewModel?.data?.shiftTimeFrom
            lblForShiftEnd?.text = viewModel?.data?.shiftTimeTo
            lblForJobType?.text = viewModel?.data?.jobType
            lblForStartDate?.text = viewModel?.data?.empStartDate ?? viewModel?.data?.startDate
            lblForEndDate?.text = viewModel?.data?.empEndDate ?? viewModel?.data?.endDate
            
            
            imgViewForCompanylogo.backgroundColor = UIColor.clear
            self.imgViewForCompanylogo?.image = UIImage(named: "company_listing_placeholder")
            imgViewForCompanylogo.contentMode = .scaleAspectFill

            if let _imageUrlStr = viewModel?.data?.logoPath,
                let imageUrl = URL(string: (_imageUrlStr)) {
                
                let url_request = URLRequest(url: imageUrl)
                self.imgViewForCompanylogo?.setImageWithURLAlamofire(url_request, placeholderImageName: "", success: {
                    [weak self] (request: URLRequest?, image: UIImage?) -> Void in
                    self?.imgViewForCompanylogo.backgroundColor = UIColor.clear
                    self?.imgViewForCompanylogo.image = image
                    }
                    ,failure: {
                        [weak self] (request:URLRequest?, error:Error?) -> Void in
                        self?.imgViewForCompanylogo.contentMode = .scaleAspectFill
                        self?.imgViewForCompanylogo?.image = UIImage(named: "company_listing_placeholder")
                        print("Not able to load profile pic" + (error?.localizedDescription ?? ""))
                })
            }
            else {
                self.imgViewForCompanylogo.contentMode = .scaleAspectFill
                self.imgViewForCompanylogo?.image = UIImage(named: "company_listing_placeholder")
            }
            
            setFollowStatus()
        }
    }
    
    private func setFollowStatus() {
        let isFollowed = viewModel?.data?.isFollowed ?? false
        let imageName = isFollowed ? "wifiActivatedIcon" : "wifiIcon"
        btnForIsFollowed.setImageForAllState(image: UIImage(named: imageName))
    }
    
    //MARK:- IBActions
    @IBAction func btnActionForIsFollowed(_ sender: UIButton) {
        
        guard let companyId = viewModel?.data?.companyId, let isFollowed = viewModel?.data?.isFollowed else {
            return
        }
        companyDelegate?.follow_UnFollowCompany(isFollowed: !isFollowed, companyEncId: companyId, completion: { (status: Bool) in
            if status {
                self.viewModel?.data?.isFollowed = !(isFollowed)
                self.setFollowStatus()
            }
        })
    }
    
    @IBAction func companyLogoClicked(_ sender: UITapGestureRecognizer) {
        guard let companyId = viewModel?.data?.companyId else {
            return
        }
        companyDelegate?.companyDetail(empEncId: companyId, completion: { (companyDetails) in
            CompanyDetailViewController.show(CompanyDetailViewModel(companyDetail: companyDetails), followedAction: {[weak self] (status) in
                self?.viewModel?.data?.isFollowed = status
                self?.setFollowStatus()
            })
        })
    }
}
