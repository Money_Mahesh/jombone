//
//  JobCardViewModel.swift
//  Jombone
//
//  Created by dev139 on 19/09/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import UIKit

protocol JobCardViewProtocol: JobCardBaseViewProtocol {
    var jobId: String? {get set}
    var jobURL: String? {get set}
    var isFavorite: Bool? {get set}
    var isApplied: Bool? {get set}
    var isRejected : Bool? {get set}
    var rejectedDate : Double? {get set}
    var isWithdrawn : Bool? {get set}
    var withdrawnDate : Double? {get set}
}

class JobCardViewModel: ViewModel<JobCardViewProtocol> {
    
    override init(data: JobCardViewProtocol?, otherDetail: Any?) {
        super.init(data: data, otherDetail: otherDetail)
    }
}
