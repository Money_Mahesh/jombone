//
//  JobCardView.swift
//  Jombone
//
//  Created by dev139 on 19/09/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import UIKit

class JobCardView: GenericView<JobCardViewProtocol>, JobAction, CompanyAction {

    @IBOutlet weak var btnForIsLiked: UIButton!
    @IBOutlet weak var btnForApply: UIButton?
    @IBOutlet weak var jobCardBaseViewStack: UIStackView?
    
    var jobCardBaseView: JobCardBaseView!
    weak var jobDelegate: JobAction?
    weak var companyDelegate: CompanyAction?

    override func xibLoaded() {
        jobCardBaseView = JobCardBaseView()
        jobCardBaseViewStack?.addArrangedSubview(jobCardBaseView)
        jobDelegate = self
        companyDelegate = self
    }

    override var viewModel: ViewModel<JobCardViewProtocol>? {
        didSet {
            jobCardBaseView.viewModel = JobCardBaseViewModel(data: viewModel?.data, otherDetail: nil)            
            setApplyStatus()
            setLikeStatus()
        }
    }
    
    private func setLikeStatus() {
        let isFavorite = viewModel?.data?.isFavorite ?? false
        let imageName = isFavorite ? "liked" : "like"
        btnForIsLiked.setTitleColorForAllState(color: (isFavorite ? UIColor(hexFromString: "#1473e7") : UIColor(hexFromString: "#9e9e9e")))
        isFavorite ? btnForIsLiked?.setTitleForAllState(title: "Liked") : btnForIsLiked?.setTitleForAllState(title: "Like")
        btnForIsLiked.setImageForAllState(image: UIImage(named: imageName))
    }
    
    private func setApplyStatus() {
        let isApplied = viewModel?.data?.isApplied ?? false
        let title = isApplied ? "APPLIED" : "APPLY"
        btnForApply?.backgroundColor = UIColor(red: 40.0/255.0, green: 191.0/255.0, blue: 97.0/255.0, alpha: (isApplied ? 0.56 : 1.0))
        btnForApply?.setTitleForAllState(title: title)
    }
    
    //MARK:- IBActions
    @IBAction func btnActionForLike(_ sender: UIButton) {
        guard let jobId = viewModel?.data?.jobId, let isFavorite = viewModel?.data?.isFavorite else {
            return
        }
        
        jobDelegate?.like_DislikeJob(isLiked: !isFavorite, jobEncId: jobId, completion: { (status: Bool) in
            if status {
                self.viewModel?.data?.isFavorite = !(isFavorite)
                self.setLikeStatus()
            }
        })
    }
    
    @IBAction func btnActionForShare(_ sender: UIButton) {
        
        guard let shareUrl = viewModel?.data?.jobURL else {
            return
        }
        jobDelegate?.share(shareDetail: [shareUrl])
    }
   
    @IBAction func btnActionForApply(_ sender: UIButton) {
        
        guard let jobId = viewModel?.data?.jobId, let isApplied = viewModel?.data?.isApplied, !isApplied else {
            return
        }
        jobDelegate?.applyJob(jobEncId: jobId, completion: { (status: Bool) in
            if status {
                self.viewModel?.data?.isApplied = !(isApplied)
                self.setApplyStatus()
            }
        })
    }
    
    @IBAction func btnActionForDirection(_ sender: UIButton) {
        
        guard let jobId = viewModel?.data?.jobId else {
            return
        }
        self.viewDirectionForJobOrCompany(isJob: true, jobOrEmployerid: jobId)
    }
}
