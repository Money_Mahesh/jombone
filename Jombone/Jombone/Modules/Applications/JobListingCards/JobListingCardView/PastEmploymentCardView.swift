//
//  PastEmploymentCardView.swift
//  Jombone
//
//  Created by dev139 on 23/09/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import UIKit

class PastEmploymentCardView: GenericView<JobCardViewProtocol> {

    @IBOutlet weak var imgViewForCompanylogo: UIImageView!
    @IBOutlet weak var lblForPostionName: UILabel?
    @IBOutlet weak var lblForCompanyName: UILabel?
    @IBOutlet weak var lblForAddress: UILabel?
    @IBOutlet weak var lblForPayRate: UILabel?
    @IBOutlet weak var lblForEndDateTime: UILabel?
    @IBOutlet weak var lblForStartDate: UILabel?
    @IBOutlet weak var withdrawButton: UIButton?
    @IBOutlet weak var lblForReason: UILabel?
}
