//
//  JobCardBaseViewModel.swift
//  Jombone
//
//  Created by Money Mahesh on 14/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation

protocol JobCardBaseViewProtocol {
    var logoPath: String? {get set}
    var employerName: String? {get set}
    var jobTitle: String? {get set}
    var rate: Double? {get set}
    var payType: String? {get set}
    var shift: String? {get set}
    var jobType: String? {get set}
    var startDate: String? {get set}
    var location: String? {get set}
    var companyId : String? {get set}
    var isFollowed: Bool? {get set}
    var truncateLocation: String? {get set}
    var shiftTimeFrom : String? {get set}
    var shiftTimeTo : String? {get set}
    var endDate : String? {get set}
    var payRateString : String? {get set}
    var empEndDate : String? {get set}
    var empStartDate : String? {get set}
}

class JobCardBaseViewModel: ViewModel<JobCardBaseViewProtocol> {
    
    override init(data: JobCardBaseViewProtocol?, otherDetail: Any?) {
        super.init(data: data, otherDetail: otherDetail)
    }
}
