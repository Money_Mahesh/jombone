//
//  ApplicationCardView.swift
//  Jombone
//
//  Created by Money Mahesh on 13/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import UIKit

class ApplicationCardView: GenericView<ApplicationCardViewProtocol>, ApplicationAction {
    
    @IBOutlet weak var btnForWithdraw: UIButton?
    @IBOutlet weak var lblForAppliedDate: UILabel?
    @IBOutlet weak var jobCardBaseViewStack: UIStackView?
    
    var jobCardBaseView: JobCardBaseView!
    weak var applicationDelegate: ApplicationAction?
    
    override func xibLoaded() {
        jobCardBaseView = JobCardBaseView()
        jobCardBaseViewStack?.addArrangedSubview(jobCardBaseView)
        applicationDelegate = self
    }
    
    override var viewModel: ViewModel<ApplicationCardViewProtocol>? {
        didSet {
            jobCardBaseView.viewModel = JobCardBaseViewModel(data: viewModel?.data, otherDetail: nil)
            lblForAppliedDate?.text = viewModel?.data?.applyDate
            btnForWithdraw?.setTitleForAllState(title: "WITHDRAW")
        }
    }
    
    
    //MARK:- IBActions
    @IBAction func btnActionForWithdraw(_ sender: UIButton) {
        
        guard let appEncId = viewModel?.data?.appEncId else {
            return
        }
        
        CustomAlertViewController.show(data: CustomAlertDataModel(title: "Withdraw", subTitle: "Are you sure you want to withdraw\nthe application?", completion: {
            
            self.applicationDelegate?.withdrawApplication(appEncId: appEncId, completion: { (status: Bool) in
                if status {
                    NotificationCenter.default.post(name: NSNotification.Name.ApplicationWithdraw, object: nil, userInfo: ["Button" : sender])
                }
            })
        }))
    }
    
    @IBAction func btnActionForDirection(_ sender: UIButton) {
        guard let jobId = viewModel?.data?.jobId else {
            return
        }
        self.viewDirectionForJobOrCompany(isJob: true, jobOrEmployerid: jobId)
    }
}
