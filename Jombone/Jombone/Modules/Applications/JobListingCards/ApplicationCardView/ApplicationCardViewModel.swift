//
//  ApplicationCardViewModel.swift
//  Jombone
//
//  Created by Money Mahesh on 13/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation

protocol ApplicationCardViewProtocol: JobCardBaseViewProtocol {
    var jobId: String? {get set}
    var applyDate: String? {get set}
    var isApplied: Bool? {get set}
    var appEncId: String? {get set}    
}

class ApplicationCardViewModel: ViewModel<ApplicationCardViewProtocol> {
    
    override init(data: ApplicationCardViewProtocol?, otherDetail: Any?) {
        super.init(data: data, otherDetail: otherDetail)
    }
}
