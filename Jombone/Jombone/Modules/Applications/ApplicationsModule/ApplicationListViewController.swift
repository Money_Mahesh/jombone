//
//  ApplicationsBaseViewController.swift
//  Jombone
//
//  Created by dev139 on 19/09/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import UIKit

class ApplicationListViewController: PopUpViewController {
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var noResultView: UIView!
    var viewModel = ApplicationsListViewModel()

    var tableWidget: GenericTableViewComponent<ApplicationCardViewProtocol, ApplicationCardView>?
    
    static func instance() -> ApplicationListViewController {
        
        let viewController = APPLICATION_STORYBOARD.instantiateViewController(withIdentifier: "ApplicationsViewController") as! ApplicationListViewController
        viewController.viewModel = ApplicationsListViewModel()
        return viewController
    }
    
    static func show() {
        
        let viewController = APPLICATION_STORYBOARD.instantiateViewController(withIdentifier: "ApplicationsViewController") as! ApplicationListViewController
        viewController.viewModel = ApplicationsListViewModel()
        APPLICATION_INSTANCE.visibleViewController()?.navigationController?.pushViewController(viewController, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationBarWithTitle("Applications", andLeftButton: .back, andRightButton: .none, withBg: .defaultColor)

        self.addObserver(self, selector: #selector(ApplicationListViewController.reloadList), notificationsName: [Notification.Name.ReloadApplicationList])
        self.addObserver(self, selector: #selector(ApplicationListViewController.updatedList), notificationsName: [Notification.Name.UpdateApplicationList])
        self.addObserver(self, selector: #selector(ApplicationListViewController.applicationWithdraw(_:)), notificationsName: [Notification.Name.ApplicationWithdraw])
        
        self.showLoader()
        self.getApplicationsList()
    }
    
    @objc func updatedList() {
        viewModel.reset()
        setUpTable()
        
        self.showLoader()
        self.getApplicationsList()
    }
    
    @objc func reloadList() {
        tableWidget?.reloadTable()
    }
    
    @objc func applicationWithdraw(_ notification: NSNotification) {
        
        if let detail = notification.userInfo?["Button"] as? UIButton {
            if let indexPath = self.tableWidget?.deleteCellIndex(sender: detail) {
                self.viewModel.cardDataModels.remove(at: indexPath.row)
                self.noResultView.isHidden = self.viewModel.dataAvailable
            }
        }
    }
    
    func getApplicationsList() {
        
        viewModel.hitGetApplicationsApi(completion: { (message) in
            self.hideLoader()
            self.noResultView.isHidden = self.viewModel.dataAvailable
        }, success: { (message) in
            self.setUpTable()
        }, failure: { (errorMsg) in
            //            self.showAlert(message: errorMsg)
        })
    }
    
    override func imageBtnAction(sender: UIButton) {
    }
    
    func setUpTable() {
        
        if let _tableWidget = tableWidget {
            _tableWidget.appendNewPage(objArray: viewModel.cardDataModels)
        }
        else {
            tableWidget = GenericTableViewComponent()
            tableWidget?.isHidden = false
            tableWidget?.genericTableViewModel = GenericTableViewModel(
                cellDataModel: viewModel.cardDataModels,
                tableFooterView: (LoadMoreView(frame: CGRect(x: 0, y: 0, width: WINDOW_WIDTH, height: 40)), false),
                tableType: .expandable,
                totalPages: viewModel.totalPages)
            tableWidget?.tableStyle(backGroundColor: UIColor.white, insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
            tableWidget?.delegate = self
            stackView.addArrangedSubview(tableWidget!)
        }
    }
}

extension ApplicationListViewController: GenericTableViewComponentDelegate {
    
    func genericTableDidSelectRowAt(_ indexPath: IndexPath, dataSource: inout [Any]) -> Bool {
        
        if let jobModel = viewModel.cardDataModels[indexPath.row] as? JobModel {
            JobsDetailViewController.show(JobsDetailViewModel(jobDetail: jobModel), parentController: self)
        }
        return false
    }
    
    func loadMore() {
        getApplicationsList()
    }
}

