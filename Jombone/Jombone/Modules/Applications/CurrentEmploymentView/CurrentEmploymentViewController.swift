//
//  CurrentEmploymentViewController.swift
//  Jombone
//
//  Created by dev139 on 19/09/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import UIKit

class CurrentEmploymentViewController: UIViewController {
    
    @IBOutlet weak var stackView: UIStackView!
    var viewModel = CurrentEmploymentViewModel()
    
    var tableWidget: GenericTableViewComponent<JobCardViewProtocol, JobCardView>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpTable()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpTable() {
        
        tableWidget = GenericTableViewComponent()
        tableWidget?.isHidden = false
        tableWidget?.genericTableViewModel = GenericTableViewModel(
            cellDataModel: viewModel.cardDataModels,
            tableType: .expandable)
        tableWidget?.tableStyle(backGroundColor: UIColor.white, insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
        stackView.addArrangedSubview(tableWidget!)
    }
}
