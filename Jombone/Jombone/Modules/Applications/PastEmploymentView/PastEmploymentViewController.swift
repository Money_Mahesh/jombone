//
//  PastEmploymentViewController.swift
//  Jombone
//
//  Created by dev139 on 19/09/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import UIKit

class PastEmploymentViewController: UIViewController {
    
    @IBOutlet weak var stackView: UIStackView!
    var viewModel = PastEmploymentViewModel()
    
    var tableWidget: GenericTableViewComponent<JobCardViewProtocol, PastEmploymentCardView>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpTable()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpTable() {
        
        tableWidget = GenericTableViewComponent()
        tableWidget?.isHidden = false
        tableWidget?.genericTableViewModel = GenericTableViewModel(
            cellDataModel: viewModel.cardDataModels,
            cellOtherDetail: GenericTableViewCellOtherDetail(generic: nil, unique: nil),
            tableType: .expandable)
        tableWidget?.tableStyle(backGroundColor: UIColor(hexFromString: "#E0E0E0"), insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
        stackView.addArrangedSubview(tableWidget!)
    }
}
