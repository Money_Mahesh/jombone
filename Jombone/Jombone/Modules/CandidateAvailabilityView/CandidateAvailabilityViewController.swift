//
//  CandidateAvailabilityViewController.swift
//  Jombone
//
//  Created by dev139 on 23/08/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import UIKit

class CandidateAvailabilityViewController: UIViewController {

    @IBOutlet weak var availableSwitch: UISwitch!
    @IBOutlet weak var textfieldForDate: CustomTextfieldView!
    var viewModel = CandidateAvailbilityViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textfieldForDate.setFont(font: UIFont.rubikRegularFontOfSize(16.0),
                                     placeholderInActiveFont: UIFont.rubikRegularFontOfSize(16.0),
                                     placeholderActiveFont: UIFont.rubikRegularFontOfSize(12.0))
        
        textfieldForDate.datePickerDataSource(identifier: "datePicker", currentDate: nil, minDate: Date(), maxDate: nil, mode: .date, delegate: self) { (pickerSelectedTextArray, indexes, inputType) -> (String) in
            return Utility.changeDateToString(date: (pickerSelectedTextArray![0] as! Date), withFormat: "yyyy/MM/dd")
        }
        availableSwitch.isOn = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func actionForSwitch(_ sender: UISwitch) {
        viewModel.isAvailable = sender.isOn ? 1 : 0
    }
    
    @IBAction func btnActionForFinish(_ sender: UIButton) {
        view.endEditing(true)
        
        if let errorMsg = viewModel.validate(availableFrom: textfieldForDate.text) {
            showAlert(message: errorMsg)
            return
        }
        showLoader()
    
        viewModel.hitSetAvailabilityApi(completion: { (message) in
            self.hideLoader()
            self.showAlert(message: message)
        }, success: { (message) in
            self.hideLoader()
            self.showAlert(message: message)
        }) { (message) in
            self.hideLoader()
            self.showAlert(message: message)
        }
    }
  
}

extension CandidateAvailabilityViewController: CustomTextfieldViewDelegate {
    func datePickerViewSelected(identifier: String, textfield: CustomTextfield) {
    }
}
