//
//  CandidateSkillsViewController.swift
//  Jombone
//
//  Created by dev139 on 24/08/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import UIKit

class CandidateSkillsViewController: UIViewController {

    @IBOutlet weak var textFieldForSkill: CustomTextfieldView!
    @IBOutlet weak var constForStackViewHeight: NSLayoutConstraint!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var titleStackView: UIStackView!
    
    let cellHeight = 41
    var viewModel: CandidateSkillsViewModel?
    var tableWidget: GenericTableViewComponent<AutoCompleteCardViewModelProtocol, AutoCompleteCardView>?
    var autoCompleteViewModel = AutoCompleteViewModel(type: AutoComplete.local(LocalFor: LocalFor.skill), isOffline: false)

    static func show(viewModel: CandidateSkillsViewModel = CandidateSkillsViewModel(selectedOptions: nil)) {
        
        let viewController = LOGIN_STORYBOARD.instantiateViewController(withIdentifier: "CandidateSkillsViewController") as! CandidateSkillsViewController
        viewController.viewModel = viewModel
        APPLICATION_INSTANCE.visibleViewController()?.present(viewController, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textFieldForSkill.borderStyle = .All
        textFieldForSkill.setJomboneFontStyle()
        textFieldForSkill.textfieldViewDelegate = self
        
        setUpTable()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closebtnAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func setUpTable() {
        
        titleStackView.isHidden = (viewModel?.selectedOptions?.count ?? 0) == 0
        
        if tableWidget == nil {
            tableWidget = GenericTableViewComponent()
            
            let cellOtherGenericDetail: (style: AutoCompleteCardViewModel.ViewStyle, action: ((_ sender: Any, _ index: IndexPath, _ obj: AutoCompleteCardViewModelProtocol)->())?) = (style: AutoCompleteCardViewModel.ViewStyle.withCheckBoxCrossWhiteTitle, action: { (_ sender: Any, _ index: IndexPath, _ obj: AutoCompleteCardViewModelProtocol) in
                
                if let _sender = (sender as? UIButton), _sender.tag == -121 {
                    print(_sender.isSelected)
                    self.hitSetPrimarySkillApi(index: index.row, deSelectedOptionsEncryptedId: obj.encryptedId, isPrimary: _sender.isSelected)
                }
                else {
                    self.viewModel?.selectedOptions?[index.row].selected = false
                    self.hitRemoveSkillApi(index: index.row, deSelectedOptionsEncryptedId: obj.encryptedId)
                }
                })

            tableWidget?.genericTableViewModel = GenericTableViewModel(
                cellDataModel: viewModel?.selectedOptions,
                cellOtherDetail: GenericTableViewCellOtherDetail(generic: cellOtherGenericDetail, unique: nil),
                tableType: .nonScrollable)
            tableWidget?.tableStyle(backGroundColor: UIColor.clear, insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
            stackView.addArrangedSubview(tableWidget!)
        }
        constForStackViewHeight.constant = CGFloat(cellHeight * (viewModel?.selectedOptions?.count ?? 0))
    }
    
    func hitRemoveSkillApi(index: Int, deSelectedOptionsEncryptedId: String?) {
        
        guard let encryptedId = deSelectedOptionsEncryptedId else {
            return
        }
        
        showLoader()
        autoCompleteViewModel.hitRemoveIndustrySkillApi (
            encryptedId: encryptedId,
            completion: { (message) in
                self.hideLoader()
                self.showAlert(message: message)
        }, success: { (message) in
            
            let selectedOptions = self.viewModel?.selectedOptions?.filter { return $0.selected }
            self.selectedOption(type: AutoComplete.local(LocalFor: .skill), optionSelected: selectedOptions)
            
        }, failure: { (message) in
            
            self.viewModel?.selectedOptions?[index].selected = true
            self.selectedOption(type:AutoComplete.local(LocalFor: .skill), optionSelected: self.viewModel?.selectedOptions)
        })
    }
    
    func hitSetPrimarySkillApi(index: Int, deSelectedOptionsEncryptedId: String?, isPrimary : Bool) {
        
        guard let encryptedId = deSelectedOptionsEncryptedId else {
            return
        }
        showLoader()
        autoCompleteViewModel.hitSetPrimarySkillApi(encSkillId: encryptedId, isPrimary: isPrimary, completion: { (message) in
            self.hideLoader()
            self.showAlert(message: message)
        }, success: { (encryptedId, message) in
            if let skill = UserDataManager.shared.profileDetail?.skill?[index] {
                skill.isPrimary = isPrimary
//                UserDataManager.shared.profileDetail?.skill?[index] = skill

                let profileDetail = UserDataManager.shared.profileDetail
                var skills = UserDataManager.shared.profileDetail?.skill
                skills?[index] = skill
                profileDetail?.skill = skills
                UserDataManager.shared.profileDetail = profileDetail
            }
        }, failure: { (message) in
            
        })
    }
    
    //MARK:- IBAction
    @IBAction func btnActionForNext(_ sender: UIButton) {
        
        if let count = viewModel?.selectedOptions?.count, count > 0 {
            self.dismiss(animated: true, completion: nil)
            return
        }
        showAlert(message: "Please select at least one skill")
    }
}

extension CandidateSkillsViewController: CustomTextfieldViewDelegate {
    
    func textFieldDidBeginEditing(_ identifier: String, textField: CustomTextfield) {
        self.view.endEditing(true)
        
        AutoCompleteViewController.show(viewModel: AutoCompleteViewModel(selectedOptions: viewModel?.selectedOptions, type: .local(LocalFor: .skill), isOffline: false, noOfSelection: .multiple), delegate: self)
    }
}

extension CandidateSkillsViewController: AutoCompleteViewDelegate {
    func selectedOption(type: AutoComplete, optionSelected: Any?) {
        
        viewModel?.selectedOptions = optionSelected as? [AutoCompleteCardViewModelProtocol]
        tableWidget?.updateTableRecord(objArray: viewModel?.selectedOptions)
        
        let profileDetail = UserDataManager.shared.profileDetail
        profileDetail?.skill = viewModel?.selectedOptions as? [AutoSuggestObj]
        UserDataManager.shared.profileDetail = profileDetail
        
        
        constForStackViewHeight.constant = CGFloat(cellHeight * (viewModel?.selectedOptions?.count ?? 0))
        titleStackView.isHidden = (viewModel?.selectedOptions?.count ?? 0) == 0
    }
}
