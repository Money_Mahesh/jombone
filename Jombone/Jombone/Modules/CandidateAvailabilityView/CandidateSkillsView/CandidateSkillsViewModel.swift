//
//  CandidateSkillsViewModel.swift
//  Jombone
//
//  Created by Money Mahesh on 24/09/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import Foundation

class CandidateSkillsViewModel: NSObject {
    
    var selectedOptions: [AutoCompleteCardViewModelProtocol]?
    
    init(selectedOptions : [AutoCompleteCardViewModelProtocol]?) {
        self.selectedOptions = selectedOptions
    }
}
