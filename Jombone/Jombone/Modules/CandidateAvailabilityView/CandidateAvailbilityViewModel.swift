//
//  CandidateAvailbilityViewModel.swift
//  Jombone
//
//  Created by dev139 on 22/09/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import UIKit

class CandidateAvailbilityViewModel: NSObject {
    
    var isAvailable : Int = 0
    var availableFrom : String!
    
    func validate(availableFrom : String?, customMessage: String? = nil) -> String? {
        
        self.availableFrom = availableFrom
        
        return DataValidationUtility.shared.validateData(
            fields: [(data: availableFrom, name: "availableFrom", customMessage: customMessage)],
            type: (generic: fieldType.text,
                   specific: nil))
    }
    
    func hitSetAvailabilityApi(completion: ((_ message: String?)->())? = nil, success:  ((_ message: String?)->())? = nil, failure: ((_ message: String?)->())? = nil) {
        
        let authenticationServices = AutheticationServices(requestTag: "CANDIDATE_AVAILIBLITY_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        //Completion Block
        authenticationServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            if statusCode == 1 {
                _ = response.0 as? SignUpResponse
                completion?(message)
                success?(message)
            }
            else {
                completion?(message)
                failure?(message)
            }
        }
        authenticationServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            completion?(nil)
            failure?(nil)
        }
        
        let parameters: [String: Any] = [
            "isAvailable": isAvailable,
            "availableFrom" : availableFrom!
        ]
        
        
        authenticationServices.hitSetAvailability(parameters: parameters, additionalHeaderElements: nil)
    }
}
