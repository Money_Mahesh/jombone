//
//  SideMenuViewModel.swift
//  Jombone
//
//  Created by Money Mahesh on 27/07/18.
//  Copyright © 2018 Money Mahesh. All rights reserved.
//

import Foundation

class SideMenuViewModel: NSObject {
    
    lazy var cardDataModels: [SettingsCardDataModel] = [
        SettingsCardDataModel(title: "My Profile", icon: UIImage(named: "iconMenuProfile")!),
        SettingsCardDataModel(title: "Preferences", icon: UIImage(named: "iconMenuPrefrences")!),
        SettingsCardDataModel(title: "Liked Jobs", icon: UIImage(named: "iconMenuFavorite")!),
        SettingsCardDataModel(title: "Followed Companies", icon: UIImage(named: "iconMenuFollowedComp")!),
        SettingsCardDataModel(title: "Terms & Conditions", icon: UIImage(named: "iconMenuTerms&Condition")!),
        SettingsCardDataModel(title: "User Agreement", icon: UIImage(named: "iconMenuUserAgreement")!),
        SettingsCardDataModel(title: "Privacy Policy", icon: UIImage(named: "iconMenuPrivacyPolicy")!),
        SettingsCardDataModel(title: "Locations", icon: UIImage(named: "iconMenuLocation")!),
        SettingsCardDataModel(title: "Contact Us", icon: UIImage(named: "iconMenuContactUs")!),
        SettingsCardDataModel(title: "Settings", icon: UIImage(named: "iconMenuSettings")!),
        SettingsCardDataModel(title: "Log Out", icon: UIImage(named: "iconMenuLogOut")!)
    ]
}
