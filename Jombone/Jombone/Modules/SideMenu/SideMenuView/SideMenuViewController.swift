//
//  SideMenuViewController.swift
//  Jombone
//
//  Created by Money Mahesh on 27/07/18.
//  Copyright © 2018 Money Mahesh. All rights reserved.
//

import UIKit
import SideMenuSwift

class SideMenuViewController: UIViewController {
    
    enum MenuOption: Int {
        case Profile = 0
        case Preferences = 1
        case SaveJobs = 2
        case FollowedJobs = 3
        case TermsAndConditions = 4
        case UserAgreement = 5
        case PrivacyPolicy = 6
        case Locations = 7
        case ContactUs = 8
        case Settings = 9
        case LogOut = 10
    }
    
    @IBOutlet weak var labelForName: UILabel!
    @IBOutlet weak var labelForJomboneId: UILabel!
    @IBOutlet weak var labelForEmailId: UILabel!
    @IBOutlet weak var labelForPhoneNo: UILabel!
    @IBOutlet weak var imageViewForProfilePic: UIImageView!
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet var viewModel: SideMenuViewModel!
    
    var menuListView: GenericTableViewComponent<SettingsCardDataModel, SettingsCardView>!
    var currentNavgationController: UINavigationController? {
        get {
            if let navigationController = ((APPLICATION_INSTANCE.visibleViewController() as? SideMenuController)?.contentViewController as? UINavigationController) {
                return navigationController
            }
            else {
                return sideMenuController?.contentViewController.navigationController
            }
        }
    }
    
    static func contentNavigationController() -> UINavigationController? {
        
        let currentNavgationController: UINavigationController? = (APPLICATION_INSTANCE.visibleViewController() as? SideMenuController)?.contentViewController.navigationController
        return currentNavgationController
    }
    
    static func instance() -> SideMenuViewController{
        
        let instance = MAIN_STORYBOARD.instantiateViewController(withIdentifier: "SideMenuViewController") as! SideMenuViewController
        return instance
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUserDetail()
        
        menuListView = GenericTableViewComponent()
        menuListView?.tableStyle(backGroundColor: UIColor.clear, insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
        menuListView.delegate = self
        menuListView.genericTableViewModel = GenericTableViewModel(
            cellDataModel: viewModel.cardDataModels,
            tableHeaderView: nil,
            tableFooterView: nil,
            tableType: TableType.expandable)
        stackView.addArrangedSubview(menuListView)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(SideMenuViewController.setUserDetail), name: Notification.Name.UserProfileUpdated, object: nil)
        
    }
    
    
    @IBAction func imageViewTapAction(_ sender: UITapGestureRecognizer) {
        sideMenuController?.hideMenu()
        currentNavgationController?.pushViewController(ProfileViewController.instance(), animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func setUserDetail() {
        
        labelForName.text = ((UserDataManager.shared.profileDetail?.personalDetailsBeans?.firstName ?? "") + " " + (UserDataManager.shared.profileDetail?.personalDetailsBeans?.lastName ?? ""))
        labelForJomboneId.text = (UserDataManager.shared.profileDetail?.personalDetailsBeans?.jomboneId ?? "")
        labelForEmailId.text = UserDataManager.shared.profileDetail?.personalDetailsBeans?.email ?? ""
        labelForPhoneNo.text = ((UserDataManager.shared.profileDetail?.personalDetailsBeans?.countryCode ?? "") + " " + (UserDataManager.shared.profileDetail?.personalDetailsBeans?.phoneNumber ?? ""))
        
        let userImageURL = UserDataManager.shared.profileDetail?.personalDetailsBeans?.profilePicPath
        if let imageUrlStr = userImageURL, let imageUrl = URL(string: imageUrlStr) {
            
            let url_request = URLRequest(url: imageUrl)
            
            imageViewForProfilePic.setImageWithURLAlamofire(url_request, placeholderImageName: nil, success: {
                [weak imageViewForProfilePic] (request:URLRequest?, image:UIImage?) -> Void in
                imageViewForProfilePic?.image = image }
                ,failure: {
                    [weak imageViewForProfilePic] (request:URLRequest?, error:Error?) -> Void in
                    imageViewForProfilePic?.image = UIImage(named: "profileDefault")
                    print("Not able to load profile pic" + (error?.localizedDescription ?? ""))
            })
        }
        else {
            imageViewForProfilePic?.image = UIImage(named: "profileDefault")
        }
    }
    
    var notificationAction: ((_ status: Bool)->()) = {(status) in
        print("Status " + String(status))
    }
    
    //MARK:- IBActions
    @IBAction func btnActionForEditProfile(_ sender: UIButton) {

        sideMenuController?.hideMenu()
        currentNavgationController?.pushViewController(ProfileViewController.instance(), animated: true)
    }
}

extension SideMenuViewController: GenericTableViewComponentDelegate {
    
    func genericTableDidSelectRowAt(_ indexPath: IndexPath, dataSource: inout [Any]) -> Bool {
    
        sideMenuController?.hideMenu()
        
        switch MenuOption(rawValue: indexPath.row)! {

        case MenuOption.Profile:
            currentNavgationController?.pushViewController(ProfileViewController.instance(), animated: true)

        case MenuOption.Preferences:
            currentNavgationController?.pushViewController(PreferencesViewController.instance(), animated: true)
            
        case MenuOption.SaveJobs:
            currentNavgationController?.pushViewController(LikedJobsViewController.instance(), animated: true)
            
        case MenuOption.FollowedJobs:
            currentNavgationController?.pushViewController(FollowedCompanyViewController.instance(), animated: true)
            
        case MenuOption.UserAgreement:
            currentNavgationController?.pushViewController(WebViewViewController.instance(viewModel: WebViewModel(urlStr: BASE_URL.absoluteString + "/jombone/user-agreement", title: "User Agreement")), animated: true)
            
        case MenuOption.Settings:
            currentNavgationController?.pushViewController(SettingsViewController.instance(), animated: true)
            
        case MenuOption.Locations:
            currentNavgationController?.pushViewController(WebViewViewController.instance(viewModel: WebViewModel(urlStr: BASE_URL.absoluteString + "/jombone/locations", title: "Locations")), animated: true)
            
        case MenuOption.ContactUs:
            currentNavgationController?.pushViewController(ContactUsViewController.instance(), animated: true)

            
        case MenuOption.TermsAndConditions:
            currentNavgationController?.pushViewController(WebViewViewController.instance(viewModel: WebViewModel(urlStr: BASE_URL.absoluteString + "/jombone/terms-and-condition", title: "Terms & Conditions")), animated: true)

        case MenuOption.PrivacyPolicy:
            currentNavgationController?.pushViewController(WebViewViewController.instance(viewModel: WebViewModel(urlStr: BASE_URL.absoluteString + "/jombone/privacy", title: "Privacy Policy")), animated: true)

        case MenuOption.LogOut:
            LogOutViewController.show()
        }

        return true
    }
}
