//
//  NotificationListViewModel.swift
//  Jombone
//
//  Created by Money Mahesh on 11/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation

class NotificationListViewModel: NSObject {
    
    var cardDataModels = [NotificationCardProtocol]()

    var pageNo = 0
    var totalPages = 0
    var dataAvailable: Bool! {
        get {
            return (cardDataModels.count != 0)
        }
    }
    
    func reset() {
        self.pageNo = 0
        cardDataModels.removeAll()
    }
    
    func hitGetNotificationsApi(completion: ((_ message: String?)->())? = nil, success:  ((_ message: String?)->())? = nil, failure: ((_ message: String?)->())? = nil) {
        
        let notificationServices = NotificationServices(requestTag: "NOTIFICATION_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        //Completion Block
        notificationServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            if let notificationList = (response.0 as? [NotificationModel]),
                let pagination = (response.1 as? Pagination),
                let totalPages = pagination.totalPages,
                let currentPage = pagination.currentPage,
                self.pageNo == currentPage, statusCode == 1 {
                
                self.pageNo += 1
                self.cardDataModels.append(contentsOf: notificationList)
                self.totalPages = totalPages
                
                completion?(message)
                success?(message)
            }
            else {
                completion?(message)
                failure?(message)
            }
        }
        notificationServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            completion?(nil)
            failure?(nil)
        }
        
        let parameters: [String: Any] = [
            "size": String(PAGE_SIZE),
            "page": String(pageNo),
        ]
        
        notificationServices.getNotifications(parameters: parameters, additionalHeaderElements: nil)
    }
    
    func deleteNotification(notificationEncId: String, completion: ((_ message: String?)->())? = nil, success:  ((_ message: String?)->())? = nil, failure: ((_ message: String?)->())? = nil) {
        
        let notificationServices = NotificationServices(requestTag: "NOTIFICATION_DELETE_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        //Completion Block
        notificationServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            if statusCode == 1 {
                completion?(message)
                success?(message)
            }
            else {
                completion?(message)
                failure?(message)
            }
        }
        notificationServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            completion?(nil)
            failure?(nil)
        }
        
        notificationServices.deleteNotification(parameters: ["notificationEncId": notificationEncId], additionalHeaderElements: nil)
    }
}
