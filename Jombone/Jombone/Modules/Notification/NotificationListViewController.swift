//
//  NotificationListViewController.swift
//  Jombone
//
//  Created by Money Mahesh on 23/02/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import UIKit

class NotificationListViewController: TabBarChildViewController {
    
    @IBOutlet weak var stackView: UIStackView?
    @IBOutlet weak var noResultView: UIView?
    var viewModel = NotificationListViewModel()
    
    var tableWidget: GenericTableViewComponent<NotificationCardProtocol, NotificationCardView>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "NOTIFICATIONS"

        self.addObserver(self, selector: #selector(NotificationListViewController.reloadList), notificationsName: [Notification.Name.ReloadNotificationList])
        self.addObserver(self, selector: #selector(NotificationListViewController.updatedList), notificationsName: [Notification.Name.UpdateNotificationList])

        self.addObserver(self, selector: #selector(NotificationListViewController.notificationDeleted(_:)), notificationsName: [Notification.Name.NotificationDeleted])

        self.showLoader()
        self.getNotificationList()
    }
    
    override func updatedList() {
        if !self.isViewLoaded {
            return
        }
        viewModel.reset()
        setUpTable()
        
        self.showLoader()
        self.getNotificationList()
    }
    
    @objc func reloadList() {
        tableWidget?.reloadTable()
    }
    
    @objc func notificationDeleted(_ notification: NSNotification) {
        
        if let detail = notification.userInfo?["DeleteBtn"] as? UIButton,
            let nId = notification.userInfo?["id"] as? String {
            
            self.deleteNotification(notificationEncId: nId) {
                if let indexPath = self.tableWidget?.deleteCellIndex(sender: detail) {
                    self.viewModel.cardDataModels.remove(at: indexPath.row)
                }
            }
        }
    }
    
    func getNotificationList(){
        
        viewModel.hitGetNotificationsApi(completion: { (message) in
            self.hideLoader()
            self.noResultView?.isHidden = self.viewModel.dataAvailable
        }, success: { (message) in
            self.setUpTable()
        }, failure: { (errorMsg) in
//            self.showAlert(message: errorMsg)
        })
    }
    
    func deleteNotification(notificationEncId: String, completion: @escaping (()->())) {
        
        self.showLoader()
        viewModel.deleteNotification(notificationEncId: notificationEncId, completion: { (message) in
            self.hideLoader()
            self.showAlert(message: message)
        }, success: { (message) in
            completion()
        })
    }
    
    override func imageBtnAction(sender: UIButton) {
    }
    
    func setUpTable() {
        
        if let _tableWidget = tableWidget {
            _tableWidget.appendNewPage(objArray: viewModel.cardDataModels)
        }
        else {
            tableWidget = GenericTableViewComponent()
            tableWidget?.isHidden = false
            tableWidget?.genericTableViewModel = GenericTableViewModel(
                cellDataModel: viewModel.cardDataModels,
                tableFooterView: (LoadMoreView(frame: CGRect(x: 0, y: 0, width: WINDOW_WIDTH, height: 40)), false),
                tableType: .expandable,
                totalPages: viewModel.totalPages)

            tableWidget?.tableStyle(backGroundColor: UIColor.white, insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
            tableWidget?.delegate = self
            stackView?.addArrangedSubview(tableWidget!)
        }
    }
    
    func navigateTo(status: Int) {
    
        var viewController: UIViewController?

        switch status {
        case 2:
            viewController = OfferListViewController.instance(navBtnType: .back)
            
        case 3:
            viewController = Reject_WithdrawListViewController.instance(navBtnType: .back)
            
        case 4:
            viewController = CurrentWorkListViewController.instance(navBtnType: .back)
        
        case 5:
            viewController = PastWorkListViewController.instance(navBtnType: .back)
            
        default:
            break
        }
        

        if let _viewController = viewController {
            APPLICATION_INSTANCE.visibleViewController()?.navigationController?.pushViewController(_viewController, animated: true)

//            self.navigationController?.pushViewController(_viewController, animated: true)
        }
//        For Status 2 - open offers listing screen
//        For Status 3 - open rejected/withdrawn listing screen
//        For Status 4 - open current work listing screen
//        For Status 5 - open past work listing screen
//        Added  Status 6 - For no any action.
    }
}

extension NotificationListViewController: GenericTableViewComponentDelegate {
    
    func genericTableDidSelectRowAt(_ indexPath: IndexPath, dataSource: inout [Any]) -> Bool {
        
        viewModel.cardDataModels[indexPath.row].read = true
        dataSource = viewModel.cardDataModels
        
        if let status = viewModel.cardDataModels[indexPath.row].status {
            navigateTo(status: status)
        }
        return false
    }
    
    func loadMore() {
        getNotificationList()
    }
}

