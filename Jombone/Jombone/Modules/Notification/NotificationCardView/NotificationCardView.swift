//
//  NotificationCardView.swift
//  Jombone
//
//  Created by Money Mahesh on 13/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import UIKit

class NotificationCardView: GenericView<NotificationCardProtocol> {
    
    @IBOutlet weak var imgViewForProfilePic: UIImageView?
    @IBOutlet weak var viewForReadStatus: UIView?

    @IBOutlet weak var lblForMessage: UILabel?
    @IBOutlet weak var lblForPostTime: UILabel?
    
    override var viewModel: ViewModel<NotificationCardProtocol>? {
        didSet {
            
            lblForMessage?.text = viewModel?.data?.message
            lblForPostTime?.text = viewModel?.data?.date
            
            self.imgViewForProfilePic?.image = UIImage(named: "defaultNotification")
            self.imgViewForProfilePic?.contentMode = .center
            imgViewForProfilePic?.backgroundColor = UIColor(hexFromString: "#fefefe")
            if let _imageUrlStr = viewModel?.data?.photoPath,
                let imageUrl = URL(string: (_imageUrlStr)) {
                
                let url_request = URLRequest(url: imageUrl)
                self.imgViewForProfilePic?.setImageWithURLAlamofire(url_request, placeholderImageName: "defaultNotification", success: {
                    [weak self] (request: URLRequest?, image: UIImage?) -> Void in
                    self?.imgViewForProfilePic?.backgroundColor = UIColor.clear
                    self?.imgViewForProfilePic?.image = image
                    self?.imgViewForProfilePic?.contentMode = .scaleAspectFill
                    }
                    ,failure: {
                        [weak self] (request:URLRequest?, error:Error?) -> Void in
                        self?.imgViewForProfilePic?.contentMode = .center
                        self?.imgViewForProfilePic?.image = UIImage(named: "defaultNotification")
                        print("Not able to load profile pic" + (error?.localizedDescription ?? ""))
                })
            }
            
            setReadStatus()
        }
    }
    
    func setReadStatus() {
        
        viewForReadStatus?.isHidden = viewModel?.data?.read ?? true
        lblForMessage?.font = UIFont.rubikMediumFontOfSize(14)
    }
    
    //MARK:-IBActions
    @IBAction func btnActionForOption(_ sender: UIButton) {
        
        guard let notificationId = viewModel?.data?.notificationEncId else {
            return
        }
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.destructive, handler: nil)
        let deleteAction = UIAlertAction(title: "Delete", style: UIAlertAction.Style.default, handler: { (action) in
            
            NotificationCenter.default.post(name: NSNotification.Name.NotificationDeleted, object: nil, userInfo: ["DeleteBtn" : sender, "id": notificationId])
        })

        actionSheet.addAction(deleteAction)
        actionSheet.addAction(cancelAction)
        
        APPLICATION_INSTANCE.visibleViewController()?.present(actionSheet, animated: true, completion: nil)
    }
}
