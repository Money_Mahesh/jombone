//
//  NotificationCardViewModel.swift
//  Jombone
//
//  Created by Money Mahesh on 13/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation

protocol NotificationCardProtocol {
    var photoPath: String? {get set}
    var status: Int? {get set}
    var message: String? {get set}
    var notificationEncId: String? {get set}
    var read: Bool? {get set}
    var time: String? {get set}
    var date: String? {get set}
}

class NotificationCardViewModel: ViewModel<NotificationCardProtocol> {
    
    override init(data: NotificationCardProtocol?, otherDetail: Any?) {
        super.init(data: data, otherDetail: otherDetail)
    }
}
