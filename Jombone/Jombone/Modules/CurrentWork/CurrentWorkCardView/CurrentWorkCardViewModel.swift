//
//  Reject_WithdrawCardView.swift
//  Jombone
//
//  Created by Money Mahesh on 13/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation

protocol CurrentWorkCardViewProtocol: JobCardViewProtocol {
    var isExpanded: Bool! {get set}
    var totalWorkTime : String? {get set}
    var isSignIn: Bool? {get set}
    var isSignOut: Bool? {get set}
    var timesheetEncId: String? {get set}
}

class CurrentWorkCardViewModel: ViewModel<CurrentWorkCardViewProtocol> {
    
    override init(data: CurrentWorkCardViewProtocol?, otherDetail: Any?) {
        super.init(data: data, otherDetail: otherDetail)
    }
    
}
