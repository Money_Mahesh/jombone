//
//  CurrentWorkCardView.swift
//  Jombone
//
//  Created by Money Mahesh on 13/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import UIKit

class CurrentWorkCardView: GenericView<CurrentWorkCardViewProtocol>, WorkAction {
    
    @IBOutlet weak var jobCardBaseViewStack: UIStackView?
    @IBOutlet weak var timesheetView: UIView?
    @IBOutlet weak var lblForTotalHrs: UILabel?
    @IBOutlet weak var btnForSignIn: UIButton?
    @IBOutlet weak var btnForSignOut: UIButton?
    @IBOutlet weak var btnForTimeSheet: UIButton?

    var jobCardBaseView: JobCardBaseView!
    weak var workDelegate: WorkAction?
    
    override func xibLoaded() {
        workDelegate = self
        jobCardBaseView = JobCardBaseView()
        jobCardBaseViewStack?.addArrangedSubview(jobCardBaseView)
    }
    
    override var viewModel: ViewModel<CurrentWorkCardViewProtocol>? {
        didSet {
            jobCardBaseView.viewModel = JobCardBaseViewModel(data: viewModel?.data, otherDetail: nil)
            lblForTotalHrs?.text = viewModel?.data?.totalWorkTime == nil ? nil : "Total hours: " + viewModel!.data!.totalWorkTime!
            setTimeSheetView()
            
            setBtn(btn: btnForSignIn, isEnable: viewModel?.data?.isSignIn, forSignIn: true)
            setBtn(btn: btnForSignOut, isEnable: (!(viewModel?.data?.isSignIn != viewModel?.data?.isSignOut)), forSignIn: false)
        }
    }
    
    func setTimeSheetView() {
        let isExpanded = (viewModel?.data?.isExpanded ?? false)
        btnForTimeSheet?.setImageForAllState(image: UIImage(named: isExpanded ? "dropUpBlueArrow" : "dropDownBlueArrow"))
        timesheetView?.isHidden = !isExpanded
    }
    
    func setBtn(btn: UIButton?, isEnable state: Bool?, forSignIn: Bool) {
        btn?.backgroundColor = UIColor(hexFromString: "#28bf62", alpha: (state ?? false) ? 0.56 : 1.0)
        
        let btnTitle = forSignIn ? ((state ?? false) ? "SIGNED IN" : "SIGN IN") : ((state ?? false) ? "SIGNED OUT" : "SIGN OUT")
        btn?.setTitleForAllState(title: btnTitle)
    }
    
    //MARK:- IBActions
    @IBAction func btnActionForTimesheet(_ sender: UIButton) {
        
        guard let _jobId = viewModel?.data?.jobId else {
            return
        }
        let isExpanded = !(viewModel?.data?.isExpanded ?? false)
        viewModel?.data?.isExpanded = isExpanded
        setTimeSheetView()
        NotificationCenter.default.post(name: NSNotification.Name.CollapseAllCurrentWorkCard, object: nil, userInfo: ["CardId" : _jobId])
    }
    
    @IBAction func btnActionForSignIn(_ sender: UIButton) {
        
        guard let isSignIn = viewModel?.data?.isSignIn,
            let jobEncId = viewModel?.data?.jobId, !isSignIn else {
                return
        }
        
        CustomAlertViewController.show(data: CustomAlertDataModel(title: "Sign In", subTitle: "You are about to sign in.\nSign in time cannot be changed.\nPlease confirm.", completion: {
            
            self.workDelegate?.signIn(jobEncId: jobEncId, completion: { (status, timesheetEncId) in
                
                if status {
                    self.viewModel?.data?.timesheetEncId = timesheetEncId
                    self.viewModel?.data?.isSignIn = true
                    self.setBtn(btn: sender, isEnable: self.viewModel?.data?.isSignIn, forSignIn: true)
                    self.setBtn(btn: self.btnForSignOut, isEnable: (!(self.viewModel?.data?.isSignIn != self.viewModel?.data?.isSignOut)), forSignIn: false)
                }
            })
        }))
    }
    
    @IBAction func btnActionForSignOut(_ sender: UIButton) {
        
        guard let isSignOut = viewModel?.data?.isSignOut,
            let timesheetEncId = viewModel?.data?.timesheetEncId, !isSignOut else {
            return
        }
        
        CustomAlertViewController.show(data: CustomAlertDataModel(title: "Sign Out", subTitle: "You are about to sign out.\nSign out time cannot be changed.\nPlease confirm.", completion: {
            
            self.workDelegate?.signOut(timesheetEncId: timesheetEncId, completion: {[weak self] (status) in
                
                if status {
                    self?.viewModel?.data?.isSignOut = true
                    self?.setBtn(btn: sender, isEnable: !(self?.viewModel?.data?.isSignIn != self?.viewModel?.data?.isSignOut), forSignIn: false)
                }
            })
        }))
    }
    
    @IBAction func btnActionForPreviousTimeSheet(_ sender: UIButton) {
        
        guard let jobEncId = viewModel?.data?.jobId else {
            return
        }
        
        TimeSheetListViewController.show(viewModel: TimeSheetListViewModel(jobEncId: jobEncId))
    }
}

