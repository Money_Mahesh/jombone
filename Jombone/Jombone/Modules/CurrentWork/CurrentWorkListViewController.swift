//
//  CurrentWorkListViewController.swift
//  Jombone
//
//  Created by Money Mahesh on 23/02/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import UIKit

class CurrentWorkListViewController: PopUpViewController {
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var noResultView: UIView!
    
    var navBtnType: NavBtnType = .menu
    var viewModel = CurrentWorkListViewModel()
    
    var tableWidget: GenericTableViewComponent<CurrentWorkCardViewProtocol, CurrentWorkCardView>?
    
    static func instance(navBtnType: NavBtnType = .back) -> CurrentWorkListViewController {
        
        let viewController = WORK_STORYBOARD.instantiateViewController(withIdentifier: "CurrentWorkListViewController") as! CurrentWorkListViewController
        viewController.navBtnType = navBtnType
        viewController.viewModel = CurrentWorkListViewModel()
        return viewController
    }
    
    static func show(navBtnType: NavBtnType = .menu) {
        
        let viewController = WORK_STORYBOARD.instantiateViewController(withIdentifier: "CurrentWorkListViewController") as! CurrentWorkListViewController
        viewController.navBtnType = navBtnType
        viewController.viewModel = CurrentWorkListViewModel()
        APPLICATION_INSTANCE.visibleViewController()?.navigationController?.pushViewController(viewController, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationBarWithTitle("Current Employment", andLeftButton: navBtnType, andRightButton: .none, withBg: .defaultColor)

        self.addObserver(self, selector: #selector(CurrentWorkListViewController.reloadList), notificationsName: [Notification.Name.ReloadCurrentWorkList])
        self.addObserver(self, selector: #selector(CurrentWorkListViewController.collapseAllCard), notificationsName: [Notification.Name.CollapseAllCurrentWorkCard])

        self.showLoader()
        self.getCurrentWorkList()
    }
    
    func updatedList() {
        viewModel.reset()
        setUpTable()
        
        self.showLoader()
        self.getCurrentWorkList()
    }
    
    @objc func reloadList() {
        tableWidget?.reloadTable()
    }
    
    func getCurrentWorkList(){
        
        viewModel.hitGetCurrentWorkListApi(completion: { (message) in
            self.hideLoader()
            self.noResultView.isHidden = self.viewModel.dataAvailable
        }, success: { (message) in
            self.setUpTable()
        }, failure: { (errorMsg) in
//            self.showAlert(message: errorMsg)
        })
    }
    
    func setUpTable() {
        
        if let _tableWidget = tableWidget {
            _tableWidget.appendNewPage(objArray: viewModel.cardDataModels)
        }
        else {
            tableWidget = GenericTableViewComponent()
            tableWidget?.isHidden = false
            tableWidget?.genericTableViewModel = GenericTableViewModel(
                cellDataModel: viewModel.cardDataModels,
                tableFooterView: (LoadMoreView(frame: CGRect(x: 0, y: 0, width: WINDOW_WIDTH, height: 40)), false),
                tableType: .expandable,
                totalPages: viewModel.totalPages)

            tableWidget?.tableStyle(backGroundColor: UIColor.white, insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
            tableWidget?.delegate = self
            stackView.addArrangedSubview(tableWidget!)
        }
    }
    
    //MARK:- Tool Tip
    @objc func collapseAllCard(notification: NSNotification) {
        
        if let cardId = notification.userInfo?["CardId"] as? String {
            
            for (index, obj) in viewModel.cardDataModels.enumerated() {
                if obj.jobId != cardId {
                    viewModel.cardDataModels[index].isExpanded = false
                }
            }
            
            self.tableWidget?.reloadTable()
        }
    }
}

extension CurrentWorkListViewController: GenericTableViewComponentDelegate {
    
    func genericTableDidSelectRowAt(_ indexPath: IndexPath, dataSource: inout [Any]) -> Bool {
        
        if let jobModel = viewModel.cardDataModels[indexPath.row] as? JobModel {
            JobsDetailViewController.show(JobsDetailViewModel(jobDetail: jobModel), parentController: self)
        }
        return false
    }
    
    func loadMore() {
        getCurrentWorkList()
    }
}

