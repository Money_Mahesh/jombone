//
//  TimeSheetCardView.swift
//  Jombone
//
//  Created by Money Mahesh on 13/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import UIKit

class TimeSheetCardView: GenericView<TimeSheetCardViewProtocol> {
    
    @IBOutlet weak var lblForDate: UILabel?
    @IBOutlet weak var lblForDay: UILabel?
    @IBOutlet weak var lblForSignIn: UILabel?
    @IBOutlet weak var lblForSignOut: UILabel?
    @IBOutlet weak var lblForTotalHrs: UILabel?
    @IBOutlet weak var lblForApprovedTime: UILabel?

    
    override func xibLoaded() {
    }
    
    override var viewModel: ViewModel<TimeSheetCardViewProtocol>? {
        didSet {
            lblForDate?.text = viewModel?.data?.displayDate
            lblForDay?.text = viewModel?.data?.weekDay
            lblForSignIn?.text = viewModel?.data?.signInTime
            lblForSignOut?.text = viewModel?.data?.signOutTime
            lblForTotalHrs?.text = viewModel?.data?.totalTime
            lblForApprovedTime?.text = viewModel?.data?.approvedTime
        }
    }
    
    
    func setBtn(btn: UIButton?, isEnable state: Bool?) {
        btn?.backgroundColor = UIColor(hexFromString: "#28bf62", alpha: (state ?? false) ? 0.56 : 1.0)
    }
    
    //MARK:- IBActions
    @IBAction func btnActionForSignIn(_ sender: UIButton) {
        
        if let _location = viewModel?.data?.signInLocation {
            NotificationCenter.default.post(name: NSNotification.Name.ShowLocationToolTip, object: nil, userInfo: ["Label" : lblForSignIn!, "location": _location])
        }
    }
    
    @IBAction func btnActionForSignOut(_ sender: UIButton) {
       
        if let _location = viewModel?.data?.signOutLocation {
            NotificationCenter.default.post(name: NSNotification.Name.ShowLocationToolTip, object: nil, userInfo: ["Label" : lblForSignOut!, "location": _location])
        }
    }
}

