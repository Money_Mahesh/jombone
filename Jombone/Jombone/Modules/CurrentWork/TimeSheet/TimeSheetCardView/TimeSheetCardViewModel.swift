//
//  Reject_WithdrawCardView.swift
//  Jombone
//
//  Created by Money Mahesh on 13/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation

protocol TimeSheetCardViewProtocol {
    var weekDay : String? {get set}
    var displayDate : String? {get set}
    var signInTime : String? {get set}
    var signOutTime : String? {get set}
    var totalTime : String? {get set}
    var approvedTime : String? {get set}
    var signInLocation : String? {get set}
    var signOutLocation : String? {get set}
}

class TimeSheetCardViewModel: ViewModel<TimeSheetCardViewProtocol> {
    
    override init(data: TimeSheetCardViewProtocol?, otherDetail: Any?) {
        super.init(data: data, otherDetail: otherDetail)
    }
    
}
