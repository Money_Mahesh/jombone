//
//  TimeSheetListViewController.swift
//  Jombone
//
//  Created by Money Mahesh on 23/02/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import UIKit
import EasyTipView

class TimeSheetListViewController: UIViewController {
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var noResultView: UIView!
    @IBOutlet weak var lblForDuration: UILabel!
    @IBOutlet weak var viewForHeader: UIView!
    @IBOutlet weak var lblForWeeklyAppHrs: UILabel!
    @IBOutlet weak var locationToolTip: LocationToolTip!
    @IBOutlet weak var btnForNext: UIButton!

    var viewModel: TimeSheetListViewModel!
    var tableWidget: GenericTableViewComponent<TimeSheetCardViewProtocol, TimeSheetCardView>?
    var preferences = EasyTipView.Preferences()
    var tipView: EasyTipView?
    var overlay: UIView!

    static func instance(viewModel: TimeSheetListViewModel) -> TimeSheetListViewController {
        
        let viewController = WORK_STORYBOARD.instantiateViewController(withIdentifier: "TimeSheetListViewController") as! TimeSheetListViewController
        viewController.viewModel = viewModel
        return viewController
    }
    
    static func show(viewModel: TimeSheetListViewModel) {
        
        let viewController = WORK_STORYBOARD.instantiateViewController(withIdentifier: "TimeSheetListViewController") as! TimeSheetListViewController
        viewController.viewModel = viewModel
        APPLICATION_INSTANCE.visibleViewController()?.navigationController?.pushViewController(viewController, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationBarWithTitle("Previous Timesheet", andLeftButton: .back, andRightButton: .none, withBg: .defaultColor)
        
        self.addObserver(self, selector: #selector(TimeSheetListViewController.reloadList), notificationsName: [Notification.Name.ReloadCurrentWorkList])
        self.addObserver(self, selector: #selector(TimeSheetListViewController.handleToolTip(notification:)), notificationsName: [Notification.Name.ShowLocationToolTip])

        self.btnForNext.isEnabled = false
        self.hitGetTimeSheetApi()
        self.setToolTipPreferences()
    }
    
    func setToolTipPreferences() {
    
        overlay = UIView(frame: CGRect(x: 0, y: 0, width: WINDOW_WIDTH, height: WINDOW_HEIGHT), backgroundColor: UIColor(white: 0, alpha: 0.4))
        overlay.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(TimeSheetListViewController.dismissToolTip))
        overlay.addGestureRecognizer(tapGesture)
        
        preferences.drawing.font = UIFont.rubikRegularFontOfSize(14)
        preferences.drawing.foregroundColor = UIColor(red: 117/255, green: 117/255, blue: 117/255, alpha: 1.0)
        preferences.drawing.backgroundColor = UIColor.white
        preferences.drawing.arrowPosition = EasyTipView.ArrowPosition.left
    }
    
    func updatedList() {
        viewModel.reset()
        setUpTable()
        
        self.showLoader()
        self.hitGetTimeSheetApi()
    }
    
    @objc func reloadList() {
        tableWidget?.reloadTable()
    }
    
    func hitGetTimeSheetApi(){
        
        lblForDuration.text = viewModel.title

        self.showLoader()
        viewModel.hitGetTimeSheetListApi(completion: { (message) in
            self.hideLoader()
            self.noResultView.isHidden = self.viewModel.dataAvailable
        }, success: { (message) in
            self.setUpTable()
        }, failure: { (errorMsg) in
//            self.showAlert(message: errorMsg)
        })
    }
    
    func setUpTable() {
        
        lblForWeeklyAppHrs.text = "WEEKLY APPROVED HOURS  : " + (viewModel.dataModels?.weeklyApprovedTime ?? "0")
        if let _tableWidget = tableWidget {
            _tableWidget.updateTableRecord(objArray: viewModel.dataModels?.previousTimesheetViewBeanList)
        }
        else {
            viewForHeader.frame.size = CGSize(width: WINDOW_WIDTH, height: 65)
            tableWidget = GenericTableViewComponent()
            tableWidget?.isHidden = false
            tableWidget?.genericTableViewModel = GenericTableViewModel(
                cellDataModel: viewModel.dataModels?.previousTimesheetViewBeanList,
                tableType: .expandable)

            tableWidget?.tableStyle(backGroundColor: UIColor.white, insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))

            stackView.addArrangedSubview(tableWidget!)
        }
    }
    
    //MARK:- IBActions
    @IBAction func btnActionForLeft(_ sender: UIButton) {
        
        viewModel.decrementDate()
        btnForNext.isEnabled = !(viewModel.weekNo == 0)
        self.hitGetTimeSheetApi()
    }
    
    @IBAction func btnActionForRight(_ sender: UIButton) {
        
        viewModel.incrementDate()
        btnForNext.isEnabled = !(viewModel.weekNo == 0)
        self.hitGetTimeSheetApi()
    }
    
    //MARK:- Tool Tip
    @objc func handleToolTip(notification: NSNotification) {
        
        if let label = notification.userInfo?["Label"] as? UILabel, let location = notification.userInfo?["location"] as? String {
            
            addOverlay()
            self.tipView = EasyTipView(text: location, preferences: preferences, delegate: self)
            self.tipView?.show(forView: label, withinSuperview: self.view)
        }
    }
    
    func addOverlay() {
        self.view.addSubview(overlay)
    }
    
    func removeOverlay() {
        overlay.removeFromSuperview()
    }
    
    @objc func dismissToolTip() {
        self.tipView?.dismiss()
    }
}

extension TimeSheetListViewController: EasyTipViewDelegate {
    func easyTipViewDidDismiss(_ tipView: EasyTipView) {
        removeOverlay()
    }
}
