//
//  TimeSheetListViewModel.swift
//  Jombone
//
//  Created by Money Mahesh on 11/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation

class TimeSheetListViewModel: NSObject {
    
    var dataModels: TimeSheetModel?
    var jobEncId: String
    
    var dataAvailable: Bool! {
        get {
            return (dataModels?.previousTimesheetViewBeanList?.count != 0)
        }
    }
    
    private var currentDate = Date()
    var weekNo: Int = 0
    var title: String {
        get {
            let startStr = changeDateFormat(currentDate.startOfWeek)
            let endStr = changeDateFormat(currentDate.endOfWeek)
            return "WEEK " + startStr + " - " + endStr
        }
    }
    
    private func changeDateFormat(_ date: Date?) -> String {
        
        guard let _date = date else {
            return String()
        }
        let calendar = Calendar.current
        let dateComponents = calendar.component(.day, from: _date)
        let numberFormatter = NumberFormatter()
        
        numberFormatter.numberStyle = .ordinal
        
        let day = numberFormatter.string(from: dateComponents as NSNumber)
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "MMM"
        
        return ("\(day!) \(dateFormatter.string(from: _date))")
    }
    
    func incrementDate() {
        
        guard currentDate < Date() else {
            return
        }
        currentDate =  Calendar.current.date(byAdding: Calendar.Component.day, value: 7, to: currentDate)!
        weekNo -= 1
    }
    
    func decrementDate() {
        currentDate =  Calendar.current.date(byAdding: Calendar.Component.day, value: -7, to: currentDate)!
        weekNo += 1
    }
    
    init(jobEncId: String) {
        self.jobEncId = jobEncId
    }
    
    func reset() {
        dataModels = nil
    }
    
    func hitGetTimeSheetListApi(completion: ((_ message: String?)->())? = nil, success:  ((_ message: String?)->())? = nil, failure: ((_ message: String?)->())? = nil) {
        
        let workServices = WorkServices(requestTag: "CURRENT_WORK_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        //Completion Block
        workServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            if let timeSheetModel = (response.0 as? TimeSheetModel), statusCode == 1 {
                
                self.dataModels = timeSheetModel
                completion?(message)
                success?(message)
            }
            else {
                completion?(message)
                failure?(message)
            }
        }
        workServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            completion?(nil)
            failure?(nil)
        }
        
        let parameters: [String: Any] = [
            "jobEncId":jobEncId,
            "week": weekNo,
        ]
        
        workServices.getTimeSheet(parameters: parameters, additionalHeaderElements: nil)
    }
}
