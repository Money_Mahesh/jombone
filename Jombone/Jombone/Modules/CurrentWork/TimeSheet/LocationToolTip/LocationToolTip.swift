//
//  LocationToolTip.swift
//  Jombone
//
//  Created by Money Mahesh on 02/04/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import UIKit

class LocationToolTip: CustomView {
    
    @IBOutlet weak var toolTipView: ToolTip!
    @IBOutlet weak var lblForMessage: UILabel!
    @IBInspectable var arrowPadding: CGFloat = 0
    
    
    private var _view: LocationToolTip?
    static var sharedInstance: LocationToolTip {
        get {
            return LocationToolTip()
        }
    }
    
    func designedView(postion: CGPoint, message: String, parentView: UIViewController? = APPLICATION_INSTANCE.visibleViewController()) -> LocationToolTip? {
        
//        let chatToolTipView = LocationToolTip(
//            point: postion,
////            size: CGSize(width: WINDOW_WIDTH-16, height: (134 - 8)),
//            direction: ToolTip.ArrowDirection.downCenter,
//            customPoint: 8,
//            parentController: parentView)
        
        self.backgroundColor = UIColor.clear
//        toolTipView.customHorizontalForPointer = 8
//        toolTipView.arrowDirection = ToolTip.ArrowDirection.downCenter
        parentView?.view.addSubview(self)
        
        self.lblForMessage.text = message
//        self._view = chatToolTipView
        
        if let _parentView = parentView?.view {
            let topConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: _parentView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1.0, constant: postion.y)
            topConstraint.isActive = true
            
            let rightConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.right, relatedBy: NSLayoutConstraint.Relation.equal, toItem: _parentView, attribute: NSLayoutConstraint.Attribute.right, multiplier: 1.0, constant: postion.x)
            rightConstraint.isActive = true
            
            _parentView.addConstraint(topConstraint)
            _parentView.addConstraint(rightConstraint)
//            parentView.layoutIfNeeded()
        }

        return self
    }
    
    convenience private init(point: CGPoint, size: CGSize? = nil, direction: ToolTip.ArrowDirection, customPoint: CGFloat? = nil, parentController: UIViewController?) {
        
//        let toolTipSize: CGSize =  size
//        let frame = CGRect(origin: point, size: toolTipSize)
        self.init()
        self.frame.origin = point
        
        self.backgroundColor = UIColor.clear
        toolTipView.customHorizontalForPointer = customPoint
        toolTipView.arrowDirection = direction
        parentController?.view.addSubview(self)
        
//        if let parentView = parentController?.view {
//            let topConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: parentView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1.0, constant: point.y)
//            topConstraint.isActive = true
//            
//            let rightConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.right, relatedBy: NSLayoutConstraint.Relation.equal, toItem: parentView, attribute: NSLayoutConstraint.Attribute.right, multiplier: 1.0, constant: point.x)
//            rightConstraint.isActive = true
//
//            parentView.addConstraint(topConstraint)
//            parentView.addConstraint(rightConstraint)
//            parentView.layoutIfNeeded()
//        }
    }
    
    override func draw(_ rect: CGRect) {
        if arrowPadding != 0 {
            toolTipView.customHorizontalForPointer = arrowPadding
        }
    }
    
    func removeView() {
        self.removeFromSuperview()
    }
}
