//
//  UpdateContactInfoViewModel.swift
//  Jombone
//
//  Created by Money Mahesh on 25/10/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import Foundation

enum ContactType {
    case email(email: String?)
    case phoneNo
    case forgotPassword(email: String)
}

class UpdateContactInfoViewModel: NSObject {
    
    var type: ContactType!
    var unFormattedMobile : String!
    var countryCode : String = CANADA_COUNTRY_CODE
    var countryIsoCode : String = CANADA_ISO_COUNTRY_CODE

    var email: String! {
        didSet {
            if email == nil {
                isInputValid = false
            }
        }
    }
    
    var mobile : String! {
        didSet {
            if mobile == nil {
                isInputValid = false
            }
        }
    }
    
    private var isInputValid = true
    func reset() {
        isInputValid = true
    }
    
    init(type: ContactType) {
        self.type = type
    }
    
    func hitUpdateDetailApi(completion: ((_ message: String?)->())? = nil, success:  ((_ message: String?)->())? = nil, failure: ((_ message: String?)->())? = nil) {
        
        if !isInputValid {
            completion?(nil)
            return
        }
        
        let authenticationServices = AutheticationServices(requestTag: "VERIFY_OTP_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        //Completion Block
        authenticationServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            if statusCode == 1 {
                
                switch self.type! {
                case .email:
                    UserDataManager.shared.profileDetail?.personalDetailsBeans?.email = self.email

                case .phoneNo:
                    UserDataManager.shared.profileDetail?.personalDetailsBeans?.countryCode = self.countryCode
                    UserDataManager.shared.profileDetail?.personalDetailsBeans?.phoneNumber = self.mobile
                    
                default:
                    break
                }
            
                completion?(message)
                success?(message)
            }
            else {
                completion?(message)
                failure?(message)
            }
        }
        authenticationServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            completion?(nil)
            failure?(nil)
        }
        
        var parameters = [String: Any]()
        var methodName = String()

        switch self.type! {
        case .email:
            parameters = ["email" : email!]
            methodName = "change-email"
            
        case .phoneNo:
            parameters = ["countryCode" : countryCode, "countryIsoCode" : countryIsoCode, "mobile" : mobile!, "mobileUnformat": unFormattedMobile!]
             methodName = "change-mobile"

        default:
            break
        }
        
        authenticationServices.hitUpdateDetail(methodName: methodName, parameters: parameters, additionalHeaderElements: nil)
    }
}
