//
//  UpdateContactInfoViewController.swift
//  Jombone
//
//  Created by Money Mahesh on 25/10/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import UIKit

protocol UpdateContactInfoViewProtocol: NSObjectProtocol {
    func mobileNoUpdated(mobile: String, countryIsoCode: String, countryCode: String)
    func emailUpdated(email: String)
}

extension UpdateContactInfoViewProtocol {
    func mobileNoUpdated(mobile: String, countryIsoCode: String, countryCode: String) {}
    func emailUpdated(email: String) {}
}

class UpdateContactInfoViewController: WhiteStatusBarViewController {

    @IBOutlet weak var lblForTitle: UILabel!
    @IBOutlet weak var textfieldForEmail: CustomTextfieldView!
    @IBOutlet weak var textfieldForCountryCode: CustomTextfieldView!
    @IBOutlet weak var textfieldForMobileNo: CustomTextfieldView!
    @IBOutlet weak var lblForPhoneNoError: UILabel!

    weak var delegate: UpdateContactInfoViewProtocol?
    var viewModel: UpdateContactInfoViewModel!

    static func show(viewModel: UpdateContactInfoViewModel, delegate: UpdateContactInfoViewProtocol?) {
        let viewController = LOGIN_STORYBOARD.instantiateViewController(withIdentifier: "UpdateContactInfoViewController") as! UpdateContactInfoViewController
        viewController.viewModel = viewModel
        viewController.delegate = delegate
        
        APPLICATION_INSTANCE.visibleViewController()?.navigationController?.pushViewController(viewController, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpView()
    }
    
    func setUpView() {
        
        switch viewModel.type! {
        case .email(let emailId):
            lblForTitle.text = "Update Email"
            
            textfieldForEmail.setJomboneEmailStyle()
            textfieldForCountryCode.isHidden = true
            textfieldForMobileNo.isHidden = true
            textfieldForEmail.text = emailId
            
        default:
            
            textfieldForEmail.isHidden = true

            //This screen will never open for forgot password as user can not edit  email while forgot password process
            lblForTitle.text = "Update Mobile Number"
            
            textfieldForCountryCode.setJomboneCountryCodeStyle(delegate: self, selectedCode: UserDataManager.shared.profileDetail?.personalDetailsBeans?.countryIsoCode)
            textfieldForMobileNo.setJomboneMobileStyle()
            textfieldForMobileNo.textfieldViewDelegate = self
            textfieldForMobileNo.text =  UserDataManager.shared.profileDetail?.personalDetailsBeans?.phoneNumber ?? ""
            
            viewModel.countryIsoCode = UserDataManager.shared.profileDetail?.personalDetailsBeans?.countryIsoCode ?? CANADA_ISO_COUNTRY_CODE
            viewModel.countryCode = UserDataManager.shared.profileDetail?.personalDetailsBeans?.countryCode ?? CANADA_COUNTRY_CODE
            viewModel.unFormattedMobile = UserDataManager.shared.profileDetail?.personalDetailsBeans?.phoneNumber?.toUnPhoneNumber()
            viewModel.mobile = UserDataManager.shared.profileDetail?.personalDetailsBeans?.phoneNumber
        }
    }
    
    func moveToNextScreen() {
        
        switch viewModel.type! {
        case .phoneNo where (UserDataManager.shared.profileDetail?.personalDetailsBeans?.countryCode != CANADA_COUNTRY_CODE):
            
            //For country other than canada otp verification
            UserDataManager.shared.isSignUpCompleted = true
            APP_DELEGATE_INSTANCE.setupInitialViewController()
            
        default:
            //This screen will never open for forgot password as user can not edit  email while forgot password process
            switch viewModel.type! {
            case .email:
                self.delegate?.emailUpdated(email: viewModel.email)

            case .phoneNo:
                self.delegate?.mobileNoUpdated(mobile: viewModel.unFormattedMobile,
                                               countryIsoCode: viewModel.countryIsoCode,
                                               countryCode: viewModel.countryCode)
            default:
                break
            }
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    //MARK:- IBAction
    @IBAction func btnActionForSubmit(_ sender: Any) {
        
        view.endEditing(true)
        
        viewModel.reset()
        switch viewModel.type! {
        case .email:
            viewModel.email = textfieldForEmail.isValidEmail()

        case .phoneNo:
        
            let validatePhoneNo = DataValidationUtility.shared.validatePhoneNo(viewModel.unFormattedMobile)
            lblForPhoneNoError.text = validatePhoneNo.errorMsg
            viewModel.unFormattedMobile = validatePhoneNo.value
            
            guard viewModel.unFormattedMobile != nil, viewModel.unFormattedMobile != "" else {
                return
            }
            
        default:
            break
        }
        
        showLoader()
        viewModel.hitUpdateDetailApi(completion: { (message) in
            self.hideLoader()
        }, success: { (message) in
            self.hideLoader()
            self.moveToNextScreen()
        }, failure: { (message) in
            self.hideLoader()
            self.showAlert(message: message, withOk: true)
        })
        
    }
}

extension UpdateContactInfoViewController: CustomTextfieldViewDelegate {
    
    func textFieldDidBeginEditing(_ identifier: String, textField: CustomTextfield) {
        
        if identifier == "MobileNo" {
            lblForPhoneNoError.text = nil
            if let text = textField.text, text.contains("(") {
                textField.text = text.toUnPhoneNumber()
            }
        }
    }
    
    func textFieldDidEndEditing(_ identifier: String, textField: CustomTextfield) {
        
        if identifier == "MobileNo" {
            viewModel.unFormattedMobile = textField.text?.toUnPhoneNumber()
            let formattedString = textField.text?.toPhoneNumber()
            textField.text = formattedString
            viewModel.mobile = formattedString
        }
    }
    
    func pickerViewSelected(identifier: String, textfield: CustomTextfield, indexes: [Int]) {
        
        if let firstIndex = indexes.first {
            viewModel.countryCode = Utility.countryCodeArray[firstIndex]["isdvalueCodes"] ?? CANADA_COUNTRY_CODE
            viewModel.countryIsoCode = Utility.countryCodeArray[firstIndex]["ISO_Code"] ?? CANADA_ISO_COUNTRY_CODE
        }
    }
}
