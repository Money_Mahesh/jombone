//
//  SelectJobLocationViewModel.swift
//  Jombone
//
//  Created by Money Mahesh on 11/11/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import Foundation

class SelectJobLocationViewModel: NSObject {
    var selectedOptions: [AutoCompleteCardViewModelProtocol]?
    
    func hitSendOTPApi(completion: ((_ message: String?)->())? = nil, success:  ((_ message: String?)->())? = nil, failure: ((_ statusCode: Int?, _ message: String?)->())? = nil) {
        
        let authenticationServices = AutheticationServices(requestTag: "SEND_OTP_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        //Completion Block
        authenticationServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            let loginDetail = response.0 as? LoginDetail
            UserDataManager.shared.token = loginDetail?.token
            if let personalDetailsBean = loginDetail?.personalDetailsBean {
                UserDataManager.shared.profileDetail = ProfileDetail(personalDetailsBean: personalDetailsBean)
            }
            
            if statusCode == 1 {
                completion?(message)
                success?(message)
            }
            else {
                completion?(message)
                failure?(statusCode, message)
            }
        }
        authenticationServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            completion?(nil)
            failure?(nil, nil)
        }
       
        authenticationServices.hitSendPhoneNoOTP(parameters: nil, additionalHeaderElements: nil)
    }
}
