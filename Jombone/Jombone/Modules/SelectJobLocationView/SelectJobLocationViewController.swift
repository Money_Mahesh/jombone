//
//  SelectJobLocationViewController.swift
//  Jombone
//
//  Created by dev139 on 27/08/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import UIKit

class SelectJobLocationViewController: WhiteStatusBarViewController {
    
    @IBOutlet weak var textfieldForJobLocation: CustomTextfieldView!
    @IBOutlet weak var stackView: UIStackView!
    
    var autoCompleteViewModel = AutoCompleteViewModel(type: AutoComplete.google(GooglePlaceFor: GooglePlaceFor.preferredJobLocation), isOffline: false)
    var viewModel: SelectJobLocationViewModel?
    var tableWidget: GenericTableViewComponent<AutoCompleteCardViewModelProtocol, AutoCompleteCardView>?
    
    static func show(viewModel: SelectJobLocationViewModel = SelectJobLocationViewModel()) {
        
        let viewController = LOGIN_STORYBOARD.instantiateViewController(withIdentifier: "SelectJobLocationViewController") as! SelectJobLocationViewController
        viewController.viewModel = viewModel
        APPLICATION_INSTANCE.visibleViewController()?.navigationController?.pushViewController(viewController, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textfieldForJobLocation.textfieldViewDelegate = self
        setUpTable()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setNavigationBarWithTitle("", andLeftButton: .none, andRightButton: .none, withBg: .bgColor(value: UIColor.clear))
        self.navigationItem.setHidesBackButton(true, animated: false)

    }
    
    func setUpTable() {
        
        if tableWidget == nil {
            tableWidget = GenericTableViewComponent()
            
            let cellOtherGenericDetail: (style: AutoCompleteCardViewModel.ViewStyle, action: ((_ sender: Any, _ index: IndexPath, _ obj: AutoCompleteCardViewModelProtocol)->())?) = (style: AutoCompleteCardViewModel.ViewStyle.withCrossWhiteTitle, action: { (_ sender: Any, _ index: IndexPath, _ obj: AutoCompleteCardViewModelProtocol) in
                self.viewModel?.selectedOptions?[index.row].selected = false
                self.hitApi(index: index.row, deSelectedOptionsEncryptedId: obj.encryptedId)
            })
            
            tableWidget?.genericTableViewModel = GenericTableViewModel(
                cellDataModel: viewModel?.selectedOptions,
                cellOtherDetail: GenericTableViewCellOtherDetail(generic: cellOtherGenericDetail, unique: nil),
                tableType: .expandable)
            tableWidget?.tableStyle(backGroundColor: UIColor.clear, insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0), indicatorStyle: .white)
            stackView.addArrangedSubview(tableWidget!)
        }
    }
    
    //MARK:- IBAction
    @IBAction func btnActionForNext(_ sender: UIButton) {
        
        if let option = viewModel?.selectedOptions, option.count > 0 {
            hitSendOtp()
//            if let countryCode = UserDataManager.shared.profileDetail?.personalDetailsBeans?.countryCode, countryCode == "+1" {
//                
//                OTPVerificationViewController.show(viewModel: OTPVerificationViewModel(
//                    purpose: .phoneNo))
//            }
//            else {
//                UserDataManager.shared.isSignUpCompleted = true
//                APP_DELEGATE_INSTANCE.setupInitialViewController()
//            }
        }
        else {
            textfieldForJobLocation.errorMsg = "Preferred location is mandatory"
        }
    }
    
    func hitSendOtp() {
        
        view.endEditing(true)
        showLoader()
        viewModel?.hitSendOTPApi(completion: { (message) in
            self.hideLoader()
        }, success: { (message) in
            OTPVerificationViewController.show(viewModel: OTPVerificationViewModel(
                purpose: .phoneNo))
        }, failure: { (statusCode, errorMessage) in
            if let errorCode = statusCode {
                if errorCode == 2 {
                    UserDataManager.shared.isSignUpCompleted = true
                    APP_DELEGATE_INSTANCE.setupInitialViewController()
                    return
                }
            }
            self.showAlert(message: errorMessage)
        })
    }
    
    //MARK:- API Hit
    func hitApi(index: Int, deSelectedOptionsEncryptedId: String?) {
        
        guard let encryptedId = deSelectedOptionsEncryptedId else {
            return
        }
        
        showLoader()
        autoCompleteViewModel.hitRemoveLocationApi(
            encryptedId: encryptedId,
            googlePlaceFor: GooglePlaceFor.preferredJobLocation,
            completion: { (message) in
                self.hideLoader()
                self.showAlert(message: message)
        }, success: { (message) in
            
            let selectedOptions = self.viewModel?.selectedOptions?.filter { return $0.selected }
            self.selectedOption(type: AutoComplete.google(GooglePlaceFor: GooglePlaceFor.preferredJobLocation), optionSelected: selectedOptions)

        }, failure: { (message) in
            
            self.viewModel?.selectedOptions?[index].selected = true
            self.selectedOption(type: AutoComplete.google(GooglePlaceFor: GooglePlaceFor.preferredJobLocation), optionSelected: self.selectedOption)
        })
    }
}

extension SelectJobLocationViewController: CustomTextfieldViewDelegate {
    
    func textFieldDidBeginEditing(_ identifier: String, textField: CustomTextfield) {
        self.view.endEditing(true)
        
        if (self.viewModel?.selectedOptions?.count ?? 0) >= 10 {
            self.showAlert(message: "Maximum 10 locations are allowed.")
            return
        }
        
        AutoCompleteViewController.show(viewModel: AutoCompleteViewModel(selectedOptions: viewModel?.selectedOptions, type: AutoComplete.google(GooglePlaceFor: .preferredJobLocation), isOffline: false, noOfSelection: .multiple), delegate: self)
    }
}

extension SelectJobLocationViewController: AutoCompleteViewDelegate {
    func selectedOption(type: AutoComplete, optionSelected: Any?) {
        
            viewModel?.selectedOptions = optionSelected as? [AutoCompleteCardViewModelProtocol]
            tableWidget?.updateTableRecord(objArray: viewModel?.selectedOptions)
       
    }
}
