//
//  TryViewController.swift
//  Jombone
//
//  Created by Money Mahesh on 11/10/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import UIKit
import GoogleMaps

class TryViewController: UIViewController {

    enum TravelModes: Int {
        case driving
        case walking
        case transit
    }
    
    var selectedRoute: [String: Any]?
    var overviewPolyline: [String: Any]?
    var originCoordinate: CLLocationCoordinate2D!
    var destinationCoordinate: CLLocationCoordinate2D!
    
    @IBOutlet weak var googleMap: GMSMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        
        let coordinates: [CLLocationCoordinate2D] = [
            CLLocationCoordinate2D(latitude: 28.571746, longitude: 77.345958),
            CLLocationCoordinate2D(latitude: 28.613180, longitude: 77.345207)
        ]
        
        let camera = GMSCameraPosition.camera(withLatitude: 28.571746, longitude: 77.345958, zoom: 100.0)
        googleMap.camera = camera
        showMarker(title: "Source", position: coordinates.first!)
        showMarker(title: "Destination", position: coordinates.last!)
        
//        drawPath(source: coordinates.first!, destination: coordinates.last!)
        
        getDirections(
            origin: "ChIJH4ZHLkfkDDkR5dYkOH8SrMc",
            destination: "ChIJd3v7l1_kDDkRVQ7YD8kO5Vc",
            travelMode: TryViewController.TravelModes.transit, completionHandler: nil)
    }

    func showMarker(title: String, position: CLLocationCoordinate2D){
        let marker = GMSMarker()
        marker.position = position
        marker.title = title
        marker.snippet = title + " Descritpion"
        marker.map = googleMap
    }
}

extension TryViewController {
    
    func drawPath(source: CLLocationCoordinate2D, destination: CLLocationCoordinate2D) {
        
        showMarker(title: "Source", position: source)
        showMarker(title: "Destination", position: destination)
        
        let path = GMSMutablePath()
        path.add(source)
        path.add(destination)
        
        let rectangle = GMSPolyline(path: path)
        rectangle.strokeWidth = 2.0
        rectangle.map = googleMap
//        self.view=googleMap;
    }
    
    func getDirections(origin: String?, destination: String?, travelMode: TravelModes, completionHandler: ((_ status: String, _ success: Bool) -> Void)?) {
        
        guard let originLocation = origin, let destinationLocation = destination else {
            return
        }
        
        var directionsURLString = GOOGLE_ROUTE_BASE_URL + "origin=place_id:" + originLocation + "&destination=place_id:" + destinationLocation
        
        //            if let routeWaypoints = waypoints {
        //                directionsURLString += "&waypoints=optimize:true"
        //
        //                for waypoint in routeWaypoints {
        //                    directionsURLString += "|" + waypoint
        //                }
        //            }
        
            var travelModeString = ""
            
            switch travelMode.rawValue {
            case TravelModes.walking.rawValue:
                travelModeString = "walking"
                
            case TravelModes.transit.rawValue:
                travelModeString = "transit"
                
            default:
                travelModeString = "driving"
            }
        
            directionsURLString += "&mode=" + travelModeString + "&key=" + GOOGLE_PLACE_API_KEY
        
        
        if let finalDirectionsURLString = directionsURLString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed), let directionsURL = URL(string: finalDirectionsURLString) {
            
            DispatchQueue.main.async {
                
                do {
                    if let directionsData = try? Data(contentsOf: directionsURL) {
                        
                        let dictionary: [String: Any] = try JSONSerialization.jsonObject(with: directionsData, options: JSONSerialization.ReadingOptions.mutableContainers) as! [String : Any]
                        
                        let status = dictionary["status"] as! String
                        if status == "OK" {
                            
                            self.selectedRoute = (dictionary["routes"] as! [[String: Any]])[0]
                            self.overviewPolyline = self.selectedRoute?["overview_polyline"] as? [String : Any]
                            
                            if let legs = self.selectedRoute?["legs"] as? [[String: Any]],
                                let start_location = legs.first?["start_location"] as? [String: Any],
                                let end_location = legs.last?["end_location"] as? [String: Any] {
                                
                                self.originCoordinate = CLLocationCoordinate2DMake(start_location["lat"] as! Double, start_location["lng"] as! Double)
                                
                                self.destinationCoordinate = CLLocationCoordinate2DMake(end_location["lat"] as! Double, end_location["lng"] as! Double)
                            }
                            
                            
                            
                            
//                            self.originAddress = legs[0]["start_address"] as String
//                            self.destinationAddress = legs[legs.count - 1]["end_address"] as String
                            
                            self.calculateTotalDistanceAndDuration()
                            
//                            completionHandler(status: status, success: true)
                        }
                        else {
//                            completionHandler(status: status, success: false)
                        }
                    }
                }
                catch let error {
                    print(error)
                }
            }
        }
    }
    
    
    func calculateTotalDistanceAndDuration() {
        
        if let legs = self.selectedRoute?["legs"] as? [[String: Any]] {
            
            var totalDistanceInMeters: UInt = 0
            var totalDurationInSeconds: UInt = 0
            
            for leg in legs {
                totalDistanceInMeters = ((leg["distance"] as! [String: Any])["value"] as? UInt) ?? 0
                totalDurationInSeconds = ((leg["duration"] as! [String: Any])["value"] as? UInt) ?? 0
            }
            
            
            let distanceInKilometers: Double = Double(totalDistanceInMeters / 1000)
            let totalDistance = "Total Distance: \(distanceInKilometers) Km"
            
            
            let mins = totalDurationInSeconds / 60
            let hours = mins / 60
            let days = hours / 24
            let remainingHours = hours % 24
            let remainingMins = mins % 60
            let remainingSecs = totalDurationInSeconds % 60
            
            let totalDuration = "Duration: \(days) d, \(remainingHours) h, \(remainingMins) mins, \(remainingSecs) secs"
            
            print("Total Distance" + (totalDistance))
            print("Total Duration" + (totalDuration))

        }
    }
}
