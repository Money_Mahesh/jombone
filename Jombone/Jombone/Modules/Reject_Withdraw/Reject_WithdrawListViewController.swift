//
//  Reject_WithdrawListViewController.swift
//  Jombone
//
//  Created by Money Mahesh on 23/02/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import UIKit

class Reject_WithdrawListViewController: PopUpViewController {
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var noResultView: UIView!
    
    var navBtnType: NavBtnType = .menu
    var viewModel = Reject_WithdrawListViewModel()
    
    var tableWidget: GenericTableViewComponent<JobCardViewProtocol, Reject_WithdrawCardView>?
    
    static func instance(navBtnType: NavBtnType = .back) -> Reject_WithdrawListViewController {
        
        let viewController = APPLICATION_STORYBOARD.instantiateViewController(withIdentifier: "Reject_WithdrawListViewController") as! Reject_WithdrawListViewController
        viewController.navBtnType = navBtnType
        viewController.viewModel = Reject_WithdrawListViewModel()
        return viewController
    }
    
    static func show(navBtnType: NavBtnType = .menu) {
        
        let viewController = APPLICATION_STORYBOARD.instantiateViewController(withIdentifier: "Reject_WithdrawListViewController") as! Reject_WithdrawListViewController
        viewController.viewModel = Reject_WithdrawListViewModel()
        viewController.navBtnType = navBtnType
        APPLICATION_INSTANCE.visibleViewController()?.navigationController?.pushViewController(viewController, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationBarWithTitle("Rejected/Withdrawn", andLeftButton: navBtnType, andRightButton: .none, withBg: .defaultColor)

        self.addObserver(self, selector: #selector(Reject_WithdrawListViewController.reloadList), notificationsName: [Notification.Name.ReloadRejectedWithdrawList])
        
        self.showLoader()
        self.getRejectedWithdrawList()
    }
    
    func updatedList() {
        viewModel.reset()
        setUpTable()
        
        self.showLoader()
        self.getRejectedWithdrawList()
    }
    
    @objc func reloadList() {
        tableWidget?.reloadTable()
    }
    
    func getRejectedWithdrawList(){
        
        viewModel.hitGetRejectedWithdrawListApi(completion: { (message) in
            self.hideLoader()
            self.noResultView.isHidden = self.viewModel.dataAvailable
        }, success: { (message) in
            self.setUpTable()
        }, failure: { (errorMsg) in
//            self.showAlert(message: errorMsg)
        })
    }
    
    func setUpTable() {
        
        if let _tableWidget = tableWidget {
            _tableWidget.appendNewPage(objArray: viewModel.cardDataModels)
        }
        else {
            tableWidget = GenericTableViewComponent()
            tableWidget?.isHidden = false
            tableWidget?.genericTableViewModel = GenericTableViewModel(
                cellDataModel: viewModel.cardDataModels,
                tableFooterView: (LoadMoreView(frame: CGRect(x: 0, y: 0, width: WINDOW_WIDTH, height: 40)), false),
                tableType: .expandable,
                totalPages: viewModel.totalPages)

            tableWidget?.tableStyle(backGroundColor: UIColor.white, insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
            tableWidget?.delegate = self
            stackView.addArrangedSubview(tableWidget!)
        }
    }
}

extension Reject_WithdrawListViewController: GenericTableViewComponentDelegate {
    
    func genericTableDidSelectRowAt(_ indexPath: IndexPath, dataSource: inout [Any]) -> Bool {
        
        if let jobModel = viewModel.cardDataModels[indexPath.row] as? JobModel {
            JobsDetailViewController.show(JobsDetailViewModel(jobDetail: jobModel), parentController: self)
        }
        return false
    }
    
    func loadMore() {
        getRejectedWithdrawList()
    }
}

