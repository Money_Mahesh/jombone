//
//  PastWorkCardView.swift
//  Jombone
//
//  Created by Money Mahesh on 13/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import UIKit

class Reject_WithdrawCardView: GenericView<JobCardViewProtocol> {
    
    @IBOutlet weak var jobCardBaseViewStack: UIStackView?
    
    var jobCardBaseView: JobCardBaseView!
    weak var companyDelegate: CompanyAction?
    @IBOutlet weak var lblForDate: UILabel?
    @IBOutlet weak var lblForRejectWithdrawTitle: UILabel?
    
    override func xibLoaded() {
        jobCardBaseView = JobCardBaseView()
        jobCardBaseViewStack?.addArrangedSubview(jobCardBaseView)
    }
    
    override var viewModel: ViewModel<JobCardViewProtocol>? {
        didSet {
            jobCardBaseView.viewModel = JobCardBaseViewModel(data: viewModel?.data, otherDetail: nil)
            
            if let rejectedDate = Utility.getDateFromTimeStampWithFormat(timestamp:viewModel?.data?.rejectedDate, format: "dd MMM, yyyy") , viewModel?.data?.isRejected == true {
                lblForRejectWithdrawTitle?.text = "Rejected on: "
                lblForDate?.text = rejectedDate
                
            } else if let withdrawnDate = Utility.getDateFromTimeStampWithFormat(timestamp:viewModel?.data?.withdrawnDate, format: "dd MMM, yyyy") , viewModel?.data?.isWithdrawn == true {
                lblForRejectWithdrawTitle?.text = "Withdrawn on: "
                lblForDate?.text = withdrawnDate
            }
            
        }
    }
    
    //MARK:- IBActions
    @IBAction func btnActionForDirection(_ sender: UIButton) {
        guard let jobId = viewModel?.data?.jobId else {
            return
        }
        self.viewDirectionForJobOrCompany(isJob: true, jobOrEmployerid: jobId)
    }
}

