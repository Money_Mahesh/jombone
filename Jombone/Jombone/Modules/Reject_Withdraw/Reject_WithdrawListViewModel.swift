//
//  Reject_WithdrawListViewModel.swift
//  Jombone
//
//  Created by Money Mahesh on 11/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation

class Reject_WithdrawListViewModel: NSObject {
    
    var cardDataModels = [JobCardViewProtocol]()

    var pageNo = 0
    var totalPages = 0
    var dataAvailable: Bool! {
        get {
            return (cardDataModels.count != 0)
        }
    }
    
    func reset() {
        self.pageNo = 0
        cardDataModels.removeAll()
    }
    
    func hitGetRejectedWithdrawListApi(completion: ((_ message: String?)->())? = nil, success:  ((_ message: String?)->())? = nil, failure: ((_ message: String?)->())? = nil) {
        
        let applicationServices = ApplicationServices(requestTag: "REJECTED_WITHDRAW_LIST_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        //Completion Block
        applicationServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            if let workList = (response.0 as? [JobModel]),
                let pagination = (response.1 as? Pagination),
                let totalPages = pagination.totalPages,
                let currentPage = pagination.currentPage,
                self.pageNo == currentPage, statusCode == 1 {
                
                self.pageNo += 1
                self.cardDataModels.append(contentsOf: workList)
                self.totalPages = totalPages
                
                completion?(message)
                success?(message)
            }
            else {
                completion?(message)
                failure?(message)
            }
        }
        applicationServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            completion?(nil)
            failure?(nil)
        }
        
        let parameters: [String: Any] = [
            "size": String(PAGE_SIZE),
            "page": String(pageNo),
        ]
        
        applicationServices.getRejectedWithdraws(parameters: parameters, additionalHeaderElements: nil)
    }
}
