//
//  SettingsViewController.swift
//  Jombone
//
//  Created by Money Mahesh on 23/08/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import UIKit

class SettingsViewController: WhiteStatusBarViewController {
    
    enum MenuOption: Int {
        case Notification = 0
        case ChangePassword = 1
    }
    
    @IBOutlet weak var stackView: UIStackView!
    
    var viewModel: SettingsViewModel!
    var settingsListView: GenericTableViewComponent<SettingsCardDataModel, SettingsCardView>!
    
    static func instance() -> SettingsViewController{
        
        let instance = MAIN_STORYBOARD.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        return instance
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        viewModel = SettingsViewModel(notificationAction: notificationAction)
        
        settingsListView = GenericTableViewComponent()
        settingsListView?.tableStyle(backGroundColor: UIColor.clear, insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
        settingsListView.delegate = self
        settingsListView.genericTableViewModel = GenericTableViewModel(
            cellDataModel: viewModel.cardDataModels,
            cellOtherDetail: GenericTableViewCellOtherDetail(generic: nil, unique: ["0": (status: UserDataManager.shared.profileDetail?.personalDetailsBeans?.enableNotification, action: notificationAction)]),
            tableHeaderView: nil,
            tableFooterView: nil,
            tableType: TableType.expandable)
        stackView.addArrangedSubview(settingsListView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNavigationBarWithTitle("Settings", andLeftButton: .back, andRightButton: .none, withBg: .defaultColor)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    lazy var notificationAction: ((_ status: Bool)->()) = {[weak self] (status) in
        print("Status " + String(status))
        self?.showLoader()
        self?.viewModel.enableNotification(isTrue: status, completion: { (message) in
            self?.hideLoader()
        }, success: { (message) in
            UserDataManager.shared.profileDetail?.personalDetailsBeans?.enableNotification = status
            self?.showAlert(message: message)
        }, failure: { (message) in
        })
        
    }
}

extension SettingsViewController: GenericTableViewComponentDelegate {
    func genericTableDidSelectRowAt(_ indexPath: IndexPath, dataSource: inout [Any]) -> Bool {

        switch MenuOption(rawValue: indexPath.row)! {

        case MenuOption.Notification: break
        case MenuOption.ChangePassword:
            ChangePasswordViewController.show(viewModel: ChangePasswordViewModel(purpose: .changePassword))
        }
        return false
    }
}
