//
//  SettingsCardView.swift
//  Jombone
//
//  Created by Money Mahesh on 23/08/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import UIKit

class SettingsCardView: GenericView<SettingsCardDataModel> {
    
    @IBOutlet weak var lableForTitle: UILabel!
    @IBOutlet weak var imageViewForIcon: UIImageView!
    @IBOutlet weak var _switch: UISwitch!
    
    var switchAction: ((_ status: Bool)->())?
    
    override func xibLoaded() {
//        _switch.transform = CGAffineTransform(scaleX: 0.65, y: 0.65)
    }
    
    override var viewModel: ViewModel<SettingsCardDataModel>! {
        didSet {
            lableForTitle.text = viewModel.data?.title
            imageViewForIcon.image = viewModel.data?.icon
            
            if let switchDetail = viewModel.otherDetail as? (status: Bool, action: ((_ status: Bool)->())) {
                _switch.isHidden = false
                _switch.isOn = switchDetail.status
                _switch.addTarget(self, action: #selector(SettingsCardView.switchToggleAction(_:)), for: UIControl.Event.valueChanged)
                switchAction = switchDetail.action
            }
        }
    }
    
    @objc func switchToggleAction(_ sender: UISwitch) {
        switchAction?(sender.isOn)
        
    }
}
