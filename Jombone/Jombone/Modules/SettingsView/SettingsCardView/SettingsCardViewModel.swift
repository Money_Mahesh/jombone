//
//  SettingsCardViewModel.swift
//  Jombone
//
//  Created by Money Mahesh on 23/08/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import Foundation
import UIKit

class SettingsCardDataModel: NSObject {
    var title: String
    var icon: UIImage

    init(title: String, icon: UIImage) {
        self.title = title
        self.icon = icon
    }
}

class SettingsCardViewModel: ViewModel<SettingsCardDataModel> {
    
    override init(data: SettingsCardDataModel?, otherDetail: Any?) {
        super.init(data: data, otherDetail: otherDetail)
    }
}
