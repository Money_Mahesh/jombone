//
//  SettingsViewModel.swift
//  Jombone
//
//  Created by Money Mahesh on 23/08/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import Foundation

class SettingsViewModel: NSObject {
    
    private var notificationAction: ((_ status: Bool)->())!

    lazy var cardDataModels: [SettingsCardDataModel] = [
        SettingsCardDataModel(title: "Notification", icon: #imageLiteral(resourceName: "notifications")),
        SettingsCardDataModel(title: "Change Password", icon: #imageLiteral(resourceName: "lock")),
    ]
    
    init(notificationAction: @escaping ((_ status: Bool)->())) {
        self.notificationAction = notificationAction
    }
    
    override private init() {
        
    }
    
    func enableNotification(isTrue: Bool, completion: ((_ message: String?)->())? = nil, success:  ((_ message: String?)->())? = nil, failure: ((_ message: String?)->())? = nil) {
        
        let notificationServices = NotificationServices(requestTag: "ENAABLE_NOTIFICATION_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        //Completion Block
        notificationServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            if statusCode == 1 {
                completion?(message)
                success?(message)
            }
            else {
                completion?(message)
                failure?(message)
            }
        }
        notificationServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            completion?(nil)
            failure?(nil)
        }
        
        notificationServices.enableNotification(parameters: ["enableNotification": isTrue], additionalHeaderElements: nil)
    }
    
}
