//
//  PreferencesViewController.swift
//  Jombone
//
//  Created by dev139 on 29/04/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import UIKit

class PreferencesViewController: UIViewController {
    
    @IBOutlet weak var viewForShift: CheckBoxView!
    @IBOutlet weak var viewForJobType: CheckBoxView!
    @IBOutlet weak var addLocationView: CustomLabelView!
    @IBOutlet weak var addIndustryView: CustomLabelView!
    @IBOutlet weak var locationTableWidget: UIStackView!
    @IBOutlet weak var industryTableWidget: UIStackView!
    
    @IBOutlet weak var constForLocationWidgetHeight: NSLayoutConstraint?
    @IBOutlet weak var constForIndustryWidgetHeight: NSLayoutConstraint?

    let cellHeight = 43
    var locationTable: GenericTableViewComponent<AutoCompleteCardViewModelProtocol, AutoCompleteCardView>?
    var industryTable: GenericTableViewComponent<AutoCompleteCardViewModelProtocol, AutoCompleteCardView>?
    var autoCompleteViewModel = AutoCompleteViewModel(type: AutoComplete.google(GooglePlaceFor: GooglePlaceFor.preferredJobLocation), isOffline: false)

    var viewModel = PreferencesViewModel()
    let jobTypeArray = PlistUtiliy.parsePlistAndReturnPlistModel("JobType")
    let jobShiftArray = PlistUtiliy.parsePlistAndReturnPlistModel("JobShift")
    
    static func instance() -> PreferencesViewController{
        let instance = PREFERENCES_STORYBOARD.instantiateViewController(withIdentifier: "PreferencesViewController") as! PreferencesViewController
        return instance
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationBarWithTitle("Preferences", andLeftButton: .back, andRightButton: .none, withBg: .defaultColor)
        setUpView()
        getUserPreferences()
    }
    
    func populateData() {
        
        viewForShift.setDetails(data: jobShiftArray, selectedData: self.viewModel.peferencesModel?.shiftTimings)
        viewForJobType.setDetails(data: jobTypeArray, selectedData: self.viewModel.peferencesModel?.jobTypes)
        
        self.selectedOption(type: AutoComplete.google(GooglePlaceFor: GooglePlaceFor.preferredJobLocation), optionSelected: self.viewModel.peferencesModel?.preferredLocations)
        self.selectedOption(type: AutoComplete.local(LocalFor: LocalFor.industry), optionSelected: self.viewModel.peferencesModel?.industries)
    }
    
    func setUpView() {
        
        viewForShift.setDetails(data: jobShiftArray, selectedData: JobFilterManager.sharedInstance.jobShift)
        viewForJobType.setDetails(data: jobTypeArray, selectedData: JobFilterManager.sharedInstance.jobType)
        
        viewForShift.setTitleFont(UIFont.rubikMediumFontOfSize(16.0))
        viewForJobType.setTitleFont(UIFont.rubikMediumFontOfSize(16.0))
        
        addLocationView.delegate = self
        addIndustryView.delegate = self
        viewForShift.delegate = self
        viewForJobType.delegate = self
        
        if locationTable == nil {
            
            locationTable = GenericTableViewComponent()
            
            let cellOtherGenericDetail: (style: AutoCompleteCardViewModel.ViewStyle, action: ((_ sender: Any, _ index: IndexPath, _ obj: AutoCompleteCardViewModelProtocol)->())?) = (style: AutoCompleteCardViewModel.ViewStyle.withCrossGreyTitle, action: { (_ sender: Any, _ index: IndexPath, _ obj: AutoCompleteCardViewModelProtocol) in
                self.viewModel.peferencesModel?.preferredLocations?[index.row].selected = false
                self.hitApi(index: index.row, googlePlaceFor: GooglePlaceFor.preferredJobLocation, deSelectedOptionsEncryptedId: obj.encryptedId)
            })
            
            locationTable?.genericTableViewModel = GenericTableViewModel(
                cellDataModel: viewModel.peferencesModel?.preferredLocations,
                cellOtherDetail: GenericTableViewCellOtherDetail(generic: cellOtherGenericDetail, unique: nil),
                tableType: .nonScrollable)
            locationTable?.tableStyle(backGroundColor: UIColor.clear, insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
            locationTableWidget.addArrangedSubview(locationTable!)
            
            constForLocationWidgetHeight?.constant = CGFloat((cellHeight * (viewModel.peferencesModel?.preferredLocations?.count ?? 0)))
        }
        
        if industryTable == nil {
            industryTable = GenericTableViewComponent()
            
            let cellOtherGenericDetail: (style: AutoCompleteCardViewModel.ViewStyle, action: ((_ sender: Any, _ index: IndexPath, _ obj: AutoCompleteCardViewModelProtocol)->())?) = (style: AutoCompleteCardViewModel.ViewStyle.withCrossGreyTitle, action: { (_ sender: Any, _ index: IndexPath, _ obj: AutoCompleteCardViewModelProtocol) in
                
                self.viewModel.peferencesModel?.industries?[index.row].selected = false
                let id = (obj as? AutoSuggestObj)?.preferenceIdEnc ?? (obj as? AutoSuggestObj)?.encryptedId
                self.hitRemoveIndustryApi(index: index.row, deSelectedOptionsEncryptedId: id)
            })
            
            industryTable?.genericTableViewModel = GenericTableViewModel(
                cellDataModel: viewModel.peferencesModel?.industries,
                cellOtherDetail: GenericTableViewCellOtherDetail(generic: cellOtherGenericDetail, unique: nil),
                tableType: .nonScrollable)
            industryTable?.tableStyle(backGroundColor: UIColor.clear, insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
            industryTableWidget.addArrangedSubview(industryTable!)
            
            constForIndustryWidgetHeight?.constant = CGFloat((cellHeight * (viewModel.peferencesModel?.industries?.count ?? 0)))
        }
    }
    
    func getUserPreferences() {
        viewModel.getPreferences(completion: {
            self.populateData()
        })
    }
    
    //MARK:- API Hit
    func hitApi(index: Int, googlePlaceFor: GooglePlaceFor , deSelectedOptionsEncryptedId: String?) {
        
        guard let encryptedId = deSelectedOptionsEncryptedId else {
            return
        }
        
        showLoader()
        autoCompleteViewModel.hitRemoveLocationApi(
            encryptedId: encryptedId,
            googlePlaceFor: googlePlaceFor,
            completion: { (message) in
                self.hideLoader()
                //self.showAlert(message: message)
        }, success: { (message) in
            
            let selectedOptions = self.viewModel.peferencesModel?.preferredLocations?.filter { return $0.selected }
            self.selectedOption(type: AutoComplete.google(GooglePlaceFor: GooglePlaceFor.preferredJobLocation), optionSelected: selectedOptions)
            NotificationCenter.default.post(name: NSNotification.Name.UpdateJobList, object: nil)

        }, failure: { (message) in
            
            self.viewModel.peferencesModel?.preferredLocations?[index].selected = true
            self.selectedOption(type: AutoComplete.google(GooglePlaceFor: GooglePlaceFor.preferredJobLocation), optionSelected: self.viewModel.peferencesModel?.preferredLocations)
        })
    }
    
    func hitRemoveIndustryApi(index: Int, deSelectedOptionsEncryptedId: String?) {
        
        guard let encryptedId = deSelectedOptionsEncryptedId else {
            return
        }
        
        showLoader()
        autoCompleteViewModel.hitRemoveIndustrySkillApi (
            encryptedId: encryptedId,
            completion: { (message) in
                self.hideLoader()
                //self.showAlert(message: message)
        }, success: { (message) in
            
            let selectedOptions = self.viewModel.peferencesModel?.industries?.filter { return $0.selected }
            self.selectedOption(type:AutoComplete.local(LocalFor: .industry), optionSelected: selectedOptions)
            NotificationCenter.default.post(name: NSNotification.Name.UpdateJobList, object: nil)

        }, failure: { (message) in
            
            self.viewModel.peferencesModel?.industries?[index].selected = true
            self.selectedOption(type:AutoComplete.local(LocalFor: .industry), optionSelected: self.viewModel.peferencesModel?.industries)
        })
    }
    
    @IBAction func updateBtnAtion(_ sender: UIButton) {
       self.navigationController?.popViewController(animated: true)
    }
}


extension PreferencesViewController : CustomLabelViewProtocol {
    
    func tapped(_ identifier: String) {
        
        print(identifier)
        switch identifier {
        case "AddLocation":
            
            if (self.viewModel.peferencesModel?.preferredLocations?.count ?? 0) >= 10 {
                self.showAlert(message: "Maximum 10 locations are allowed.")
                return
            }
            
            AutoCompleteViewController.show(viewModel: AutoCompleteViewModel(selectedOptions: viewModel.peferencesModel?.preferredLocations, type: AutoComplete.google(GooglePlaceFor: .preferredJobLocation), isOffline: false, noOfSelection: .multiple), delegate: self)
            
        case "AddIndustry":
            
            if (self.viewModel.peferencesModel?.industries?.count ?? 0) >= 3 {
                self.showAlert(message: "Maximum 3 industries are allowed.")
                return
            }
            AutoCompleteViewController.show(viewModel: AutoCompleteViewModel(
                selectedOptions: self.viewModel.peferencesModel?.industries,
                type: .local(LocalFor: LocalFor.industry),
                isOffline: false, noOfSelection: .multiple), parentController: self, delegate: self)

        default:
            break
        }
    }
}


extension PreferencesViewController: AutoCompleteViewDelegate {
    
    func selectedOption(type: AutoComplete, optionSelected: Any?) {
        
        switch type {
            
        case .google(let googlePlaceFor):
            
            switch googlePlaceFor {
            case .preferredJobLocation:
                
                self.viewModel.peferencesModel?.preferredLocations = (optionSelected as? [GooglePlace])
                let widgetHeight = CGFloat((cellHeight * (viewModel.peferencesModel?.preferredLocations?.count ?? 0)))
                locationTableWidget.isHidden = (widgetHeight == 0)
                
                locationTable?.updateTableRecord(objArray: self.viewModel.peferencesModel?.preferredLocations)
                constForLocationWidgetHeight?.constant = CGFloat((cellHeight * (viewModel.peferencesModel?.preferredLocations?.count ?? 0)))
                NotificationCenter.default.post(name: NSNotification.Name.UpdateJobList, object: nil)
                self.view.layoutIfNeeded()
                
            default: return
            }
            
        case .local(let localFor):
            switch localFor {
            case .industry:
                
                self.viewModel.peferencesModel?.industries = (optionSelected as? [AutoSuggestObj])
                let widgetHeight = CGFloat((cellHeight * (viewModel.peferencesModel?.industries?.count ?? 0)))
                industryTableWidget.isHidden = (widgetHeight == 0)
                
                industryTable?.updateTableRecord(objArray: viewModel.peferencesModel?.industries)
                constForIndustryWidgetHeight?.constant = CGFloat((cellHeight * (viewModel.peferencesModel?.industries?.count ?? 0)))
                NotificationCenter.default.post(name: NSNotification.Name.UpdateJobList, object: nil)
                self.view.layoutIfNeeded()

            default: return
            }
        }
    }
}

extension PreferencesViewController : CheckBoxViewProtocol {
    func optionTapped(_ identifier: String?, optionIndex: Int, state: Bool) {
        switch identifier {
        case "shiftTime":
            switch state {
            case true:
                viewModel.setPreferredShift(shiftValueId: jobShiftArray[optionIndex].valueCode, completion: { (status) in
                    
                    if status {
                        NotificationCenter.default.post(name: NSNotification.Name.UpdateJobList, object: nil)
                    }
                })
            case false:
                if let preferencId = self.viewModel.peferencesModel?.shiftTimings?.filter({return $0.displayValue.lowercased() == jobShiftArray[optionIndex].displayValue.lowercased()}).first?.preferenceIdEnc {
                    self.viewModel.removePreferredShift(preferenceIdEnc: preferencId, completion: { (status) in
                        
                        if status {
                            NotificationCenter.default.post(name: NSNotification.Name.UpdateJobList, object: nil)
                        }
                    })
                }
            }
        case "jobType":
            switch state {
            case true:
                viewModel.setPreferredJobType(jobTypeId: jobTypeArray[optionIndex].valueCode, completion: { (status) in
                    
                    if status {
                        NotificationCenter.default.post(name: NSNotification.Name.UpdateJobList, object: nil)
                    }
                })
            case false:
                if let preferencId = self.viewModel.peferencesModel?.jobTypes?.filter({return $0.displayValue.lowercased() == jobTypeArray[optionIndex].displayValue.lowercased()}).first?.preferenceIdEnc {
                    self.viewModel.removePreferredShift(preferenceIdEnc: preferencId, completion: { (status) in
                        
                        if status {
                            NotificationCenter.default.post(name: NSNotification.Name.UpdateJobList, object: nil)
                        }
                    })
                }
            }
        default:
            break
        }
    }
    
}
