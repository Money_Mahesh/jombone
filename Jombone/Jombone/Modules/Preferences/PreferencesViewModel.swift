//
//  PreferencesViewModel.swift
//  Jombone
//
//  Created by dev139 on 29/04/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import UIKit

class PreferencesViewModel: NSObject {
    
    var peferencesModel: PreferencesModel?
    
    func getPreferences(completion: (()->())?) {
        
        let preferenceServices = PreferenceServices(requestTag: "GET_PREFERENCE_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        APPLICATION_INSTANCE.visibleViewController()?.showLoader()
        //Completion Block
        preferenceServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            APPLICATION_INSTANCE.visibleViewController()?.hideLoader()
            if let userPreferences = (response.0 as? PreferencesModel), statusCode == 1 {
                self.peferencesModel = userPreferences
                if let industries = userPreferences.industries {
                    for index in 0..<industries.count {
                        self.peferencesModel?.industries?[index].selected = true
                    }
                }
                completion?()
            }
        }
        preferenceServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            APPLICATION_INSTANCE.visibleViewController()?.hideLoader()
        }
        preferenceServices.getPreferences(parameters: nil, additionalHeaderElements: nil)
    }
    
    func setPreferredShift(shiftValueId:String, completion: ((_ status: Bool)->())?) {
        
        let preferenceServices = PreferenceServices(requestTag: "SET_PREFERRED_SHIFT", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        APPLICATION_INSTANCE.visibleViewController()?.showLoader()
        //Completion Block
        preferenceServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            APPLICATION_INSTANCE.visibleViewController()?.hideLoader()
        //APPLICATION_INSTANCE.visibleViewController()?.showAlert(message: message)
            if statusCode == 1 {
                completion?(true)
            }
            else {
                completion?(false)
            }
        }
        preferenceServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            APPLICATION_INSTANCE.visibleViewController()?.hideLoader()
        }
        preferenceServices.setPreferredShift(parameters: ["shiftTime" : shiftValueId], additionalHeaderElements: nil)
    }
    
    func setPreferredJobType(jobTypeId:String, completion: ((_ status: Bool)->())?) {
        
        let preferenceServices = PreferenceServices(requestTag: "SET_PREFERRED_JOBTYPE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        APPLICATION_INSTANCE.visibleViewController()?.showLoader()
        //Completion Block
        preferenceServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            APPLICATION_INSTANCE.visibleViewController()?.hideLoader()
            //APPLICATION_INSTANCE.visibleViewController()?.showAlert(message: message)
            if statusCode == 1 {
                completion?(true)
            }
            else {
                completion?(false)
            }
        }
        preferenceServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            APPLICATION_INSTANCE.visibleViewController()?.hideLoader()
        }
        preferenceServices.setPreferredJobType(parameters: ["jobType" : jobTypeId], additionalHeaderElements: nil)
    }
    
    func removePreferredShift(preferenceIdEnc:String, completion: ((_ status: Bool)->())?) {
        
        let preferenceServices = PreferenceServices(requestTag: "REMOVE_PREFERENCE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        APPLICATION_INSTANCE.visibleViewController()?.showLoader()
        //Completion Block
        preferenceServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            APPLICATION_INSTANCE.visibleViewController()?.hideLoader()
            //APPLICATION_INSTANCE.visibleViewController()?.showAlert(message: message)
            if statusCode == 1 {
                completion?(true)
            }
            else {
                completion?(false)
            }
        }
        preferenceServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            APPLICATION_INSTANCE.visibleViewController()?.hideLoader()
        }
        preferenceServices.removePreference(parameters: ["preferenceIdEnc" : preferenceIdEnc], additionalHeaderElements: nil)
    }
    
}
