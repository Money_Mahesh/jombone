//
//  LogOutViewModel.swift
//  Jombone
//
//  Created by Money Mahesh on 05/04/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation

class LogOutViewModel: NSObject {
    
    func hitLogoutApi(completion: ((_ message: String?)->())? = nil, success:  ((_ message: String?)->())? = nil, failure: ((_ message: String?)->())? = nil) {
        
        let autheticationServices = AutheticationServices(requestTag: "LOGOUT_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        //Completion Block
        autheticationServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            if statusCode == 1, let response = response.0 as? Bool, response {
                
                UserDataManager.shared.logout()
                completion?(message)
                success?(message)
            }
            else {
                completion?(message)
                failure?(message)
            }
        }
        autheticationServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            completion?(nil)
            failure?(nil)
        }
        
        let parameters: [String: Any] = [
            "deviceId" : DEVICE_ID
        ]
        autheticationServices.hitLogout(parameters: parameters, additionalHeaderElements: nil)
    }
}
