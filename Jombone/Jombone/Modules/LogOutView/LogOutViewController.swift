//
//  LogOutViewController.swift
//  Jombone
//
//  Created by Money Mahesh on 15/11/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import UIKit

class LogOutViewController: WhiteStatusBarViewController {

    var viewModel = LogOutViewModel()
    
    static func show() {
        
        let viewController = LOGIN_STORYBOARD.instantiateViewController(withIdentifier: "LogOutViewController")
        APPLICATION_INSTANCE.visibleViewController()?.present(viewController, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK:- IBAction
    @IBAction func btnActionForYes(_ sender: UIButton) {
        
        showLoader()
        viewModel.hitLogoutApi(completion: { (message) in
            self.hideLoader()
        }, success: { (message) in
            APP_DELEGATE_INSTANCE.setupInitialViewController()
        }, failure: { (message) in
            self.showAlert(message: message, withOk: true)
        })
    }
    
    @IBAction func btnActionForNo(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
