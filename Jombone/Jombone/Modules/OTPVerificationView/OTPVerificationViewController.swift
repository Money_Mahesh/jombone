//
//  OTPVerificationViewController.swift
//  Jombone
//
//  Created by dev139 on 22/08/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import UIKit

class OTPVerificationViewController: WhiteStatusBarViewController {

    
    @IBOutlet weak var lblForTitle: UILabel!
    @IBOutlet weak var lblForTypeValue: UILabel!
    @IBOutlet weak var btnForEdit: UIButton!
    @IBOutlet weak var lblForError: UILabel!

    @IBOutlet weak var otpView: OTP_View!
    var otp : String!
    var viewModel : OTPVerificationViewModel!
    
    static func show(viewModel: OTPVerificationViewModel) {
        
        let viewController = LOGIN_STORYBOARD.instantiateViewController(withIdentifier: "OTPVerificationViewController") as! OTPVerificationViewController
        
        viewController.viewModel = viewModel
        APPLICATION_INSTANCE.visibleViewController()?.navigationController?.pushViewController(viewController, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpView()
        self.otpView.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpView() {
    
        btnForEdit.isHidden = false
        switch viewModel.purpose! {
        case .email:
            lblForTitle.text = "Verify Your Email"
            lblForTypeValue.text = viewModel.email
            btnForEdit.setTitleForAllState(title: "Edit Email")
           
        case .forgotPassword(let email):
            viewModel.email = email
            btnForEdit.isHidden = true
            lblForTitle.text = "Verify Your Email"
            lblForTypeValue.text = viewModel.email
            btnForEdit.setTitleForAllState(title: "Edit Email")
            
        default:
            lblForTitle.text = "Verify Your Mobile"
            lblForTypeValue.text = (viewModel.countrycode == nil ? "" : (viewModel.countrycode +
                " ")) + (viewModel.mobile.toPhoneNumber())
            btnForEdit.setTitleForAllState(title: "Edit Mobile")
        }
    }
    
    func moveToNextStep() {
        
        switch viewModel.purpose! {
        case .phoneNo:
            UserDataManager.shared.isSignUpCompleted = true
            APP_DELEGATE_INSTANCE.setupInitialViewController()
            
        case .forgotPassword( _):
            ChangePasswordViewController.show(viewModel: ChangePasswordViewModel(purpose: ChangePurpose.resetPassword))
            
        default:
            SelectJobLocationViewController.show()
        }
    }

    //MARK:- IBAction
    @IBAction func btnActionForVerify(_ sender: UIButton) {
        
        view.endEditing(true)
        
        if let errorMsg = otpView.validate() {
            lblForError.text = errorMsg
            return
        }
        viewModel.otp = self.otp
        
        showLoader()
        viewModel.hitVerifyOTPApi(success: { (message) in
            self.hideLoader()
            self.moveToNextStep()
            self.showAlert(message: message)
        }) { (message) in
            self.hideLoader()
            self.lblForError.text = message
        }
    }
    
    @IBAction func btnActionForResend(_ sender: UIButton) {
        
        view.endEditing(true)
        showLoader()
        viewModel.hitResendOTPApi(success: { (message) in
            self.hideLoader()
            self.showAlert(message: message)
        }) { (message) in
            self.hideLoader()
            self.showAlert(message: message)
        }
    }
    
    @IBAction func btnActionForEdit(_ sender: UIButton) {
        
        view.endEditing(true)
        UpdateContactInfoViewController.show(viewModel: UpdateContactInfoViewModel(type: viewModel.purpose), delegate: self)
    }
}

extension OTPVerificationViewController: OTP_TextFieldDelegate {
    
    func textFieldUpdate(completeString: String) {
        print(completeString)
        lblForError.text = nil
        self.otp = completeString
    }
}

extension OTPVerificationViewController: UpdateContactInfoViewProtocol {
    
    func mobileNoUpdated(mobile: String, countryIsoCode: String, countryCode: String) {
        viewModel.countrycode = countryCode
        viewModel.countryIsoCode = countryIsoCode
        viewModel.mobile = mobile
        setUpView()
    }
    
    func emailUpdated(email: String) {
        viewModel.purpose = .email(email: email)
        viewModel.email = email
        setUpView()
    }
}
