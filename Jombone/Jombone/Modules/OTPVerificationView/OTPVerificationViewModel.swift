//
//  OTPVerificationViewModel.swift
//  Jombone
//
//  Created by dev139 on 22/09/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import UIKit


class OTPVerificationViewModel: NSObject {
    
    var email: String!
    var mobile : String!
    var countryIsoCode : String!
    var countrycode : String!
    var otp : String!
    var purpose: ContactType!
    
    override private init() {}
    
    init(purpose: ContactType) {
        self.purpose = purpose
        self.email = UserDataManager.shared.profileDetail?.personalDetailsBeans?.email ?? ""
        self.mobile = UserDataManager.shared.profileDetail?.personalDetailsBeans?.phoneNumber ?? ""
        self.countrycode = UserDataManager.shared.profileDetail?.personalDetailsBeans?.countryCode ?? ""
    }
    
    func validate(otp : String?) -> String? {
        
        self.otp = otp
    
        return DataValidationUtility.shared.validateData(
            fields: [(data: otp, name: "otp", customMessage: nil)],
            type: (generic: fieldType.otp,
                   specific: nil))
    }
    
    func hitVerifyOTPApi(completion: ((_ message: String?)->())? = nil, success:  ((_ message: String?)->())? = nil, failure: ((_ message: String?)->())? = nil) {
        
        let authenticationServices = AutheticationServices(requestTag: "VERIFY_OTP_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        //Completion Block
        authenticationServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            
            if statusCode == 1 {
                
                switch self.purpose! {
                case .phoneNo:
                    if let response = response.0 as? LoginDetail {
                        
                        UserDataManager.shared.token = response.token
                        if let personalDetailsBean = response.personalDetailsBean {
                            UserDataManager.shared.profileDetail = ProfileDetail(personalDetailsBean: personalDetailsBean)
                        }
                    }
                    
                default:
                    UserDataManager.shared.profileDetail?.personalDetailsBeans?.email = self.email
                }
                
                completion?(message)
                success?(message)
            }
            else {
                completion?(message)
                failure?(message)
            }
        }
        authenticationServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            completion?(nil)
            failure?(nil)
        }
        
        let parameters: [String: Any] = [
            "otp" : otp!
        ]
        
        switch self.purpose! {
        case .phoneNo:
            authenticationServices.hitVerifyPhoneNoOTP(parameters: parameters, additionalHeaderElements: nil)
            
        default:
            authenticationServices.hitVerifyEmailOTP(parameters: parameters, additionalHeaderElements: nil)
        }
    }
    
    func hitResendOTPApi(completion: ((_ message: String?)->())? = nil, success:  ((_ message: String?)->())? = nil, failure: ((_ message: String?)->())? = nil) {
        
        let authenticationServices = AutheticationServices(requestTag: "RESEND_OTP_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        //Completion Block
        authenticationServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            if statusCode == 1 {
                completion?(message)
                success?(message)
            }
            else {
                completion?(message)
                failure?(message)
            }
        }
        authenticationServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            completion?(nil)
            failure?(nil)
        }
        
        var methodName = String()
        switch self.purpose! {
        case .phoneNo:
            methodName = "/resend-mobile-otp"
            
        default:
            methodName = "resend-email-otp"
        }
        authenticationServices.hitResendOTP(methodName: methodName, parameters: nil, additionalHeaderElements: nil)
    }
}
