//
//  LoginViewController.swift
//  Jombone
//
//  Created by Money Mahesh on 13/08/18.
//  Copyright © 2018 Money Mahesh. All rights reserved.
//

import UIKit

class LoginViewController: WhiteStatusBarViewController, CustomTextfieldViewDelegate {

    @IBOutlet weak var textfieldForEmail: CustomTextfieldView!
    @IBOutlet weak var textfieldForPassword: CustomTextfieldView!
    @IBOutlet var viewModel: LoginViewModel!
    
    var isTokenExpired: Bool = false

    static func showWithAnimation(_ animated: Bool) {
        
        let viewController = LOGIN_STORYBOARD.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        APPLICATION_INSTANCE.visibleViewController()?.navigationController?.pushViewController(viewController, animated: animated)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Set Textfield Preferences
        textfieldForEmail.setJomboneEmailStyle()
        textfieldForPassword.setJombonePasswordStyle()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnActionForLogin(_ sender: UIButton) {
      
        view.endEditing(true)
        
        viewModel.reset()
        viewModel.email = textfieldForEmail.isValidEmail()
        viewModel.password = textfieldForPassword.isEmpty("password")
        
        showLoader()
        viewModel.hitLoginApi(completion: { (message) in
            self.hideLoader()
        }, success: { (message) in
            UserDataManager.shared.isSignUpCompleted = true
            APP_DELEGATE_INSTANCE.setupInitialViewController()
        }, failure: { (statusCode, errorMessage) in
            if let errorCode = statusCode {
                self.handleError(errorCode: errorCode)
                self.showAlert(message: errorMessage)
            }
        })
    }
    
    func handleError(errorCode: Int) {
        
        switch errorCode {
        case 2:
            //Email not verified - open email verify screen
            OTPVerificationViewController.show(viewModel: OTPVerificationViewModel(
                purpose: .email(email: UserDataManager.shared.profileDetail?.personalDetailsBeans?.email)))
            
        case 3:
            //successful login but location not provided - open location screen
            SelectJobLocationViewController.show()
        
        case 4:
            //mobile not verified, open mobile verify screen
            OTPVerificationViewController.show(viewModel: OTPVerificationViewModel(
                purpose: .phoneNo))
            
        default:
            break
        }
    }
    
    @IBAction func btnActionForSignUp(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnActionForForgotPassword(_ sender: UIButton) {
        ForgotPasswordViewController.show()
    }
}

