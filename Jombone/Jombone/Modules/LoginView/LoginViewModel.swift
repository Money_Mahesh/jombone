//
//  LoginViewModel.swift
//  Jombone
//
//  Created by Money Mahesh on 13/08/18.
//  Copyright © 2018 Money Mahesh. All rights reserved.
//

import Foundation

class LoginViewModel: NSObject {
    
    var email: String! {
        didSet {
            if email == nil {
                isInputValid = false
            }
        }
    }
    var password: String! {
        didSet {
            if password == nil {
                isInputValid = false
            }
        }
    }
    
    private var isInputValid = false
    func reset() {
        isInputValid = true
    }
    
    func hitLoginApi(completion: ((_ message: String?)->())? = nil, success:  ((_ message: String?)->())? = nil, failure: ((_ statusCode: Int?, _ message: String?)->())? = nil) {
        
        if !isInputValid {
            completion?(nil)
            return
        }
        
        let loginServices = AutheticationServices(requestTag: "LOGIN_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        //Completion Block
        loginServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            UserDataManager.shared.token = (response.0 as? LoginDetail)?.token
            if let personalDetailsBean = (response.0 as? LoginDetail)?.personalDetailsBean {
                UserDataManager.shared.profileDetail = ProfileDetail(personalDetailsBean: personalDetailsBean)
            }
            else {
                UserDataManager.shared.profileDetail = nil
            }
            
            let loginDetail = response.0 as? LoginDetail
            UserDataManager.shared.token = loginDetail?.token
            if let personalDetailsBean = loginDetail?.personalDetailsBean {
                UserDataManager.shared.profileDetail = ProfileDetail(personalDetailsBean: personalDetailsBean)
            }
            
            if statusCode == 1 {
                
                completion?(message)
                success?(message)
            }
            else {
                completion?(message)
                failure?(statusCode, message)
            }
        }
        loginServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            completion?(nil)
            failure?(nil, nil)
        }
        
        let parameters: [String: Any] = [
            "email": email!,
            "password": password!,
            "role": UserDataManager.shared.role,
            "deviceId" : DEVICE_TOKEN,
            "deviceType": DEVICE_TYPE
        ]
        
        loginServices.hitLogin(parameters: parameters, additionalHeaderElements: nil)
    }
}
