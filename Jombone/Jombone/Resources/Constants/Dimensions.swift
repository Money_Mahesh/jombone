//
//  Dimensions.swift
//  Jombone
//
//  Created by Money Mahesh on 13/08/18.
//  Copyright © 2018 Money Mahesh. All rights reserved.
//

import Foundation
import UIKit

//MARK:- Window Dimension
let WINDOW_HEIGHT = UIScreen.main.bounds.height
let WINDOW_WIDTH = UIScreen.main.bounds.width


