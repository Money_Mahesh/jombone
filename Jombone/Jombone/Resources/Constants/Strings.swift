//
//  Strings.swift
//  Jombone
//
//  Created by Money Mahesh on 13/08/18.
//  Copyright © 2018 Money Mahesh. All rights reserved.
//

import Foundation

let DEVICE_TOKEN_KEY                        = "deviceToken"
let IS_DEVICE_REGISTER_FOR_NOTIFICATION     = "isDeviceRegisterForNotification"

let REQUESTING_SERVER = "Please wait..."

let ALERT_NO_NETWORK_TITLE = "No Internet Connection"
let ALERT_OK_BUTTON_TITLE = "Ok"
let ALERT_CANCEL_BUTTON_TITLE = "Cancel"

let LOADER_MSG_LOADING = "Loading details, please wait..."

let HIDE_LOADER = "Hide Loader"
let HEADER_TOKEN_KEY = "X-Auth-Token"
