//
//  Constants.swift
//  Jombone
//
//  Created by Money Mahesh on 13/08/18.
//  Copyright © 2018 Money Mahesh. All rights reserved.
//

import Foundation
import UIKit
//"http://35.182.26.122/api/"
// MARK: - URLS
var BASE_URL: URL! {
    get {
        #if DEBUG
            return URL(string: ("https://www.jombone.com/api/" + "v1"))
        #else
            return URL(string: ("https://www.jombone.com/api/" + "v1"))
        #endif
    }
}

var APP_VERSION: String {
    get {
        if let currentAppVersion = Bundle.main.infoDictionary!["CFBundleVersion"] {
            return String(describing: currentAppVersion)
        }
        else {
            return ""
        }
    }
}

var DEVICE_ID: String {
    get {
        if let deviceIdentifier = UIDevice.current.identifierForVendor {
            return String(describing: deviceIdentifier)
        }
        else {
            return ""
        }
    }
}

var GOOGLE_PLACE_API_KEY = "AIzaSyDUN_qrPr2bO-iAHCaJP328wKcGRa4T2bA" 
let GOOGLE_ROUTE_BASE_URL = "https://maps.googleapis.com/maps/api/directions/json?"
let GOOGLE_NEARBY_BASE_URL = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?"


var OS_VERSION = UIDevice.current.systemVersion

let APP_DELEGATE_INSTANCE = UIApplication.shared.delegate as! AppDelegate
let APPLICATION_INSTANCE = UIApplication.shared
var KEY_WINDOW = UIApplication.shared.keyWindow

let ALERT_STORYBOARD = UIStoryboard(name: "Alert", bundle: nil)
let LOGIN_STORYBOARD = UIStoryboard(name: "Login", bundle: nil)
let MAIN_STORYBOARD = UIStoryboard(name: "Main", bundle: nil)
let APPLICATION_STORYBOARD = UIStoryboard(name: "Applications", bundle: nil)
let JOBS_STORYBOARD = UIStoryboard(name: "Jobs", bundle: nil)
let PROFILE_STORYBOARD = UIStoryboard(name: "Profile", bundle: nil)
let TRY_STORYBOARD = UIStoryboard(name: "TryStoryboard", bundle: nil)
let WORK_STORYBOARD = UIStoryboard(name: "Work", bundle: nil)
let COMPANY_STORYBOARD = UIStoryboard(name: "Company", bundle: nil)
let PREFERENCES_STORYBOARD = UIStoryboard(name: "Preferences", bundle: nil)



let DEFAULT_NAVIGATION_COLOR = UIColor(hexFromString: "#1174E7")

let OTP_LENGTH = 4
let PIN_CODE_LENGTH = 6
let PHONE_NO_MIN_LENGTH = 10
let PASSWORD_MIN_LENGTH = 6
let CANADA_COUNTRY_CODE = "+1"
let CANADA_ISO_COUNTRY_CODE = "ca"
let DEVICE_TYPE = "2"
let DEVICE_TOKEN = "token"
let PAGE_SIZE = 10
let ACCOUNT_NO_MIN_LENGTH = 5
