//
//  ErrorMessages.swift
//  Jombone
//
//  Created by Money Mahesh on 13/08/18.
//  Copyright © 2018 Money Mahesh. All rights reserved.
//

let NETWORK_FAILURE_MSG = "Can't Connect. Please check your Internet Connection"

let SERVER_ERROR_MSG = "Something went wrong. Please try after sometime."

let ERROR_EMPTY_USER_NAME = "Please enter username"
let ERROR_EMPTY_PWD = "Please enter password"
let ERROR_IN_CORRECT_CREDENTIAL = "Incorrect  username or password"
let ERROR_TOKEN_EXPIRED = "Session expired. Please login again."
let ERROR_CONFIRM_PASSWORD = "Confirm password should be same as new password"
let ERROR_ONLY_CHARACTER_ALLOWED = "Only characters and spaces allowed"
let ERROR_CONFIRM_Email = "Confirm email should be same"
let ERROR_REQUIRED_FIELD = "This is a required field"
let ERROR_ONLY_POSITIVE_NUMBER_ALLOWED = "Please enter only numeric"



enum AppError {
    
    case empty(field: String)
    case valid(field: String)
    case min(field: String, length: Int)
    case max(field: String, length: Int)
    case equal(field: String, length: Int)
    case fileLength(max: String)
    
    var message: String {
        switch self {
        case .empty(let field):
            return empty(field: field)
            
        case .valid(let field):
            return valid(field: field)
            
        case .min(let field, let length):
            return min(field: field, length: length)
            
        case .max(let field, let length):
            return max(field: field, length: length)
            
        case .equal(let field, let length):
            return equal(field: field, length: length)
            
        case .fileLength(let maxLength):
            return fileLength(maxLength: maxLength)
        }
    }
    
    
    private func fileLength(maxLength: String) -> String {
        return "You can not upload document larger than " + maxLength
    }
    
    private func empty(field: String) -> String {
        return "Please enter " + field.lowercased()
    }
    
    private func valid(field: String) -> String {
        return "Please enter valid " + field.lowercased()
    }
    
    private func min(field: String, length: Int) -> String {
        return field.capitalized + " length should be greater than or equal to " + String(length)
    }
    
    private func max(field: String, length: Int) -> String {
        return field.capitalized + " length should be less than or equal to " + String(length)
    }
    
    private func equal(field: String, length: Int) -> String {
        return field.capitalized + " length should be " + String(length)
    }
}

func validInputMessageForField(_ fieldName: String) -> String{
    return "Please provide valid " + fieldName.lowercased()
}
