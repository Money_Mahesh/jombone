//
//  DateExtension.swift
//  Jombone
//
//  Created by Money Mahesh on 14/09/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import Foundation

extension Date {
    var ticks: UInt64 {
        return UInt64((self.timeIntervalSince1970 + 62_135_596_800) * 10_000_000)
    }
    
    var startOfWeek: Date? {
        let gregorian = Calendar(identifier: .gregorian)
        guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return nil }
        return gregorian.date(byAdding: .day, value: 1, to: sunday)
    }
    
    var endOfWeek: Date? {
        let gregorian = Calendar(identifier: .gregorian)
        guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return nil }
        return gregorian.date(byAdding: .day, value: 7, to: sunday)
    }
    
    var year: Int {
        let gregorian = Calendar(identifier: .gregorian)
        return gregorian.component(Calendar.Component.year, from: self)
    }
    
    var weekNumber: Int! {
        let calendar = Calendar.current
        let weekOfYear = calendar.component(.weekOfYear, from: self)
        return weekOfYear
    }
}
