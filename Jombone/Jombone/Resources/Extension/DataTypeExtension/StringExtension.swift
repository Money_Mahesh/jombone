//
//  StringExtension.swift
//  Jombone
//
//  Created by Money Mahesh on 13/08/18.
//  Copyright © 2018 Money Mahesh. All rights reserved.
//

import Foundation
import UIKit

extension String {
    
    var isSinNo: Bool {
        get {
            if (self.count == 0) { return false }
            let numberRegEx = "^([1-9][1-9]{8}|)$"
            let numberTest = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
            return numberTest.evaluate(with: self)
        }
    }
    
    var isPhoneNo: Bool {
        get {
            if (self.count == 0 || self.first == "0" || self.first == "1") {
                return false
            }
            return true
        }
    }
    
    var isNo: Bool {
        get {
            if (self.count == 0) { return false }
            let numberRegEx = "^\\d*$"
            let numberTest = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
            return numberTest.evaluate(with: self)
        }
    }
    
    var isText: Bool {
        get {
            if (self.count == 0) { return false }
            let textRegEx = "^([a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁ ÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+)$"
            let stringTest = NSPredicate(format:"SELF MATCHES %@", textRegEx)
            return stringTest.evaluate(with: self)
        }
    }
    
    var isEmail: Bool {
        get {
            if (self.count == 0) { return false }
            let emailRegEx = "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$"
            let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
            return emailTest.evaluate(with: self)
        }
    }
    
    var isPassword: Bool {
        get {
            if (self.count == 0) { return false }
            let passwordRegEx = "^(?=.*)(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z].*[a-z].*[a-z]).{6,}$"

            let passwordTest = NSPredicate(format:"SELF MATCHES %@", passwordRegEx)
            return passwordTest.evaluate(with: self)
        }
    }
    
    public func toPhoneNumber() -> String {
        return self.replacingOccurrences(of: "(\\d{3})(\\d{3})(\\d+)", with: "($1) $2-$3", options: .regularExpression, range: nil)
    }
    
    public func toUnPhoneNumber() -> String {
        var str = self
        str = str.replacingOccurrences(of: ")", with: "")
        str = str.replacingOccurrences(of: " ", with: "")
        str = str.replacingOccurrences(of: "-", with: "")
        str = str.replacingOccurrences(of: "(", with: "")
        return str
    }
    
    public func appendTrailingSpace() -> String {
        
        if self.count == 0 {
            return ""
        }
        
        return (self + " ")
    }
    
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
