//
//  NotificationNameExtension.swift
//  Jombone
//
//  Created by Money Mahesh on 24/08/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let UserLoginStatusUpdated = Notification.Name("UserLoginStatusUpdated")
    static let UserProfileUpdated = Notification.Name("UserProfileUpdated")
    static let ScoreUpdated = Notification.Name("ScoreUpdated")
    static let TabSelectionChanged = Notification.Name("TabSelectionChanged")
    
    static let UpdateJobList = Notification.Name("UpdateJobList")
    static let ReloadJobList = Notification.Name("ReloadJobList")
    static let SetSignInSignOutFloatingBtn = Notification.Name("SetSignInSignOutFloatingBtn")
    
    static let UpdatedCompanyList = Notification.Name("UpdatedCompanyList")
    static let ReloadCompanyList = Notification.Name("ReloadCompanyList")
    
    static let ReloadNotificationList = Notification.Name("ReloadNotificationList")
    static let UpdateNotificationList = Notification.Name("UpdateNotificationList")
    static let NotificationDeleted = Notification.Name("NotificationDeleted")
    
    static let ReloadApplicationList = Notification.Name("ReloadApplicationList")
    static let UpdateApplicationList = Notification.Name("UpdateApplicationList")
    static let ApplicationWithdraw = Notification.Name("ApplicationWithdraw")
    
    static let ReloadOfferList = Notification.Name("ReloadOfferList")
    static let UpdateOfferList = Notification.Name("UpdateOfferList")
    static let OfferRejected = Notification.Name("OfferRejected")

    static let ReloadPastWorkList = Notification.Name("ReloadPastWorkList")
    static let UpdatePastWorkList = Notification.Name("UpdatePastWorkList")

    static let ReloadRejectedWithdrawList = Notification.Name("ReloadRejectedWithdrawList")
    
    static let ReloadCurrentWorkList = Notification.Name("ReloadCurrentWorkList")
    static let CollapseAllCurrentWorkCard = Notification.Name("CollapseAllCurrentWorkCard")

    static let ShowLocationToolTip = Notification.Name("ShowLocationToolTip")

    static let NotificationCountUpdated = Notification.Name("NotificationCountUpdated")
    
    static let IdDeleted = Notification.Name("IdDeleted")
    static let EducationDeleted = Notification.Name("EducationDeleted")
    static let SkillDeleted = Notification.Name("SkillDeleted")
    
    static let ReloadEmploymentList = Notification.Name("ReloadEmploymentList")

}
