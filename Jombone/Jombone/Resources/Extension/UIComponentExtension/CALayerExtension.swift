//
//  CALayerExtension.swift
//  Jombone
//
//  Created by Money Mahesh on 13/08/18.
//  Copyright © 2018 Money Mahesh. All rights reserved.
//

import Foundation
import UIKit

enum GradientDirection {
    case upDown
    case downUp
    case leftRight
    case rightLeft
    case upLeftCornerToBottomRightCorner
    case upRightCornerToBottomLeftCorner
    case downLeftCornerToUpRightCorner
    case downRightCornerToUpLeftCorner
}

enum ShadowDirection {
    case up
    case down
    case left
    case right
    case center
    case custom(CGSize)
}

extension CALayer {
    
    func addGradient(colors: [CGColor], locations: [NSNumber], direction: GradientDirection, frame: CGRect? = nil) {
        
        if let sublayers = sublayers {
            for sublayer in sublayers {
                if sublayer.name == "gradient" {
                    sublayer.removeFromSuperlayer()
                }
            }
        }
        
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.colors = colors
        gradient.locations = locations
        
        switch direction {
        case .upDown:
            gradient.startPoint = CGPoint(x: 1.0, y: 0.0)
            gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
            
        case .downUp:
            gradient.startPoint = CGPoint(x: 1.0, y: 1.0)
            gradient.endPoint = CGPoint(x: 1.0, y: 0.0)
            
        case .leftRight:
            gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
            gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
            
        case .rightLeft:
            gradient.startPoint = CGPoint(x: 1.0, y: 1.0)
            gradient.endPoint = CGPoint(x: 0.0, y: 1.0)
            
        case .upLeftCornerToBottomRightCorner:
            gradient.startPoint = CGPoint(x: 0.0, y: 0.0)
            gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
            
        case .upRightCornerToBottomLeftCorner:
            gradient.startPoint = CGPoint(x: 1.0, y: 0.0)
            gradient.endPoint = CGPoint(x: 0.0, y: 1.0)
            
        case .downLeftCornerToUpRightCorner:
            gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
            gradient.endPoint = CGPoint(x: 1.0, y: 0.0)
            
        case .downRightCornerToUpLeftCorner:
            gradient.startPoint = CGPoint(x: 1.0, y: 1.0)
            gradient.endPoint = CGPoint(x: 0.0, y: 0.0)
        }
        
        gradient.frame = frame ?? self.frame
        
        gradient.name = "gradient"
        insertSublayer(gradient, at: 0)
    }
    
    func addShadow(color: CGColor, direction: ShadowDirection, radius: CGFloat, spread: Float) {
        
        shadowColor = color
        shadowOpacity = spread
        shadowRadius = radius
        
        switch direction {
        case .center:
            shadowOffset = CGSize.zero
        case .up:
            shadowOffset = CGSize(width: 0.0, height: -(CGFloat(spread) + 2))
        case .left:
            shadowOffset = CGSize(width: -(CGFloat(spread) + 2), height: 0)
        case .down:
            shadowOffset = CGSize(width: 0, height: (CGFloat(spread) + 2))
        case .right:
            shadowOffset = CGSize(width: (CGFloat(spread) + 2), height: 0)
        case let .custom(offset):
            shadowOffset = offset
        }

    }
}
