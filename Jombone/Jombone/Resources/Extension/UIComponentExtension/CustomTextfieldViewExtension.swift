//
//  CustomTextfieldViewExtension.swift
//  Jombone
//
//  Created by Money Mahesh on 05/02/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import UIKit

//AppSpecific
extension CustomTextfieldView {
    
    func setJomboneFontStyle() {
        setFont(font: UIFont.rubikRegularFontOfSize(16.0),
                placeholderInActiveFont: UIFont.rubikRegularFontOfSize(16.0),
                placeholderActiveFont: UIFont.rubikRegularFontOfSize(12.0))
        textAlignment = .left
    }
    
    func setJomboneEmailStyle() {
        inputType = .Keyboard(type: .emailAddress)
        textfield.autocapitalizationType = .none
        setJomboneFontStyle()
    }
    
    func setJomboneMobileStyle() {
        inputType = .Keyboard(type: .numberPad)
        setJomboneFontStyle()
    }
    
    func setJombonePasswordStyle() {
        
        setJomboneFontStyle()
        textfield.MaxLength = 16
        setRightBtnStyle(style: CustomTextfieldView.BtnStyle.imageName(selected: "eye", deSelected: "eye_off"), target: self, action: #selector(CustomTextfieldView.showPassword))
    }
    
    @objc func showPassword() {
        textfield.isSecureTextEntry = rightBtn.isSelected
        rightBtn.isSelected = !rightBtn.isSelected
    }
    
    func setJomboneCountryCodeStyle(delegate: CustomTextfieldViewDelegate, selectedCode: String? = nil) {
        
        setJomboneFontStyle()
        
        //Set Country Code
        let countryCodeArray: [[String: String]] = Utility.countryCodeArray!
        var selectedCodeIndex: [Int]?
        
        var index = 0
        let codeArray = countryCodeArray.map({ (obj: [String: String]) -> String in
            
            if let isoCode = obj["ISO_Code"]?.lowercased(), let _selectedCode = selectedCode?.lowercased(), isoCode == _selectedCode {
                selectedCodeIndex = [index]
            }
            
            index += 1
            if let countryName = obj["displayFullName"], let flag  = obj["flag"] {
                return flag + " " + countryName
            }
            return ""
        })
        
        pickerDataSource(array: [codeArray], selectedIndex: (selectedCodeIndex ?? [30]), delegate: delegate,  pickerSeletedTextFormat: { (textArray, indexes, InputType) -> (String) in
            return (countryCodeArray[(indexes.first!)]["flag"])! + " " + (countryCodeArray[(indexes.first!)]["isdvalueCodes"])!
        })
    }
    
    func setBorderStyle() {
        
        setJomboneFontStyle()
        self.stackViewForBottomLine.isHidden = true
    }
}

extension CustomTextfieldView {
    
    func isValidName(_ fieldName: String = "name", customMessage: String? = nil) -> String? {
        self.errorMsg = DataValidationUtility.shared.validateData(
            fields: [(data: self.text, name: fieldName, customMessage: customMessage)],
            type: (generic: nil,
                   specific: ["0" : fieldType.onlyCharacterText]))
        
        return (self.errorMsg == nil) ? self.text : nil
    }
    
    func isValidSIN(_ fieldName: String = "sin number", customMessage: String? = nil) -> String? {
        self.errorMsg = DataValidationUtility.shared.validateData(
            fields: [(data: self.text, name: fieldName, customMessage: customMessage)],
            type: (generic: nil,
                   specific: ["0" : fieldType.sin]))
        
        return (self.errorMsg == nil) ? self.text : nil
    }
    
    func isValidAccountNumber(_ fieldName: String = "Account Number", customMessage: String? = nil) -> String? {
        self.errorMsg = DataValidationUtility.shared.validateData(
            fields: [(data: self.text, name: fieldName, customMessage: customMessage)],
            type: (generic: nil,
                   specific: ["0" : fieldType.accountNo]))
        
        return (self.errorMsg == nil) ? self.text : nil
    }
    
    func isValidEmail(_ fieldName: String = "email", customMessage: String? = nil) -> String? {
        self.errorMsg = DataValidationUtility.shared.validateData(
            fields: [(data: self.text, name: fieldName, customMessage: customMessage)],
            type: (generic: nil,
                   specific: ["0" : fieldType.email]))
        
        return (self.errorMsg == nil) ? self.text : nil
    }
    
    func isValidPassword(_ fieldName: String = "password", customMessage: String? = nil) -> String? {
        self.errorMsg =  DataValidationUtility.shared.validateData(
            fields: [(data: self.text, name: fieldName, customMessage: customMessage)],
            type: (generic: nil,
                   specific: ["0": fieldType.password]))
        
        return (self.errorMsg == nil) ? self.text : nil
    }
    
    func isValidConfirmPassword(password: String?, customMessage: String? = nil) -> String? {
        self.errorMsg =  DataValidationUtility.shared.validateData(
            fields: [(data: self.text, name: "confirm password", customMessage: customMessage)],
            type: (generic: nil,
                   specific: ["0": fieldType.password]))
        
        if (self.errorMsg == nil) {
            if password != self.text {
                self.errorMsg = ERROR_CONFIRM_PASSWORD
            }
        }
        return (self.errorMsg == nil) ? self.text : nil
    }
    
    func isValidPhoneNo(_ fieldName: String = "mobile", customMessage: String? = nil) -> String? {
        self.errorMsg =  DataValidationUtility.shared.validateData(
            fields: [(data: self.text, name: fieldName, customMessage: customMessage)],
            type: (generic: nil,
                   specific: ["0": fieldType.phoneNo]))
        
        return (self.errorMsg == nil) ? self.text : nil
    }
    
    func isEmpty(_ fieldName: String, customMessage: String? = nil) -> String? {
        self.errorMsg =  DataValidationUtility.shared.validateData(
            fields: [(data: self.text, name: fieldName, customMessage: customMessage)],
            type: (generic: nil,
                   specific: ["0": fieldType.text]))
        
        return (self.errorMsg == nil) ? self.text : nil
    }
    
    func isNumberWithRange(_ min: Int? = nil, max: Int? = nil, fieldName: String, customMessage: String? = nil) -> String? {
        self.errorMsg =  DataValidationUtility.shared.validateData(
            fields: [(data: self.text, name: fieldName, customMessage: customMessage)],
            type: (generic: nil,
                   specific: ["0": fieldType.numberRange(min: min, max: max)]))
        
        return (self.errorMsg == nil) ? self.text : nil
    }
    
    func isOfLength(_ fieldName: String, lenght: Int, customMessage: String? = nil) -> String? {
        self.errorMsg =  DataValidationUtility.shared.validateData(
            fields: [(data: self.text, name: fieldName, customMessage: customMessage)],
            type: (generic: nil,
                   specific: ["0": fieldType.length(length: lenght)]))
        
        return (self.errorMsg == nil) ? self.text : nil
    }
}
