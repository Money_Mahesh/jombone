//
//  UITextFieldExtension.swift
//  Jombone
//
//  Created by Money Mahesh on 13/08/18.
//  Copyright © 2018 Money Mahesh. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
    
    @IBInspectable var ClearRunTime: Bool {
        set {
            if newValue == true {
                self.text = ""
            }
        }
        get {
            return self.text == "" ? true : false
        }
    }
}
