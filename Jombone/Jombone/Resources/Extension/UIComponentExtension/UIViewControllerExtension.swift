//
//  UIViewController.swift
//  Jombone
//
//  Created by Money Mahesh on 13/08/18.
//  Copyright © 2018 Money Mahesh. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
   
    enum NavBtnType {
        case none
        case close
        case back
        case menu
        case both
        case title(title: String)
        case image(image: UIImage)
    }
    
    enum NavBG {
        case image(image: UIImage)
        case bgColor(value: UIColor)
        case imageName(value: String)
        case defaultImage
        case defaultColor
    }
    
    func visibleViewController() -> UIViewController {
        
        if let presented = self.presentedViewController {
            return presented.visibleViewController()
        }
        
        if let navigation = self as? UINavigationController {
            return navigation.visibleViewController?.visibleViewController() ?? navigation
        }
        
        if let tab = self as? UITabBarController {
            return tab.selectedViewController?.visibleViewController() ?? tab
        }
        
        return self
    }
    
    func setNavigationBarWithSearchBar(title: String, searchBarTitle: String = "Type here...",
                                                andLeftButton leftBtnType: NavBtnType = .none,
                                                andRightButton rightBtnType: NavBtnType = .none,
                                                withBg bg: NavBG) {
        
        
        setNavigationBar(withTitle: title,
                         andLeftButton: leftBtnType,
                         andRightButton: rightBtnType,
                         withBg: bg)
        
        var navBarLeftButtons: [UIBarButtonItem]? = navigationItem.leftBarButtonItems
        let navBarRightButtons: [UIBarButtonItem]? = navigationItem.rightBarButtonItems
        let leftPadding: CGFloat = CGFloat((navBarLeftButtons?.count ?? 0) * 50)
        let rightPadding: CGFloat = CGFloat((navBarRightButtons?.count ?? 0) * 50)

        let searchBar: UISearchBar = UISearchBar(frame: CGRect(x: 0, y: -10, width: WINDOW_WIDTH - (leftPadding + rightPadding), height: 20))
        searchBar.placeholder = searchBarTitle
        searchBar.tintColor = UIColor(red: 0/255, green: 122/255, blue: 255/255, alpha: 1.0)
        searchBar.layer.cornerRadius = 1
        
        navBarLeftButtons?.append(UIBarButtonItem(customView:searchBar))
        navigationItem.leftBarButtonItems = navBarLeftButtons
    }
    
    func setNavigationBarWithTitle(_ title: String = "",
                          andLeftButton leftBtnType: NavBtnType = .none,
                          andRightButton  rightBtnType: NavBtnType = .none,
                          withBg bg: NavBG) {
        
        self.title = title
        navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.white,
            NSAttributedString.Key.font: UIFont.rubikMediumFontOfSize(16)
        ]
        
        setNavigationBar(withTitle: title,
                         andLeftButton: leftBtnType,
                         andRightButton: rightBtnType,
                         withBg: bg)
    }
    
    private func setNavigationBar(withTitle title: String?,
                          andLeftButton leftBtnType: NavBtnType = .none,
                          andRightButton rightBtnType: NavBtnType = .none,
                          withBg bg: NavBG) {
        
        self.title = title
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.isNavigationBarHidden = false
        
        //Set Navigation bar background color
        switch bg {
        case let .image(image: image):
            navigationController?.navigationBar.setBackgroundImage(image, for: .default)
            
        case let .bgColor(value: color):
            navigationController?.navigationBar.shadowImage = UIImage()
            navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
            navigationController?.navigationBar.tintColor = UIColor.clear
            navigationController?.navigationBar.barTintColor = color
            navigationController?.view.backgroundColor = UIColor.clear
            navigationController?.view.tintColor = UIColor.clear
            navigationController?.navigationBar.isTranslucent = (color == UIColor.clear)

        case let .imageName(value: imageName):
            navigationController?.navigationBar.setBackgroundImage(UIImage(named: imageName), for: .default)
            
        case .defaultColor:
            navigationController?.navigationBar.isTranslucent = false
            navigationController?.navigationBar.shadowImage = UIImage()
            navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
            navigationController?.navigationBar.tintColor = UIColor.clear
            navigationController?.navigationBar.barTintColor = DEFAULT_NAVIGATION_COLOR
            navigationController?.view.backgroundColor = UIColor.clear
            navigationController?.view.tintColor = UIColor.clear
            
        case .defaultImage:
            navigationController?.navigationBar.setBackgroundImage(UIImage(named: ""), for: .default)
        }

        let navBarLeftButtons: [UIBarButtonItem]? = getNavBtn(btnType: leftBtnType)
        let navBarRightButtons: [UIBarButtonItem]? = getNavBtn(btnType: rightBtnType)

        navigationItem.leftBarButtonItems = navBarLeftButtons
        navigationItem.rightBarButtonItems = navBarRightButtons
    }
    
    func getNavBtn(btnType: NavBtnType) -> [UIBarButtonItem]? {
        
        var navBarButtons: [UIBarButtonItem]?
        
        let backButton: UIBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "backIcon"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(backAction(sender:)))
        backButton.tintColor = UIColor.white
        backButton.imageInsets = UIEdgeInsets.init(top: backButton.imageInsets.top, left: backButton.imageInsets.left - 5, bottom: backButton.imageInsets.bottom, right: backButton.imageInsets.right)
        
        let closeButton: UIBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "closeWhiteNav"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(closeAction(sender:)))
        closeButton.tintColor = UIColor.white
        closeButton.imageInsets = UIEdgeInsets.init(top: closeButton.imageInsets.top, left: closeButton.imageInsets.left - 5, bottom: closeButton.imageInsets.bottom, right: closeButton.imageInsets.right)
        
        let menuButton: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "menuIcon"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(menuAction(sender:)))
        menuButton.tintColor = UIColor.white
        menuButton.imageInsets = UIEdgeInsets.init(top: menuButton.imageInsets.top, left: menuButton.imageInsets.left - 5, bottom: menuButton.imageInsets.bottom, right: menuButton.imageInsets.right)
        
        let imageButton: UIBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "menuIcon"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(imageBtnAction(sender:)))
        imageButton.tintColor = UIColor.white
        imageButton.imageInsets = UIEdgeInsets.init(top: imageButton.imageInsets.top, left: imageButton.imageInsets.left - 5, bottom: imageButton.imageInsets.bottom, right: imageButton.imageInsets.right)
        
        let titleButton: UIBarButtonItem = UIBarButtonItem(title: nil, style: .done, target: self, action:  #selector(titleBtnAction(sender:)))
        titleButton.tintColor = UIColor.white
//        titleButton.imageInsets = UIEdgeInsetsMake(imageButton.imageInsets.top, imageButton.imageInsets.left - 5, imageButton.imageInsets.bottom, imageButton.imageInsets.right)
        
        switch btnType {
        case .back:
            navBarButtons = [backButton]
            
        case .close:
            navBarButtons = [closeButton]
            
        case .menu:
            navBarButtons = [menuButton]
            
        case .both:
            navBarButtons = [backButton, menuButton]
         
        case .image(let image):
            imageButton.image = image
            navBarButtons = [imageButton]
            
        case .title(let title):
            titleButton.title = title
            navBarButtons = [titleButton]
            
        default:
            navBarButtons?.removeAll()
        }
        
        return navBarButtons
    }
    
    @objc func backAction(sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func closeAction(sender: UIButton) {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    @objc func menuAction(sender: UIButton) {
        sideMenuController?.revealMenu()
    }
    
    @objc func imageBtnAction(sender: UIButton) {
        //Defination will be in child class
    }
    
    @objc func titleBtnAction(sender: UIButton) {
        //Defination will be in child class
    }
    
    //UIViewController Presented or pushed
    func isModal() -> Bool {
        if((self.presentingViewController) != nil) {
            return true
        }
        
        if(self.presentingViewController?.presentedViewController == self) {
            return true
        }
        if(self.navigationController?.presentingViewController?.presentedViewController == self.navigationController) {
            return true
        }
        if self.tabBarController?.presentingViewController is UITabBarController {
            return true
        }
        return false
    }
    
    //SHOW ALERT
    func showAlert(_ title: String = "", message: String?, style: UIAlertController.Style = .alert, withOk showOk: Bool = true, withCancel showCancel: Bool = true, withAnimation animated: Bool = true, withCustomAction alertActions: [UIAlertAction] = [UIAlertAction]()) -> Void {
        
        guard let message = message else {
            return
        }
        
        let alert = UIAlertController(title:title, message: message, preferredStyle: style)
        for alertAction in alertActions {
            alert.addAction(alertAction)
        }
        
        if style == .alert {
            
            if showOk {
                alert.addAction(UIAlertAction(title: ALERT_OK_BUTTON_TITLE, style: .cancel, handler: nil))
            }
        }
        else {
            if showCancel {
                alert.addAction(UIAlertAction(title: ALERT_CANCEL_BUTTON_TITLE, style: .cancel, handler: nil))
            }
        }
        
        self.present(alert, animated: animated, completion: nil);
    }
    
    //Show Loader
    func showLoader(message: String = "") {
        LoaderView.shared.showToView(self.view, message: message)
    }
    
    func hideLoader() {
        LoaderView.shared.hide()
    }
}
