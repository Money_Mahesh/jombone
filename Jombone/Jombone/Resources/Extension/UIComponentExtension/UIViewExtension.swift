//
//  UIViewExtension.swift
//  Jombone
//
//  Created by Money Mahesh on 13/08/18.
//  Copyright © 2018 Money Mahesh. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    @IBInspectable var BorderWidth: CGFloat {
        set {
            self.layer.borderWidth = newValue
            self.clipsToBounds = true
        }
        get {
            return self.layer.borderWidth
        }
    }
    
    @IBInspectable var BorderColor: UIColor {
        set {
            self.layer.borderColor = newValue.cgColor
        }
        get {
            if let borderColor = layer.borderColor {
                return UIColor(cgColor: borderColor)
            }
            return UIColor.clear
        }
    }
    
    @IBInspectable var Corner2xRadius: CGFloat {
        set {
            if newValue == -1 {
                self.layer.cornerRadius = self.frame.width / 2
                self.clipsToBounds = true
            }
            else {
                if !Utility.DeviceType.IS_IPHONE_6P {
                    self.layer.cornerRadius = newValue
                    self.clipsToBounds = true
                }
            }
        }
        get {
            return self.layer.cornerRadius
        }
    }
    
    @IBInspectable var Corner3xRadius: CGFloat {
        set {
            if newValue == -1 {
                self.layer.cornerRadius = self.frame.width / 2
                self.clipsToBounds = true
            }
            else {
                if Utility.DeviceType.IS_IPHONE_6P {
                    self.layer.cornerRadius = newValue
                    self.clipsToBounds = true
                }
            }
        }
        get {
            return self.layer.cornerRadius
        }
    }
    
    convenience init(frame: CGRect, backgroundColor: UIColor) {
        self.init(frame: frame)
        self.backgroundColor = backgroundColor
    }
    
    func addDashedBorder(color:UIColor, lineWidth: CGFloat = 1, viewFrame: CGRect? = nil) {
        let colorCg = color.cgColor

        let shapeLayer:CAShapeLayer = CAShapeLayer()
        shapeLayer.name = "DottedLine"
        let frameSize = viewFrame?.size ?? self.frame.size
        let shapeRect = viewFrame ?? CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height)

        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = colorCg
        shapeLayer.lineWidth = lineWidth
        shapeLayer.lineJoin = CAShapeLayerLineJoin.miter
        shapeLayer.lineDashPattern = [2,2]
        shapeLayer.path = UIBezierPath(rect: (viewFrame ?? self.bounds)).cgPath

        if let dottedLineLayer = self.layer.sublayers?.last, dottedLineLayer.name == "DottedLine" {
            dottedLineLayer.removeFromSuperlayer()
        }
        self.layer.addSublayer(shapeLayer)
    }
}
