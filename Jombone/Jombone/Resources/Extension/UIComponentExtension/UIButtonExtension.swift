//
//  UIButtonExtension.swift
//  Jombone
//
//  Created by Money Mahesh on 13/08/18.
//  Copyright © 2018 Money Mahesh. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
    
    @IBInspectable var Font3x: CGFloat {
        set {
            if Utility.DeviceType.IS_IPHONE_6P {
                if let currentTitle = self.titleLabel {
                    self.titleLabel!.font = currentTitle.font.withSize(newValue)
                }
            }
        }
        get {
            return self.titleLabel!.font.pointSize
        }
    }
    
    func setTitleForAllState(title: String?) {
        
        self.setTitle(title, for: UIControl.State.highlighted)
        self.setTitle(title, for: UIControl.State.selected)
        self.setTitle(title, for: UIControl.State.normal)
        
    }
    
    func setTitleColorForAllState(color: UIColor) {
        
        self.setTitleColor(color, for: UIControl.State.highlighted)
        self.setTitleColor(color, for: UIControl.State.selected)
        self.setTitleColor(color, for: UIControl.State.normal)
        
    }
    
    func setImageForAllState(image: UIImage?) {
        
        self.setImage(image, for: UIControl.State.highlighted)
        self.setImage(image, for: UIControl.State.selected)
        self.setImage(image, for: UIControl.State.normal)
        
    }
    
    func setAttributedTitleForAllState(title: NSAttributedString?) {
        self.setAttributedTitle(title, for: UIControl.State.highlighted)
        self.setAttributedTitle(title, for: UIControl.State.selected)
        self.setAttributedTitle(title, for: UIControl.State.normal)
    }
    
    func setButtonImage(img: UIImage){
        self.setImage(img, for: UIControl.State.highlighted)
        self.setImage(img, for: UIControl.State.selected)
        self.setImage(img, for: UIControl.State.normal)
    }
    
    func setButtonImage(imgName: String){
        self.setImage(UIImage(named: imgName), for: UIControl.State.highlighted)
        self.setImage(UIImage(named: imgName), for: UIControl.State.selected)
        self.setImage(UIImage(named: imgName), for: UIControl.State.normal)
    }
    
    func setButtonBackgroundImageForAllState(img: UIImage){
        self.setBackgroundImage(img, for: UIControl.State.highlighted)
        self.setBackgroundImage(img, for: UIControl.State.selected)
        self.setBackgroundImage(img, for: UIControl.State.normal)
    }
    
    func setButtonBackgroundImageForAllState(imgName: String){
        self.setBackgroundImage(UIImage(named: imgName), for: UIControl.State.highlighted)
        self.setBackgroundImage(UIImage(named: imgName), for: UIControl.State.selected)
        self.setBackgroundImage(UIImage(named: imgName), for: UIControl.State.normal)
    }
    
    func setMultilineTitleForAllState(titles: [String]) {
        self.titleLabel?.lineBreakMode = .byWordWrapping
        self.titleLabel?.textAlignment = .center
        
        var titleString = ""
        for title in titles {
            titleString = titleString + title + "\n"
        }
        
        self.setTitle(titleString, for: UIControl.State.highlighted)
        self.setTitle(titleString, for: UIControl.State.selected)
        self.setTitle(titleString, for: UIControl.State.normal)
        
    }
}
