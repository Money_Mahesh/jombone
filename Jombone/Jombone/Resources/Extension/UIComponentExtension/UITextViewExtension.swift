//
//  UITextFieldExtension.swift
//  Jombone
//
//  Created by Money Mahesh on 13/08/18.
//  Copyright © 2018 Money Mahesh. All rights reserved.
//

import Foundation
import UIKit

extension UITextView {
    
    @IBInspectable var ClearRunTime: Bool {
        set {
            if newValue == true {
                self.text = ""
            }
        }
        get {
            return self.text == "" ? true : false
        }
    }
    
    var contentHeight: CGFloat! {
        var tempHeight: CGFloat = 0.0
        
        isScrollEnabled = true
        tempHeight = contentSize.height
        isScrollEnabled = false
        
        return tempHeight
    }
    
    func formatLink(link: String, linkFont: UIFont, linkColor: UIColor = UIColor(red: 215/255, green: 215/255, blue: 215/255, alpha: 1.0)) {
        
        let _text = text ?? ""
        
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 3.5
        style.lineBreakMode = .byWordWrapping
        
        let linkRange = (_text as NSString).range(of: link)
        let attributedString = NSMutableAttributedString()
        attributedString.append(NSAttributedString(
            string: _text,
            attributes: [
                NSAttributedString.Key.font: font!,
                NSAttributedString.Key.foregroundColor: textColor ?? UIColor.black,
                NSAttributedString.Key.paragraphStyle : style]
        ))
        
        attributedString.addAttributes(
            [
                NSAttributedString.Key.font: linkFont,
                NSAttributedString.Key.foregroundColor: linkColor,
                NSAttributedString.Key.paragraphStyle : style
            ],
            range: linkRange)
        
        linkTextAttributes = convertToOptionalNSAttributedStringKeyDictionary([NSAttributedString.Key.foregroundColor.rawValue: linkColor])
        self.attributedText = attributedString
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
