//
//  UILabelExtension.swift
//  Jombone
//
//  Created by Money Mahesh on 13/08/18.
//  Copyright © 2018 Money Mahesh. All rights reserved.
//

import UIKit

extension UILabel {
    
    @IBInspectable var Font3x: CGFloat {
        set {
            if Utility.DeviceType.IS_IPHONE_6P {
                if let currentFont = self.font {
                    self.font = currentFont.withSize(newValue)
                }
            }
        }
        get {
            return self.font!.pointSize
        }
    }
    
    @IBInspectable var ClearRunTime: Bool {
        set {
            if newValue == true {
                self.text = ""
            }
        }
        get {
            return self.text == "" ? true : false
        }
    }
}
