//
//  UIApplicationExtension.swift
//  Jombone
//
//  Created by Money Mahesh on 13/08/18.
//  Copyright © 2018 Money Mahesh. All rights reserved.
//

import Foundation
import UIKit

extension UIApplication {
    func visibleViewController() -> UIViewController? {
        return self.keyWindow?.rootViewController?.visibleViewController()
    }
}
