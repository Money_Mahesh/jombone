//
//  UIImageView.swift
//  Jombone
//
//  Created by Money Mahesh on 13/08/18.
//  Copyright © 2018 Money Mahesh. All rights reserved.
//

import Foundation
import UIKit
import AlamofireImage
import Alamofire

extension UIImageView {
    
    @IBInspectable var ClearRunTime: Bool {
        set {
            if newValue == true {
                self.image = nil
            }
        }
        get {
            return (self.image == nil)
        }
    }
    
    func setImageWithURLAlamofire<T: URLConvertible>(_ url: T?,
                                  placeholderImageName: String? = nil,
                                  success:((URLRequest, UIImage) -> Void)? = nil,
                                  failure:((URLRequest?, Error?) -> Void)? = nil
        ) {
        
        var placeholderImage: UIImage?
        if placeholderImageName != nil {
            placeholderImage = UIImage(named: placeholderImageName!)
        }
        
        guard let imageURL = try? url?.asURL(), imageURL != nil else {
            
            image = placeholderImage
            if placeholderImageName != nil {
                contentMode = UIView.ContentMode.scaleAspectFill
            }
            failure?(nil, nil)
            return
        }
        
        self.af_setImage(withURL: imageURL!, placeholderImage: placeholderImage) { response in
            self.image = response.result.value
            
            if let imageRecieved = response.result.value {
                self.contentMode = UIView.ContentMode.scaleToFill
                success?(response.request!, imageRecieved)
            }else if response.result.isFailure {
                failure?(response.request, response.result.error!)
            }
        }
    }
    
    func setImageWithURLAlamofire<T: URLRequestConvertible>(_ urlRequest: T?,
                                            placeholderImageName: String? = nil,
                                            success:((URLRequest, UIImage) -> Void)? = nil,
                                            failure:((URLRequest?, Error?) -> Void)? = nil
                                        ) {
        
        var placeholderImage: UIImage?
        if placeholderImageName != nil {
            placeholderImage = UIImage(named: placeholderImageName!)
        }
        
        guard let imageURLRequest = try? urlRequest?.asURLRequest(), imageURLRequest != nil else {
            
            image = placeholderImage
            if placeholderImageName != nil {
                contentMode = UIView.ContentMode.scaleAspectFill
            }
            failure?(nil, nil)
            return
        }
        
        self.af_setImage(withURLRequest: imageURLRequest!, placeholderImage: placeholderImage) { response in
            self.image = response.result.value
            
            if let imageRecieved = response.result.value {
                self.contentMode = UIView.ContentMode.scaleToFill
                success?(response.request!, imageRecieved)
            }else if response.result.isFailure {
                failure?(response.request, response.result.error!)
            }
        }
    }
}
