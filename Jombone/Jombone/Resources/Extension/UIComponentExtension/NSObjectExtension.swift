//
//  NSObjectExtension.swift
//  Jombone
//
//  Created by Money Mahesh on 13/08/18.
//  Copyright © 2018 Money Mahesh. All rights reserved.
//

import Foundation

extension NSObject {
    
    func addObserver(_ observer: Any, selector: Selector, notificationsName: [Notification.Name]) {
        for notificationName in notificationsName {
            NotificationCenter.default.addObserver(observer, selector: selector, name: notificationName, object: nil)
        }
    }
    
    func removeObserver(_ observer: Any, notificationsName: [Notification.Name]) {
        for notificationName in notificationsName {
            NotificationCenter.default.removeObserver(observer, name: notificationName, object: nil)
        }
    }
}

