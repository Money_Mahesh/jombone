//
//  RubikFont.swift
//

import Foundation
import UIKit

extension UIFont {
    
    class func rubikRegularFontOfSize(_ size: CGFloat) -> UIFont! {
        return UIFont(name: "Rubik-Regular", size: size)
    }
    
    class func rubikBoldFontOfSize(_ size: CGFloat) -> UIFont! {
        return UIFont(name: "Rubik-Bold", size: size)
    }
    
    class func rubikBoldItalicFontOfSize(_ size: CGFloat) -> UIFont! {
        return UIFont(name: "Rubik-BoldItalic", size: size)
    }
    
    class func rubikItalicFontOfSize(_ size: CGFloat) -> UIFont! {
        return UIFont(name: "Rubik-Italic", size: size)
    }
    
    class func rubikMediumItalicFontOfSize(_ size: CGFloat) -> UIFont! {
        return UIFont(name: "Rubik-MediumItalic", size: size)
    }
    
    class func rubikBlackFontOfSize(_ size: CGFloat) -> UIFont! {
        return UIFont(name: "Rubik-Black", size: size)
    }
    
    class func rubikBlackItalicFontOfSize(_ size: CGFloat) -> UIFont! {
        return UIFont(name: "Rubik-BlackItalic", size: size)
    }
    
    class func rubikMediumFontOfSize(_ size: CGFloat) -> UIFont! {
        return UIFont(name: "Rubik-Medium", size: size)
    }
    
    class func rubikLightFontOfSize(_ size: CGFloat) -> UIFont! {
        return UIFont(name: "Rubik-Light", size: size)
    }
    
    class func rubikLightItalicFontOfSize(_ size: CGFloat) -> UIFont! {
        return UIFont(name: "Rubik-LightItalic", size: size)
    }
 
}
