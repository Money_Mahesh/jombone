//
//  OfferActionExtension.swift
//  Jombone
//
//  Created by Money Mahesh on 18/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation

protocol OfferAction: Action {
    func acceptOffer(appEncId: String, completion: ((_ status: Bool)->())?)
    func rejectOffer(appEncId: String, completion: ((_ status: Bool)->())?)
}

extension OfferAction {
    
    func acceptOffer(appEncId: String, completion: ((_ status: Bool)->())?) {
        
        let offerServices = OfferServices(requestTag: "OFFER_REJECT_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        APPLICATION_INSTANCE.visibleViewController()?.showLoader()
        //Completion Block
        offerServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            APPLICATION_INSTANCE.visibleViewController()?.hideLoader()
            APPLICATION_INSTANCE.visibleViewController()?.showAlert(message: message)
            if statusCode == 1 {
                completion?(true)
            }
            else {
                completion?(false)
            }
        }
        offerServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            APPLICATION_INSTANCE.visibleViewController()?.hideLoader()
            completion?(false)
        }
        
        offerServices.acceptOffer(parameters: ["appEncId": appEncId], additionalHeaderElements: nil)
    }
    
    func rejectOffer(appEncId: String, completion: ((_ status: Bool)->())?) {
        
        let offerServices = OfferServices(requestTag: "OFFER_ACCEPT_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        APPLICATION_INSTANCE.visibleViewController()?.showLoader()
        //Completion Block
        offerServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            APPLICATION_INSTANCE.visibleViewController()?.hideLoader()
            APPLICATION_INSTANCE.visibleViewController()?.showAlert(message: message)
            if statusCode == 1 {
                completion?(true)
            }
            else {
                completion?(false)
            }
        }
        offerServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            APPLICATION_INSTANCE.visibleViewController()?.hideLoader()
            completion?(false)
        }
        
        offerServices.rejectOffer(parameters: ["appEncId": appEncId], additionalHeaderElements: nil)
    }
}
