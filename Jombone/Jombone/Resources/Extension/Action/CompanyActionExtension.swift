//
//  CompanyActionExtension.swift
//  Jombone
//
//  Created by Money Mahesh on 10/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation

protocol CompanyAction: Action {
    func follow_UnFollowCompany(isFollowed: Bool, companyEncId: String, completion: ((_ status: Bool)->())?)
    func companyDetail(empEncId: String?, completion: ((_ obj: CompanyModel)->())?)
}

extension CompanyAction {
    
    func companyDetail(empEncId: String?, completion: ((_ obj: CompanyModel)->())?) {

        guard let _empEncId = empEncId else {
            return
        }
        
        let companyServices = CompanyServices(requestTag: "JOB_DETAIL_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        APPLICATION_INSTANCE.visibleViewController()?.showLoader()
        //Completion Block
        companyServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            APPLICATION_INSTANCE.visibleViewController()?.hideLoader()
            APPLICATION_INSTANCE.visibleViewController()?.showAlert(message: message)
            if let companyDetail = (response.0 as? [CompanyModel])?.first, statusCode == 1 {
                completion?(companyDetail)
            }
            
        }
        companyServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            APPLICATION_INSTANCE.visibleViewController()?.hideLoader()
        }
        
        companyServices.getCompanyDetail(parameters: ["empEncId": _empEncId], additionalHeaderElements: nil)
    }
    
    func follow_UnFollowCompany(isFollowed: Bool, companyEncId: String, completion: ((_ status: Bool)->())?) {

        let companyServices = CompanyServices(requestTag: "COMPANY_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        APPLICATION_INSTANCE.visibleViewController()?.showLoader()
        //Completion Block
        companyServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            APPLICATION_INSTANCE.visibleViewController()?.hideLoader()
            //APPLICATION_INSTANCE.visibleViewController()?.showAlert(message: message)
            
            if statusCode == 1 {
                completion?(true)
            }
            else {
                completion?(false)
            }
        }
        companyServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            APPLICATION_INSTANCE.visibleViewController()?.hideLoader()
            completion?(false)
        }
        
        companyServices.followUnFollowdCompany(isFollow: isFollowed, parameters: ["employerEncId": companyEncId], additionalHeaderElements: nil)
    }
}
