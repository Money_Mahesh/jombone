//
//  Action.swift
//  Jombone
//
//  Created by Money Mahesh on 10/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import UIKit

protocol Action: NSObjectProtocol {
    func share(shareDetail: [Any])
}

extension Action {
    func share(shareDetail: [Any]) {
        
        guard let visibleViewController = APPLICATION_INSTANCE.visibleViewController() else {
            return
        }
        let activityViewController = UIActivityViewController(activityItems: shareDetail, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = visibleViewController.view
        
        // present the view controller
        visibleViewController.present(activityViewController, animated: true, completion: nil)
    }
}
