//
//  JobActionExtension.swift
//  Jombone
//
//  Created by Money Mahesh on 09/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation

protocol JobAction: Action {
    func like_DislikeJob(isLiked: Bool, jobEncId: String, completion: ((_ status: Bool)->())?)
    func applyJob(jobEncId: String, completion: ((_ status: Bool)->())?)
    func jobDetail(jobEncId: String?, completion: ((_ obj: JobModel)->())?)
}

extension JobAction {
    
    func jobDetail(jobEncId: String?, completion: ((_ obj: JobModel)->())?) {
        
        guard let _jobEncId = jobEncId else {
            return
        }
        let jobsServices = JobsServices(requestTag: "JOB_DETAIL_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        APPLICATION_INSTANCE.visibleViewController()?.showLoader()
        //Completion Block
        jobsServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            APPLICATION_INSTANCE.visibleViewController()?.hideLoader()
            APPLICATION_INSTANCE.visibleViewController()?.showAlert(message: message)
            if let jobDetail = (response.0 as? [JobModel])?.first, statusCode == 1 {
                completion?(jobDetail)
            }
        }
        jobsServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            APPLICATION_INSTANCE.visibleViewController()?.hideLoader()
        }
        
        jobsServices.getJobDetail(parameters: ["jobEncId": _jobEncId], additionalHeaderElements: nil)
    }

    func like_DislikeJob(isLiked: Bool, jobEncId: String, completion: ((_ status: Bool)->())?) {
    
        let jobsServices = JobsServices(requestTag: "JOB_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        APPLICATION_INSTANCE.visibleViewController()?.showLoader()
        //Completion Block
        jobsServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            APPLICATION_INSTANCE.visibleViewController()?.hideLoader()
            //APPLICATION_INSTANCE.visibleViewController()?.showAlert(message: message)
            if statusCode == 1 {
                completion?(true)
            }
            else {
                completion?(false)
            }
        }
        jobsServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            APPLICATION_INSTANCE.visibleViewController()?.hideLoader()
            completion?(false)
        }
        
        jobsServices.likeDislikeJob(isLiked: isLiked, parameters: ["jobEncId": jobEncId], additionalHeaderElements: nil)
    }
    
    func applyJob(jobEncId: String, completion: ((_ status: Bool)->())?) {
        
        let jobsServices = JobsServices(requestTag: "JOB_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        APPLICATION_INSTANCE.visibleViewController()?.showLoader()
        //Completion Block
        jobsServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            APPLICATION_INSTANCE.visibleViewController()?.hideLoader()
            APPLICATION_INSTANCE.visibleViewController()?.showAlert(message: message)
            if statusCode == 1 {
                completion?(true)
            }
            else {
                completion?(false)
            }
        }
        jobsServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            APPLICATION_INSTANCE.visibleViewController()?.hideLoader()
            completion?(false)
        }
        jobsServices.applyJob(parameters: ["jobEncId": jobEncId], additionalHeaderElements: nil)
    }
}
