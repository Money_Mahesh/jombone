//
//  WorkAction.swift
//  Jombone
//
//  Created by Money Mahesh on 28/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import CoreLocation

protocol WorkAction: NSObjectProtocol {
    func signIn(jobEncId: String, completion: ((_ status: Bool, _ timesheetEncId: String?)->())?)
    func signOut(timesheetEncId: String, completion: ((_ status: Bool)->())?)
}

extension WorkAction {
    
    private func updateToAddressBeanObj(_ placeDetail: GooglePlaceDetail?) -> AddressBean {
        
        var addressBean = AddressBean()
        var latLong: String?
        if let lat = placeDetail? .lat, let long = placeDetail? .long {
            latLong = String(describing: lat) + "," + String(describing: long)
        }
        addressBean.latLong = latLong
        addressBean.addressLine1 = placeDetail? .street_number
        addressBean.addressLine2 = placeDetail? .route
        addressBean.city = placeDetail? .locality
        addressBean.completeAddress = placeDetail? .dislayAddress
        addressBean.fullAddress = placeDetail? .dislayAddress
        addressBean.googlePlaceId = placeDetail? .place_id
        addressBean.state = placeDetail? .administrative_area_level_1
        addressBean.zipcode = placeDetail? .postal_code
        addressBean.country = placeDetail? .country
        return addressBean
    }
    
    private func getCurrentLocationDetail(completion: @escaping ((_ obj: AddressBean)->())) {
        
        APPLICATION_INSTANCE.visibleViewController()?.showLoader()
        LocationManager.instance.setUp(completion: { (status: Bool, locationDetail: CurrentLocationDetail?, coordinate: CLLocationCoordinate2D?) in
            
            APPLICATION_INSTANCE.visibleViewController()?.hideLoader()
            
            if status {
                if let _coordinate = coordinate {
                    
                    GooglePlaceUtility.getFetchPlaceIdFromLatLng(latitude: _coordinate.latitude, longitude: _coordinate.longitude, completion: nil, success: { (placeDetail: GooglePlaceDetail, message) in
                        completion(self.updateToAddressBeanObj(placeDetail))
                    }, failure: nil)
                }
            }
        })
    }
    
    func signIn(jobEncId: String, completion: ((_ status: Bool, _ timesheetEncId: String?)->())?) {
        
        getCurrentLocationDetail { (locationDetail: AddressBean) in
            
            guard let _locationDetailDict = locationDetail.dictionary() else {
                return
            }
            
            let workServices = WorkServices(requestTag: "SIGN_IN_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
            
            APPLICATION_INSTANCE.visibleViewController()?.showLoader()
            //Completion Block
            workServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
                
                APPLICATION_INSTANCE.visibleViewController()?.hideLoader()
                APPLICATION_INSTANCE.visibleViewController()?.showAlert(message: message)
                if let timesheetEncId = response.0 as? String, statusCode == 1 {
                    completion?(true, timesheetEncId)
                }
            }
            workServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
                APPLICATION_INSTANCE.visibleViewController()?.hideLoader()
            }
            
            let parameters: [String : Any] = [
                "jobEncId": jobEncId,
                "currentLocation": _locationDetailDict
                ]
            
            workServices.signIn(parameters: parameters, additionalHeaderElements: nil)
        }
    }

    func signOut(timesheetEncId: String, completion: ((_ status: Bool)->())?) {
        
        getCurrentLocationDetail { (locationDetail: AddressBean) in
            
            guard let _locationDetailDict = locationDetail.dictionary() else {
                return
            }
            
            let workServices = WorkServices(requestTag: "SIGN_OUT_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
            
            APPLICATION_INSTANCE.visibleViewController()?.showLoader()
            //Completion Block
            workServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
                
                APPLICATION_INSTANCE.visibleViewController()?.hideLoader()
                APPLICATION_INSTANCE.visibleViewController()?.showAlert(message: message)
                if statusCode == 1 {
                    completion?(true)
                }
            }
            workServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
                APPLICATION_INSTANCE.visibleViewController()?.hideLoader()
            }
            
            let parameters: [String : Any] = [
                "timesheetEncId": timesheetEncId,
                "currentLocation": _locationDetailDict
            ]
            
            workServices.signOut(parameters: parameters, additionalHeaderElements: nil)
        }
    }
}
