//
//  ApplicationActionExtension.swift
//  Jombone
//
//  Created by Money Mahesh on 16/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation

protocol ApplicationAction: Action {
        func withdrawApplication(appEncId: String, completion: ((_ status: Bool)->())?)
}

extension ApplicationAction {
    
    func withdrawApplication(appEncId: String, completion: ((_ status: Bool)->())?) {
        
        let applicationServices = ApplicationServices(requestTag: "APPLICATION_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        APPLICATION_INSTANCE.visibleViewController()?.showLoader()
        //Completion Block
        applicationServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            APPLICATION_INSTANCE.visibleViewController()?.hideLoader()
            APPLICATION_INSTANCE.visibleViewController()?.showAlert(message: message)
            if statusCode == 1 {
                completion?(true)
            }
            else {
                completion?(false)
            }
        }
        applicationServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            APPLICATION_INSTANCE.visibleViewController()?.hideLoader()
            completion?(false)
        }
        
        applicationServices.withdrawApplication(parameters: ["appEncId": appEncId], additionalHeaderElements: nil)
    }
}
