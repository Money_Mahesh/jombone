//
//  GlobalSearchHandler.swift
//  Jombone
//
//  Created by Money Mahesh on 20/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import Alamofire

class GlobalSearchHandler: NSObject {
    
    static var instance = GlobalSearchHandler()
    var request: DataRequest?
    var data: [GlobalSearchModel]?
    
    func search(string: String?, completion: ((_ message: String?)->())? = nil, success:  ((_ message: String?)->())? = nil, failure: ((_ message: String?)->())? = nil) {
        
        request?.cancel()
        
        let globalService = GlobalService(requestTag: "GLOBAL_SEARCH_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        //Completion Block
        globalService.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            if let globalSearchList = (response.0 as? [GlobalSearchModel]),
                statusCode == 1 {
                self.data = globalSearchList
                completion?(message)
                success?(message)
            }
            else {
                self.data?.removeAll()
                completion?(message)
                success?(message)
            }
        }
        globalService.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            self.data?.removeAll()
            completion?(nil)
            success?(nil)
        }
        request = globalService.search(string: string, additionalHeaderElements: nil)
    }
}
