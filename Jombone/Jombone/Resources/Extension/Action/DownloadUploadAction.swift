//
//  DownloadUploadAction.swift
//  Jombone
//
//  Created by Money Mahesh on 25/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import UIKit
import MobileCoreServices
import PhotosUI

struct FileDetail {
    var name: String
    var extention: String
    var data: Data
}

protocol DownloadUploadActionProtocol: NSObjectProtocol {
    func download(fileName: String?, urlStr: String?)
    func fetchFile(completion: @escaping ((_ fileDetail: FileDetail) -> ()))
    func upload<T: Codable>(_ uploadFilePath: String, keyName: String, completion: ((_ data: T?) -> ())?)
}

enum FileType: String {
    case doc = "doc"
    case docx = "docx"
    case pdf = "pdf"
    case jpeg = "jpeg"
    case png = "png"
    case jpg = "jpg"
    case tiff = "tiff"
}

class DownloadUploadAction:NSObject, DownloadUploadActionProtocol {
    
    var imagePicker = UIImagePickerController()
    var completionFetchDocument: ((_ fileDetail: FileDetail) -> ())?
    
    func fetchFile(completion: @escaping ((_ fileDetail: FileDetail) -> ())) {
        selectDocument()
        self.completionFetchDocument = completion
    }
    
    func upload<T: Codable>(_ uploadFilePath: String, keyName: String,  completion: ((_ data: T?) -> ())?) {
        
        let documentProviderMenu = UIDocumentPickerViewController(documentTypes: [
            "com.microsoft.word.doc",
            "org.openxmlformats.wordprocessingml.document",
            kUTTypePDF as String,
            kUTTypeJPEG as String,
            kUTTypeTIFF as String,
            kUTTypePNG as String
            ], in: UIDocumentPickerMode.import)
        documentProviderMenu.delegate = self
        APPLICATION_INSTANCE.visibleViewController()?.present(documentProviderMenu, animated: true, completion: nil)
        
        self.completionFetchDocument = {(_ fileDetail: FileDetail) in
            
            self.hitUploadFile(uploadFilePath, fileName: fileDetail.name, file: fileDetail.data, keyName: keyName, parameter: nil, completion: completion)
        }
    }
    
    func download(fileName: String?, urlStr: String?) {
        
        guard let _fileName = fileName, let _urlStr = urlStr else {
            return
        }
        
        APPLICATION_INSTANCE.visibleViewController()?.showLoader()
        self.hitDownloadResponse(fileName: _fileName, filePath: _urlStr, completion: { (status, message, url) in
            APPLICATION_INSTANCE.visibleViewController()?.hideLoader()
            if status {
                guard let _url = url else {
                    return
                }
                self.openFile(url: _url)
            }
        })
    }
    
    private func openFile(url: URL) {
        
        let documentProviderMenu = UIDocumentPickerViewController(url: url, in: UIDocumentPickerMode.moveToService)
        documentProviderMenu.delegate = self
        APPLICATION_INSTANCE.visibleViewController()?.present(documentProviderMenu, animated: true, completion: nil)
    }
    
    private func getImage(path: String){
        
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: path){
           
        }else{
            print("No Image")
        }
    }
    
    //MARK:- Services
    private func hitDownloadResponse(fileName: String, filePath: String, completion: ((_ status: Bool, _ message: String?, _ url: URL?)->())?) {
        
        let downloadService = DownloadUploadService(requestTag: "DOWNLOAD_FILE_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        APPLICATION_INSTANCE.visibleViewController()?.showLoader()
        //Completion Block
        downloadService.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            APPLICATION_INSTANCE.visibleViewController()?.hideLoader()
            if let url = response.0 as? URL {
                completion?(true, message, url)
            }
            else {
                APPLICATION_INSTANCE.visibleViewController()?.showAlert(message: message)
                completion?(false , message, nil)
            }
        }
        downloadService.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            APPLICATION_INSTANCE.visibleViewController()?.hideLoader()
            completion?(false, nil, nil)
        }
        
        downloadService.hitGetDownloadedFile(filePath, fileName: fileName, parameters: nil, additionalHeaderElements: nil)
    }
    
    private func hitUploadFile<T: Codable>(_ uploadFilePath: String, fileName: String, file: Data, keyName: String, parameter: [String : Any]?, completion: ((_ data: T?) -> ())?) {
        
        let downloadService = DownloadUploadService(requestTag: "DOWNLOAD_FILE_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        APPLICATION_INSTANCE.visibleViewController()?.showLoader()
        //Completion Block
        downloadService.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            APPLICATION_INSTANCE.visibleViewController()?.hideLoader()
            if let response = response.0 as? T {
                completion?(response)
            }
            else {
                APPLICATION_INSTANCE.visibleViewController()?.showAlert(message: message)
                completion?(nil)
            }
        }
        downloadService.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            APPLICATION_INSTANCE.visibleViewController()?.hideLoader()
            completion?(nil)
        }
        
        let filesDetail: (keysName: [String], filesName: [String], filesData: [Data]) =
            (keysName: [keyName], filesName: [fileName], filesData: [file])
        downloadService.hitUploadFile(uploadFilePath, filesDetail: filesDetail, parameters: parameter, additionalHeaderElements: nil, completion: { (data: T) in
        })
    }
}

extension DownloadUploadAction: UIDocumentPickerDelegate {
    
    //MARK:- UIDocumentPickerDelegate
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        
        if controller.documentPickerMode == UIDocumentPickerMode.moveToService {
            // This is what it should be
            APPLICATION_INSTANCE.visibleViewController()?.showAlert(message: "Document downloaded successfully", withOk: true)
        }
        else  if controller.documentPickerMode == UIDocumentPickerMode.import {
            // This is what it should be
        }
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        
        if controller.documentPickerMode == UIDocumentPickerMode.moveToService {
            // This is what it should be
            APPLICATION_INSTANCE.visibleViewController()?.showAlert(message: "Document downloaded successfully", withOk: true)
        }
        else  if controller.documentPickerMode == UIDocumentPickerMode.import {
            // This is what it should be
            if let url = urls.first {
                
                let fileName = url.lastPathComponent
                let fileExt = url.pathExtension
                
                do {
                    if let contentFromFile = try? Data(contentsOf: url) {
                        
                        self.completionFetchDocument?(FileDetail(name: fileName, extention: fileExt, data: contentFromFile))
                    }
                }
                catch let error as NSError {
                    print("An error took place: \(error)")
                }

            }
        }
    }
}

extension DownloadUploadAction: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func selectDocument() {
        
        imagePicker.delegate = self
        
        APPLICATION_INSTANCE.visibleViewController()?.showAlert(message: "Select Document", style: UIAlertController.Style.actionSheet, withCancel: true, withCustomAction: [
            UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default, handler: { (action) in self.openImageGallery() }),
            UIAlertAction(title: "File Manager", style: UIAlertAction.Style.default, handler: { (action) in self.openFileManger() })
            ]
        )
    }
    
    private func openImageGallery() {
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = true
        
        let photoAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        switch photoAuthorizationStatus {
        case .authorized:
            APPLICATION_INSTANCE.visibleViewController()?.present(imagePicker, animated: true, completion: nil)
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization({[weak imagePicker]
                (newStatus) in
                DispatchQueue.main.async {
                    if newStatus ==  PHAuthorizationStatus.authorized {
                        if let _imagePicker = imagePicker {
                            APPLICATION_INSTANCE.visibleViewController()?.present(_imagePicker, animated: true, completion: nil)
                        }
                    }else{
                        print("User denied")
                    }
                }})
            break
        case .restricted:
            APPLICATION_INSTANCE.visibleViewController()?.showAlert(message: "You do not have permissions enabled for this. Please enable it from phone setting.")
            
            print("restricted")
            break
        case .denied:
            APPLICATION_INSTANCE.visibleViewController()?.showAlert(message: "You do not have permissions enabled for this. Please enable it from phone setting.")
            print("denied")
            break
        }
    }
    
    private func openFileManger() {
        
        let documentProviderMenu = UIDocumentPickerViewController(documentTypes: [
            "com.microsoft.word.doc",
            "org.openxmlformats.wordprocessingml.document",
            kUTTypePDF as String,
            kUTTypeJPEG as String,
            kUTTypeTIFF as String,
            kUTTypePNG as String
            ], in: UIDocumentPickerMode.import)
        documentProviderMenu.delegate = self
        APPLICATION_INSTANCE.visibleViewController()?.present(documentProviderMenu, animated: true, completion: nil)
    }
    
    //MARK:- Image Picker Delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        self.imagePicker.dismiss(animated: true, completion: {
            if let pickerImage = (info[UIImagePickerController.InfoKey.editedImage] as? UIImage), let imgData = pickerImage.jpegData(compressionQuality: 1.0) {
                
                self.completionFetchDocument?(FileDetail(name: "ImageFile.jpg", extention: "jpg", data: imgData))
            }
        })
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.imagePicker.dismiss(animated: true, completion: nil)
    }
}
