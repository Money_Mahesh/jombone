//
//  CustomView.swift
//  Jombone
//
//  Created by Money Mahesh on 13/08/18.
//  Copyright © 2018 tbsl. All rights reserved.
//

import UIKit

class CustomView: UIView {

    var view: UIView!

    convenience init(frame: CGRect, data: Any) {
        
        self.init(frame: frame)
        self.xibLoaded(data: data)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        // Setup view from .xib file
        xibSetUp()
        xibLoaded()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        // Setup view from .xib file
        xibSetUp()
        xibLoaded()
    }
    
    func xibLoaded(data: Any) {
        //To assign properties immediate after xib is loaded
    }
    
    func xibLoaded() {
        //To assign properties immediate after xib is loaded
    }
    
    func xibSetUp() {
        view = loadNib()
        // use bounds not frame or it'll be offset
        view.frame = bounds
        // Adding custom subview on top of our view
        addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[childView]|",
                                                      options: [],
                                                      metrics: nil,
                                                      views: ["childView": view]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[childView]|",
                                                      options: [],
                                                      metrics: nil,
                                                      views: ["childView": view]))
    }
    
    /** Loads instance from nib with the same name. */
    func loadNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nibName = type(of: self).description().components(separatedBy: ".").last!
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as! UIView
    }

    //Remove all the observer added
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
