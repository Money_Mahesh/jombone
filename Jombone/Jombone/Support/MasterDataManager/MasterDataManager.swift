//
//  MasterDataManager.swift
//  Jombone
//
//  Created by Money Mahesh on 26/04/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation

class MasterDataManager: NSObject {
    
    enum MasterDataEnum: String {
        case education = "/master/reference/highestEducation"
        case govtId = "/master/reference/IDType2"
        case photoGovtId = "/master/reference/IDType1"
        case salaryTransferTypes = "/master/reference/salaryTransferTypes"
    }

    static var shared = MasterDataManager()
    
    private var _education: [MasterData]?
    func education(completion: @escaping ((_ data: [MasterData]?)->())) {
        if let data = _education {
            completion(data)
            return
        }
        
        hitService(type: MasterDataManager.MasterDataEnum.education) { (data) in
            self._education = data
            completion(data)
        }
    }
    
    private var _govtIds: [MasterData]?
    func govtIds(completion: @escaping ((_ data: [MasterData]?)->())) {
        if let data = _govtIds {
            completion(data)
            return
        }
        
        hitService(type: MasterDataManager.MasterDataEnum.govtId) { (data) in
            self._govtIds = data
            completion(data)
        }
    }
    
    private var _photoGovtIds: [MasterData]?
    func photoGovtIds(completion: @escaping ((_ data: [MasterData]?)->())) {
        if let data = _photoGovtIds {
            completion(data)
            return
        }
        
        hitService(type: MasterDataManager.MasterDataEnum.photoGovtId) { (data) in
            self._photoGovtIds = data
            completion(data)
        }
    }
    
    private var _salaryTransferTypes: [MasterData]?
    func salaryTransferTypes(completion: @escaping ((_ data: [MasterData]?)->())) {
        if let data = _salaryTransferTypes {
            completion(data)
            return
        }
        
        hitService(type: MasterDataManager.MasterDataEnum.salaryTransferTypes) { (data) in
            self._salaryTransferTypes = data
            completion(data)
        }
    }
    
    
    func hitService(type: MasterDataEnum, completion: @escaping ((_ data: [MasterData]?)->())) {
        
        let masterDataServices = MasterDataServices(requestTag: "MASTER_DATA_SERVICE", requestPriority:REQUEST_PRIORITY.moderate_PRIORITY)
        
        //Completion Block
        masterDataServices.completionBlockWithSuccess = { (url : String?, statusCode: Int, requestType: String?, response: (Any?, Any?), message: String?) in
            
            completion(response.0 as? [MasterData])
        }
        masterDataServices.completionBlockWithFailure = { (nil, requestType: String?, error: Error?) in
            completion(nil)
        }
        
        masterDataServices.getListFor(type.rawValue, parameters: nil, additionalHeaderElements: nil)
    }
}
