//
//	ChangePasswordReponse.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct ChangePasswordReponse : Codable {
    //Change data types for data and errors
	let data : String?
	let errors : String?
	let message : String?
	let status : Int?
}
