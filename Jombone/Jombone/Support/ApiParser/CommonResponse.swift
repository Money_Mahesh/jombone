//
//	CommonResponse.swift
//
//	Create by Money Mahesh on 30/7/2018
//	Copyright © 2018. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct CommonResponse<T: Codable>: Codable {

	var status : Int!
	var message : String?
	var errors : String?
    var data : T?
    var pagination: Pagination?
    
    enum CommonResponseCodingKeys: String, CodingKey {
        case status = "status"
        case message = "message"
        case errors = "errors"
        case data = "data"
        case pagination = "pagination"
    }
    
    init(status: Int, message: String?, errors: String?, data: T?, pagination: Pagination?) {
        self.status = status
        self.message = message
        self.errors = errors
        self.data = data
        self.pagination = pagination
    }
    
    init(from decoder: Decoder) throws {
        do {
            let container = try decoder.container(keyedBy: CommonResponseCodingKeys.self)
            status = (try? container.decode(Int.self, forKey: CommonResponseCodingKeys.status)) ?? 0
            message = try? container.decode(String.self, forKey: CommonResponseCodingKeys.message)
            errors = try? container.decode(String.self, forKey: CommonResponseCodingKeys.errors)
            data = try? container.decode(T.self, forKey: CommonResponseCodingKeys.data)
            pagination = try? container.decode(Pagination.self, forKey: CommonResponseCodingKeys.pagination)
        }
    }
}

struct Pagination: Codable {
    var currentPage : Int?
    var responseMessage : String?
    var result : Int?
    var totalElement : Int?
    var totalPages : Int?
}
