//
//  DocumentDetailsBean.swift
//  Jombone
//
//  Created by Money Mahesh on 07/04/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation

struct DocumentDetailsBean: Codable, IDCardProtocol {
    
    var comment : String?
    var createdDate : Int?
    var documentDomain : String?
    var documentEncId : String?
    var documentFileEncId : String?
    var documentPath : String?
    var encGovtIdDocumentType : String?
    var encPhotoGovtIdDocumentType : String?
    var govtIdDocument : String?
    var govtIdDocumentTypeName : String?
    var govtIdExpiry : Bool?
    var govtIdExpiryDate : String?
    var govtIdNumber : String?
    var isVerified : Bool?
    var photoGovtIdDocument : String?
    var photoGovtIdExpiry : Bool?
    var photoGovtIdExpiryDate : String?
    var photoGovtIdNumber : String?
    var showToCandidate : Bool?
    var viewDocumentExpiryDate : String?
    var viewDocumentFileName : String?
    var viewDocumentIdNumber : String?
    var viewDocumentTypeName : String?
    var viewDocumentTypeDisplayName : String?
    
    init() {}
}
