//
//  EmploymentDetailsBean.swift
//  Jombone
//
//  Created by Money Mahesh on 07/04/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation

struct EmploymentDetailsBean: Codable {
    
    var designation : String?
    var employerAddress : String?
    var employerName : String?
    var employmentDocPath : String?
    var employmentFileName : String?
    var employmentIdEnc : String?
    var fromDate : String?
    var fromDateObj : Int64?
    var isCurrent : Bool?
    var isVerified : Bool?
    var job : Int64?
    var referenceCountryCode : String?
    var referenceCountryIsoCode : String?
    var referenceEmail : String?
    var referenceMobile : String?
    var referenceMobileFormatted : String?
    var referenceName : String?
    var toDate : String?
    var toDateObj : Int64?
    var unitNumber : String?
    var isExpanded : Bool? = false

    init() {}
}

