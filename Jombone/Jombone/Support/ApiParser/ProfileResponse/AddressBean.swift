//
//	AddressBean.swift
//
//	Create by Money Mahesh on 15/11/2018
//	Copyright © 2018. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct AddressBean: Codable {

    var addressLine1 : String?
    var addressLine2 : String?
    var city : String?
    var completeAddress : String?
    var country : String?
    var fullAddress : String?
    var googlePlaceId : String?
    var latLong : String?
    var locationIdEnc : String?
    var state : String?
    var unit : String?
    var zipcode : String?
    
    init() {}
    
    init(lat: Double, long: Double, fullAddress: String) {
        
        self.latLong = String(lat) + "," + String(long)
        self.fullAddress = fullAddress
    }
    
    func jsonString() -> String? {
        
        let jsonEncoder = JSONEncoder()
        do {
            let jsonData = try jsonEncoder.encode(self)
            return String(data: jsonData, encoding: .utf8)
        }
        catch {
            print("error")
        }
        
        return nil
    }
    
    func dictionary() -> [String: Any]? {
        
        if let data = jsonString()?.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        
        return nil
    }
}
