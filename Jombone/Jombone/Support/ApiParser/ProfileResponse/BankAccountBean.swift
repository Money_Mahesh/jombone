//
//	BankAccountBean.swift
//
//	Create by Money Mahesh on 15/11/2018
//	Copyright © 2018. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct BankAccountBean: Codable {

    var accountDetailsDocument : String?
    var accountDetailsDocumentPath : String?
    var accountDocFileName : String?
    var accountDocFsEncId : String?
    var accountHolderName : String?
    var accountNumber : String?
    var bankAccountConfirmEmail : String?
    var bankAccountEmail : String?
    var branch : String?
    var encTransferType : String?
    var institution : String?
    var sinNumber : String?
    var maskedSinNumber : String?
    var transferType : Int?
    var transferTypeName : String?
    var accountDocFileDisplayName: String?
    var detail: [[String: String]]? {
        get {
            var data = [[String: String]]()
            
            if let _transferTypeName = transferTypeName, _transferTypeName.count > 0 {
                data.append(["Deposit salary by": _transferTypeName])
            }
            
            if let _bankAccountEmail = bankAccountEmail, _bankAccountEmail.count > 0 {
                data.append(["Account Email": _bankAccountEmail])
            }
            
            if let _branch = branch, _branch.count > 0 {
                data.append(["Branch/Transit Number": _branch])
            }
            
            if let _institution = institution, _institution.count > 0 {
                data.append(["Institution Number": _institution])
            }
            
            if let _accountNumber = accountNumber, _accountNumber.count > 0 {
                data.append(["Account Number": _accountNumber])
            }
            
            if let _maskedSinNumber = maskedSinNumber, _maskedSinNumber.count > 0 {
                data.append(["SIN Number ": _maskedSinNumber])
            }
            
            return data.count == 0 ? nil : data
        }
    }
    
    init() {}
}
