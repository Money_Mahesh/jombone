//
//	PhysicalAnalysisDocumentBean.swift
//
//	Create by Money Mahesh on 15/11/2018
//	Copyright © 2018. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct PhysicalAnalysisDocumentBean: Codable {

	var documentEncId : String?
	var documentFileEncId : String?
	var downloadPhysicalFormId : String?
	var physicalAnalysisDocument : String?
	var viewDocumentFileName : String?
    var isVerified : Bool?
    var documentFilePath : String?
    var physicalAnalysisFormPath : String?
    var physicalAnalysisDownloadFormName : String?
    var viewDocumentFileDisplayName: String?
    init() {}
}
