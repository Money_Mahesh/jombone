//
//  JsonUtility.swift
//  Jombone
//
//  Created by Money Mahesh on 22/04/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation

class JsonUtility<T: Codable> {
    
    class func jsonString(data: T) -> String? {
        
        let jsonEncoder = JSONEncoder()
        do {
            let jsonData = try jsonEncoder.encode(data)
            return String(data: jsonData, encoding: .utf8)
        }
        catch {
            print("error")
        }
        return nil
    }
    
    class func dictionary(data: T?) -> [String: Any]? {
        
        guard let _data = data else {
            return nil
        }
        
        if let data = jsonString(data: _data)?.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
}
