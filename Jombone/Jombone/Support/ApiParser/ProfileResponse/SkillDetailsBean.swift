//
//  SkillDetailsBean.swift
//  Jombone
//
//  Created by Money Mahesh on 07/04/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation

struct SkillDetailsBean: Codable, AutoCompleteCardViewModelProtocol {
    
    var encryptedId: String?
    var title: String!
    var selected: Bool! = false
    var isPrimary : Bool?
    var skill : String?
    
    enum OptionCodingKeys: String, CodingKey {
        case encryptedId = "encSkillId"
        case title = "skillName"
        case isPrimary = "isPrimary"
        case selected = "selected"
        case skill = "skill"
    }
    
    init(from decoder: Decoder) throws {
        do {
            let container = try decoder.container(keyedBy: OptionCodingKeys.self)
            encryptedId = try? container.decode(String.self, forKey: OptionCodingKeys.encryptedId)
            isPrimary = try? container.decode(Bool.self, forKey: OptionCodingKeys.isPrimary)
            title = (try? container.decode(String.self, forKey: OptionCodingKeys.title)) ?? ""
            selected = true
            skill = try? container.decode(String.self, forKey: OptionCodingKeys.encryptedId)
        }
    }
    
    init(data: AutoCompleteCardViewModelProtocol) {
        
        self.encryptedId = data.encryptedId
        self.title = data.title
        self.selected = true
    }
}
