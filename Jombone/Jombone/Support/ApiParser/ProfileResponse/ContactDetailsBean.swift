//
//	ContactDetailsBean.swift
//
//	Create by Money Mahesh on 15/11/2018
//	Copyright © 2018. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct ContactDetailsBean: Codable {

    var emergencyContactName : String?
    var emergencyContactNumber : String?
    var emergencyContactNumberFormatted : String?
    var emergencyContactNumberCountryCode : String?
    var emergencyContactNumberCountryIsoCode : String?
    var homePhoneNumber : String?
    var homePhoneNumberFormatted : String?
    var homePhoneNumberCountryCode : String?
    var homePhoneNumberCountryIsoCode : String?
    var detail: [[String: String]]? {
        get {
            var data = [[String: String]]()
            
            if let _homePhoneNumber = homePhoneNumberFormatted, _homePhoneNumber.count > 0 {
                data.append(["Home Phone Number": ((homePhoneNumberCountryCode ?? "").appendTrailingSpace() + _homePhoneNumber)])
            }
            
            if let _emergencyContactName = emergencyContactName, _emergencyContactName.count > 0 {
                data.append(["Emergency Contact Name": _emergencyContactName])
            }
            
            if let _emergencyContactNumber = emergencyContactNumberFormatted, _emergencyContactNumber.count > 0 {
                data.append(["Emergency Contact Number": ((emergencyContactNumberCountryCode ?? "").appendTrailingSpace() + _emergencyContactNumber)])
            }
            
            return data.count == 0 ? nil : data
        }
    }
    
    init() {}
}
