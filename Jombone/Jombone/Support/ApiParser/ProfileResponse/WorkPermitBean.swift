//
//	WorkPermitBean.swift
//
//	Create by Money Mahesh on 15/11/2018
//	Copyright © 2018. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct WorkPermitBean: Codable {

    var hasWorkPermit : Int?
    var hasWorkPermitStr : String {
        get {
            let value = hasWorkPermit ?? 0
            switch value {
            case 1:
                return "Yes"
                
            case 0:
                return "No"
                
            default:
                return "I am a PR or Citizen"
            }
        }
    }
    var permitDocument : String?
    var permitDocumentEncId : String?
    var permitDocumentName : String?
    var permitDocumentPath : String?
    var permitExpiry : String?
    var permitExpiryDate : Int?
    var permitType : String?
    var permitDocumentDisplayName : String?
    var detail: [[String: String]]? {
        get {
            var data = [[String: String]]()
            
            if hasWorkPermit != nil {
                data.append(["Are you legally entitled to work in Canada?": hasWorkPermitStr])
            }
            
            if let _permitType = permitType, _permitType.count > 0 {
                data.append(["Do you have Work Permit or Study Permit?": _permitType])
            }
            
            if let _permitExpiry = permitExpiry, _permitExpiry.count > 0 {
                data.append(["Expiry date": _permitExpiry])
            }
            
            return data.count == 0 ? nil : data
        }
    }
}
