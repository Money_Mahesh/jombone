//
//  EducationDetailsBean.swift
//  Jombone
//
//  Created by Money Mahesh on 07/04/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation

struct EducationDetailsBean: Codable, EducationCardProtocol {
    
    var eduFileEncId : String?
    var educationDocName : String?
    var educationDocPath : String?
    var educationIdEnc : String?
    var educationProof : String?
    var encQualificationId : String?
    var instituteName : String?
    var isVerified : Bool?
    var qualificationId : String?
    var qualificationName : String?
    var yearCompleted : String?
    var educationDocDisplayName : String?
    init() {}
}
