//
//	PersonalDetailsBean.swift
//
//	Create by Money Mahesh on 15/11/2018
//	Copyright © 2018. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct PersonalDetailsBean: Codable {
    
    var availability : Bool?
    var availableFrom : String?
    var availableFromDate : Int64?
    var countryCode : String?
    var countryIsoCode : String?
    var email : String?
    var enableNotification : Bool?
    var exp : Int?
    var firstName : String?
    var highestEducationEncId : String?
    var highestEducationId : Int?
    var highestEducationName : String?
    var jomboneId : String?
    var jomboneScore : Int?
    var lastName : String?
    var mobileUnformat : String?
    var phoneNumber : String?
    var picFsEncId : String?
    var profileCompleteness : Int?
    var profilePicPath : String?
    var skills : String?
    
    init(firstName: String?, lastName: String?, email : String?, countryCode: String?, phoneNumber : String?, countryIsoCode: String?) {
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.countryCode = countryCode
        self.phoneNumber = phoneNumber
        self.countryIsoCode = countryIsoCode
    }
    
    init() {}
}
