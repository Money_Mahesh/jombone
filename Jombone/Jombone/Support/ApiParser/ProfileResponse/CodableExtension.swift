//
//  BaseCodable.swift
//  Jombone
//
//  Created by Money Mahesh on 22/04/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation

class BaseCodable: Codable {
    
    func jsonString() -> String? {
        
        let jsonEncoder = JSONEncoder()
        do {
            let jsonData = try jsonEncoder.encode(self)
            return String(data: jsonData, encoding: .utf8)
        }
        catch {
            print("error")
        }
        return nil
    }
    
    func dictionary() -> [String: Any]? {
        
        if let data = jsonString()?.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
}
