//
//	ScoreModel.swift
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class ScoreModel : Codable{

    var jomboneScore : Int?
	var profileCompleteness : Int?
}
