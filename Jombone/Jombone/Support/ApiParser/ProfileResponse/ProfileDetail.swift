//
//	ProfileDetail.swift
//
//	Create by Money Mahesh on 15/11/2018
//	Copyright © 2018. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class ProfileDetail: NSObject, Codable {

    var personalDetailsBeans : PersonalDetailsBean?
    var contactDetailsBean : ContactDetailsBean?
    var documentBeans : [DocumentDetailsBean]?
    var educationBeans : [EducationDetailsBean]?
    var employmentBeans : [EmploymentDetailsBean]?
    var workPermitBean : WorkPermitBean?
    var bankAccountBean : BankAccountBean?
    var addressBean : AddressBean?
    var skill : [AutoSuggestObj]?
	var physicalAnalysisDocumentBean : PhysicalAnalysisDocumentBean?
    
    init(personalDetailsBean: PersonalDetailsBean) {
        super.init()
        self.personalDetailsBeans = personalDetailsBean
    }
}
