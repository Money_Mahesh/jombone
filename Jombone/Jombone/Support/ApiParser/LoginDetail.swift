//
//	ResponseData.swift
//
//	Create by Money Mahesh on 30/7/2018
//	Copyright © 2018. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct LoginDetail: Codable {

	var created : Int?
	var token : String?
    var personalDetailsBean: PersonalDetailsBean?
}
