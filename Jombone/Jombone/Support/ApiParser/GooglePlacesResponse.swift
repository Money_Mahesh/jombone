//
//  GooglePlacesResponse.swift
//  Jombone
//
//  Created by Money Mahesh on 20/09/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import Foundation

struct GooglePlacesResponse {
    var places: [GooglePlace]?
    
    init() {
        places = [GooglePlace]()
    }
}

class GooglePlace: Codable, AutoCompleteCardViewModelProtocol {
    var isPrimary: Bool?
    var title: String!
    var selected: Bool! = false
    var placeId: String!
    var fieldType: [String]!
    var encryptedId: String?
    
    enum OptionCodingKeys: String, CodingKey {
        case encryptedId = "preferenceIdEnc"
        case title = "fullAddress"
    }
    
    init (title: String, selected: Bool, placeId: String, fieldType: [String], encryptedId: String) {
    
        self.title = title
        self.selected = selected
        self.placeId = placeId
        self.fieldType = fieldType
        self.encryptedId = encryptedId
    }
    
    required init(from decoder: Decoder) throws {
        do {
            let container = try decoder.container(keyedBy: OptionCodingKeys.self)
            if let _encryptedId = try? container.decode(String.self, forKey: OptionCodingKeys.encryptedId) {
                encryptedId = _encryptedId
            }
            if let _title = try? container.decode(String.self, forKey: OptionCodingKeys.title) {
                title = _title
            }
            selected = true
        }
    }
}

struct GooglePlaceDetail {
    
    var dislayAddress: String?
    var street_number: String?
    var route: String?
    var locality: String?
    var postal_code: String?
    var administrative_area_level_1: String?
    var country: String?
    var place_id: String?
    var lat: Double?
    var long: Double?

    init(placeId: String, lat: Double, long: Double, dislayAddress: String?, otherDetails: [String: String]) {
        
        place_id = placeId
        self.dislayAddress = dislayAddress
        self.lat = lat
        self.long = long
        
        street_number = otherDetails["street_number"]
        route = otherDetails["route"]
        locality = otherDetails["locality"]
        postal_code = otherDetails["postal_code"]
        administrative_area_level_1 = otherDetails["administrative_area_level_1"]
        country = otherDetails["country"]
    }
}
