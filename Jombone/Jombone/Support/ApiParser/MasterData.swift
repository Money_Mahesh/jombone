//
//  MasterData.swift
//  Jombone
//
//  Created by Money Mahesh on 26/04/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation

class MasterData : Codable {

    var companyId : String?
    var createdDate : Int?
    var displayName : String?
    var displayOrder : String?
    var domain : String?
    var encryptedId : String?
    var file : String?
    var imagePath : String?
    var isDeleted : Bool?
    var name : String?
    var newField : Bool?
    var updatedDate : String?
    
    init(displayName: String, encryptedId: String) {
        self.displayName = displayName
        self.encryptedId = encryptedId
    }
}
