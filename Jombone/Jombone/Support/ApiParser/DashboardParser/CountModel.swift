//
//	CountModel.swift
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class CountModel : Codable{

	var currentEmpCount : Int?
	var jobAppCount : Int?
	var jobOfferCount : Int?
	var msgCount : Int?
	var notificationCount : Int?
    var pastEmpCount : Int?
	var floatingIcon : FloatingIconModel?
}

class FloatingIconModel : Codable{
    
    var enableSignIn : Bool?
    var enableSignOut : Bool?
    var showFloatingIcon : Bool?
    var jobTitle : String?
    var jobEncId : String?
    var timesheetEncId : String?
}
