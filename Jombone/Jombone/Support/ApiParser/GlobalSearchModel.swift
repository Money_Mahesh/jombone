//
//  GlobalSearchModel.swift
//  Jombone
//
//  Created by Money Mahesh on 19/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation

class GlobalSearchModel : Codable, GlobalSearchModelProtocol {
    
    var employerId : String?
    var employerLogoId : String?
    var employerLogoPath : String?
    var employerName : String?
    var encryptedEmployerId : String?
    var encryptedEmployerLogoId : String?
    var encryptedJobId : String?
    var encryptedJobTitleId : String?
    var jobId : String?
    var jobTitle : String?
    var jobTitleId : String?
    var status : Int?
}
