//
//	PreviousTimesheetViewBeanList.swift
//
//	Create by Money Mahesh on 1/4/2019
//	Copyright © 2019. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class PreviousTimesheetViewBeanList: Codable, TimeSheetCardViewProtocol {

	var approvedHour : Int?
	var approvedMin : Int?
	var approvedTime : String?
	var date : String?
	var displayDate : String?
	var signInLocation : String?
	var signInTime : String?
	var signOutLocation : String?
	var signOutTime : String?
	var totalTime : String?
	var weekDay : String?
}
