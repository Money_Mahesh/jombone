//
//	RootClass.swift
//
//	Create by Money Mahesh on 1/4/2019
//	Copyright © 2019. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class TimeSheetModel: Codable  {

	var previousTimesheetViewBeanList : [PreviousTimesheetViewBeanList]?
	var thisWeek : Int?
	var weeklyApprovedTime : String?


}
