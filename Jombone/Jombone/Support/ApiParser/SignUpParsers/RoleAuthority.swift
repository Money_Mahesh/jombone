//
//	RoleAuthority.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct RoleAuthority : Codable {

	let authorities : Authority?
	let createdDate : Int?
	let encryptedId : String?
	let newField : Bool?
	let role : Int?
	let updatedDate : Int?


	enum CodingKeys: String, CodingKey {
		case authorities
		case createdDate = "createdDate"
		case encryptedId = "encryptedId"
		case newField = "new"
		case role = "role"
		case updatedDate = "updatedDate"
	}
	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		authorities = try Authority(from: decoder)
		createdDate = try values.decodeIfPresent(Int.self, forKey: .createdDate)
		encryptedId = try values.decodeIfPresent(String.self, forKey: .encryptedId)
		newField = try values.decodeIfPresent(Bool.self, forKey: .newField)
		role = try values.decodeIfPresent(Int.self, forKey: .role)
		updatedDate = try values.decodeIfPresent(Int.self, forKey: .updatedDate)
	}


}