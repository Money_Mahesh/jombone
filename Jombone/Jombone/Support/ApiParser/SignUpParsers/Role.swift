//
//	Role.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct Role : Codable {

	let createdDate : Int?
	let encryptedId : String?
	let name : String?
	let newField : Bool?
	let roleAuthorities : [RoleAuthority]?
	let updatedDate : Int?


	enum CodingKeys: String, CodingKey {
		case createdDate = "createdDate"
		case encryptedId = "encryptedId"
		case name = "name"
		case newField = "new"
		case roleAuthorities = "roleAuthorities"
		case updatedDate = "updatedDate"
	}
	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		createdDate = try values.decodeIfPresent(Int.self, forKey: .createdDate)
		encryptedId = try values.decodeIfPresent(String.self, forKey: .encryptedId)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		newField = try values.decodeIfPresent(Bool.self, forKey: .newField)
		roleAuthorities = try values.decodeIfPresent([RoleAuthority].self, forKey: .roleAuthorities)
		updatedDate = try values.decodeIfPresent(Int.self, forKey: .updatedDate)
	}


}