//
//	SignUpResponse.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct SignUpResponse : Codable {

    //change the data type later

    let data : SignUpData?
	let errors : String?
	let message : String?
	let status : Int?
}
