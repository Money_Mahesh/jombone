//
//	Data.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct SignUpData : Codable {

    //change data type later
    
	let confirmPassword : String?
	let countryCode : String?
	let createdDate : String?
	let email : String?
	let emailVerified : Bool?
	let enabled : Bool?
	let encryptedId : String?
	let mobile : String?
	let mobileVerified : Bool?
	let newField : Bool?
	let notification : Bool?
	let password : String?
	let role : Role?
	let updatedDate : String?
}
