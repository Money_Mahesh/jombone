//
//	JobModel.swift
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class JobModel : Codable, JobDetailHeaderProtocol, ApplicationCardViewProtocol, OfferCardViewModelProtocol, PastWorkCardProtocol, CurrentWorkCardViewProtocol {

	var appEncId : String?
	var applyDate : String?
	var candidateGooglePlaceId : String?
	var candidateLatLong : String?
	var companyId : String?
	var downloadRoePath : String?
	var empEndDate : String?
	var empStartDate : String?
	var employerName : String?
	var endDate : String?
	var isApplied : Bool?
	var isFavorite : Bool?
	var isFollowed : Bool?
	var isOfferRejected : Bool?
	var jd : String?
	var jdDocName : String?
	var jdDocPath : String?
	var jobGooglePlaceId : String?
	var jobId : String?
	var jobLatLong : String?
	var jobTitle : String?
	var jobType : String?
	var jobURL : String?
	var leaveReason : String?
	var location : String?
	var logoPath : String?
	var payType : String?
	var rate : Double?
	var shift : String?
	var shiftTimeFrom : String?
	var shiftTimeTo : String?
	var specialInstruction : String?
	var startDate : String?
	var totalWorkTime : String?
    var candidateLat_long : String?
    var jobLat_long : String?
    var isSignIn: Bool?
    var isSignOut: Bool?
    var timesheetEncId: String?
    var truncateLocation : String?
    var payRateString : String?
    var offerAcceptedDate: String?
    var jobOfferedDate : String?
    var isOfferAccepted : Bool?
    var isRejected : Bool?
    var rejectedDate : Double?
    var isWithdrawn : Bool?
    var withdrawnDate : Double?
    var isExpanded: Bool! = false
}
