//
//  AutoSuggestResponse.swift
//  Jombone
//
//  Created by Money Mahesh on 22/09/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import Foundation

struct AutoSuggestResponse: Codable {
    
    var content: [AutoSuggestObj]?
}

class AutoSuggestObj: NSObject, AutoCompleteCardViewModelProtocol, Codable {
    
    var title: String!
    var selected: Bool! = false
    var createdDate : u_long?
    var displayOrder : u_long?
    var domain : String?
    var encryptedId : String?
    var isDeleted : Bool?
    var newField : Bool?
    var updatedDate : u_long?
    var preferenceIdEnc : String?
    var isPrimary: Bool? = false
    
    enum OptionCodingKeys: String, CodingKey {
        case style = "style"
        case createdDate = "createdDate"
        case displayOrder = "displayOrder"
        case industryName = "industryName"
        case domain = "domain"
        case encryptedId = "encryptedId"
        case isDeleted = "isDeleted"
        case displayName = "displayName"
        case newField = "newField"
        case updatedDate = "updatedDate"
        case preferenceIdEnc = "preferenceIdEnc"
        case isPrimary = "isPrimary"
        case encSkillId = "encSkillId"
        case skillName = "skillName"
        case selected = "selected"
    }
    
    required init(from decoder: Decoder) throws {
        do {
            let container = try decoder.container(keyedBy: OptionCodingKeys.self)
            
            encryptedId = ""
            if let encSkillId = (try? container.decode(String.self, forKey: OptionCodingKeys.encSkillId)) {
                encryptedId = encSkillId
            }
            else if let _encryptedId = (try? container.decode(String.self, forKey: OptionCodingKeys.encryptedId)) {
                encryptedId = _encryptedId
            }

            if let _isPrimary = (try? container.decode(Bool.self, forKey: OptionCodingKeys.isPrimary)) {
                isPrimary = _isPrimary
            }
            createdDate = try? container.decode(u_long.self, forKey: OptionCodingKeys.createdDate)
            displayOrder = try? container.decode(u_long.self, forKey: OptionCodingKeys.displayOrder)
            domain = try? container.decode(String.self, forKey: OptionCodingKeys.domain)
            newField = try? container.decode(Bool.self, forKey: OptionCodingKeys.newField)
            updatedDate = try? container.decode(u_long.self, forKey: OptionCodingKeys.updatedDate)
            if let _title = (try? container.decode(String.self, forKey: OptionCodingKeys.industryName)) {
                title = _title
            }
            else if let _title = (try? container.decode(String.self, forKey: OptionCodingKeys.skillName)) {
                title = _title
            }
            else if let _title = (try? container.decode(String.self, forKey: OptionCodingKeys.displayName)) {
                title = _title
            }
            else {
                title = ""
            }

            selected = false
            preferenceIdEnc = (try? container.decode(String.self, forKey: OptionCodingKeys.preferenceIdEnc))
        }
    }
}
