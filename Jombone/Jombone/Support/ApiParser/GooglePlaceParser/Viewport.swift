//
//	Viewport.swift
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class Viewport : Codable {

	var northeast : Location?
	var southwest : Location?
    
}
