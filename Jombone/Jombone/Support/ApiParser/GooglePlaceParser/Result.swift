//
//	Result.swift
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class Result : Codable {

	var geometry : Geometry?
	var icon : String?
	var id : String?
	var name : String?
	var photos : [Photo]?
	var place_id : String?
	var reference : String?
	var scope : String?
	var types : [String]?
	var vicinity : String?
    
}
