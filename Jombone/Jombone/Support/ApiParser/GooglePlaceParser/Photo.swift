//
//	Photo.swift
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class Photo : Codable {

	var height : Int?
	var htmlAttributions : [String]?
	var photoReference : String?
	var width : Int?
}
