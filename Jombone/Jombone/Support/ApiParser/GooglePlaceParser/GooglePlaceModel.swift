//
//	GooglePlaceModel.swift
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GooglePlaceModel : Codable {
    
    var htmlAttributions : [String]?
	var results : [Result]?
	var status : String?
}
