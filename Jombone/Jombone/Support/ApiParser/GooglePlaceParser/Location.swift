//
//	Location.swift
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class Location : Codable {

	var lat : Float?
	var lng : Float?
    
}
