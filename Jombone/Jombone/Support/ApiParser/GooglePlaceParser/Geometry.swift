//
//	Geometry.swift
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class Geometry : Codable {

	var location : Location?
	var viewport : Viewport?

}
