//
//  NotificationModel.swift
//  Jombone
//
//  Created by Money Mahesh on 13/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation

class NotificationModel : Codable, NotificationCardProtocol {
    
    var message : String?
    var status : Int?
    var notificationEncId : String?
    var photoPath : String?
    var read: Bool?
    var time: String?
    var date : String?

    init(message: String, status : Int, notificationEncId : String, photoPath : String, time: String?, date: String?, read: Bool) {
        
        self.message = message
        self.status = status
        self.notificationEncId = notificationEncId
        self.photoPath = photoPath
        self.read = read
        self.time = time
        self.date =  date
    }
}
