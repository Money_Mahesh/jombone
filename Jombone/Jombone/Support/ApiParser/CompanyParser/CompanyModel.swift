//
//	TimeSheetModel.swift
//
//	Create by Money Mahesh on 10/3/2019
//	Copyright © 2019. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class CompanyModel: Codable, CompanyCardViewProtocol, CompanyDetailHeaderProtocol, CompanyDetailViewProtocol {    

	var aboutUs : String?
	var candidateAddress : String?
	var candidateGooglePlaceId : String?
	var candidateLatLong : String?
	var employerAddress : String?
	var employerContact : [EmployerContact]?
	var employerEncId : String?
	var employerFollowers : Int?
	var employerGooglePlaceId : String?
	var employerIndustries : [EmployerIndustry]?
	var employerLatLong : String?
	var employerLocations : [EmployerLocation]?
	var employerName : String?
	var employerPhoto : String?
	var employerShareURL : String?
	var employerWebsite : String?
	var isFollowed : Bool?
}
