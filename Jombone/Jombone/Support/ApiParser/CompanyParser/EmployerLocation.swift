//
//	EmployerLocation.swift
//
//	Create by Money Mahesh on 10/3/2019
//	Copyright © 2019. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class EmployerLocation: Codable, LocationDetailProtocol {

	var employerName : String?
	var location : String?
}
