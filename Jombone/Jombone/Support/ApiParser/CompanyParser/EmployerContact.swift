//
//  EmployerContact.swift
//  Jombone
//
//  Created by Money Mahesh on 19/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation

class EmployerContact: Codable, EmployerContactDetailProtocol {

    var description: String?
    var name: String?
    var photo: String?
    var designation: String?
}
