//
//	PreferencesModel.swift
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class PreferencesModel : Codable {

	var industries : [AutoSuggestObj]?
	var jobTypes : [JobType]?
	var preferredLocations : [GooglePlace]?
	var shiftTimings : [JobType]?
    
}
