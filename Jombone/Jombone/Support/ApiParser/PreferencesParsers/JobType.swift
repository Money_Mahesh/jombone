//
//	JobType.swift
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class JobType : Codable, CheckBoxCardDataModel {
    
    var displayValue: String!
    var state: Bool!
	var preferenceIdEnc : String?
	var preferenceType : String?
    
    enum OptionCodingKeys: String, CodingKey {
        case displayValue = "name"
        case preferenceIdEnc = "preferenceIdEnc"
    }
    
    required init(from decoder: Decoder) throws {
        do {
            let container = try decoder.container(keyedBy: OptionCodingKeys.self)
            displayValue = try? container.decode(String.self, forKey: OptionCodingKeys.displayValue)
            preferenceIdEnc = try? container.decode(String.self, forKey: OptionCodingKeys.preferenceIdEnc)
            state = true
        }
    }
}
