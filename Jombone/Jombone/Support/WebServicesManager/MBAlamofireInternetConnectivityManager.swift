//
//  MBAlamofireInternetConnectivityManager.swift
//  Jombone
//
//  Created by Money Mahesh on 13/08/18.
//  Copyright © 2018 tbsl. All rights reserved.
//

import UIKit
import Foundation
import Alamofire

class MBAlamofireInternetConnectivityManager : NSObject{
    //Class signleton object
    static var internetErrorAlert: UIAlertController {
        get {
            let alert = UIAlertController(title: ALERT_NO_NETWORK_TITLE, message: NETWORK_FAILURE_MSG, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: ALERT_OK_BUTTON_TITLE, style: .cancel, handler: { (alertAction) in
                MBAlamofireInternetConnectivityManager.isAlertShown = false
            }))
            return alert
        }
    }
        
    static var isAlertShown = false
    
    static var _sharedManager : MBAlamofireInternetConnectivityManager?
    static var sharedManager: MBAlamofireInternetConnectivityManager!{
        get{
            if _sharedManager == nil{
                _sharedManager = MBAlamofireInternetConnectivityManager()
            }
            
            return _sharedManager
        }
    }
    
    //Class function
    class func isInternetAvailable() -> Bool{
        let reachabilityManager = Alamofire.NetworkReachabilityManager(host: BASE_URL.host!)
        
        if let reachabilityManager = reachabilityManager, reachabilityManager.networkReachabilityStatus == .reachable(.ethernetOrWiFi) || reachabilityManager.networkReachabilityStatus == .reachable(.wwan) {
            return true
        }else {
            return false
        }
    }
    
    class func checkInternetAvailability() -> Bool {
        
        if !isInternetAvailable() {
            MBAlamofireInternetConnectivityManager.sharedManager.showNoNetworkAlert()
            return false
        }
        else {
            return true
        }
    }
    
    override init() {
        super.init()
    }
    
    //Instance function
    func startMonitoring() {
        stopMonitoring()
        
        let notificationCenter = NotificationCenter.default
        
        notificationCenter.addObserver(
            self,
            selector: #selector(MBAlamofireInternetConnectivityManager.networkRequestDidStart(notification:)),
            name: Foundation.Notification.Name.Task.DidResume,
            object: nil
        )
        
        notificationCenter.addObserver(
            self,
            selector: #selector(MBAlamofireInternetConnectivityManager.networkRequestDidComplete(notification:)),
            name: Foundation.Notification.Name.Task.DidComplete,
            object: nil
        )
    }
    
    func stopMonitoring() {
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK:- Network notification methods
    @objc func networkRequestDidStart(notification : Foundation.Notification) {
        
        if !MBAlamofireInternetConnectivityManager.isInternetAvailable() {
            showNoNetworkAlert()
        }
    }
    
    @objc func networkRequestDidComplete(notification : Foundation.Notification) {
    
    }
    
    //MARK:- Check methods
    static func checkForServerError( _ error : Error) {
        var localizedErrorString : String?
        switch error._code{
        case -1003, -1001, -1011, -1020:
            localizedErrorString = "Looks like we have encountered a problem. Please try again."
        case -1009, -1005:
            localizedErrorString = "Seems you are offline. Please check your network connection."
        case -1018:
            localizedErrorString = "Looks like you are Roaming. Please check your network connection."
        default:
            if error._code == 3840 {
                localizedErrorString = "Oops, something went wrong. Please try after sometime."
            }
        }
        
        if localizedErrorString == nil || localizedErrorString?.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            return
        }
        
        MBAlamofireInternetConnectivityManager.sharedManager.showServerErrorAlert(error: localizedErrorString!)
    }
    
    private func showNoNetworkAlert() {
        
        if !MBAlamofireInternetConnectivityManager.isAlertShown {
            if let visibleViewController = APPLICATION_INSTANCE.visibleViewController(){
                visibleViewController.present(MBAlamofireInternetConnectivityManager.internetErrorAlert, animated: false, completion: nil)
                MBAlamofireInternetConnectivityManager.isAlertShown = true
            }
        }
    }
    
    private func showServerErrorAlert(error: String) {
        
        if !MBAlamofireInternetConnectivityManager.isAlertShown {
            
            if let visibleViewController = APPLICATION_INSTANCE.visibleViewController(){
                
                let serverErrorAlert = UIAlertController(title: ALERT_NO_NETWORK_TITLE, message: NETWORK_FAILURE_MSG, preferredStyle: .alert)
                serverErrorAlert.addAction(UIAlertAction(title: ALERT_OK_BUTTON_TITLE, style: .cancel, handler: { (alertAction) in
                    MBAlamofireInternetConnectivityManager.isAlertShown = false
                }))
                
                visibleViewController.present(serverErrorAlert, animated: false, completion: nil)
                MBAlamofireInternetConnectivityManager.isAlertShown = true
            }
        }
    }
}
