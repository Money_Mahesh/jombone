//
//  DownloadUploadService.swift
//  Jombone
//
//  Created by Money Mahesh on 12/04/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import Alamofire

class DownloadUploadService: MBAlamofireServerConnect {
    
    func hitGetDownloadedFile(_ filePath: String, fileName: String, parameters: [String: Any]?, additionalHeaderElements: [String: String]?) {
        
        let _ = MBAlamofireServerConnect.requestOperationManager.download(filePath, fileName: fileName, method: .get, parameters: parameters, headerElements: additionalHeaderElements, success: {(url : String? , responseObject: CommonResponse<URL>?) in
            
            let serverResponse = self.processAfterMakingRequestWithParameters(responseObject)
            if let serverResponse = serverResponse as? CommonResponse<URL>  {
                
                self.completionBlockWithSuccess?(url, (serverResponse.status == 1) ? 1 : 0, self.requestTag, (serverResponse.data, serverResponse.pagination), serverResponse.message)
            }
            else {
                self.completionBlockWithFailure?(nil, self.requestTag, nil)
            }
        }, failure: {(url : String?, error:Error?) in
            self.completionBlockWithFailure?(nil, self.requestTag, error)
        })
    }
    
    func hitUploadFile<T: Codable>(_ urlString: String, filesDetail: (keysName: [String], filesName: [String], filesData: [Data])?, parameters: [String: Any]?, additionalHeaderElements: [String: String]?, completion: ((_ data: T)->())) {
        
        MBAlamofireServerConnect.requestOperationManager.hitUploadApi(urlString, fileDetail: filesDetail, method: HTTPMethod.post, parameters: parameters, headerElements: additionalHeaderElements, success: {(url : String? , responseObject: CommonResponse<T>?) in
            
            let serverResponse = self.processAfterMakingRequestWithParameters(responseObject)
            if let serverResponse = serverResponse as? CommonResponse<T>  {
                
                self.completionBlockWithSuccess?(url, (serverResponse.status == 1) ? 1 : 0, self.requestTag, (serverResponse.data, serverResponse.pagination), serverResponse.message)
            }
            else {
                self.completionBlockWithFailure?(nil, self.requestTag, nil)
            }
        }, failure: {(url : String?, error:Error?) in
            self.completionBlockWithFailure?(nil, self.requestTag, error)
        })
    }
}
