//
//  AutoSuggestServices.swift
//  Jombone
//
//  Created by Money Mahesh on 22/09/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import Foundation
import Alamofire

class AutoSuggestServices: MBAlamofireServerConnect {
    
    func getAutoSuggestList(relativeUrl: String, parameters: [String: Any]?, additionalHeaderElements: [String: String]?) {
        
        let _ = MBAlamofireServerConnect.requestOperationManager.hitApi(relativeUrl, method: .get, parameters: parameters, encoding: URLEncoding.default, headerElements: additionalHeaderElements, success: {(url : String? , responseObject: CommonResponse<AutoSuggestResponse>?) in
            
            let serverResponse = self.processAfterMakingRequestWithParameters(responseObject)
            if let serverResponse = serverResponse as? CommonResponse<AutoSuggestResponse> {
                self.completionBlockWithSuccess?(url, (serverResponse.status == 1) ? 1 : 0, self.requestTag, (serverResponse.data, serverResponse.pagination), serverResponse.message)
            }
            else {
                self.completionBlockWithFailure?(nil, self.requestTag, nil)
            }
        }, failure: {(url : String?, error: Error?) in
            self.completionBlockWithFailure?(nil, self.requestTag, error)
        })
    }
}
