//
//  LoginServices.swift
//  Jombone
//
//  Created by Money Mahesh on 13/08/18.
//  Copyright © 2018 Money Mahesh. All rights reserved.
//

import Foundation
import Alamofire

class AutheticationServices: MBAlamofireServerConnect {
    
    func hitSignUp(parameters: [String: Any]?, additionalHeaderElements: [String: String]?) {
        
        let _ = MBAlamofireServerConnect.requestOperationManager.hitApi("/signup/candidate-signup", method: .post, parameters: parameters, encoding: JSONEncoding.default, headerElements: additionalHeaderElements, success: {(url : String? , responseObject: CommonResponse<String>?) in
            
            let serverResponse = self.processAfterMakingRequestWithParameters(responseObject)
            if let serverResponse = serverResponse as? CommonResponse<String> {
                self.completionBlockWithSuccess?(url, (serverResponse.status == 1) ? 1 : 0, self.requestTag, (serverResponse.data, serverResponse.pagination), serverResponse.message)
            }
            else {
                self.completionBlockWithFailure?(nil, self.requestTag, nil)
            }
        }, failure: {(url : String?, error: Error?) in
            self.completionBlockWithFailure?(nil, self.requestTag, error)
        })
        
    }
    
    func hitLogin(parameters: [String: Any]?, additionalHeaderElements: [String: String]?) {
        
        let _ = MBAlamofireServerConnect.requestOperationManager.hitApi("/login/do-login", method: .post, parameters: parameters, encoding: JSONEncoding.default, headerElements: additionalHeaderElements, success: {(url : String? , responseObject: CommonResponse<LoginDetail>?) in
            
            let serverResponse = self.processAfterMakingRequestWithParameters(responseObject)
            if let serverResponse = serverResponse as? CommonResponse<LoginDetail> {
                self.completionBlockWithSuccess?(url, serverResponse.status, self.requestTag, (serverResponse.data, serverResponse.pagination), serverResponse.message)
            }
            else {
                self.completionBlockWithFailure?(nil, self.requestTag, nil)
            }
        }, failure: {(url : String?, error: Error?) in
            self.completionBlockWithFailure?(nil, self.requestTag, error)
        })
    }
    
    func hitChangePassword(parameters: [String: Any]?, additionalHeaderElements: [String: String]?) {
        
        let _ = MBAlamofireServerConnect.requestOperationManager.hitApi(("/login/change-password"), method: .post, parameters: parameters, encoding: JSONEncoding.default, headerElements: additionalHeaderElements, success: {(url : String? , responseObject: CommonResponse<ChangePasswordReponse>?) in
            
            let serverResponse = self.processAfterMakingRequestWithParameters(responseObject)
            if let serverResponse = serverResponse as? CommonResponse<ChangePasswordReponse> {
                self.completionBlockWithSuccess?(url, (serverResponse.status == 1) ? 1 : 0, self.requestTag, (serverResponse.data, serverResponse.pagination), serverResponse.message)
            }
            else {
                self.completionBlockWithFailure?(nil, self.requestTag, nil)
            }
        }, failure: {(url : String?, error: Error?) in
            self.completionBlockWithFailure?(nil, self.requestTag, error)
        })
        
    }
    
    func hitResetPassword(parameters: [String: Any]?, additionalHeaderElements: [String: String]?) {
        
        let _ = MBAlamofireServerConnect.requestOperationManager.hitApi("/login/reset-password", method: .post, parameters: parameters, encoding: JSONEncoding.default, headerElements: additionalHeaderElements, success: {(url : String? , responseObject: CommonResponse<LoginDetail>?) in
            
            let serverResponse = self.processAfterMakingRequestWithParameters(responseObject)
            if let serverResponse = serverResponse as? CommonResponse<LoginDetail> {
                self.completionBlockWithSuccess?(url, serverResponse.status, self.requestTag, (serverResponse.data, serverResponse.pagination), serverResponse.message)
            }
            else {
                self.completionBlockWithFailure?(nil, self.requestTag, nil)
            }
        }, failure: {(url : String?, error: Error?) in
            self.completionBlockWithFailure?(nil, self.requestTag, error)
        })
        
    }
    
    func hitUpdateDetail(methodName: String, parameters: [String: Any]?, additionalHeaderElements: [String: String]?) {
        
        let _ = MBAlamofireServerConnect.requestOperationManager.hitApi(("/signup/" + methodName), method: .post, parameters: parameters, encoding: URLEncoding.default, headerElements: additionalHeaderElements, success: {(url : String? , responseObject: CommonResponse<LoginDetail>?) in
            
            let serverResponse = self.processAfterMakingRequestWithParameters(responseObject)
            if let serverResponse = serverResponse as? CommonResponse<LoginDetail> {
                self.completionBlockWithSuccess?(url, (serverResponse.status == 1) ? 1 : 0, self.requestTag, (serverResponse.data, serverResponse.pagination), serverResponse.message)
            }
            else {
                self.completionBlockWithFailure?(nil, self.requestTag, nil)
            }
        }, failure: {(url : String?, error: Error?) in
            self.completionBlockWithFailure?(nil, self.requestTag, error)
        })
        
    }
    
    func hitSendPhoneNoOTP(parameters: [String: Any]?, additionalHeaderElements: [String: String]?) {
        
        let _ = MBAlamofireServerConnect.requestOperationManager.hitApi(("/signup/verify-mobile"), method: .post, parameters: parameters, encoding: JSONEncoding.default, headerElements: additionalHeaderElements, success: {(url : String? , responseObject: CommonResponse<LoginDetail>?) in
            
            let serverResponse = self.processAfterMakingRequestWithParameters(responseObject)
            if let serverResponse = serverResponse as? CommonResponse<LoginDetail> {
                self.completionBlockWithSuccess?(url, serverResponse.status, self.requestTag, (serverResponse.data, serverResponse.pagination), serverResponse.message)
            }
            else {
                self.completionBlockWithFailure?(nil, self.requestTag, nil)
            }
        }, failure: {(url : String?, error: Error?) in
            self.completionBlockWithFailure?(nil, self.requestTag, error)
        })
        
    }
    
    func hitVerifyPhoneNoOTP(parameters: [String: Any]?, additionalHeaderElements: [String: String]?) {

        let _ = MBAlamofireServerConnect.requestOperationManager.hitApi(("/signup/verify-mobile-otp"), method: .post, parameters: parameters, encoding: JSONEncoding.default, headerElements: additionalHeaderElements, success: {(url : String? , responseObject: CommonResponse<LoginDetail>?) in
            
            let serverResponse = self.processAfterMakingRequestWithParameters(responseObject)
            if let serverResponse = serverResponse as? CommonResponse<LoginDetail> {
                self.completionBlockWithSuccess?(url, (serverResponse.status == 1) ? 1 : 0, self.requestTag, (serverResponse.data, serverResponse.pagination), serverResponse.message)
            }
            else {
                self.completionBlockWithFailure?(nil, self.requestTag, nil)
            }
        }, failure: {(url : String?, error: Error?) in
            self.completionBlockWithFailure?(nil, self.requestTag, error)
        })
        
    }
    
    func hitVerifyEmailOTP(parameters: [String: Any]?, additionalHeaderElements: [String: String]?) {

        let _ = MBAlamofireServerConnect.requestOperationManager.hitApi(("/signup/verify-email-otp"), method: .post, parameters: parameters, encoding: JSONEncoding.default, headerElements: additionalHeaderElements, success: {(url : String? , responseObject: CommonResponse<String>?) in
            
            let serverResponse = self.processAfterMakingRequestWithParameters(responseObject)
            if let serverResponse = serverResponse as? CommonResponse<String> {
                self.completionBlockWithSuccess?(url, (serverResponse.status == 1) ? 1 : 0, self.requestTag, (serverResponse.data, serverResponse.pagination), serverResponse.message)
            }
            else {
                self.completionBlockWithFailure?(nil, self.requestTag, nil)
            }
        }, failure: {(url : String?, error: Error?) in
            self.completionBlockWithFailure?(nil, self.requestTag, error)
        })
        
    }
    
    func hitResendOTP(methodName: String, parameters: [String: Any]?, additionalHeaderElements: [String: String]?) {
        
        let _ = MBAlamofireServerConnect.requestOperationManager.hitApi(("/signup/" + methodName), method: .post, parameters: parameters, encoding: JSONEncoding.default, headerElements: additionalHeaderElements, success: {(url : String? , responseObject: CommonResponse<String>?) in
            
            let serverResponse = self.processAfterMakingRequestWithParameters(responseObject)
            if let serverResponse = serverResponse as? CommonResponse<String> {
                self.completionBlockWithSuccess?(url, (serverResponse.status == 1) ? 1 : 0, self.requestTag, (serverResponse.data, serverResponse.pagination), serverResponse.message)
            }
            else {
                self.completionBlockWithFailure?(nil, self.requestTag, nil)
            }
        }, failure: {(url : String?, error: Error?) in
            self.completionBlockWithFailure?(nil, self.requestTag, error)
        })
    }
    
    func hitSetAvailability(parameters: [String: Any]?, additionalHeaderElements: [String: String]?) {
        
        let _ = MBAlamofireServerConnect.requestOperationManager.hitApi("/candidate/set-availability", method: .post, parameters: parameters, encoding: URLEncoding.default, headerElements: additionalHeaderElements, success: {(url : String? , responseObject: CommonResponse<SignUpResponse>?) in
            
            let serverResponse = self.processAfterMakingRequestWithParameters(responseObject)
            if let serverResponse = serverResponse as? CommonResponse<SignUpResponse> {
                self.completionBlockWithSuccess?(url, (serverResponse.status == 1) ? 1 : 0, self.requestTag, (serverResponse.data, serverResponse.pagination), serverResponse.message)
            }
            else {
                self.completionBlockWithFailure?(nil, self.requestTag, nil)
            }
        }, failure: {(url : String?, error: Error?) in
            self.completionBlockWithFailure?(nil, self.requestTag, error)
        })
        
    }
    
    func hitForgotPassword(parameters: [String: Any]?, additionalHeaderElements: [String: String]?) {
        
        let _ = MBAlamofireServerConnect.requestOperationManager.hitApi("/login/forgot-password", method: .post, parameters: parameters, encoding: URLEncoding.default, headerElements: additionalHeaderElements, success: {(url : String? , responseObject: CommonResponse<String>?) in
            
            let serverResponse = self.processAfterMakingRequestWithParameters(responseObject)
            if let serverResponse = serverResponse as? CommonResponse<String> {
                self.completionBlockWithSuccess?(url, (serverResponse.status == 1) ? 1 : 0, self.requestTag, (serverResponse.data, serverResponse.pagination), serverResponse.message)
            }
            else {
                self.completionBlockWithFailure?(nil, self.requestTag, nil)
            }
        }, failure: {(url : String?, error: Error?) in
            self.completionBlockWithFailure?(nil, self.requestTag, error)
        })
    }
    
    func hitLogout(parameters: [String: Any]?, additionalHeaderElements: [String: String]?) {
        
        let _ = MBAlamofireServerConnect.requestOperationManager.hitApi("/login/logout", method: .post, parameters: parameters, encoding: JSONEncoding.default, headerElements: additionalHeaderElements, success: {(url : String? , responseObject: CommonResponse<Bool>?) in
            
            let serverResponse = self.processAfterMakingRequestWithParameters(responseObject)
            if let serverResponse = serverResponse as? CommonResponse<Bool> {
                self.completionBlockWithSuccess?(url, (serverResponse.status == 1) ? 1 : 0, self.requestTag, (serverResponse.data, serverResponse.pagination), serverResponse.message)
            }
            else {
                self.completionBlockWithFailure?(nil, self.requestTag, nil)
            }
        }, failure: {(url : String?, error: Error?) in
            self.completionBlockWithFailure?(nil, self.requestTag, error)
        })
    }
}

