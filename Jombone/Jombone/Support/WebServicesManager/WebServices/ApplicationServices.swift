//
//  ApplicationServices.swift
//  Jombone
//
//  Created by dev139 on 19/09/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import UIKit
import Alamofire

class ApplicationServices: MBAlamofireServerConnect {
    
    func getApplicationList(parameters: [String: Any]?, additionalHeaderElements: [String: String]?) {
        
        let _ = MBAlamofireServerConnect.requestOperationManager.hitApi("/application/applications", method: .get, parameters: parameters, encoding: URLEncoding.default, headerElements: additionalHeaderElements, success: {(url : String? , responseObject: CommonResponse<[JobModel]>?) in
            
            let serverResponse = self.processAfterMakingRequestWithParameters(responseObject)
            if let serverResponse = serverResponse as? CommonResponse<[JobModel]> {
                self.completionBlockWithSuccess?(url, (serverResponse.status == 1) ? 1 : 0, self.requestTag, (serverResponse.data, serverResponse.pagination), serverResponse.message)
            }
            else {
                self.completionBlockWithFailure?(nil, self.requestTag, nil)
            }
        }, failure: {(url : String?, error: Error?) in
            self.completionBlockWithFailure?(nil, self.requestTag, error)
        })
    }
    
    func getRejectedWithdraws(parameters: [String: Any]?, additionalHeaderElements: [String: String]?) {
        
        let _ = MBAlamofireServerConnect.requestOperationManager.hitApi("/application/rejected-withdrawn-jobs", method: .get, parameters: parameters, encoding: URLEncoding.default, headerElements: additionalHeaderElements, success: {(url : String? , responseObject: CommonResponse<[JobModel]>?) in
            
            let serverResponse = self.processAfterMakingRequestWithParameters(responseObject)
            if let serverResponse = serverResponse as? CommonResponse<[JobModel]> {
                self.completionBlockWithSuccess?(url, (serverResponse.status == 1) ? 1 : 0, self.requestTag, (serverResponse.data, serverResponse.pagination), serverResponse.message)
            }
            else {
                self.completionBlockWithFailure?(nil, self.requestTag, nil)
            }
        }, failure: {(url : String?, error: Error?) in
            self.completionBlockWithFailure?(nil, self.requestTag, error)
        })
    }
    
    func withdrawApplication(parameters: [String: Any]?, additionalHeaderElements: [String: String]?) {
        
        let _ = MBAlamofireServerConnect.requestOperationManager.hitApi("/application/withdraw", method: .post, parameters: parameters, encoding: URLEncoding.default, headerElements: additionalHeaderElements, success: {(url : String? , responseObject: CommonResponse<String>?) in
            
            let serverResponse = self.processAfterMakingRequestWithParameters(responseObject)
            if let serverResponse = serverResponse as? CommonResponse<String> {
                self.completionBlockWithSuccess?(url, (serverResponse.status == 1) ? 1 : 0, self.requestTag, (serverResponse.data, serverResponse.pagination), serverResponse.message)
            }
            else {
                self.completionBlockWithFailure?(nil, self.requestTag, nil)
            }
        }, failure: {(url : String?, error: Error?) in
            self.completionBlockWithFailure?(nil, self.requestTag, error)
        })
    }
}
