//
//  WorkServices.swift
//  Jombone
//
//  Created by Money Mahesh on 25/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import Alamofire

class WorkServices: MBAlamofireServerConnect {
    
    func getCurrentWorks(parameters: [String: Any]?, additionalHeaderElements: [String: String]?) {
        
        let _ = MBAlamofireServerConnect.requestOperationManager.hitApi("/employment/current-employment", method: .get, parameters: parameters, encoding: URLEncoding.default, headerElements: additionalHeaderElements, success: {(url : String? , responseObject: CommonResponse<[JobModel]>?) in
            
            let serverResponse = self.processAfterMakingRequestWithParameters(responseObject)
            if let serverResponse = serverResponse as? CommonResponse<[JobModel]> {
                self.completionBlockWithSuccess?(url, (serverResponse.status == 1) ? 1 : 0, self.requestTag, (serverResponse.data, serverResponse.pagination), serverResponse.message)
            }
            else {
                self.completionBlockWithFailure?(nil, self.requestTag, nil)
            }
        }, failure: {(url : String?, error: Error?) in
            self.completionBlockWithFailure?(nil, self.requestTag, error)
        })
    }
    
    func getPastWorks(parameters: [String: Any]?, additionalHeaderElements: [String: String]?) {
        
        let _ = MBAlamofireServerConnect.requestOperationManager.hitApi("/employment/past-employment", method: .get, parameters: parameters, encoding: URLEncoding.default, headerElements: additionalHeaderElements, success: {(url : String? , responseObject: CommonResponse<[JobModel]>?) in
            
            let serverResponse = self.processAfterMakingRequestWithParameters(responseObject)
            if let serverResponse = serverResponse as? CommonResponse<[JobModel]> {
                self.completionBlockWithSuccess?(url, (serverResponse.status == 1) ? 1 : 0, self.requestTag, (serverResponse.data, serverResponse.pagination), serverResponse.message)
            }
            else {
                self.completionBlockWithFailure?(nil, self.requestTag, nil)
            }
        }, failure: {(url : String?, error: Error?) in
            self.completionBlockWithFailure?(nil, self.requestTag, error)
        })
    }
    
    func signIn(parameters: [String: Any]?, additionalHeaderElements: [String: String]?) {
        
        let _ = MBAlamofireServerConnect.requestOperationManager.hitApi("/timesheet/signin-job", method: .post, parameters: parameters, encoding: JSONEncoding.default, headerElements: additionalHeaderElements, success: {(url : String? , responseObject: CommonResponse<String>?) in
            
            let serverResponse = self.processAfterMakingRequestWithParameters(responseObject)
            if let serverResponse = serverResponse as? CommonResponse<String> {
                self.completionBlockWithSuccess?(url, (serverResponse.status == 1) ? 1 : 0, self.requestTag, (serverResponse.data, serverResponse.pagination), serverResponse.message)
            }
            else {
                self.completionBlockWithFailure?(nil, self.requestTag, nil)
            }
        }, failure: {(url : String?, error: Error?) in
            self.completionBlockWithFailure?(nil, self.requestTag, error)
        })
    }
    
    func signOut(parameters: [String: Any]?, additionalHeaderElements: [String: String]?) {
        
        let _ = MBAlamofireServerConnect.requestOperationManager.hitApi("/timesheet/signout-job", method: .post, parameters: parameters, encoding: JSONEncoding.default, headerElements: additionalHeaderElements, success: {(url : String? , responseObject: CommonResponse<String>?) in
            
            let serverResponse = self.processAfterMakingRequestWithParameters(responseObject)
            if let serverResponse = serverResponse as? CommonResponse<String> {
                self.completionBlockWithSuccess?(url, (serverResponse.status == 1) ? 1 : 0, self.requestTag, (serverResponse.data, serverResponse.pagination), serverResponse.message)
            }
            else {
                self.completionBlockWithFailure?(nil, self.requestTag, nil)
            }
        }, failure: {(url : String?, error: Error?) in
            self.completionBlockWithFailure?(nil, self.requestTag, error)
        })
    }
    
    func getTimeSheet(parameters: [String: Any]?, additionalHeaderElements: [String: String]?) {
        
        let _ = MBAlamofireServerConnect.requestOperationManager.hitApi("/timesheet/previous-timesheet", method: .get, parameters: parameters, encoding: URLEncoding.default, headerElements: additionalHeaderElements, success: {(url : String? , responseObject: CommonResponse<TimeSheetModel>?) in
            
            let serverResponse = self.processAfterMakingRequestWithParameters(responseObject)
            if let serverResponse = serverResponse as? CommonResponse<TimeSheetModel> {
                self.completionBlockWithSuccess?(url, (serverResponse.status == 1) ? 1 : 0, self.requestTag, (serverResponse.data, serverResponse.pagination), serverResponse.message)
            }
            else {
                self.completionBlockWithFailure?(nil, self.requestTag, nil)
            }
        }, failure: {(url : String?, error: Error?) in
            self.completionBlockWithFailure?(nil, self.requestTag, error)
        })
    }
}
