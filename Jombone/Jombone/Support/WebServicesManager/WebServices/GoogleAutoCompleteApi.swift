//
//  GoogleAutoCompleteApi.swift
//  Jombone
//
//  Created by Money Mahesh on 20/09/18.
//  Copyright © 2018 quarks. All rights reserved.
//

import UIKit
import GooglePlaces

class GoogleAutoCompleteServices: NSObject {
    
    var success: ((Any) -> Void)?
    var failure: ((String?) -> Void)?
    var googlePlaceFetcher: GMSAutocompleteFetcher?
    
    init(success: ((Any) -> Void)?, failure: ((String?) -> Void)?) {
        
        self.success = success
        self.failure = failure
    }
    
    func hitFetchPlaces(string: String, placeFor: GooglePlaceFor) {
        
        // Set up the autocomplete filter.
        let filter = GMSAutocompleteFilter()
        
        switch placeFor {
        case .city:
            filter.type = .city
            
        default:
            filter.type = .noFilter
        }

        // Create the fetcher.
        googlePlaceFetcher = GMSAutocompleteFetcher(bounds: nil, filter: filter)
        googlePlaceFetcher?.delegate = self
        
        googlePlaceFetcher?.sourceTextHasChanged(string)
    }
    
    func hitFetchPlaceDetail(_ placeID: String, title: String?, searchType: AutoComplete) {
                
        GMSPlacesClient.shared().lookUpPlaceID(placeID, callback: { (place, error) -> Void in
            if let error = error {
                print("lookup place id query error: \(error.localizedDescription)")
                return
            }
            
            guard let place = place else {
                print("No place details for \(placeID)")
                self.failure!(error?.localizedDescription)
                return
            }
            
            var otherDetails = [String: String]()
            if let addressComponents = place.addressComponents {
                for addressComponent in addressComponents {
                    otherDetails[addressComponent.type] = addressComponent.name
                    print("Type: \(String(describing: addressComponent.type)) Name \(String(describing: addressComponent.name))")
                }
            }
        
            var dislayAddress = title ?? place.formattedAddress
            
            switch searchType {
            case .google(let googlePlaceFor):
                
                if googlePlaceFor == .preferredJobLocation {
                    var formattedAddress = String()
                    if let city =  otherDetails["locality"] {
                        formattedAddress = city
                    }
                    if let state =  otherDetails["administrative_area_level_1"] {
                        formattedAddress += formattedAddress.count == 0 ? state : (", " + state)
                    }
                    if let country =  otherDetails["country"] {
                        formattedAddress += formattedAddress.count == 0 ? country : (", " + country)
                    }
                    dislayAddress = formattedAddress
                    /*
                     [0] = (key = "locality", value = "Noida")
                     [1] = (key = "sublocality_level_1", value = "Sector 16A")
                     [2] = (key = "administrative_area_level_1", value = "Uttar Pradesh")
                     [3] = (key = "administrative_area_level_2", value = "Gautam Buddh Nagar")
                     [4] = (key = "premise", value = "FC 6")
                     [5] = (key = "country", value = "India")
                     [6] = (key = "postal_code", value = "201301")
                     [7] = (key = "sublocality_level_2", value = "Film City")
                     */
                }
                
            default:
                break
            }
            
            let placedetail = GooglePlaceDetail(placeId: placeID,
                                                lat: place.coordinate.latitude,
                                                long: place.coordinate.longitude,
                                                dislayAddress: dislayAddress,
                                                otherDetails: otherDetails)
            self.success?(placedetail)
        })
    }
}

extension GoogleAutoCompleteServices: GMSAutocompleteFetcherDelegate {
    
    func didAutocomplete(with predictions: [GMSAutocompletePrediction]) {
        
        var response = GooglePlacesResponse()
        for prediction in predictions {
            response.places?.append(GooglePlace(title: prediction.attributedFullText.string, selected: false, placeId: prediction.placeID, fieldType: prediction.types, encryptedId: ""))
        }
        
        success?(response)
    }
    
    func didFailAutocompleteWithError(_ error: Error) {
        print("GoogleApi error: " + error.localizedDescription)
        failure?(error.localizedDescription)
    }
}
