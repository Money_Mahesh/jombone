//
//  CompanyServices.swift
//  Jombone
//
//  Created by Money Mahesh on 10/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import Alamofire

class CompanyServices: MBAlamofireServerConnect {
    
    func getCompanies(parameters: [String: Any]?, additionalHeaderElements: [String: String]?) {
        
        MBAlamofireServerConnect.requestOperationManager.hitMultipartApi("/candidate/employers", images: nil, method: HTTPMethod.post, parameters: parameters, headerElements: additionalHeaderElements, success: {(url : String? , responseObject: CommonResponse<[CompanyModel]>?) in
            
            let serverResponse = self.processAfterMakingRequestWithParameters(responseObject)
            if let serverResponse = serverResponse as? CommonResponse<[CompanyModel]> {
                self.completionBlockWithSuccess?(url, (serverResponse.status == 1) ? 1 : 0, self.requestTag, (serverResponse.data, serverResponse.pagination), serverResponse.message)
            }
            else {
                self.completionBlockWithFailure?(nil, self.requestTag, nil)
            }
        }, failure: {(url : String?, error: Error?) in
            self.completionBlockWithFailure?(nil, self.requestTag, error)
        })
    }
    
    func getCompanyDetail(parameters: [String: Any]?, additionalHeaderElements: [String: String]?) {
        
        let _ = MBAlamofireServerConnect.requestOperationManager.hitApi("/candidate/employer-details", method: .get, parameters: parameters, encoding: URLEncoding.default, headerElements: additionalHeaderElements, success: {(url : String? , responseObject: CommonResponse<[CompanyModel]>?) in
            
            let serverResponse = self.processAfterMakingRequestWithParameters(responseObject)
            if let serverResponse = serverResponse as? CommonResponse<[CompanyModel]> {
                self.completionBlockWithSuccess?(url, (serverResponse.status == 1) ? 1 : 0, self.requestTag, (serverResponse.data, serverResponse.pagination), serverResponse.message)
            }
            else {
                self.completionBlockWithFailure?(nil, self.requestTag, nil)
            }
        }, failure: {(url : String?, error: Error?) in
            self.completionBlockWithFailure?(nil, self.requestTag, error)
        })
    }
    
    func followUnFollowdCompany(isFollow: Bool, parameters: [String: Any]?, additionalHeaderElements: [String: String]?) {
        
        let _ = MBAlamofireServerConnect.requestOperationManager.hitApi((isFollow ? "/job/follow-company" : "/job/unfollow-company"), method: .post, parameters: parameters, encoding: URLEncoding.default, headerElements: additionalHeaderElements, success: {(url : String? , responseObject: CommonResponse<[JobModel]>?) in
            
            let serverResponse = self.processAfterMakingRequestWithParameters(responseObject)
            if let serverResponse = serverResponse as? CommonResponse<[JobModel]> {
                self.completionBlockWithSuccess?(url, (serverResponse.status == 1) ? 1 : 0, self.requestTag, (serverResponse.data, serverResponse.pagination), serverResponse.message)
            }
            else {
                self.completionBlockWithFailure?(nil, self.requestTag, nil)
            }
        }, failure: {(url : String?, error: Error?) in
            self.completionBlockWithFailure?(nil, self.requestTag, error)
        })
    }
    
}
