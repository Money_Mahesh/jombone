//
//  PreferenceServices.swift
//  Jombone
//
//  Created by dev139 on 02/05/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import UIKit
import Alamofire

class PreferenceServices: MBAlamofireServerConnect {

    func getPreferences(parameters: [String: Any]?, additionalHeaderElements: [String: String]?) {
        
        let _ = MBAlamofireServerConnect.requestOperationManager.hitApi("/candidate/get-preferences", method: .get, parameters: parameters, encoding: JSONEncoding.default, headerElements: additionalHeaderElements, success: {(url : String? , responseObject: CommonResponse<PreferencesModel>?) in
            
            let serverResponse = self.processAfterMakingRequestWithParameters(responseObject)
            if let serverResponse = serverResponse as? CommonResponse<PreferencesModel> {
                self.completionBlockWithSuccess?(url, (serverResponse.status == 1) ? 1 : 0, self.requestTag, (serverResponse.data, serverResponse.pagination), serverResponse.message)
            }
            else {
                self.completionBlockWithFailure?(nil, self.requestTag, nil)
            }
        }, failure: {(url : String?, error: Error?) in
            self.completionBlockWithFailure?(nil, self.requestTag, error)
        })
    }
    
    
    func setPreferredShift(parameters: [String: Any]?, additionalHeaderElements: [String: String]?) {
        
        let _ = MBAlamofireServerConnect.requestOperationManager.hitMultipartApi("/candidate/set-preferred-shift", images: nil, method: .post, parameters: parameters, headerElements: additionalHeaderElements, success: {(url : String? , responseObject: CommonResponse<String>?) in
            
            let serverResponse = self.processAfterMakingRequestWithParameters(responseObject)
            if let serverResponse = serverResponse as? CommonResponse<String> {
                self.completionBlockWithSuccess?(url, (serverResponse.status == 1) ? 1 : 0, self.requestTag, (serverResponse.data, serverResponse.pagination), serverResponse.message)
            }
            else {
                self.completionBlockWithFailure?(nil, self.requestTag, nil)
            }
        }, failure: {(url : String?, error: Error?) in
            self.completionBlockWithFailure?(nil, self.requestTag, error)
        })
    }
    
    func removePreference(parameters: [String: Any]?, additionalHeaderElements: [String: String]?) {
        
        let _ = MBAlamofireServerConnect.requestOperationManager.hitApi(("/candidate/remove-preference"), method: .post, parameters: parameters, encoding: JSONEncoding.default, headerElements: additionalHeaderElements, success: {(url : String? , responseObject: CommonResponse<String>?) in
            
            let serverResponse = self.processAfterMakingRequestWithParameters(responseObject)
            if let serverResponse = serverResponse as? CommonResponse<String> {
                self.completionBlockWithSuccess?(url, (serverResponse.status == 1) ? 1 : 0, self.requestTag, (serverResponse.data, serverResponse.pagination), serverResponse.message)
            }
            else {
                self.completionBlockWithFailure?(nil, self.requestTag, nil)
            }
        }, failure: {(url : String?, error: Error?) in
            self.completionBlockWithFailure?(nil, self.requestTag, error)
        })
    }
    
    func setPreferredJobType(parameters: [String: Any]?, additionalHeaderElements: [String: String]?) {
        
        let _ = MBAlamofireServerConnect.requestOperationManager.hitMultipartApi("/candidate/set-preferred-jobtype", images: nil, method: .post, parameters: parameters, headerElements: additionalHeaderElements, success: {(url : String? , responseObject: CommonResponse<String>?) in
            
            let serverResponse = self.processAfterMakingRequestWithParameters(responseObject)
            if let serverResponse = serverResponse as? CommonResponse<String> {
                self.completionBlockWithSuccess?(url, (serverResponse.status == 1) ? 1 : 0, self.requestTag, (serverResponse.data, serverResponse.pagination), serverResponse.message)
            }
            else {
                self.completionBlockWithFailure?(nil, self.requestTag, nil)
            }
        }, failure: {(url : String?, error: Error?) in
            self.completionBlockWithFailure?(nil, self.requestTag, error)
        })
    }
    
}
