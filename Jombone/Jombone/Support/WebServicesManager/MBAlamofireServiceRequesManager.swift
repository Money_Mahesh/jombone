//
//  MBAlamofireServiceRequesManager.swift
//  Jombone
//
//  Created by Money Mahesh on 13/08/18.
//  Copyright © 2018 tbsl. All rights reserved.
//

import Foundation
import Alamofire
import CoreTelephony
import UIKit

class MBAlamofireServiceRequesManager: SessionManager {
    
    enum NetworkError: Error {
        case invalidURL(String)
        case parsingError(String)
        case TokenExpire(String)
    }
    
    override func request(_ url: URLConvertible, method: HTTPMethod, parameters: Parameters?, encoding: ParameterEncoding, headers: HTTPHeaders?) -> DataRequest {
        
        if parameters != nil && (parameters is Dictionary<String, String>) {
            return super.request(url, method: method, parameters: parameters, encoding: encoding, headers: headers)
        }
        
        return super.request(url, method: method, parameters: parameters, encoding: encoding, headers: headers)
    }
    
    func hitApi<T>(_ URLString: String!, method: HTTPMethod, parameters: Parameters?, encoding: ParameterEncoding = URLEncoding.default, headerElements: [String: String]?, success: ((String?, CommonResponse<T>?) -> Void)!, failure: ((String?, Error?) -> Void)!) -> DataRequest? {
        
        var apiURL: String
        if URLString.starts(with: "http") {
            apiURL = URLString
        } else {
            guard let completeURL = URL(string: (BASE_URL as URL).absoluteString + URLString) else {
                failure(URLString, NetworkError.invalidURL("INVALID url for making API hit."))
                return nil
            }
            
            apiURL = completeURL.absoluteString
        }
        
        var requestHeaders = SessionManager.defaultHTTPHeaders
        
        if let additionalHeaderElements = headerElements {
            for (key, value) in additionalHeaderElements {
                requestHeaders[key] = (value)
            }
        }
        
        let request = self.request(apiURL, method: method, parameters: parameters, encoding: encoding, headers: requestHeaders)
        
        apiURL = (request.task?.originalRequest?.url?.absoluteString)!
        
        return request
            .validate(statusCode: 200..<201)
            .responseData { response in
                
                if let authtoken = response.response?.allHeaderFields["X-Auth-Token"] as? String {
                    UserDataManager.shared.token = authtoken
                }
                
                switch response.result {
                case .success:
                    if let value = response.result.value {
                        do {
                            let response = try JSONDecoder().decode(CommonResponse<T>.self, from: value)
                            success(apiURL, response)
                        } catch let error  {
                            failure(apiURL, error)
                        }
                    }else {
                        failure(apiURL, NetworkError.parsingError("Error while parsing response"))
                    }
                case .failure(let error):
                    
                    MBAlamofireInternetConnectivityManager.checkForServerError(error)
                    failure(apiURL, error)
                }
        }
    }
    
    func hitMultipartApi<T>(_ URLString: String!, images: [UIImage]?, method: HTTPMethod, parameters: Parameters?, headerElements: [String: String]?, success: ((String?, CommonResponse<T>?) -> Void)!, failure: ((String?, Error?) -> Void)!) {
        
        var apiURL: String
        if URLString.starts(with: "http") {
            apiURL = URLString
        } else {
            guard let completeURL = URL(string: (BASE_URL as URL).absoluteString + URLString) else {
                failure(URLString, NetworkError.invalidURL("INVALID url for making API hit."))
                return
            }
            
            apiURL = completeURL.absoluteString
        }
        
        var requestHeaders = SessionManager.defaultHTTPHeaders
        
        if let additionalHeaderElements = headerElements {
            for (key, value) in additionalHeaderElements {
                requestHeaders[key] = (value)
            }
        }
        
        var imagesData: [Data]?
        
        if let _images = images {
            imagesData = [Data]()
            for image in _images {
                let imgData = image.jpegData(compressionQuality: 0.2)!
                imagesData?.append(imgData)
            }
        }
        
        upload(multipartFormData: { multipartFormData in
            // import image to request
            var index = 0
            if let _imagesData = imagesData {
                for imageData in _imagesData {
                    multipartFormData.append(imageData, withName: "imageFiles", fileName: "\(Date().ticks).jpg", mimeType: "image/jpg")
                    
                    index += 1
                }
            }
            
            if let parameters = parameters {
                for (key, value) in parameters {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
            }
        }, to: apiURL,
           method: method,
           headers: requestHeaders,
           encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseData(completionHandler: { response in
                    
                    if let authtoken = response.response?.allHeaderFields["X-Auth-Token"] as? String {
                        UserDataManager.shared.token = authtoken
                    }
                    
                    switch response.result {
                    case .success:
                        if let value = response.result.value {
                            do {
                                let response = try JSONDecoder().decode(CommonResponse<T>.self, from: value)
                                success(apiURL, response)
                            } catch let error  {
                                failure(apiURL, error)
                            }
                        }else {
                            failure(apiURL, NetworkError.parsingError("Error while parsing response"))
                        }
                    case .failure(let error):
                        
                            MBAlamofireInternetConnectivityManager.checkForServerError(error)
                            failure(apiURL, error)
                    }
                })
            case .failure(let error):
                MBAlamofireInternetConnectivityManager.checkForServerError(error)
                failure(apiURL, error)
            }
            
        })
    }
    
    func hitUploadApi<T>(_ URLString: String, fileDetail: (keysName: [String], filesName: [String], filesData: [Data])?, method: HTTPMethod, parameters: Parameters?, headerElements: [String: String]?, success: ((String?, CommonResponse<T>?) -> Void)!, failure: ((String?, Error?) -> Void)!) {
        
        var apiURL: String
        if URLString.starts(with: "http") {
            apiURL = URLString
        } else {
            guard let completeURL = URL(string: (BASE_URL as URL).absoluteString + URLString) else {
                failure(URLString, NetworkError.invalidURL("INVALID url for making API hit."))
                return
            }
            
            apiURL = completeURL.absoluteString
        }
        
        var requestHeaders = SessionManager.defaultHTTPHeaders
        
        if let additionalHeaderElements = headerElements {
            for (key, value) in additionalHeaderElements {
                requestHeaders[key] = (value)
            }
        }
        
        upload(multipartFormData: { multipartFormData in
            // import image to request
            
            if let _fileDetail = fileDetail {
                for index in 0..<_fileDetail.filesData.count {
                    multipartFormData.append(_fileDetail.filesData[index], withName: _fileDetail.keysName[index], fileName: _fileDetail.filesName[index], mimeType: "*/*")
                }
            }
            
            if let parameters = parameters {
                for (key, value) in parameters {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
            }
        }, to: apiURL,
           method: method,
           headers: requestHeaders,
           encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseData(completionHandler: { response in
                    
                    if let authtoken = response.response?.allHeaderFields["X-Auth-Token"] as? String {
                        UserDataManager.shared.token = authtoken
                    }
                    
                    switch response.result {
                    case .success:
                        if let value = response.result.value {
                            do {
                                let response = try JSONDecoder().decode(CommonResponse<T>.self, from: value)
                                success(apiURL, response)
                            } catch let error  {
                                failure(apiURL, error)
                            }
                        }else {
                            failure(apiURL, NetworkError.parsingError("Error while parsing response"))
                        }
                    case .failure(let error):
                        
                        MBAlamofireInternetConnectivityManager.checkForServerError(error)
                        failure(apiURL, error)
                    }
                })
            case .failure(let error):
                MBAlamofireInternetConnectivityManager.checkForServerError(error)
                failure(apiURL, error)
            }
            
        })
    }
    
    func download(_ URLString: String!, fileName: String, method: HTTPMethod, parameters: Parameters?, headerElements: [String: String]?, success: ((String?, CommonResponse<URL>?) -> Void)!, failure: ((String?, Error?) -> Void)!) -> DownloadRequest? {
        
        var apiURL: String
        if URLString.starts(with: "http") {
            apiURL = URLString
        } else {
            guard let completeURL = URL(string: (BASE_URL as URL).absoluteString + URLString) else {
                failure(URLString, NetworkError.invalidURL("INVALID url for making API hit."))
                return nil
            }
            
            apiURL = completeURL.absoluteString
        }
        
        var requestHeaders = SessionManager.defaultHTTPHeaders
        if let additionalHeaderElements = headerElements {
            for (key, value) in additionalHeaderElements {
                requestHeaders[key] = (value)
            }
        }
        requestHeaders["Accept"] = "*/*"
        
//        var fileName: String = "Jombone File.pdf"
//        if let _fileName = parameters?["fileName"] as? String {
//            fileName = _fileName + ".pdf"
//        }
        
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            documentsURL.appendPathComponent(fileName)
            return (documentsURL, [.createIntermediateDirectories, .removePreviousFile])
        }
        
        let downloadRequest = download(apiURL, method: method, parameters: parameters, encoding: URLEncoding.default, headers: requestHeaders, to: destination)
        
        return downloadRequest
            .validate(statusCode: 200..<4000)
            .response(completionHandler: { (response) in
                
                if let error = response.error {
                    
                    MBAlamofireInternetConnectivityManager.checkForServerError(error)
                    failure(apiURL, error)
                }
                else {
                
                    if let url = response.destinationURL {
                        success(apiURL, CommonResponse<URL>(status: 1, message: "downloaded", errors: nil, data: url, pagination: nil))
                    }
                    else {
                        success(apiURL, CommonResponse<URL>(status: 0, message: "Not able to downloaded", errors: "failure", data: nil, pagination: nil))
                    }
                }
            })
    }
    
    func hitNativeApi<T>(_ URLString: String!, method: HTTPMethod, parameters: String?, encoding: ParameterEncoding = URLEncoding.default, headerElements: [String: String]?, success: ((String?, CommonResponse<T>?) -> Void)!, failure: ((String?, Error?) -> Void)!) {
        
        guard let parameters = parameters else {
            return
        }
        
        //        do{
        let data = parameters.data(using: .utf8)
        var request = URLRequest(url: URL(string: BASE_URL.absoluteString + URLString)!)
        request.httpMethod = method.rawValue
        request.httpBody = data
        
        if let httpAdditionalHeaders = MBAlamofireServerConnect.requestOperationManager.session.configuration.httpAdditionalHeaders {
            for (key, value) in httpAdditionalHeaders {
                if let value = value as? String, let key = key as? String {
                    request.addValue(value, forHTTPHeaderField: key)
                }
            }
        }
        
        for (key, value) in SessionManager.defaultHTTPHeaders {
            request.addValue(value, forHTTPHeaderField: key)
        }
        
        if let additionalHeaderElements = headerElements {
            for (key, value) in additionalHeaderElements {
                request.addValue(value, forHTTPHeaderField: key)
            }
        }
                
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: request, completionHandler: {data, response, error -> Void in
            
            
            DispatchQueue.main.sync {
                if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                    
                    if statusCode == 200 {
                        if let responseUrl = response?.url, let value = data {
                            do {
                                let responseObj = try JSONDecoder().decode(CommonResponse<T>.self, from: value)
                                success(responseUrl.absoluteString, responseObj)
                            } catch let error  {
                                failure(responseUrl.absoluteString, error)
                            }
                        }else {
                            failure(nil, NetworkError.parsingError("Error while parsing response"))
                        }
                    }
                    else {

                        failure(response?.url?.absoluteString, nil)
                    }
                    
                }
                else {
                    
                    if let error = error {
                        MBAlamofireInternetConnectivityManager.checkForServerError(error)
                        failure(request.url?.absoluteString, error)
                    }
                    else {
                        failure(request.url?.absoluteString, nil)
                        
                    }
                    
                }
            }
            
            print(error)
            
            print(response)
        })
        
        task.resume()
    }
}
