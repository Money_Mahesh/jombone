//
//  MBAlamofireServerConnect.swift
//  Jombone
//
//  Created by Money Mahesh on 13/08/18.
//  Copyright © 2018 tbsl. All rights reserved.
//

import Alamofire

/**
 HTTP Request Type Enum
 
 - GET_REQUEST:  To mark request of HTTP GET type
 - POST_REQUEST: To mark request of HTTP POST type
 */
enum HTTP_REQUEST_TYPE{
    case get_REQUEST
    case post_REQUEST
    case put_REQUEST
}

/**
 Enum to define Web Request Priorty
 
 - LOW_PRIORITY:      To mark request as Low Priority
 - MODERATE_PRIORITY: To mark request as Moderate Priority
 - HIGH_PRIORITY:     To mark request as High Priority
 */
enum REQUEST_PRIORITY{
    case low_PRIORITY
    case moderate_PRIORITY
    case high_PRIORITY
}

typealias CompletionBlockType  = ((_ url : String?, _ statusCode: Int, _ requestType: String?, _ response: (Any?, Any?), _ message: String?)->Void)
typealias FailureBlockType  = ((_ url : String?, _ requestType: String?, _ error: Error?)->Void)

/**
 Base class for all web services to control server communication.
 */
class MBAlamofireServerConnect{
    /**
     Http Request Type for the web service call
     */
    var httpRequestType: HTTP_REQUEST_TYPE?
    
    /**
     Request Identifier to distinguish various requests
     */
    var requestTag: String
    
    /**
     Priorty for request
     */
    var requestPriority: REQUEST_PRIORITY
    
    var completionBlockWithSuccess: CompletionBlockType?
    var completionBlockWithFailure: FailureBlockType?
    
    static var requestOperationManagerSingleton : MBAlamofireServiceRequesManager!
    
    static var requestOperationManager: MBAlamofireServiceRequesManager!{
        get
        {
            if requestOperationManagerSingleton == nil
            {
                let configuration = URLSessionConfiguration.default
                setCommonRequestHeaders(configuration: configuration)
                
                requestOperationManagerSingleton = MBAlamofireServiceRequesManager(configuration: configuration)
            }
            
            return requestOperationManagerSingleton
        }
        set{
            
        }
    }
    
    /**
     Place common request headers in each and every request made to server
     */
    static func setCommonRequestHeaders(configuration: URLSessionConfiguration) {
        
        configuration.httpAdditionalHeaders = [
            "Accept": "application/json",
            //"Content-Type": "application/json",
            "platform": "iOS"]
        
        setLoginTokenInRequestHeader(configuration: configuration)
    }
    
    init(httpRequestType: HTTP_REQUEST_TYPE, requestTag: String, requestPriority: REQUEST_PRIORITY){
        self.httpRequestType = httpRequestType
        self.requestTag = requestTag
        self.requestPriority = requestPriority
    }
    
    init(requestTag: String, requestPriority: REQUEST_PRIORITY){
        self.requestTag = requestTag
        self.requestPriority = requestPriority
    }
    
    func processBeforeMakingRequestWithParameters(_ parameters:Dictionary<String,String?>!) -> Dictionary<String, String>!{
        if parameters == nil {
            return nil;
        }
        
        var sanitizeRequestParameters = Dictionary<String, String>()
        for (key, value) in parameters {
            if value != nil{
                sanitizeRequestParameters[key] = value
            }
        }
        
        return sanitizeRequestParameters
    }
    
    /**
     Process response after making the web request
     @param response The server response object of the current request
     @returns response object after performing required processing
     */
    func processAfterMakingRequestWithParameters<T>(_ response: T) -> T?{
        return response
    }
    
}
