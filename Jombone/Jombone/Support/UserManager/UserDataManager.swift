//
//  UserDataManager.swift
//  Jombone
//
//  Created by Money Mahesh on 13/08/18.
//  Copyright © 2018 Money Mahesh. All rights reserved.
//

import Foundation

func setLoginTokenInRequestHeader(configuration: URLSessionConfiguration) {
    
    let config = configuration
    guard var headers = config.httpAdditionalHeaders else {
        return
    }
    if let token = UserDataManager.shared.token {
        headers[HEADER_TOKEN_KEY] = token
        config.httpAdditionalHeaders = headers
    }
    else {
        headers.removeValue(forKey: HEADER_TOKEN_KEY)
        config.httpAdditionalHeaders = headers
    }
    
    MBAlamofireServerConnect.requestOperationManagerSingleton =  MBAlamofireServiceRequesManager(configuration: config)
}

class UserDataManager {
    
    static var shared = UserDataManager()
    
    private enum User: String {
        case token = "token"
        case role = "role"
        case profileDetail = "profileDetail"
        case signUpCompleted = "signUpCompleted"
        case countDetail = "countDetail"
    }
    
    private var _countDetail: CountModel?
    var countDetail: CountModel? {
        get{
            if _countDetail == nil {
                
                if let data = Utility.fetchCustomObjectFromDefault(User.countDetail.rawValue) as? Data {
                    if let detail = try? JSONDecoder().decode(CountModel.self, from: data) {
                        _countDetail = detail
                    }
                }
            }
            return _countDetail
        }
        set{
            _countDetail = newValue
            if let value = newValue {
                
                if let encoded = try? JSONEncoder().encode(value) {
                    Utility.saveCustomObjectInDefault(User.countDetail.rawValue, object: encoded)
                }
            } else {
                Utility.removeParameterFromUserDefault(User.countDetail.rawValue)
            }
            NotificationCenter.default.post(name: NSNotification.Name.NotificationCountUpdated, object: nil)
        }
    }
    
    private var _profileDetail: ProfileDetail?
    var profileDetail: ProfileDetail? {
        get{
            if _profileDetail == nil {
                
                if let data = Utility.fetchCustomObjectFromDefault(User.profileDetail.rawValue) as? Data {
                    if let detail = try? JSONDecoder().decode(ProfileDetail.self, from: data) {
                        _profileDetail = detail
                    }
                }
            }
            return _profileDetail
        }
        set{
            _profileDetail = newValue
            if let value = newValue {
                
                if let encoded = try? JSONEncoder().encode(value) {
                    Utility.saveCustomObjectInDefault(User.profileDetail.rawValue, object: encoded)
                }
            } else {
                Utility.removeParameterFromUserDefault(User.profileDetail.rawValue)
            }
            
            NotificationCenter.default.post(name: NSNotification.Name.UserProfileUpdated, object: nil)
            ProfileViewModel.hitScoreApi()
        }
    }
    
    private var _token: String?
    var token : String?{
        get{
            if _token == nil {
                _token = Utility.fetchStringFromDefault(User.token.rawValue)
            }
            return _token
        }
        set{
            _token = newValue
            if let value = newValue {
                Utility.saveStringInDefault(User.token.rawValue, value: value)
                setLoginTokenInRequestHeader(configuration: MBAlamofireServerConnect.requestOperationManager.session.configuration)
            } else {
                Utility.removeParameterFromUserDefault(User.token.rawValue)
            }
            NotificationCenter.default.post(name: NSNotification.Name.UserLoginStatusUpdated, object: nil)
        }
    }
    
//    private var _role: String?
    var role : String {
        get{
            return "1"
        }
    }
    
    var isLogin : Bool?{
        get{
            if isSignUpCompleted == false || token == nil {
                return false
            }
            else {
                return true
            }
        }
    }
    
    var isSignUpCompleted : Bool! {
        get{
            return Utility.fetchBoolFromDefault(User.signUpCompleted.rawValue)
        }
        set{
            Utility.saveBoolInDefault(User.signUpCompleted.rawValue, value: newValue)
        }
    }
    
    func logout() {
        token = nil
        profileDetail = nil
        isSignUpCompleted = false
        countDetail = nil
        
        setLoginTokenInRequestHeader(configuration: MBAlamofireServerConnect.requestOperationManager.session.configuration)
    }
}
