//
//  GooglePlacesUtility.swift
//  Jombone
//
//  Created by dev139 on 24/04/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import Alamofire

// Specify the place data types to return.

class GooglePlaceUtility: NSObject {
    
    class func getFetchPlaceIdFromLatLng(
        latitude lat: Double,
        longitude long: Double,
        completion: ((_ message: String?)->())? = nil,
        success:  ((_ placeDetail: GooglePlaceDetail, _ message: String?)->())? = nil,
        failure: ((_ message: String?)->())? = nil) {
        
        let viewModel = AutoCompleteViewModel(type: AutoComplete.google(GooglePlaceFor: GooglePlaceFor.address), isOffline: true)
        
        let placeIdUrl = GOOGLE_NEARBY_BASE_URL + "location=\(lat),\(long)" + "&radius=1" + "&key=\(GOOGLE_PLACE_API_KEY)"
        
        let getURL = URL(string: placeIdUrl)!
        var getRequest = URLRequest(url: getURL, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 30.0)
        getRequest.httpMethod = "GET"
        getRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        getRequest.setValue("application/json", forHTTPHeaderField: "Accept")
        
        URLSession.shared.dataTask(with: getRequest, completionHandler: { (data, response, error) -> Void in
            if error != nil {
                print("GET Request: Communication error: \(error!)")
                failure?(nil)
                return
            }
            if data != nil {
                DispatchQueue.main.async(execute: {
                    do {
                        let decoder = JSONDecoder()
                        if let place_id = try decoder.decode(GooglePlaceModel.self, from: data!).results?.first?.place_id {
                            print(place_id)
                            
                            viewModel.fetchPlaceDetail(placeId: place_id, title: nil, completion: completion, success: { (message) in
                                
                                if let placeDetail = viewModel.placeDetail {
                                    success?(placeDetail, message)
                                    return
                                }
                                failure?(nil)
                            }, failure: failure)
                        }
                        else {
                            failure?(nil)
                        }
                    } catch {
                        failure?(nil)
                        print("Unable to parse JSON response")
                    }
                    return
                })
            } else {
                DispatchQueue.main.async(execute: {
                    failure?(nil)
                    print("Received empty response.")
                })
            }
        }).resume()
        
        print("Maps place id URL :\(placeIdUrl)")
    }
    
}

