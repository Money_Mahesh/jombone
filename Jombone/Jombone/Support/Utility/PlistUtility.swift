//
//  PlistUtility.swift
//  Jombone
//
//  Created by Money Mahesh on 04/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation

class PlistUtiliy {
    
    // FETCH AND PARSE PLIST
    class func parsePlistAndReturnPlistModel(_ plistName : String) -> [PlistModel] {
        
        var myArray: NSMutableArray = NSMutableArray()
        if let path = Bundle.main.path(forResource: plistName, ofType: "plist") {
            myArray = NSMutableArray(contentsOfFile: path)!
        }
        
        var plistArray = [PlistModel]()
        plistArray = myArray.map({ (obj) -> PlistModel in
            return PlistModel(data: obj)
        })
        return plistArray
    }
    
    class func parsePlistAndReturnDictionary(_ plistName : NSString) -> NSMutableArray {
        var myArray: NSMutableArray = NSMutableArray()
        if let path = Bundle.main.path(forResource: plistName as String, ofType: "plist") {
            myArray = NSMutableArray(contentsOfFile: path)!
        }
        return myArray
    }
}
