//
//  Utility.swift
//  Jombone
//
//  Created by Money Mahesh on 13/08/18.
//  Copyright © 2018 Money Mahesh. All rights reserved.
//

import UIKit

class Utility: NSObject {
    
    enum UIUserInterfaceIdiom : Int
    {
        case unspecified
        case phone
        case pad
    }
    
    struct ScreenSize
    {
        static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
        static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
        static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    }
    
    struct DeviceType {
        
        static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
        static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
        static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
        static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
        static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
    }
    
    static var countryCodeArray: [[String: String]]! {
        get {
            let countryTypeDict =  PlistUtiliy.parsePlistAndReturnDictionary("CountryCode") as! [[String: String]]
            return countryTypeDict
        }
    }
    
    class func heightForView(_ text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0,  width: width, height: CGFloat.greatestFiniteMagnitude))

        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }
    
    class func widthForView(_ text:String, font:UIFont, height:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: CGFloat.greatestFiniteMagnitude, height: height))
        label.numberOfLines = 1
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.width
    }
    
    class func sizeForView(_ text:String, font:UIFont) -> CGSize{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: CGFloat.greatestFiniteMagnitude, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.size
    }
    
    //SHOW ALERT VIEW
    class func showAlert(_ title:String,message:String, delegate:UIViewController,cancelButtonTitle:String, handler: ((UIAlertAction) -> Swift.Void)? = nil) -> Void {
        
        let alert = UIAlertController(title:title, message:message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title:cancelButtonTitle, style: UIAlertAction.Style.default, handler: handler))
        delegate.present(alert, animated: true, completion: nil);
    }
        
    // Used to return any random index from the total count
    class func getRandomNoFunction(_ randomArrayCount : Int) ->Int {
        
        let randomIndex = Int(arc4random_uniform(UInt32(randomArrayCount)))
        return randomIndex
    }

    //MARK:- UIViewController Presented or pushed
    class func isModal(_ viewController : UIViewController) -> Bool {
        if((viewController.presentingViewController) != nil) {
            return true
        }
        
        if(viewController.presentingViewController?.presentedViewController == viewController) {
            return true
        }
        
        if viewController.navigationController?.presentingViewController?.presentedViewController == viewController.navigationController {
            return true
        }
        if viewController.tabBarController?.presentingViewController is UITabBarController {
            return true
        }
        return false
    }
    
       // ADD SUFFIX FOR FLOOR
    class func addSuffixToNumber(_ num :Int)-> String {
        var suffix = ""
        let ones = num % 10
        let tens = (num/10) % 10
        
        if (tens == 1) {
            suffix = "th";
        } else if (ones == 1){
            suffix = "st"
        } else if (ones == 2){
            suffix = "nd"
        } else if (ones == 3){
            suffix = "rd"
        } else {
            suffix = "th"
        }
        return suffix
        
    }
    
    //MARK: Utilities For UserDefaults
    //SAVE INT IN USER DEFAULT
    class func saveIntInDefault(_ key:String,value:Int) {
        UserDefaults.standard.set(value, forKey: key);
        UserDefaults.standard.synchronize();
    }
    
    //FETCH INT FROM USER DEFAULT
    class func fetchIntFromDefault(_ key:String) -> Int? {
        return UserDefaults.standard.integer(forKey: key);
    }
    
    //SAVE STRING IN USER DEFAULT
    class func saveStringInDefault(_ key:String,value:String) {
        UserDefaults.standard.set(value, forKey: key);
        UserDefaults.standard.synchronize();
    }
    
    //FETCH STRING FROM USER DEFAULT
    class func fetchStringFromDefault(_ key:String) -> String? {
        return UserDefaults.standard.object(forKey: key) as? String
    }
    
    //SAVE CUSTOM OBJECT IN USER DEFAULT
    class func saveCustomObjectInDefault(_ key:String, object: Any?) {
        if let obj = object {
            
            let encodedData = NSKeyedArchiver.archivedData(withRootObject: obj)
            UserDefaults.standard.set(encodedData, forKey: key)
            UserDefaults.standard.synchronize()
        }
    }
    
    //FETCH CUSTOM OBJECT FROM USER DEFAULT
    class func fetchCustomObjectFromDefault(_ key:String)->Any? {
        if let decoded  = UserDefaults.standard.object(forKey: key) as? Foundation.Data {
            return NSKeyedUnarchiver.unarchiveObject(with: decoded)
        }
        return nil
    }
    
    //SAVE BOOL IN USER DEFAULT
    class func saveBoolInDefault(_ key:String,value:Bool) {
        UserDefaults.standard.set(value, forKey: key);
        UserDefaults.standard.synchronize();
    }
    //FETCH BOOL FROM USER DEFAULT
    class func fetchBoolFromDefault(_ key:String)->Bool {
        return UserDefaults.standard.bool(forKey: key);
    }
    
    
    //REMOVE NSUSER DEFAULT
    class func removeParameterFromUserDefault(_ key:String) {
        UserDefaults.standard.removeObject(forKey: key);
        UserDefaults.standard.synchronize();
    }
    
//    //MARK:- ADD GA TRACKING
//    class func addGATrackingForScreenName(_ screenName : String) {
//        let tracker = GAI.sharedInstance().tracker(withTrackingId: "UA-41692793-2")
//        tracker?.set(kGAIScreenName, value: screenName)
//        let builder = GAIDictionaryBuilder.createScreenView()
//        tracker?.send(((builder?.build())! as NSDictionary) as! [AnyHashable: Any])
//    }
//
//    class func addGATrackingForEventName(_ catagoryName : String, actionName: String, labelName: String) {
//
//        // May return nil if a tracker has not already been initialized with a property
//        // ID.
//        let tracker = GAI.sharedInstance().tracker(withTrackingId: "UA-41692793-2")
//        let eventTracker: NSObject = GAIDictionaryBuilder.createEvent(
//            withCategory: catagoryName,
//            action: actionName,
//            label: labelName,
//            value: nil).build()
//        tracker?.send(eventTracker as! [AnyHashable: Any])
//    }
    
    
    //MARK:- Date Formatter
    class func getMonthNo(month: String)-> Int {
        
        let df = DateFormatter()
        df.dateFormat = "LLL"  // if you need 3 letter month just use "LLL"
        if let date = df.date(from: month) {
            let month  = NSCalendar.current.component(.month, from: date)
            
            return month
        }
        else {
            return -1
        }
    }
    
    class func getMonthFromNo(monthNo: String)-> String {
        
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "M"
        
        if let date = dateFormatter.date(from: monthNo) {
            let dateFormatter: DateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM"
            
            let monthStr = dateFormatter.string(from: date)
            return monthStr
        }
        return ""
    }
    
    class func getTodayNoInWeek()-> Int {
        
        let todayDate = Date()
        let myCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        let myComponents = myCalendar.components(.weekday, from: todayDate as Date)
        let weekDay = myComponents.weekday
        return weekDay!
    }
    
    class func changeStringToDate(dateStr: String?, inputFormat: String?) -> Date {
        
        if dateStr == nil {
            return Date()
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = inputFormat
        
        let date = dateFormatter.date(from: dateStr!)
        return date! as Date
    }
    
    class func getMobileTimeFormat() -> Int {
        let locale = NSLocale.current
        let formatter : String = DateFormatter.dateFormat(fromTemplate: "j", options:0, locale:locale)!
        if formatter.contains("a") {
            return 12
        } else {
            return 24
        }
    }
    
    class func changeDateFormat(dateStr: String?, inputFormat: String, outputFormat: String) -> String {
        if dateStr == nil || dateStr == "" {
            return ""
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = inputFormat
        
        let date = dateFormatter.date(from: dateStr!)
        dateFormatter.dateFormat = outputFormat
        
        return dateFormatter.string(from: date!) + " "
    }
    
    class func changeDateFormatFromDate(date: Date?, inputFormat: String, outputFormat: String) -> String {
        
        let dateStr = changeDateToString(date: date, withFormat: inputFormat)
        if dateStr == "" {
            return ""
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = inputFormat
        
        let date = dateFormatter.date(from: dateStr)
        dateFormatter.dateFormat = outputFormat
        
        return dateFormatter.string(from: date!) + " "
    }
    
    class func changeDateToString(date: Date?, withFormat outputFormat: String) -> String {
        if date == nil {
            return ""
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = outputFormat
        
        print(dateFormatter.string(from: date! as Date))
        return dateFormatter.string(from: date! as Date) + " "
    }
    
    class func daysBetweenDate(_ startDate: Date, endDate: Date) -> Int {
        let calendar = Calendar.current
        
        let components = (calendar as NSCalendar).components([.day], from: startDate, to: endDate, options: [])
        
        return components.day!
    }
    class func getPostedAgo(_ days:Int) -> String{
        
            let noOfDays = days
            var noOfUnit = 0
            var unit = ""
            if (noOfDays >= 365) {
                noOfUnit = noOfDays / 365;
                unit = "year";
            } else if (noOfDays >= 30) {
                noOfUnit = noOfDays / 30;
                unit = "month";
            } else if (noOfDays >= 7) {
                noOfUnit = noOfDays / 7;
                unit = "week";
            } else if (noOfDays > 0) {
                noOfUnit = noOfDays;
                unit = "day";
            }
            if (noOfUnit > 1) {
                unit = unit + "s";
            }
            return String(noOfUnit) + " " + unit + " " + "ago";
    }
    
    //MARK:- Currency formatter
    class func convertIntToIndianCurrency(price: NSNumber) -> String? {
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.string(from: price)
        
        formatter.locale = NSLocale(localeIdentifier: "en_IN") as Locale?
        formatter.minimumFractionDigits = 0
        return formatter.string(from: price)
    }
    
    //MARK:- Convert Obj to Json
    class func convertToJson(dictionaryOrArray: Any) -> String {
        
        do {
            
            //Convert to Data
            let jsonData = try! JSONSerialization.data(withJSONObject: dictionaryOrArray, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            //Convert back to string. Usually only do this for debugging
            if let JSONString = String(data: jsonData, encoding: String.Encoding.utf8) {
                print(JSONString)
                return JSONString
            }
            
            //In production, you usually want to try and cast as the root data structure. Here we are casting as a dictionary. If the root object is an array cast as [AnyObject].
            var _ = try JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String: AnyObject]
            
            return ""
        } catch let error as NSError {
            print(error.description)
            return ""
        }
    }
    
    class func convertToObject(jsonString: String) -> Any? {
        
        // convert String to NSData
        guard let data: Data = jsonString.data(using: String.Encoding.utf8) else {
            return nil
        }
        
        do {
            return try JSONSerialization.jsonObject(with: data, options: [])
        } catch let myJSONError {
            print(myJSONError)
            return nil
        }
    }
    
    //MARK:- Convert String to Attributed String
    class func convertStringToAttributedString(from string: String) -> NSMutableAttributedString {
        
        let selectedAttributedStr = NSMutableAttributedString(string: string)
        
        selectedAttributedStr.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(hexFromString: "9e9e9e", alpha: 1.0), range: NSRange(location: 0, length: 6))
        
        selectedAttributedStr.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(hexFromString: "424242", alpha: 1.0), range: NSRange(location: 7, length: string.count - 7))
        
        
        return selectedAttributedStr
    }
    
    class func getDateFromTimeStampWithFormat(timestamp : Double?, format : String) -> String? {
        
        guard let _timestamp = timestamp else {
            return nil
        }
        
        let date = Date(timeIntervalSince1970:_timestamp / 1000)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        
        let dateString = dateFormatter.string(from: date)
        
        return dateString
        
    }
}

