//
//  DataValidationUtility.swift
//  Jombone
//
//  Created by Money Mahesh on 13/08/18.
//  Copyright © 2018 Money Mahesh. All rights reserved.
//

import UIKit

enum fieldType {
    case onlyCharacterText
    case text
    case Integer
    case phoneNo
    case email
    case alphanumeric
    case password
    case otp
    case sin
    case accountNo
    case length(length: Int)
    case numberRange(min: Int?, max: Int?)
}

class DataValidationUtility: NSObject {
    
    static var shared = DataValidationUtility()
    func validateData(fields: [(data: Any?, name: String, customMessage: String?)], type: (generic: fieldType?, specific: [String: fieldType]?)) -> String? {
        
        for (index, field) in fields.enumerated() {
            
            if let specific = type.specific, specific.keys.contains(String(index)) {
                let type = specific[String(index)]
                if let errorMsg = validateField(field: field, type: type!) {
                    return errorMsg
                }
            }
            else {
                if let errorMsg = validateField(field: field, type: type.generic!) {
                    return errorMsg
                }
            }
        }
        return nil
    }
    
    func validateField(field: (data: Any?, name: String, customMessage: String?), type: fieldType) -> String? {
        
        switch type {
        case .onlyCharacterText:
            guard let data = field.data as? String, data.count != 0 else {
                return AppError.empty(field: field.name).message
            }
            
            return data.isText ? nil : (field.customMessage ?? AppError.valid(field: field.name).message)
            
        case .accountNo:
            guard let data = field.data as? String, data.count != 0 else {
                return AppError.empty(field: field.name).message
            }
            
            return data.count < ACCOUNT_NO_MIN_LENGTH ? (field.customMessage ?? AppError.valid(field: field.name).message) : nil
            
        case .email:
            guard let data = field.data as? String, data.count != 0 else {
                return AppError.empty(field: field.name).message
            }
            
            return data.isEmail ? nil : (field.customMessage ?? AppError.valid(field: field.name).message)
        
        case .sin:
            guard let data = field.data as? String, data.count != 0 else {
                return AppError.empty(field: field.name).message
            }
            
            return data.isSinNo ? nil : (field.customMessage ?? AppError.valid(field: field.name).message)
            
        case .phoneNo:
            guard let data = field.data as? String, data.count != 0 else {
                return AppError.empty(field: field.name).message
            }
            
            guard let validData = field.data as? String, validData.first != "0", validData.first != "1" else {
                return AppError.valid(field: field.name).message
            }
            
            return data.count != PHONE_NO_MIN_LENGTH ? (field.customMessage ?? AppError.valid(field: field.name).message) : nil
            
        case .password:
            guard let data = field.data as? String, data.count != 0 else {
                return AppError.empty(field: field.name).message
            }
            
            return data.isPassword ? nil : (field.customMessage ?? AppError.valid(field: field.name).message)
//            return data.isPassword < PASSWORD_MIN_LENGTH ? AppError.min(field: field.name, length: PASSWORD_MIN_LENGTH).message : nil
            
        case .Integer:
            guard let _ = field.data as? Int else {
                return AppError.empty(field: field.name).message
            }
            
            return nil
            
        case .otp:
            guard let data = field.data as? String, data.count != 0 else {
                return AppError.empty(field: field.name).message
            }
            
            return data.count != OTP_LENGTH ? (field.customMessage ?? AppError.valid(field: field.name).message) : nil
            
        case .length(let length):
            guard let data = field.data as? String, data.count != 0 else {
                return AppError.empty(field: field.name).message
            }
            
            return data.count != length ? (field.customMessage ?? AppError.valid(field: field.name).message) : nil
            
        case .numberRange(let min, let max):
            guard let data = field.data as? String, data.count != 0 else {
                return AppError.empty(field: field.name).message
            }
            
            if data.isNo {
                if let number = Int(data) {
                    if let _min = min, number < _min {
                        return  (field.customMessage ?? AppError.valid(field: field.name).message)
                    }
                    else if let _max = max, number > _max {
                        return  (field.customMessage ?? AppError.valid(field: field.name).message)
                    }
                    else {
                        return  nil
                    }
                }
            }
            return (field.customMessage ?? AppError.valid(field: field.name).message)
        
            
        default:
            guard let data = field.data as? String, data.count != 0 else {
                return AppError.empty(field: field.name).message
            }
            return nil
        }
    }
    
    func validateString(_ data: String?, fieldName: String, customMessage: String? = nil) -> (errorMsg: String?, value: String?) {
        let errorMsg =  DataValidationUtility.shared.validateData(
            fields: [(data: data, name: fieldName, customMessage: customMessage)],
            type: (generic: nil,
                   specific: ["0": fieldType.text]))
        return (errorMsg, ((errorMsg == nil) ? data : nil))
    }
    
    func validateOnlyCharacterString(_ data: String?, fieldName: String, customMessage: String? = nil) -> (errorMsg: String?, value: String?) {
        let errorMsg =  DataValidationUtility.shared.validateData(
            fields: [(data: data, name: fieldName, customMessage: customMessage)],
            type: (generic: nil,
                   specific: ["0": fieldType.onlyCharacterText]))
        return (errorMsg, ((errorMsg == nil) ? data : nil))
    }
    
    func validatePhoneNo(_ data: String?, fieldName: String = "mobile number", customMessage: String? = nil) -> (errorMsg: String?, value: String?) {
        
        let errorMsg = DataValidationUtility.shared.validateData(
            fields: [(data: data, name: fieldName, customMessage: customMessage)],
            type: (generic: nil,
                   specific: ["0": fieldType.phoneNo]))
        return (errorMsg, ((errorMsg == nil) ? data : nil))
    }
    
    func validateName(firstName: String?, lastName: String?, customMessage: String? = nil) -> (firstName: (errorMsg: String?, value: String?), lastName: (errorMsg: String?, value: String?)) {
        return (validateOnlyCharacterString(firstName, fieldName: "first name", customMessage: customMessage), validateOnlyCharacterString(lastName, fieldName: "last name", customMessage: customMessage))
    }
    
    func validateSize(_ sizeInByte: Int64?, fileDetail: FileDetail?, fileTypeAllowed: [FileType], fieldName: String = "document", customMessage: String? = nil) -> (message: String?, value: FileDetail?) {
        
        guard let size = sizeInByte, let _fileDetail = fileDetail else {
            return ("Please upload " + fieldName, nil)
        }
        
        if fileTypeAllowed.filter({$0.rawValue.lowercased() == _fileDetail.extention.lowercased()}).count == 0 {
            let extensions = fileTypeAllowed.map { (obj: FileType) -> String in
                return obj.rawValue
            }.joined(separator: ", ")
            return ("Only files of type " + extensions + " are allowed", nil)
        }
        
        if size < _fileDetail.data.count {
            
            let bcf = ByteCountFormatter()
            bcf.allowedUnits = [.useMB] // optional: restricts the units to MB only
            bcf.countStyle = .file
            let string = bcf.string(fromByteCount: Int64(5000000))
            
            return ("You can not upload " + fieldName + " larger than " + string, nil)
        }
        return (nil, fileDetail)
    }
}
