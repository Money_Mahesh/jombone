//
//  CustomTextfieldView.swift
//  Jombone
//
//  Created by Money Mahesh on 03/09/16.
//  Copyright © 2016 tbsl. All rights reserved.
//



import UIKit

class CustomTextfield: UITextField {
    
    var rightViewBtn: UIButton?
    var MaxLength: Int = -1
    
    var placeholderColor: UIColor = UIColor(red: 154.0/255.0, green: 154.0/255.0, blue: 154.0/255.0, alpha: 1.0) {
        didSet {
            attributedPlaceholder = NSAttributedString(
                string: (placeholder ?? ""),
                attributes: [NSAttributedString.Key.foregroundColor: placeholderColor])
        }
    }
    
    var leftPadding: CGFloat = 0
    var rightPadding: CGFloat = 0
    var initialSpaceAllowed = true
    var bottomBorder: (height: CGFloat, color: UIColor)?
    var bottomLine: UIView?
    
    var leftImage: String? {
        didSet {
            
            if let image = leftImage {
                leftViewMode = .always
                
                let imgView = UIImageView(image: UIImage(named: image))
                imgView.contentMode = .left
                leftView = imgView
                leftView?.frame.size = CGSize(width: (leftView?.frame.size.width)! + leftPadding, height: (leftView?.frame.size.height)!)
            }
        }
    }
    
    var rightImage: String? {
        didSet {
            
            if let image = rightImage {
                
                rightViewMode = .always
                let imageView = UIImageView(image: UIImage(named: image))
                imageView.frame = CGRect(x: -rightPadding, y: imageView.frame.origin.y, width: (imageView.frame.size.width + rightPadding), height: imageView.frame.size.height)
                imageView.contentMode = .center
                rightView = imageView
                
                rightViewBtn = UIButton(frame: imageView.frame)
                rightViewBtn?.isUserInteractionEnabled = false
                rightViewBtn?.isExclusiveTouch = true
                rightView?.addSubview(rightViewBtn!)
                
                rightView?.frame.size = CGSize(width: (rightView?.frame.size.width)! + rightPadding, height: (rightView?.frame.size.height)!)
            }
        }
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)

        if leftImage == nil || leftImage!.count == 0 {
            leftViewMode = .always
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: leftPadding, height: self.frame.height))
            leftView = paddingView
        }
        
        if rightImage == nil || rightImage?.count == 0 {
            rightViewMode = .always
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: rightPadding, height: self.frame.height))
            rightView = paddingView
        }
        
        if let bottomBorder = bottomBorder, bottomLine == nil {
            bottomLine = UIView(frame: CGRect(x: 0, y: (rect.height - bottomBorder.height), width: rect.width, height: bottomBorder.height), backgroundColor: bottomBorder.color)
            self.addSubview(bottomLine!)
        }
    }
    
    func avoidInitialSpace(_ ch: String) -> Bool {
        if (ch == " " && self.text == "") || (MaxLength != -1 && ch != "" && self.text?.count == MaxLength) {
            return false
        }
        else {
            return true
        }
    }
}


protocol CustomTextfieldViewDelegate: NSObjectProtocol {
    func textFieldDidBeginEditing(_ identifier: String, textField: CustomTextfield)
    func textFieldDidEndEditing(_ identifier: String, textField: CustomTextfield)
    func textFieldDidChange(_ identifier: String, textField: CustomTextfield)
    func textField(_ textField: CustomTextfieldView, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    
    func pickerViewSelected(identifier: String, textfield: CustomTextfield, indexes: [Int])
    func datePickerViewSelected(identifier: String, textfield: CustomTextfield)
    func pickerComponentRelation(identifier: String, pickerView: UIPickerView, didSelectRow row: Int, component: Int)
}

extension CustomTextfieldViewDelegate {
    
    func textFieldDidBeginEditing(_ identifier: String, textField: CustomTextfield) {}
    func textFieldDidEndEditing(_ identifier: String, textField: CustomTextfield) {}
    func textFieldDidChange(_ identifier: String, textField: CustomTextfield) {}
    func textField(_ textField: CustomTextfieldView, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {return true}
    
    func pickerViewSelected(identifier: String, textfield: CustomTextfield, indexes: [Int]) {}
    func datePickerViewSelected(identifier: String, textfield: CustomTextfield) {}
    func pickerComponentRelation(identifier: String, pickerView: UIPickerView, didSelectRow row: Int, component: Int) {}
}

@IBDesignable class CustomTextfieldView: CustomView {
    
    enum BorderLineStyle {
        case Bottom
        case All
        case none
    }
    
    enum InputType {
        case Keyboard(type: UIKeyboardType)
        case Picker
        case DatePicker
    }
    
    @IBInspectable var Identifier: String! = "CustomTextfieldView"
    @IBOutlet weak var labelForPlaceholder: UILabel!
    @IBOutlet weak var viewForBottomLine: UIView!
    
    @IBOutlet weak var imgViewForLeft: UIImageView!
    @IBOutlet weak var imgViewForRight: UIImageView!
    @IBOutlet weak var stackViewContainer: UIStackView!

    @IBOutlet weak var mainStackView: UIStackView!
    @IBOutlet weak var stackViewForBottomLine: UIStackView!
    @IBOutlet weak var stackViewForPlaceholder_Textfield: UIStackView!
    @IBOutlet weak var lblForError: UILabel!
    
    @IBOutlet weak var viewForTextfield: UIView?
    @IBOutlet weak var constForTextfieldMinHeight: NSLayoutConstraint?

    @IBOutlet weak var constForTop: NSLayoutConstraint!
    @IBOutlet weak var constForBottomLineHeight: NSLayoutConstraint!
    @IBInspectable var InterSpacing: CGFloat = 10 {
        didSet {
            stackViewForPlaceholder_Textfield.spacing = InterSpacing
        }
    }
    
    @IBInspectable var BotomLineInterSpacing: CGFloat = 10 {
        didSet {
            mainStackView.spacing = InterSpacing
        }
    }
    
    @IBInspectable var TextFieldMinHeight: CGFloat = 0 {
        didSet {
            setTextFieldMinHeight()
        }
    }
    
    
    //MARK:-
    //MARK:- Right Btn
    enum BtnStyle {
        case title(selectedDetail: (title: String, color: UIColor), deSelectedDetail: (title: String, color: UIColor), font: UIFont)
        case imageName(selected: String, deSelected: String)
    }
    @IBOutlet weak var rightBtn: UIButton!

    
    //MARK:-
    //MARK:- Textfield
    @IBOutlet weak var textfield: CustomTextfield!

    @IBInspectable var MaxLength: Int = -1 {
        didSet {
            textfield.MaxLength = MaxLength
        }
    }
    
    @IBInspectable var enableSecureEntry: Bool = false {
        didSet {
            textfield.isSecureTextEntry = enableSecureEntry
        }
    }
    
    @IBInspectable var avoidInitialSpace: Bool = false {
        
        didSet {
            
            if avoidInitialSpace == true {
                textfield.delegate = self
            }
        }
    }
    
    @IBInspectable var textColor: UIColor? {
        didSet {
            textfield.textColor = textColor
        }
    }
    
    @IBInspectable var leftImage: String? {
        didSet {
//            if let image = leftImage {
//                textfield.leftPadding = 13.0
//                textfield.leftImage = image
//            }
            setLeftRightImage()
        }
    }
    
    @IBInspectable var rightImage: String? {
        didSet {
//            if let image = rightImage {
//                textfield.rightImage = image
//            }
            setLeftRightImage()
        }
    }
    
    var errorMsg: String? {
        didSet {
            lblForError.text = errorMsg
            if let _errorMsg = errorMsg, _errorMsg.count > 0 {
                lblForError.isHidden = false
                viewForBottomLine.backgroundColor = errorLineColor
            }
            else {
                lblForError.isHidden = true
                viewForBottomLine.backgroundColor = textfield.isFirstResponder ? activeLineColor : inActiveLineColor
            }
        }
    }
    
    func setStyleForError(color: UIColor = UIColor(hexFromString: "#f93550"), font: UIFont = UIFont.rubikRegularFontOfSize(12)) {
        
        lblForError.textColor = color
        lblForError.font = font
    }
    
    weak var textfieldViewDelegate: CustomTextfieldViewDelegate? {
        didSet {
            if textfieldViewDelegate != nil {
                textfield.delegate = self
            }
        }
    }

    func setText(text: String?) {
        textfield.text = text
        endEditingWithAnimation(false)
    }
    
    func resetValue() {
        textfield.text = nil
        endEditingWithAnimation(false)
    }
    
    var textAlignment: NSTextAlignment = .left {
        didSet {
            textfield.textAlignment = textAlignment
        }
    }
    
    var text: String? {
        set {
            textfield.text = newValue
            endEditingWithAnimation(false)
        }
        get {
            return textfield.text
        }
    }
    
    var inputType: InputType = .Keyboard(type: .default) {
        didSet {
            switch inputType {
            case let .Keyboard(type):
                textfield.keyboardType = type
            case .Picker:
                textfield.inputView = pickerView
            case .DatePicker:
                textfield.inputView = datePicker
            }
        }
    }
    
    //MARK:-
    //MARK:- Animation
    @IBInspectable var animateTitle: Bool = true {
        
        didSet {
            
            if animateTitle == true {
                labelForPlaceholder.isHidden = false
                textfield.delegate = self
                labelForPlaceholder.text = placeholder
            }
            else {
                
                labelForPlaceholder.isHidden = true
                self.textfield.isHidden = false
                self.textfield.placeholder = placeholder
                self.textfield.placeholderColor = inActivePlaceholderColor
            }
        }
    }
   
    //MARK:- Placeholder
    @IBInspectable var placeholder: String? {
        didSet {
            
            if animateTitle == true {
                labelForPlaceholder.isHidden = false
                labelForPlaceholder.text = placeholder
            }
            else {
                labelForPlaceholder.isHidden = true
                self.textfield.placeholder = placeholder
                self.textfield.placeholderColor = inActivePlaceholderColor
            }
        }
    }
    
    @IBInspectable var PlaceholderSidePadding: CGFloat = 0 {
        didSet {
            stackViewContainer.layoutMargins = UIEdgeInsets(top: 0, left: PlaceholderSidePadding, bottom: 0, right: PlaceholderSidePadding)
        }
    }
    
    @IBInspectable var inActivePlaceholderColor: UIColor = UIColor(red: 80.0/255.0, green: 80.0/255.0, blue: 80.0/255.0, alpha: 0.9){
        didSet {
            self.labelForPlaceholder.textColor = inActivePlaceholderColor
        }
    }
    
    @IBInspectable var activePlaceholderColor: UIColor = UIColor(red: 117.0/255.0, green: 117.0/255.0, blue: 117.0/255.0, alpha: 0.8)
    
    private func setTextFieldMinHeight() {
        if TextFieldMinHeight != 0 {
            constForTextfieldMinHeight?.isActive = true
            constForTextfieldMinHeight?.constant = TextFieldMinHeight
        }
        else {
            constForTextfieldMinHeight?.isActive = false
        }
    }
    
    func setLeftRightImage() {
        
        if let image = leftImage {
            self.imgViewForLeft.image = UIImage(named: image)
            self.imgViewForLeft.isHidden = false
        }
        
        if let image = rightImage {
            self.imgViewForRight.image = UIImage(named: image)
            self.imgViewForRight.isHidden = false
        }
    }
    
    override func xibLoaded(data: Any) {
        textfield.tintColor = activePlaceholderColor
        textfield.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        setBorder(active: false)
        setTextFieldMinHeight()
        setLeftRightImage()
    }
    
    override func xibLoaded() {
        textfield.tintColor = activePlaceholderColor
        textfield.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        setBorder(active: false)
        setTextFieldMinHeight()
        setLeftRightImage()
    }
    
    func setAsFirstResponder() {
        textfield.becomeFirstResponder()
    }
    
    //Border
    private func setBorder(active: Bool) {
        
        self.viewForTextfield?.BorderColor = UIColor.clear
        self.viewForTextfield?.BorderWidth = 0
        stackViewForBottomLine.isHidden = true
        
        switch borderStyle {
        case .All:
            self.viewForTextfield?.BorderColor = inActiveLineColor
            self.viewForTextfield?.BorderWidth = BottomLineHeight
            
        case .Bottom:
            stackViewForBottomLine.isHidden = false
            
        case .none:
            break
        }
    }
    
    @IBInspectable var BottomLinePadding: CGFloat = 0 {
        didSet {
            stackViewForBottomLine.layoutMargins = UIEdgeInsets(top: 0, left: BottomLinePadding, bottom: 0, right: 0)
        }
    }
    
    @IBInspectable var BottomLineHeight: CGFloat = 1 {
        didSet {
            constForBottomLineHeight.constant = BottomLineHeight
        }
    }
    
    @IBInspectable var inActiveLineColor: UIColor = UIColor(red: 80.0/255.0, green: 80.0/255.0, blue: 80.0/255.0, alpha: 0.9) {
        didSet {
            viewForBottomLine.backgroundColor = inActiveLineColor
        }
    }
    @IBInspectable var activeLineColor: UIColor = UIColor(red: 255.0/255.0, green: 157.0/255.0, blue: 30.0/255.0, alpha: 0.8)
    @IBInspectable var errorLineColor: UIColor = UIColor(hexFromString: "#f93550")

    
    var borderStyle: BorderLineStyle = .Bottom {
        didSet {
            setBorder(active: false)
        }
    }
    
    var isPlaceholderShiftUp = false
    
    lazy private var datePicker = UIDatePicker()
    
    //MARK:- Picker
    lazy private var pickerView = UIPickerView()
    lazy private var pickerDataArray: [[String]] = [[String]]()
    private var pickerSelectedIndexes: [Int] {
        set {
            for componentIndex in 0..<newValue.count {
                pickerView.selectRow(newValue[componentIndex], inComponent: componentIndex, animated: false)
            }
        }
        get {
            var indexes = [Int]()
            for componentIndex in 0..<pickerView.numberOfComponents {
                indexes.append(pickerView.selectedRow(inComponent: componentIndex))
            }
            return indexes
        }
    }
    
    private var placeholderInActiveFont: UIFont! = UIFont(name: "HelveticaNeue", size: 12)! {
        didSet {
            if !isPlaceholderShiftUp {
                labelForPlaceholder.font = placeholderInActiveFont
            }
        }
    }
    
    private var pickerSeletedTextFormat: ((_ pickerSelectedText: [Any]?, _ indexes: [Int], _ inputType: InputType)->(String)) = { (pickerSelectedText, indexes, inputType) in
        
        switch inputType {
        case .Picker:
            if let textArray = pickerSelectedText as? [String] {
                return textArray.joined(separator: " ")
            }
            return ""
            
        case .DatePicker:
            if let dateArray = pickerSelectedText as? [Date] {
                
                let textArray = dateArray.map { (date) -> String in
                    return Utility.changeDateToString(date: date, withFormat: "dd-MM-yyyy")
                }
                return textArray.joined(separator: " ")
            }
            return ""
            
        default:
            return ""
        }
    }
    
    private lazy var placeholderActiveFont: UIFont! = self.placeholderInActiveFont
    func setFont(font: UIFont = UIFont(name: "HelveticaNeue", size: 12)!, placeholderInActiveFont: UIFont? = nil, placeholderActiveFont: UIFont? = nil) {
        
        textfield.font = font
        self.placeholderInActiveFont = placeholderInActiveFont ?? font
        self.placeholderActiveFont = placeholderActiveFont ?? self.placeholderInActiveFont
    }
    
    func dataSource(identifier: String? = nil, placeholder: String, text: String?, delegate: CustomTextfieldViewDelegate? = nil) {
        
        if let _identifier =  identifier {
            Identifier = _identifier
        }
        
        labelForPlaceholder.text = placeholder
        setText(text: text)
        
        textfield.tintColor = activePlaceholderColor
        textfieldViewDelegate = delegate
    }
    
    func pickerDataSource1(identifier: String? = nil, array: [[String]], currentValues: [String]?, defaultValueIndex: [Int]? = nil, delegate: CustomTextfieldViewDelegate? = nil, pickerSeletedTextFormat: ((_ pickerSelectedText: [Any]?, _ indexes: [Int], _ inputType: InputType)->(String))? = nil) {
        
        if let _identifier =  identifier {
            Identifier = _identifier
        }
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.backgroundColor = UIColor.white
        
        pickerDataArray = array
        inputType = .Picker
        
        if let pickerSeletedTextFormat = pickerSeletedTextFormat {
            self.pickerSeletedTextFormat = pickerSeletedTextFormat
        }
        
        pickerSelectedIndexes = defaultValueIndex ?? [Int]()
        if let _curretValue = currentValues {
            var selectedValueIndex = [Int]()
            for (index1, values) in array.enumerated() {
                for (index, value) in values.enumerated() {
                    if _curretValue[index1] == value {
                        selectedValueIndex.append(index)
                        break
                    }
                }
                if selectedValueIndex.count < index1 {
                    selectedValueIndex.append(0)
                }
            }
            pickerSelectedIndexes = selectedValueIndex
        }
        else if let _defaultValueIndex = defaultValueIndex, _defaultValueIndex.count > 0 {
            pickerSelectedIndexes = _defaultValueIndex
            textfieldViewDelegate?.pickerViewSelected(identifier: Identifier, textfield: textfield, indexes: _defaultValueIndex)
            
        }
        
        text = pickerSelectedText()
        textfield.tintColor = UIColor.clear
        textfieldViewDelegate = delegate
    }
    
    func pickerDataSource(identifier: String? = nil, array: [[String]], selectedIndex: [Int]? = nil, delegate: CustomTextfieldViewDelegate? = nil, pickerSeletedTextFormat: ((_ pickerSelectedText: [Any]?, _ indexes: [Int], _ inputType: InputType)->(String))? = nil) {
        
        if let _identifier =  identifier {
            Identifier = _identifier
        }
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.backgroundColor = UIColor.white

        pickerDataArray = array
        inputType = .Picker
        
        if let pickerSeletedTextFormat = pickerSeletedTextFormat {
            self.pickerSeletedTextFormat = pickerSeletedTextFormat
        }
        
        pickerSelectedIndexes = selectedIndex ?? [0]
        if selectedIndex != nil {
            text = pickerSelectedText()
        }
        
        textfield.tintColor = UIColor.clear
        textfieldViewDelegate = delegate
    }
    
    func datePickerDataSource(identifier: String? = nil, currentDate: Date?, minDate: Date? = nil, maxDate: Date? = nil, mode: UIDatePicker.Mode = .date, delegate: CustomTextfieldViewDelegate? = nil, pickerSeletedTextFormat: ((_ pickerSelectedText: [Any]?, _ indexes: [Int], _ inputType: InputType)->(String))? = nil) {
        
        datePicker.maximumDate = maxDate
        datePicker.minimumDate = minDate
        datePicker.datePickerMode = mode
        datePicker.date = currentDate ?? minDate ?? maxDate ?? Date()
        datePicker.backgroundColor = UIColor.white
        inputType = .DatePicker
        if let pickerSeletedTextFormat = pickerSeletedTextFormat {
            self.pickerSeletedTextFormat = pickerSeletedTextFormat
        }
        
        if currentDate != nil {
            text = pickerSelectedText()
        }
        else {
            resetValue()
        }
        textfield.tintColor = UIColor.clear
        textfieldViewDelegate = delegate
    }
    
    func setMaxDate(date: Date) {
        datePicker.maximumDate = date
    }
    
    func currentDate() -> Date {
        return datePicker.date
    }
    
    func setMinDate(date: Date) {
        datePicker.minimumDate = date
    }
    
    func setRightBtnStyle(style: BtnStyle, target: Any?, action: Selector) {
        
        rightBtn.isHidden = false
        
        switch style {
        case .title(let selectedDetail, let deSelectedDetail, let font):
            rightBtn.setTitle(deSelectedDetail.title, for: UIControl.State.normal)
            rightBtn.setTitle(selectedDetail.title, for: UIControl.State.selected)
            rightBtn.setTitleColor(deSelectedDetail.color, for: UIControl.State.normal)
            rightBtn.setTitleColor(selectedDetail.color, for: UIControl.State.selected)

            rightBtn.titleLabel?.font = font
            
        case .imageName(let selected, let deSelected):
            rightBtn.setImage(UIImage(named: deSelected), for: UIControl.State.normal)
            rightBtn.setImage(UIImage(named: selected), for: UIControl.State.selected)
        }
        
        rightBtn.addTarget(target, action: action, for: UIControl.Event.touchUpInside)
    }
    
    //MARK:- Data Source Method
    var currentDataSource: (dataSource: [[String]], selectedIndex: [Int]) {
        get {
            return (self.pickerDataArray, selectedIndex: self.pickerSelectedIndexes)
        }
    }
    
    func updatePickerDataArray(dataSource: [[String]], selectedIndex: [Int]) {
        self.pickerDataArray = dataSource
        self.pickerSelectedIndexes = selectedIndex
    }
    
    //MARK:- Animation Method
    let ANIMATION_DURATION = 0.2
    @IBAction func tapGestureAction(_ sender: UITapGestureRecognizer) {
        enableEditing()
    }
    
    private func enableEditing(_ makeFirstResponder: Bool = true) {
        
        updateBottomLineColor()
        guard animateTitle else {
            return
        }

        UIView.animate(withDuration: ANIMATION_DURATION, animations: {
            self.textfield.isHidden = false
            self.labelForPlaceholder.textColor = self.activePlaceholderColor
            self.labelForPlaceholder.font = self.placeholderActiveFont
            self.layoutIfNeeded()
        }, completion: {[weak self] (status) in
            if makeFirstResponder {
                self?.textfield.becomeFirstResponder()
                //Text field is just made first responder and in this case textfield did begin editing will not be called
                self?.setTintColor()
            }
        })
    }
    
    func setTintColor() {
        DispatchQueue.main.asyncAfter(deadline: (DispatchTime.now() + 0.2), execute: {
            switch self.inputType {
            case .Keyboard( _):
                self.textfield.tintColor = self.activePlaceholderColor
            default:
                self.textfield.tintColor = UIColor.clear
            }
        })
    }
    
    private func endEditingWithAnimation(_ animate: Bool) {
        
        updateBottomLineColor()
        guard animateTitle else {
            return
        }
        
        func liftPlaceholder() {
            self.textfield.isHidden = false
            self.labelForPlaceholder.textColor = self.activePlaceholderColor
            self.labelForPlaceholder.font = self.placeholderActiveFont
        }
        
        func dropPlaceholder() {
            self.textfield.isHidden = true
            self.labelForPlaceholder.textColor = self.inActivePlaceholderColor
            self.labelForPlaceholder.font = self.placeholderInActiveFont
        }
        
        if animate {
            UIView.animate(withDuration: ANIMATION_DURATION) {
                if let text = self.textfield.text, text.count > 0 {
                    liftPlaceholder()
                }
                else {
                    dropPlaceholder()
                    self.layoutIfNeeded()
                }
            }
        }
        else {
            if let text = textfield.text, text.count > 0 {
                liftPlaceholder()
            }
            else {
                dropPlaceholder()
                self.layoutIfNeeded()
            }
        }
    }
    
    func updateBottomLineColor() {

        if textfield.isFirstResponder {
            self.viewForBottomLine.backgroundColor = self.activeLineColor
        }
        else {
            self.viewForBottomLine.backgroundColor = self.inActiveLineColor
        }
    }
    
    private func pickerSelectedText() -> String? {
        
        var pickerSelectedTextArray: [Any]?
        switch inputType {
        case .Picker:
            var index = 0
            pickerSelectedTextArray =  pickerSelectedIndexes.map({ (pickerSelectedIndex) -> String in
                if pickerDataArray.count > index && pickerDataArray[index].count > pickerSelectedIndex {
                    let str = pickerDataArray[index][pickerSelectedIndex]
                    index += 1
                    return str
                }
                return ""
            })
            
        case .DatePicker:
            pickerSelectedTextArray = [datePicker.date]
            
        default:
            break
        }
        
        return pickerSeletedTextFormat(pickerSelectedTextArray, pickerSelectedIndexes, inputType)
    }
}

extension CustomTextfieldView: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        errorMsg = nil
        //As textfield become first responder
        self.setTintColor()
        enableEditing(false)
        textfieldViewDelegate?.textFieldDidBeginEditing(Identifier, textField: self.textfield)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textfieldViewDelegate?.textField(self, shouldChangeCharactersIn: range, replacementString: string) ?? true {
            if avoidInitialSpace {
                return textfield.avoidInitialSpace(string)
            }
        }
        return textfieldViewDelegate?.textField(self, shouldChangeCharactersIn: range, replacementString: string) ?? true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        switch inputType {
        case .Picker:
            
            textfield.text = pickerSelectedText()
            textfieldViewDelegate?.pickerViewSelected(identifier: Identifier, textfield: textfield, indexes: pickerSelectedIndexes)
            
        case .DatePicker:
            textfield.text = pickerSelectedText()
            textfieldViewDelegate?.datePickerViewSelected(identifier: Identifier, textfield: textfield)
            
        default:
            break
        }
        
        endEditingWithAnimation(true)
        textfieldViewDelegate?.textFieldDidEndEditing(Identifier, textField: self.textfield)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        textfieldViewDelegate?.textFieldDidChange(Identifier, textField: self.textfield)
    }
}

extension CustomTextfieldView: UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return pickerDataArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerDataArray[component].count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerDataArray[component][row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        textfieldViewDelegate?.pickerComponentRelation(identifier: Identifier, pickerView: pickerView, didSelectRow: row, component: component)
    }
}


