//
//  FilterParameterObject.swift
//  Jombone
//
//  Created by Money Mahesh on 28/02/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation

//MARK: - Generice Search Parameter Class
open class FilterParameterObject: NSObject, NSCopying {
    
    //MARK: - Properties
    var displayValue : String!
    var valueCode : String!
    var index : Int?
    
//    //MARK: - Class Methods
//    class func createSearchParameter(_ plistName : String, rootName : String, indexArray : [Int]) -> [PlistModel]! {
//        let plistModelObjArray: [PlistModel] = PlistUtiliy.parsePlistAndReturnPlistModel(plistName)
//
//        let filterParameterObjects = [PlistModel]()
//        for currentIndex in indexArray {
//            let searchParameters = plistModelObjArray[currentIndex]
//            let filterParameterObject = FilterParameterObject()
//
//            filterParameterObject.displayValue = (searchParameters.object(forKey: "displayValue") as! String)
//            filterParameterObject.valueCode = (searchParameters.object(forKey: "valueCode") as! String)
//            filterParameterObject.index = currentIndex
//            filterParameterObjects.add(filterParameterObject)
//        }
//
//        return filterParameterObjects
//    }
    
    //MARK: - Default Constructor
    required override public init () {
        super.init()
        
        displayValue = ""
        valueCode = ""
        index = -1
    }
    
    //MARK: - Custom Constructor
    convenience init(displayValue : String, valueCode : String, index : Int) {
        self.init()
        
        self.displayValue = displayValue
        self.valueCode = valueCode
        self.index = index
    }
    
    //MARK: Description
    override open var description: String {
        var _description : String
        if self.displayValue != nil{
            _description = self.displayValue!
        }else{
            _description = ""
        }
        
        if self.valueCode != nil{
            _description += "|\(self.valueCode!)"
        }else{
            _description += "|"
        }
        
        if self.index != nil{
            _description += "|\(self.index!)"
        }else{
            _description += "|0"
        }
        
        return _description
    }
    
    public func copy(with zone: NSZone? = nil) -> Any { // <== NSCopying
        // *** Construct "one of my current class". This is why init() is a required initializer
        let theCopy = type(of: self).init()
        
        theCopy.displayValue = self.displayValue
        theCopy.valueCode = self.valueCode
        theCopy.index = self.index
        return theCopy
    }
}
