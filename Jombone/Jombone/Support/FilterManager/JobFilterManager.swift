//
//  JobFilterManager.swift
//  Jombone
//
//  Created by Money Mahesh on 27/02/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation

class JobFilterManager: FilterManager {
    
    static var sharedInstance = JobFilterManager()

    var location : GooglePlaceDetail?
    var company : AutoCompleteCardViewModelProtocol?
    var jobTitle : AutoCompleteCardViewModelProtocol?

    fileprivate var _payRate : [PlistModel]?
    var payRate : [PlistModel]!{
        set {
            
            _payRate = newValue
            
            if _payRate == nil{
                _payRate = [PlistModel]()
            }
        }
        get {
            if _payRate == nil {
                //Default Value
                _payRate = PlistModel.createSearchParameter("PayRate", rootName : "Pay Rate", indexArray : [])
            }
            return _payRate
        }
    }
    
    fileprivate var _jobShift : [PlistModel]?
    var jobShift : [PlistModel]!{
        set {
            
            _jobShift = newValue
            
            if _jobShift == nil{
                _jobShift = [PlistModel]()
            }
                    }
        get {
            if _jobShift == nil {
                //Default Value
                _jobShift = PlistModel.createSearchParameter("JobShift", rootName : "Shift", indexArray : [])
            }
            return _jobShift
        }
    }
    
    fileprivate var _jobType : [PlistModel]?
    var jobType : [PlistModel]!{
        set {
            
            _jobType = newValue
            
            if _jobType == nil{
                _jobType = [PlistModel]()
            }
        }
        get {
            if _jobType == nil {
                //Default Value
                _jobType = PlistModel.createSearchParameter("JobType", rootName : "JobType", indexArray : [])
            }
            return _jobType
        }
    }
    
    fileprivate var _preferredJobShift : [JobType]?
    var preferredJobShift : [JobType]!{
        set {
            
            _preferredJobShift = newValue
            
            if _preferredJobShift == nil{
                _preferredJobShift = [JobType]()
            }
        }
        get {
            if _preferredJobShift == nil {
                //Default Value
                //_preferredJobShift = PlistModel.createSearchParameter("JobShift", rootName : "Shift", indexArray : [])
            }
            return _preferredJobShift
        }
    }
    
    fileprivate var _preferredJobType : [JobType]?
    var preferredJobType : [JobType]!{
        set {
            
            _preferredJobType = newValue
            
            if _preferredJobType == nil{
                _preferredJobType = [JobType]()
            }
        }
        get {
            if _preferredJobType == nil {
                //Default Value
                //_jobType = PlistModel.createSearchParameter("JobType", rootName : "JobType", indexArray : [])
            }
            return _preferredJobType
        }
    }
    
    fileprivate var _jobCategory : [PlistModel]?
    var jobCategory : [PlistModel]!{
        set {
            
            _jobCategory = newValue
            
            if _jobCategory == nil{
                _jobCategory = [PlistModel]()
            }
        }
        get {
            if _jobCategory == nil {
                //Default Value
                _jobCategory = PlistModel.createSearchParameter("JobCategory", rootName : "Job Category", indexArray : [])
            }
            return _jobCategory
        }
    }
    
    func reset() {
        location = nil
        company = nil
        jobTitle = nil
        _payRate = nil
        _jobShift = nil
        _jobCategory = nil
    }
    
    override func createUrl() -> [String: Any] {
        
        let _payRateString = payRate.map { (obj: PlistModel) -> String in
            return obj.valueCode
        }
        
        let _shift = jobShift.map { (obj: PlistModel) -> String in
            return obj.valueCode
        }
        
        let _jobCategory = jobCategory.map { (obj: PlistModel) -> String in
            return obj.valueCode
        }
        
        let dict: [String: Any] = [
            "jobTitle": jobTitle?.title ?? "",
            "jobTitleId": jobTitle?.encryptedId ?? "",
            "location": location?.locality ?? "",
            "companyName": company?.title ?? "",
            "employerId": company?.encryptedId ?? "",
            "payRate": _payRateString,
            "shift": _shift,
            "jobCategory": _jobCategory
        ]
        
        return dict
    }
}
