//
//  FilterManager.swift
//  Jombone
//
//  Created by Money Mahesh on 10/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation

class FilterManager {
    
    func createUrl() -> [String: Any] {
        return [String: Any]()
    }
    
    func jsonString() -> String{
        return Utility.convertToJson(dictionaryOrArray: createUrl())
    }
}
