//
//  CompanyFilterManager.swift
//  Jombone
//
//  Created by Money Mahesh on 27/02/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation

class CompanyFilterManager: FilterManager {
    
    static var sharedInstance = CompanyFilterManager()

    var location : GooglePlaceDetail?
    var company : AutoCompleteCardViewModelProtocol?
    var industry : AutoCompleteCardViewModelProtocol?
    
    fileprivate var _followCompany : Bool?
    var followCompany : Bool!{
        set {
            
            _followCompany = newValue
            
            if _followCompany == nil{
                _followCompany = false
            }
        }
        get {
            if _followCompany == nil {
                //Default Value
                _followCompany = false
            }
            return _followCompany
        }
    }
    
    func reset() {
        location = nil
        company = nil
        industry = nil
        followCompany = false
    }
    
    override func createUrl() -> [String: Any] {
        
        let dict: [String: Any] = [
            "industryName": industry?.title ?? "",
            "industryId": industry?.encryptedId ?? "",
            "location": location?.locality ?? "",
            "companyName": company?.title ?? "",
            "employerId": company?.encryptedId ?? "",
            "followed": followCompany ? "true" : "false"
        ]
    
        return dict
    }
}

