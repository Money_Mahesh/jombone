//
//  OTP_View.swift
//  Jombone
//

import UIKit

class OTP_View: UIView, UITextFieldDelegate {

    @IBInspectable var noOfDigit : Int = 0
    @IBInspectable var noOfDigitPerField : Int = 1
    @IBInspectable var interspace : CGFloat = 5
    @IBInspectable var ShowPlaceholder : Bool = false
    
    @IBInspectable var fontSize: CGFloat = 10.0
    @IBInspectable var fontFamily: String = "Rubik-Regular"

    @IBInspectable var inactiveLineColor : UIColor = UIColor.clear
    @IBInspectable var activeLineColor : UIColor = UIColor.red
    @IBInspectable var textColor : UIColor = UIColor.black

    weak var delegate: OTP_TextFieldDelegate?
    
    var isDrew : Bool = false
    
    var textAlignment: NSTextAlignment = .center
    var completeString = String()
    var oTP_TextField = [UITextField]()
    var oTP_TextField_BottomLine = [UIView]()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.build_OTPView()
        makeFirstResponder()
    }

    func build_OTPView() {
        
        if isDrew {return}
        isDrew = true
        
        let otp_TextfieldView = UIView(frame: self.bounds)
        
        for position in 0..<noOfDigit {
            
            let viewWidthAfterRemovingInterSpace = self.bounds.size.width - interspace * CGFloat(noOfDigit-1)
            
            let textfield = UITextField(frame: CGRect(x : (CGFloat(position) * (viewWidthAfterRemovingInterSpace / CGFloat(noOfDigit))) + (CGFloat(position) * interspace),y : 0, width : (viewWidthAfterRemovingInterSpace / CGFloat(noOfDigit)), height : self.bounds.size.height-2))
        
            textfield.tag =  position
            textfield.font =  UIFont(name: self.fontFamily, size: self.fontSize)!
            textfield.textColor =  textColor
            textfield.keyboardType = .numberPad
            textfield.textAlignment = textAlignment
            textfield.delegate = self

            if ShowPlaceholder {
 
                var placeholderString = ""
                
                for _ in 0..<noOfDigitPerField {
                    placeholderString += "x"
                }
                textfield.placeholder = placeholderString
            }

            oTP_TextField.append(textfield)
            
            let bottomLine = UIView(frame: CGRect(x: textfield.frame.origin.x, y : textfield.frame.size.height + 1, width : textfield.frame.size.width, height : 1))
            bottomLine.backgroundColor = inactiveLineColor
            oTP_TextField_BottomLine.append(bottomLine)
            
            otp_TextfieldView.addSubview(oTP_TextField[position])
            otp_TextfieldView.addSubview(bottomLine)
            
            self.addSubview(otp_TextfieldView)
        }
    }
    
    func makeFirstResponder() {
        oTP_TextField.first?.becomeFirstResponder()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let bottomLine = oTP_TextField_BottomLine[textField.tag]
        bottomLine.backgroundColor = activeLineColor
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let bottomLine = oTP_TextField_BottomLine[textField.tag]
        if textField.text == nil || textField.text!.count == 0 {
            bottomLine.backgroundColor = inactiveLineColor
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let returnStatus : Bool!
        
        if string == "" {
            //character deleted
            if textField.tag == 0 {
                returnStatus = true
            }
            else {
                if textField.text?.count == 1 {
                    oTP_TextField[textField.tag].text! = string
                    oTP_TextField[textField.tag - 1].becomeFirstResponder()
                    returnStatus = false
                }
                else {
                    returnStatus = true
                }
            }
        }
        else {
            //character added
            if (textField.text?.count)! < noOfDigitPerField - 1 {
                returnStatus = true
            }
            else {
                
                if range.location == 0 {
                    if oTP_TextField[textField.tag].text?.count == 0 {
                        oTP_TextField[textField.tag].text = string
                        oTP_TextField[textField.tag].becomeFirstResponder()
                    }
                    else {
                        let startIndex = oTP_TextField[textField.tag].text?.startIndex
                        oTP_TextField[textField.tag].text?.replaceSubrange(
                            (oTP_TextField[textField.tag].text?.index(startIndex!, offsetBy: range.location))!..<(oTP_TextField[textField.tag].text?.index(startIndex!, offsetBy: 1))!, with: string)
                    }
                }
                else {
                    if textField.tag == noOfDigit - 1 {
                        self.endEditing(true)
                    }
                    else {
                        if oTP_TextField[textField.tag + 1].text?.count == 0 {
                            oTP_TextField[textField.tag + 1].text = string
                            oTP_TextField[textField.tag + 1].becomeFirstResponder()
                        }
                        else {
                            
                            let startIndex = oTP_TextField[textField.tag].text?.startIndex

                            oTP_TextField[textField.tag].text?.replaceSubrange(
                                (oTP_TextField[textField.tag].text?.index(startIndex!, offsetBy: range.location - 1))!..<(oTP_TextField[textField.tag].text?.index(startIndex!, offsetBy: range.location))!, with: string)
                        }
                        oTP_TextField[textField.tag + 1].becomeFirstResponder()
                    }
                }
                returnStatus = false
            }
        }
        
        let _ = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(OTP_View.callUpdate), userInfo: nil, repeats: false)
        
        return returnStatus
    }
    
    @objc func callUpdate() {
        completeString = ""
        for index in 0..<noOfDigit {
            completeString += oTP_TextField[index].text!
        }
       
//        if completeString.count == 0 {
//            let bottomLine = oTP_TextField_BottomLine[0]
//            bottomLine.backgroundColor = inactiveLineColor
//        }
        
        print("completeString \(completeString)")
        if delegate != nil {
            delegate?.textFieldUpdate?(completeString: String(completeString))
        }
    }
    
    
    func validate() -> String? {
        return (completeString.count < noOfDigit) ? "Please provide a valid otp" : nil
    }

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}

@objc protocol OTP_TextFieldDelegate : NSObjectProtocol {
    @objc optional func textFieldUpdate(completeString : String)
}
