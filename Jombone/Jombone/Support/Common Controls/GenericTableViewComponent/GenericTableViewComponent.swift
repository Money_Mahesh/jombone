//
//  GenericTableViewComponent.swift
//  Jombone
//
//  Created by Money Mahesh on 08/08/18.
//  Copyright © 2018 Money Mahesh. All rights reserved.
//

import UIKit

extension Notification.Name {
    static let TableHeaderSizeUpdated = Notification.Name("TableHeaderView")
    static let TableFooterSizeUpdated = Notification.Name("TableFooterView")
    static let TableCellSizeUpdated = Notification.Name("TableCell")
}

protocol GenericTableViewComponentDelegate: class {
    func genericTableDidSelectRowAt(_ indexPath: IndexPath, dataSource: inout [Any]) -> Bool
    func loadMore()
}
extension GenericTableViewComponentDelegate {
    func genericTableDidSelectRowAt(_ indexPath: IndexPath, dataSource: inout [Any]) -> Bool {return false}
    func loadMore() {}
}

class GenericTableViewComponent<D: Any, V: GenericView<D>>: CustomView, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak private var viewForShadow: UIView?
    @IBOutlet weak private var tableView: UITableView?
    @IBOutlet weak private var constForTableHeight: NSLayoutConstraint?
    @IBOutlet weak private var constForTop: NSLayoutConstraint?
    @IBOutlet weak private var constForLeading: NSLayoutConstraint?
    @IBOutlet weak private var constForTrailing: NSLayoutConstraint?
    @IBOutlet weak private var constForBottom: NSLayoutConstraint?

    private var heightConstant: CGFloat?
    private var iscontentSizeObserverAdded = false
    weak var delegate: GenericTableViewComponentDelegate? {
        didSet {
            if delegate != nil {
                setLoadMoreView()
            }
        }
    }
    
    override func loadNib() -> UIView {
        let nib = UINib(nibName: "GenericTableViewComponent", bundle: nil)
        return nib.instantiate(withOwner: self, options: nil).first as! UIView
    }
    
    var genericTableViewModel: GenericTableViewModel<D>! = GenericTableViewModel() {
        didSet {
            tableView?.tableHeaderView = genericTableViewModel.tableHeaderView?.view
            tableView?.tableFooterView = genericTableViewModel.tableFooterView?.view

            switch genericTableViewModel.tableType {
            case .nonScrollable:
                tableView?.addObserver(self, forKeyPath: "contentSize", options: [NSKeyValueObservingOptions.new, NSKeyValueObservingOptions.old], context: nil)
                iscontentSizeObserverAdded = true
                tableView?.isScrollEnabled = false
                constForTableHeight?.isActive = true
                
            case let .height(height):
                heightConstant = height
                constForTableHeight?.constant = heightConstant!
                tableView?.addObserver(self, forKeyPath: "contentSize", options: [NSKeyValueObservingOptions.new, NSKeyValueObservingOptions.old], context: nil)
                iscontentSizeObserverAdded = true
                tableView?.isScrollEnabled = true
                constForTableHeight?.isActive = true

            case .expandable:
                constForTableHeight?.isActive = false
                tableView?.isScrollEnabled = true
            }
            
            
            //Add observer
            if genericTableViewModel.tableHeaderView?.expandable == true  {
                tableView?.addObserver(self, selector: #selector(GenericTableViewComponent.valueUpdated(_:)), notificationsName: [Notification.Name.TableHeaderSizeUpdated])
            }
            if genericTableViewModel.tableFooterView?.expandable == true {
                tableView?.addObserver(self, selector: #selector(GenericTableViewComponent.valueUpdated(_:)), notificationsName: [Notification.Name.TableFooterSizeUpdated])
            }
            if genericTableViewModel.cellExpandable {
                tableView?.addObserver(self, selector: #selector(GenericTableViewComponent.valueUpdated(_:)), notificationsName: [Notification.Name.TableCellSizeUpdated])
            }
            if genericTableViewModel.cellDataModel.count == 0 {
                constForTableHeight?.isActive = false
            }
            
            setLoadMoreView()
        }
    }
    
    func tableStyle(backGroundColor: UIColor, insets: UIEdgeInsets, indicatorStyle: UIScrollView.IndicatorStyle = .white) {
        tableView?.backgroundColor = backGroundColor
        viewForShadow?.backgroundColor = backGroundColor
        
        constForTop?.constant = insets.top
        constForBottom?.constant = insets.bottom
        constForLeading?.constant = insets.left
        constForTrailing?.constant = insets.right
        
        tableView?.indicatorStyle = .white
    }
    
    override func xibLoaded() {
        
        tableView?.register(GenericTableViewCell<V>.self, forCellReuseIdentifier: "GenericTableViewCell")
        tableView?.estimatedRowHeight = 40
        tableView?.rowHeight = UITableView.automaticDimension
        
//        viewForShadow?.layer.addShadow(color: CARD_SHADOW_COLOR.cgColor, direction: .down, radius: 4.0, spread: 2)
        viewForShadow?.layer.cornerRadius = 10
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if let change = change, let oldvalue = change[NSKeyValueChangeKey.oldKey] as? CGSize,
            let newvalue = change[NSKeyValueChangeKey.newKey] as? CGSize,
            let table = tableView, oldvalue != newvalue {
                        
            var newHeight = table.contentSize.height
            if let constHeight = heightConstant, newHeight >= constHeight {
                newHeight = constHeight
            }
            table.layer.removeAllAnimations()
            constForTableHeight?.constant = newHeight
        }
    }
    
    deinit {
        if iscontentSizeObserverAdded {
            iscontentSizeObserverAdded = false
            tableView?.removeObserver(self, forKeyPath: "contentSize")
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return genericTableViewModel.numberOfSection
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return genericTableViewModel.numberOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "GenericTableViewCell") as! GenericTableViewCell<V>
        cell.view?.index = indexPath
        var cellStyle: Any?
        if let style = genericTableViewModel.cellOtherDetail {
            if let style = style.unique?[String(indexPath.row)] {
                cellStyle = style
            }
            else {
                cellStyle = style.generic
            }
        }
        
        cell.view?.viewModel = ViewModel(data: genericTableViewModel.cellDataModel[indexPath.row] as? D, otherDetail: cellStyle)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard let _delegate = delegate else {
            return
        }
        if indexPath.row == (genericTableViewModel.cellDataModel.count - 1) && genericTableViewModel.loadMore {
            _delegate.loadMore()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let reloadTable = delegate?.genericTableDidSelectRowAt(indexPath, dataSource: &genericTableViewModel.cellDataModel), reloadTable {
            tableView.reloadData()
        }
    }
    
    func reloadTable() {
        self.tableView?.reloadData()
    }
    
    func reloadTableVisibleCell() {
        self.tableView?.beginUpdates()
        self.tableView?.endUpdates()
    }
    
    func updateTableRecord(objArray: [D]?) {
        
        guard let objArray = objArray else {
            genericTableViewModel.cellDataModel.removeAll()
            self.reloadTable()
            return
        }
        
        if genericTableViewModel.cellDataModel.count != objArray.count {
            genericTableViewModel.cellDataModel.removeAll()
            self.reloadTable()

            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
                DispatchQueue.main.async {
                    self.genericTableViewModel.cellDataModel = objArray
                    self.setLoadMoreView()
                    self.reloadTable()
                }
            }
        }
        else {
            self.genericTableViewModel.cellDataModel = objArray
            self.reloadTable()
        }
    }
    
    func appendNewPage(objArray: [D]?) {
        
        guard let objArray = objArray else {
            return
        }
        
        self.genericTableViewModel.cellDataModel = objArray
        self.genericTableViewModel.currentPage += 1
        self.setLoadMoreView()
        self.reloadTable()
    }
    
    func deleteCellIndex<T: UIView>(sender: T) -> IndexPath? {
        
        let position:CGPoint = sender.convert(CGPoint.zero, to: self.tableView)
        if let indexPath = tableView?.indexPathForRow(at: position) {
            print(genericTableViewModel.cellDataModel.count)
            genericTableViewModel.cellDataModel.remove(at: indexPath.row)
            print(genericTableViewModel.cellDataModel.count)
            
            if genericTableViewModel.cellDataModel.count == 0 {
                tableView?.deleteSections([0], with: UITableView.RowAnimation.fade)
            }
            else {
                tableView?.deleteRows(at: [indexPath], with: UITableView.RowAnimation.fade)
            }
            return indexPath
        }
        return nil
    }
    
    private func setLoadMoreView() {
        if (delegate != nil && !genericTableViewModel.loadMore) || genericTableViewModel.currentPage == 0 {
            tableView?.tableFooterView = nil
        }
    }
    
    //MARK:- Notification Observer Methods
    //handle notification
    @objc func valueUpdated(_ notification: NSNotification) {
        
        if notification.name == Notification.Name.TableHeaderSizeUpdated {
            tableView?.tableHeaderView = genericTableViewModel.tableHeaderView?.view
            self.reloadTable()
        }
        else if notification.name == Notification.Name.TableFooterSizeUpdated {
            tableView?.tableFooterView = genericTableViewModel.tableFooterView?.view
            self.reloadTable()
        }
        else if notification.name == Notification.Name.TableCellSizeUpdated {
            self.reloadTable()
        }
    }
}

class GenericView<D: Any>: CustomView {
    var viewModel: ViewModel<D>?
    var index: IndexPath!
    
    func viewDirectionForJobOrCompany(isJob : Bool, jobOrEmployerid : String) {
        
        guard let token = UserDataManager.shared.token else {
            return
        }
        let jobSecondaryUrl = "/map/fetch-job-map?jobEncId="
        let companySecondaryUrl = "/map/fetch-company-map?empEncId="
        
        let secondaryUrl = isJob ? jobSecondaryUrl : companySecondaryUrl
        let urlString = BASE_URL.absoluteString + "\(secondaryUrl)\(jobOrEmployerid)&token=\(token)"
        let vieWModel = WebViewModel(urlStr: urlString, title: "View Directions", willPresent: true)
        let webView = WebViewViewController.instance(viewModel:vieWModel)
        APPLICATION_INSTANCE.visibleViewController()?.present(webView, animated: true, completion: nil)
    }
    
}

class ViewModel<Data>: NSObject {
    
    var data: Data?
    var otherDetail: Any?
    init(data: Data?, otherDetail: Any?) {
        self.data = data
        self.otherDetail = otherDetail
    }
}
