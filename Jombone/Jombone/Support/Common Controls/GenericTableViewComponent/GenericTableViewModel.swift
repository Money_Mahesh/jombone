//
//  GenericTableViewModel.swift
//  Jombone
//
//  Created by Money Mahesh on 08/08/18.
//  Copyright © 2018 Money Mahesh. All rights reserved.
//

import Foundation
import UIKit

enum TableType {
    case nonScrollable
    case height(height: CGFloat)
    case expandable
}

class GenericTableViewModel<D: Any>: NSObject {
    
    var currentPage = 0
    var tableHeaderView: (view: UIView, expandable: Bool)?
    var tableFooterView: (view: UIView, expandable: Bool)?
    var cellExpandable: Bool = false
    var tableType: TableType = .nonScrollable
    
    var cellDataModel = [Any]()
    var cellOtherDetail: GenericTableViewCellOtherDetail?
    var totalPages: Int = 1
    
    var numberOfSection: Int {
        return (cellDataModel.count > 0 ? 1 : 0)
    }
    
    var loadMore: Bool {
        get {
            return (currentPage != (totalPages - 1))
        }
    }
    
    override init() {
        self.tableHeaderView = nil
        self.tableFooterView = nil
        self.tableType = .nonScrollable
        self.cellDataModel = [Any]()
        self.cellOtherDetail = nil
    }
    
    init(cellDataModel: [D]? = nil,
         cellOtherDetail: GenericTableViewCellOtherDetail? = nil,
         tableHeaderView: (view: UIView, expandable: Bool)? = nil,
         tableFooterView: (view: UIView, expandable: Bool)? = nil,
         cellExpandable: Bool = false,
         tableType: TableType = .nonScrollable,
         totalPages: Int = 1) {
        
        self.tableHeaderView = tableHeaderView
        self.tableFooterView = tableFooterView
        self.tableType = tableType
        self.cellExpandable = cellExpandable
        self.cellDataModel = cellDataModel ?? [D]()
        self.cellOtherDetail = cellOtherDetail
        self.totalPages = totalPages
    }
    
    func numberOfRows() -> Int {
        return cellDataModel.count 
    }
}
