//
//  CustomAlertViewController.swift
//  Jombone
//
//  Created by Money Mahesh on 02/04/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import UIKit

struct CustomPopUpDataModel {
    var title: String?
    var subTitle: String?
    var completion: (()->())? 
}

class CustomAlertViewController: UIViewController {
    
    @IBOutlet weak var lblForTitle: UILabel?
    @IBOutlet weak var lblForSubTitle: UILabel?
    @IBOutlet weak var btnForCancel: UIButton?
    @IBOutlet weak var btnForYes: UIButton?
    var viewModel: CustomPopUpDataModel? {
        didSet {
            lblForTitle?.text = viewModel?.title
            lblForSubTitle?.text = viewModel?.subTitle
        }
    }
    
    static func show(data: CustomPopUpDataModel?) {
        
        let viewController = ALERT_STORYBOARD.instantiateViewController(withIdentifier: "JobsDetailViewController") as! CustomAlertViewController
        viewController.viewModel = data
        APPLICATION_INSTANCE.visibleViewController()?.present(viewController, animated: false, completion: nil)
    }
    
    func dismiss() {
        self.dismiss(animated: false, completion: nil)
    }
    
    //MARK:- IBActions
    @IBAction func btnActionForYes(_ sender: UIButton) {
        viewModel?.completion?()
        dismiss()
    }
    
    @IBAction func btnActionForCancel(_ sender: UIButton) {
        dismiss()
    }
}
