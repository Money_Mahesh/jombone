//
//  CustomAlertViewController.swift
//  Jombone
//
//  Created by Money Mahesh on 02/04/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import UIKit

struct CustomAlertDataModel {
    var title: String?
    var subTitle: String?
    var completion: (()->())? 
}

class CustomAlertViewController: UIViewController {
    
    @IBOutlet weak var lblForTitle: UILabel?
    @IBOutlet weak var lblForSubTitle: UILabel?
    @IBOutlet weak var btnForCancel: UIButton?
    @IBOutlet weak var btnForYes: UIButton?
    var viewModel: CustomAlertDataModel?
    
    static func show(data: CustomAlertDataModel?) {
        
        let viewController = ALERT_STORYBOARD.instantiateViewController(withIdentifier: "CustomAlertViewController") as! CustomAlertViewController
        viewController.viewModel = data
        APPLICATION_INSTANCE.visibleViewController()?.present(viewController, animated: false, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblForTitle?.text = viewModel?.title
        lblForSubTitle?.text = viewModel?.subTitle
    }
    
    func dismiss() {
        self.dismiss(animated: false, completion: nil)
    }
    
    //MARK:- IBActions
    @IBAction func btnActionForYes(_ sender: UIButton) {
        viewModel?.completion?()
        dismiss()
    }
    
    @IBAction func btnActionForCancel(_ sender: UIButton) {
        dismiss()
    }
}
