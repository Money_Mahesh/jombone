//
//  WhiteStatusBarViewController.swift
//  Jombone
//
//  Created by Money Mahesh on 13/08/18.
//  Copyright © 2018 Money Mahesh. All rights reserved.
//

import UIKit

class WhiteStatusBarViewController: UIViewController {

    var lastNavigationBarVisibilityStatus: Bool!
    var lastTabBarVisibilityStatus: Bool!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        lastNavigationBarVisibilityStatus = self.navigationController?.isNavigationBarHidden
        setNavigationBarWithTitle("", andLeftButton: .back, andRightButton: .none, withBg: .bgColor(value: UIColor.clear))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        lastTabBarVisibilityStatus = self.navigationController?.tabBarController?.tabBar.isHidden
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.tabBarController?.tabBar.isHidden = lastTabBarVisibilityStatus ?? true
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func backAction(sender: UIButton) {
        super.backAction(sender: sender)
    }
}
