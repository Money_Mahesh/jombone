//
//  CustomLabelView.swift
//  Jombone
//
//  Created by Money Mahesh on 28/02/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import UIKit

protocol CustomLabelViewProtocol: NSObjectProtocol {
    func tapped(_ identifier: String)
    func crossBtnTapped(_ identifier: String)
}

extension CustomLabelViewProtocol {
    func tapped(_ identifier: String) {}
    func crossBtnTapped(_ identifier: String) {}
}

@IBDesignable class CustomLabelView: CustomView {
    
    @IBInspectable var Identifier: String! = "CustomLabelView"
    @IBOutlet weak var lblForData: UILabel!
    @IBOutlet weak var lblForTitle: UILabel!
    @IBOutlet weak var rightBtn: UIButton!
    @IBOutlet weak private var lblForError: UILabel!
    @IBOutlet weak private var containerView: UIView!

    private lazy var titleFont: UIFont! = UIFont.rubikMediumFontOfSize(18.0)
    private lazy var textFont: UIFont! = UIFont.rubikRegularFontOfSize(16)
    private lazy var placeholderFont: UIFont! = UIFont.rubikRegularFontOfSize(16)
    @IBInspectable var placeholderColor: UIColor = UIColor(hexFromString: "#757575"){
        didSet {
            setStyle()
        }
    }
    @IBInspectable var textColor: UIColor! = UIColor(hexFromString: "#424242") {
        didSet {
            setStyle()
        }
    }
    
    @IBInspectable var titleColor: UIColor! = UIColor(hexFromString: "#424242") {
        didSet {
            setStyle()
        }
    }
    
    @IBInspectable var containerBorderRadius: CGFloat = 2.0 {
        didSet {
            containerView.Corner2xRadius = containerBorderRadius
            containerView.Corner3xRadius = containerBorderRadius
        }
    }
    
    @IBInspectable var containerBorderWidth: CGFloat = 1.0 {
        didSet {
            containerView.BorderWidth = containerBorderWidth
        }
    }
    
    @IBInspectable var containerBorderColor: UIColor = UIColor(hexFromString: "#BDBDBD") {
        didSet {
            containerView.BorderColor = containerBorderColor
        }
    }
    
    @IBInspectable var containerBackgroundColor: UIColor = UIColor(hexFromString: "#fafafa") {
        didSet {
            containerView.backgroundColor = containerBackgroundColor
        }
    }
    
    @IBInspectable var showRightBtn: Bool = false {
        didSet {
            setStyle()
        }
    }

    weak var delegate: CustomLabelViewProtocol?

    @IBInspectable var text: String? {
        didSet {
            setStyle()
        }
    }
    
    @IBInspectable var placeholder_Text: String? {
        didSet {
            setStyle()
        }
    }
    
    @IBInspectable var title: String? {
        didSet {
            lblForTitle.text = title
        }
    }
    
    var errorMsg: String? {
        didSet {
            lblForError.text = errorMsg
        }
    }
    
    private func setStyle() {
        if text == nil || text!.count < 1 {
            self.lblForData.text = placeholder_Text
            self.lblForData.textColor = placeholderColor
            self.lblForData.font = placeholderFont
            self.rightBtn.isHidden = true
        }
        else {
            self.lblForData.text = text
            self.lblForData.textColor = textColor
            self.lblForData.font = textFont
            self.rightBtn.isHidden = !showRightBtn
        }
        lblForTitle.font = titleFont
        lblForTitle.textColor = titleColor
        lblForTitle.text = title
    }
    
    func setData(text: String?) {
        self.text = text
        setStyle()
    }
    
    enum BtnStyle {
        case title(selectedDetail: (title: String, color: UIColor), deSelectedDetail: (title: String, color: UIColor), font: UIFont)
        case imageName(selected: String, deSelected: String)
    }
    
    func setRightBtnStyle(style: BtnStyle, target: Any?, action: Selector) {
        
        rightBtn.isHidden = false
        
        switch style {
        case .title(let selectedDetail, let deSelectedDetail, let font):
            rightBtn.setTitle(deSelectedDetail.title, for: UIControl.State.normal)
            rightBtn.setTitle(selectedDetail.title, for: UIControl.State.selected)
            rightBtn.setTitleColor(deSelectedDetail.color, for: UIControl.State.normal)
            rightBtn.setTitleColor(selectedDetail.color, for: UIControl.State.selected)
            
            rightBtn.titleLabel?.font = font
            
        case .imageName(let selected, let deSelected):
            rightBtn.setImage(UIImage(named: deSelected), for: UIControl.State.normal)
            rightBtn.setImage(UIImage(named: selected), for: UIControl.State.selected)
        }
        
        rightBtn.addTarget(target, action: action, for: UIControl.Event.touchUpInside)
    }
    
    func setStyleForError(color: UIColor = UIColor(hexFromString: "#f93550"), font: UIFont = UIFont.rubikRegularFontOfSize(12)) {
        
        lblForError.textColor = color
        lblForError.font = font
    }
    
    func setTitle(_ title: String? = nil, text: String? = nil, placeholder: String? = nil) {
        self.title = title
        self.text = text
        self.placeholder_Text = placeholder
        setStyle()
    }
    
    func setFontForLabel(font: UIFont = UIFont.rubikRegularFontOfSize(16) , placeholderFont: UIFont = UIFont.rubikRegularFontOfSize(12), titleFont: UIFont = UIFont.rubikRegularFontOfSize(12)) {
        
        self.placeholderFont = placeholderFont
        self.textFont = font
        self.titleFont = font

        setStyle()
    }
    
    override func xibLoaded() {
        setStyle()
    }
    
    //MARK:- IBAction
    @IBAction func tapGestureAction(_ sender: UITapGestureRecognizer) {
        delegate?.tapped(Identifier)
    }
    
    @IBAction func btnActionForRight(_ sender: UIButton) {
        self.setData(text: nil)
        delegate?.crossBtnTapped(Identifier)
    }
}


