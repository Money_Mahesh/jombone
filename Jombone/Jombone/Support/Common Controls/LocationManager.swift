//
//  LocationManager.swift
//  Jombone
//
//  Created by Money Mahesh on 03/10/18.
//  Copyright © 2018 Money Mahesh. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

struct CurrentLocationDetail: Codable {
    var addressLine1: String?
    var addressLine2: String?
    var googlePlaceId: String?
    var zipcode: String?
    var city: String?
    var state: String?
    var country: String?
    var latLong: String?
    var fullAddress: String?
    var lat: Double?
    var long: Double?

    init() {}
    
    init(lat: Double, long: Double, fullAddress: String) {
        
        self.latLong = String(lat) + "," + String(long)
        self.fullAddress = fullAddress
    }
    
    func jsonString() -> String? {
        
        let jsonEncoder = JSONEncoder()
        do {
            let jsonData = try jsonEncoder.encode(self)
            return String(data: jsonData, encoding: .utf8)
        }
        catch {
            print("error")
        }
        
        return nil
    }
    
    func dictionary() -> [String: Any]? {
        
        if let data = jsonString()?.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        
        return nil
    }
}

class LocationManager: NSObject {
    
    static var instance = LocationManager()
    var location: CLLocationManager!
    
    var completion: ((_ status: Bool, _ locationDetail: CurrentLocationDetail?, _ coordinate: CLLocationCoordinate2D?) -> ())?
    
    func setUp(completion: ((_ status: Bool, _ locationDetail: CurrentLocationDetail?, _ coordinate: CLLocationCoordinate2D?) -> ())?) {
        
        self.completion = completion
        location = CLLocationManager()
        location.delegate = self
        location.desiredAccuracy = kCLLocationAccuracyBest
        location.requestWhenInUseAuthorization()
        location.startUpdatingLocation()
        location.pausesLocationUpdatesAutomatically = false
    }
    
    func getAddressFromLatLon(latitude: Double, withLongitude longitude: Double) {
        let center : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        let ceo: CLGeocoder = CLGeocoder()
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil) {
                    self.completion?(false, nil, nil)
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                    return
                }
                
                if let pm = placemarks, pm.count > 0 {
                    
                    let pm = placemarks![0]
                    
                    var addressString : String = ""
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + " "
                    }
                    
                    var currentLocationDetail = CurrentLocationDetail()
                    currentLocationDetail.addressLine1 = pm.thoroughfare
                    currentLocationDetail.addressLine2 = ""
                    currentLocationDetail.googlePlaceId = ""
                    currentLocationDetail.zipcode = pm.postalCode
                    currentLocationDetail.city = pm.locality
                    currentLocationDetail.state = pm.administrativeArea
                    currentLocationDetail.country = pm.subAdministrativeArea
                    currentLocationDetail.latLong = (String(latitude) + "," + String(longitude))
                    currentLocationDetail.fullAddress = addressString
                    currentLocationDetail.lat = latitude
                    currentLocationDetail.long = longitude
                    
                    self.completion?(true, currentLocationDetail, center)
                }
                else {
                    self.completion?(false, nil, nil)
                }
        })
        
    }
}

extension LocationManager: CLLocationManagerDelegate {
    
    //    func locationManager(_ manager: CLLocationManager,
    //                         didChangeAuthorization status: CLAuthorizationStatus) {
    //
    //        switch status {
    //        case .restricted, .denied, .notDetermined:
    //            // Disable your app's location features
    //            completion?(false, nil)
    //
    //        case .authorizedWhenInUse, .authorizedAlways:
    //            // Enable only your app's when-in-use features.
    //            completion?(false, nil)
    //        }
    //    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let latitude = locations.last?.coordinate.latitude, let longitude = locations.last?.coordinate.longitude {
            getAddressFromLatLon(latitude: latitude, withLongitude: longitude)
            location = nil
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        location = nil
        completion?(false, nil, nil)
    }
}
