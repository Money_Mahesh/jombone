//
//  KDCalendarView.swift
//  KDCalendar
//
//  Created by Michael Michailidis on 02/04/2015.
//  Copyright (c) 2015 Karmadust. All rights reserved.
//

import UIKit
import EventKit

let cellReuseIdentifier = "CalendarDayCell"

let NUMBER_OF_DAYS_IN_WEEK = 7
let MAXIMUM_NUMBER_OF_ROWS = 6

let HEADER_DEFAULT_HEIGHT : CGFloat = 80.0


let FIRST_DAY_INDEX = 0
let NUMBER_OF_DAYS_INDEX = 1

enum DateSelectionType: Int {
    case Beyond = 0
    case Earlier = 1
}


extension EKEvent {
    var isOneDay : Bool {
        let components = (Calendar.current as NSCalendar).components([.era, .year, .month, .day], from: self.startDate, to: self.endDate, options: NSCalendar.Options())
        return (components.era == 0 && components.year == 0 && components.month == 0 && components.day == 0)
    }
}

@objc protocol CalendarViewDataSource {
    
    func startDate() -> Date?
    func endDate() -> Date?
    
}

@objc protocol CalendarViewDelegate {
    
    @objc optional func calendar(_ calendar : CalendarView, canSelectDate date : Date) -> Bool
    func calendar(_ calendar : CalendarView, didScrollToMonth month : Int, year: Int) -> Void
    func calendar(_ calendar : CalendarView, didSelectDate date : Date, isSelected: Bool) -> Void
    @objc optional func calendar(_ calendar : CalendarView, didDeselectDate date : Date) -> Void
    @objc optional func calendarDateSelectedBeyondSelectionLimit(dateType: Int) -> Void
}


class CalendarView: UIView, UICollectionViewDataSource, UICollectionViewDelegate {
    
    @IBOutlet weak var headerView: CalendarHeaderView!
    @IBOutlet weak var calendarView: UICollectionView!
    @IBOutlet weak var btnForPrev: UIButton!
    @IBOutlet weak var btnForNext: UIButton!

    @IBOutlet weak var constForCalendarHeight: NSLayoutConstraint!

    var currentSection = 0
    var dataSource  : CalendarViewDataSource?
    var delegate    : CalendarViewDelegate?
    
    var displayDate : Date?
    var multipleSelectionAllowed : Bool = true
    
    lazy var gregorian : Calendar = {
        
        var cal = Calendar(identifier: Calendar.Identifier.gregorian)
        
        return cal
    }()
    
    var calendar : Calendar {
        return self.gregorian
    }
    
    var direction : UICollectionView.ScrollDirection = .horizontal {
        didSet {
            if let layout = self.calendarView.collectionViewLayout as? CalendarFlowLayout {
                layout.scrollDirection = direction
                self.calendarView.reloadData()
            }
        }
    }
    
    var cellSize : CGSize  {
        get {
            return CGSize(width: (WINDOW_WIDTH - (2 * 10)) / CGFloat(NUMBER_OF_DAYS_IN_WEEK), height: (WINDOW_WIDTH - (2 * 10)) / CGFloat(NUMBER_OF_DAYS_IN_WEEK))
        }
    }
    
    fileprivate var startDateCache : Date = Date()
    fileprivate var endDateCache : Date = Date()
    fileprivate var startOfMonthCache : Date = Date()
    fileprivate var selectionLimit : Int = -1

    var alreadySelectedDate = [Date]()
    fileprivate(set) var selectedDates : [Date] = [Date]()

    private enum QuickSelectionType: Int {
        case none = 0
        case everyDay = 1
        case everyWeekeed = 2
    }
    private var quickSelection: QuickSelectionType = .none
    
    override init(frame: CGRect) {
        super.init(frame : frame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setUpCalenderView(canSelectUpto: Int) {
        
        direction = .horizontal
        calendarView.allowsMultipleSelection = true
        
        let layout = self.calendarView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = cellSize
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        
        selectionLimit = canSelectUpto
    }
    
    //MARK:- IBActions
    @IBAction func nextMonthAction(sender: UIButton) {
        
        if calendarView.numberOfSections > (currentSection + 1) {
            sectionChangedBy(change: 1)
        }
    }
    
    @IBAction func prevMonthAction(sender: UIButton) {
        
        if (currentSection - 1) > -1 {
            sectionChangedBy(change: -1)
        }
    }
    
    private func sectionChangedBy(change: Int) {
    
        selectedDates.removeAll()
        calendarView.reloadData()
        
        currentSection += change
        updateCalendarHeight()
        
        let year = getCurrentMonthDetail()?.year
        if let delegate = self.delegate, let detail = getCurrentMonthDetail() {
            quickSelection = .none
            delegate.calendar(self, didScrollToMonth: detail.month, year: year!)
        }
        
        calendarView.scrollToItem(at: IndexPath(item: 0, section: currentSection), at: UICollectionView.ScrollPosition.top, animated: true)
    }

    func updateCalendarHeight() {
        let numberOfRows = (calendarView.numberOfItems(inSection: currentSection) / 7) + ((calendarView.numberOfItems(inSection: currentSection) % 7) == 0 ? 0 : 1)
        
        constForCalendarHeight.constant = (cellSize.height * CGFloat(numberOfRows))
        
        UIView.animate(withDuration: 0.5) { 
            self.calendarView.layoutIfNeeded()
        }
    }
    
    func indexPathForDate(_ date : Date) -> IndexPath? {
     
        let distanceFromStartComponent = (self.gregorian as NSCalendar).components( [.month, .day], from:startOfMonthCache, to: date, options: NSCalendar.Options() )
        
        guard let currentMonthInfo : [Int] = monthInfo[distanceFromStartComponent.month!] else {
            return nil
        }
        
        
        let item = distanceFromStartComponent.day! + currentMonthInfo[FIRST_DAY_INDEX]
        let indexPath = IndexPath(item: item, section: distanceFromStartComponent.month!)
        
        return indexPath
        
    }
    
    func setDisplayDate(_ date : Date, animated: Bool) {
        
        if let dispDate = self.displayDate {
            
            // skip is we are trying to set the same date
            if  date.compare(dispDate) == ComparisonResult.orderedSame {
                return
            }
            
            
            // check if the date is within range
            if  date.compare(startDateCache) == ComparisonResult.orderedAscending ||
                date.compare(endDateCache) == ComparisonResult.orderedDescending   {
                return
            }
            
        
            let difference = (self.gregorian as NSCalendar).components([NSCalendar.Unit.month], from: startOfMonthCache, to: date, options: NSCalendar.Options())
            
            let distance : CGFloat = CGFloat(difference.month!) * self.calendarView.frame.size.width
            
            self.calendarView.setContentOffset(CGPoint(x: distance, y: 0.0), animated: animated)
            
            _ = self.calculateDateBasedOnScrollViewPosition()
        }
        
    }
    
    // MARK: UICollectionViewDataSource
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        guard let startDate = self.dataSource?.startDate(), let endDate = self.dataSource?.endDate() else {
            return 0
        }
        
        startDateCache = startDate
        endDateCache = endDate
        
        // check if the dates are in correct order
        if (self.gregorian as NSCalendar).compare(startDate, to: endDate, toUnitGranularity: .nanosecond) != ComparisonResult.orderedAscending {
            print("1...")
            return 0
        }
        
        var firstDayOfStartMonth = (self.gregorian as NSCalendar).components( [.era, .year, .month], from: startDateCache)
        firstDayOfStartMonth.day = 1
        
        guard let dateFromDayOneComponents = self.gregorian.date(from: firstDayOfStartMonth) else {
            print("2...")
            return 0
        }
        
        startOfMonthCache = dateFromDayOneComponents
        
        let differenceComponents = (self.gregorian as NSCalendar).components(NSCalendar.Unit.month, from: startDateCache, to: endDateCache, options: NSCalendar.Options())
        
        return differenceComponents.month! + 1 // if we are for example on the same month and the difference is 0 we still need 1 to display it
    }
    
    var monthInfo : [Int:[Int]] = [Int:[Int]]()
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        var monthOffsetComponents = DateComponents()
        
        // offset by the number of months
        monthOffsetComponents.month = section;
        
        guard let correctMonthForSectionDate = (self.gregorian as NSCalendar).date(byAdding: monthOffsetComponents, to: startOfMonthCache, options: NSCalendar.Options()) else {
            return 0
        }
        
        let numberOfDaysInMonth = (self.gregorian as NSCalendar).range(of: .day, in: .month, for: correctMonthForSectionDate).length
        
        var firstWeekdayOfMonthIndex = (self.gregorian as NSCalendar).component(NSCalendar.Unit.weekday, from: correctMonthForSectionDate)
        firstWeekdayOfMonthIndex = firstWeekdayOfMonthIndex - 1 // firstWeekdayOfMonthIndex should be 0-Indexed
        firstWeekdayOfMonthIndex = (firstWeekdayOfMonthIndex + 6) % 7 // push it modularly so that we take it back one day so that the first day is Monday instead of Sunday which is the default
        
        monthInfo[section] = [firstWeekdayOfMonthIndex, numberOfDaysInMonth]
        
        return (firstWeekdayOfMonthIndex + numberOfDaysInMonth)//NUMBER_OF_DAYS_IN_WEEK * MAXIMUM_NUMBER_OF_ROWS // 7 x 6 = 42
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let dayCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! CalendarDayCell
        dayCell.pBackgroundView.layer.cornerRadius = (cellSize.width - 10) / 2

        let currentMonthInfo : [Int] = monthInfo[(indexPath as NSIndexPath).section]! // we are guaranteed an array by the fact that we reached this line (so unwrap)
        
        let fdIndex = currentMonthInfo[FIRST_DAY_INDEX]
        let nDays = currentMonthInfo[NUMBER_OF_DAYS_INDEX]
        
        if (indexPath as NSIndexPath).item >= fdIndex &&
            (indexPath as NSIndexPath).item < fdIndex + nDays {
            
            let day = (indexPath.row - (fdIndex - 1))
            dayCell.textLabel.text = String(day)

            let cellDate = getDate(day: day - 1, month: indexPath.section)
            dayCell.date = cellDate
            
            dayCell.isAlreadySchedule = false

            for date in alreadySelectedDate {
                if isDateEqual(date1: cellDate!, date2: date) {
                    dayCell.isAlreadySchedule = true
                    break
                }
            }
            
            dayCell.isModified = false
            
            for date in selectedDates {
                if isDateEqual(date1: cellDate!, date2: date) {
                    dayCell.isModified = true
                    break
                }
            }
        }
        else {
            dayCell.date = nil
            dayCell.textLabel.text = ""
            dayCell.isAlreadySchedule = false
            dayCell.isModified = false
        }
        
        
        if indexPath.section == 0 && indexPath.item == 0 {
            self.scrollViewDidEndDecelerating(collectionView)
        }
        
        return dayCell
    }
    
    // MARK: UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let currentMonthInfo : [Int] = monthInfo[(indexPath as NSIndexPath).section]!
        let fdIndex = currentMonthInfo[FIRST_DAY_INDEX]
        let ldIndex = fdIndex + currentMonthInfo[NUMBER_OF_DAYS_INDEX] - 1
        
        if let idx = indexPathForDate(Date()) {
            
            if (idx.section < indexPath.section && fdIndex <= indexPath.item && ldIndex >= indexPath.item) || (idx.section == indexPath.section && idx.item  <= indexPath.item && ldIndex >= indexPath.item) {
                
                let dayCell = collectionView.cellForItem(at: indexPath) as! CalendarDayCell
                if let cellDate = dayCell.date, let index = self.selectedDates.index(of: cellDate) {
                    selectedDates.remove(at: index)
                    dayCell.isModified = false
                    
                    delegate?.calendar(self, didSelectDate: dayCell.date!, isSelected: false)
                }
                else {
                    
                    if multipleSelectionAllowed == false {
                        selectedDates.removeAll()
                    }
                    selectedDates.append(dayCell.date!)
                    dayCell.isModified = true
                    
                    delegate?.calendar(self, didSelectDate: dayCell.date!, isSelected: true)
                }
            }
            else if fdIndex <= indexPath.item && ldIndex >= indexPath.item {
                
                if let delegate = self.delegate, let call = delegate.calendarDateSelectedBeyondSelectionLimit {
                    call(DateSelectionType.Earlier.rawValue)
                }
            }
        }
        
        collectionView.reloadData()
    }
    
    func reloadData() {
        self.calendarView.reloadData()
    }
    
    // MARK: UIScrollViewDelegate
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let _ = self.calculateDateBasedOnScrollViewPosition()
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        let _ = self.calculateDateBasedOnScrollViewPosition()
    }
    
    
    func calculateDateBasedOnScrollViewPosition() -> Date? {
        
        if currentSection == 0 {
            //btnForPrev.isEnabled = false
        }
        else {
            //btnForPrev.isEnabled = true
        }
        
        if currentSection == (calendarView.numberOfSections - 1) {
            //btnForNext.isEnabled = false
        }
        else {
            //btnForNext.isEnabled = true
        }
        
        if let dateDetail = getCurrentMonthDetail() {
            
//            self.headerView.monthLabel.text = dateDetail.monthStr.uppercased() + " " + String(dateDetail.year)
            self.displayDate = dateDetail.yearDate
            
            return dateDetail.yearDate
        }
        
        return nil
    }
    
    //MARK:- Internal Methods
    func selectAllDays() {
        
        if let noOfDays = monthInfo[currentSection], let startIndexPath = indexPathForDate(Date()) {
            
            selectedDates.removeAll()
            quickSelection = .everyDay

            let start = (startIndexPath.section == currentSection) ? (startIndexPath.item - noOfDays[FIRST_DAY_INDEX]) : 0
            for index in start..<noOfDays[NUMBER_OF_DAYS_INDEX] {
                
                if let date = getDate(day: index, month: currentSection) {
                    selectedDates.append(date)
                }
            }
            calendarView.reloadData()
        }
    }
    
    func selectAllWeekdays() {
        
        if let noOfDays = monthInfo[currentSection], let startIndexPath = indexPathForDate(Date()) {
            
            selectedDates.removeAll()
            quickSelection = .everyWeekeed
            
            let start = (startIndexPath.section == currentSection) ? (startIndexPath.item - noOfDays[FIRST_DAY_INDEX]) : 0

            for index in start..<noOfDays[NUMBER_OF_DAYS_INDEX] {
                
                if let date = getDate(day: index, month: currentSection), !isWeekend(date: date) {
                    selectedDates.append(date)
                }
            }
            calendarView.reloadData()
        }
    }
    
    func deSelectAll() {
        
        quickSelection = .none
        selectedDates.removeAll()
        calendarView.reloadData()
    }
    
    //MARK:- Private Method
    private func getCurrentMonthDetail() -> (weekday: Int, month: Int, monthStr: String, year: Int, yearDate: Date)? {
        
        var monthsOffsetComponents = DateComponents()
        monthsOffsetComponents.month = currentSection

        guard let yearDate = (self.gregorian as NSCalendar).date(byAdding: monthsOffsetComponents, to: self.startOfMonthCache, options: NSCalendar.Options()) else {
            return nil
        }
        
        let components = Calendar(identifier: .gregorian).dateComponents(Set(arrayLiteral: .weekday, .month, .year), from: yearDate)
        let monthName = DateFormatter().shortMonthSymbols[(components.month!-1) % 12] // 0 indexed array
        
        return (components.weekday!, components.month!, monthName, components.year!, yearDate)
    }
    
    private func getDate(day: Int, month: Int) -> Date? {
        
        var monthsOffsetComponents = DateComponents()
        monthsOffsetComponents.month = month
        monthsOffsetComponents.day = day
    
        return  Calendar(identifier: .gregorian).date(byAdding: monthsOffsetComponents, to: self.startOfMonthCache)
    }
    
    private func isWeekend(date: Date) -> Bool {
        
        let components = Calendar(identifier: .gregorian).dateComponents(Set(arrayLiteral: .weekday), from: date)
        return (components.weekday == 7 || components.weekday == 1)
    }
    
    private func isDateEqual(date1: Date, date2: Date) -> Bool {
    
        let isDaySame = Calendar.current.compare(date1, to: date2, toGranularity: Calendar.Component.day) == .orderedSame
        let isMonthSame = Calendar.current.compare(date1, to: date2, toGranularity: Calendar.Component.month) == .orderedSame
        let isYearSame = Calendar.current.compare(date1, to: date2, toGranularity: Calendar.Component.year) == .orderedSame
        
        if  isDaySame && isMonthSame && isYearSame {
            return true
        }
        return false
    }
}

