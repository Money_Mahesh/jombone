//
//  KDCalendarDayCell.swift
//  KDCalendar
//
//  Created by Michael Michailidis on 02/04/2015.
//  Copyright (c) 2015 Karmadust. All rights reserved.
//

import UIKit

let cellColorDefault = UIColor.clear
let cellSelectedColor = UIColor(white: 1.0, alpha: 0.2)
let borderColor = UIColor(red: 10.0/255.0, green: 153.0/255.0, blue: 217.0/255.0, alpha: 0.8)

class CalendarDayCell: UICollectionViewCell {
    
    @IBOutlet weak var textLabel : UILabel!
    @IBOutlet weak var pBackgroundView: UIView!
    
    var date: Date?
    var eventsCount = 0 {
        didSet {
            for sview in self.dotsView.subviews {
                sview.removeFromSuperview()
            }
            
            let stride = self.dotsView.frame.size.width / CGFloat(eventsCount+1)
            let viewHeight = self.dotsView.frame.size.height
            let halfViewHeight = viewHeight / 2.0
            
            for _ in 0..<eventsCount {
                let frm = CGRect(x: (stride+1.0) - halfViewHeight, y: 0.0, width: viewHeight, height: viewHeight)
                let circle = UIView(frame: frm)
                circle.layer.cornerRadius = halfViewHeight
                circle.backgroundColor = borderColor
                self.dotsView.addSubview(circle)
            }
        }
    }
    
    var isAlreadySchedule : Bool = false {
        
        didSet {
            
            if isModified {
                self.pBackgroundView.backgroundColor = isModified ? UIColor.red : UIColor.clear
            }
            else {
                self.pBackgroundView.backgroundColor = isAlreadySchedule ? UIColor.green : UIColor.clear
            }
        }
    }
    
    var isModified : Bool = false {
        didSet {
            
            if isModified {
                self.pBackgroundView.backgroundColor = isModified ? UIColor.red : UIColor.clear
            }
            else {
                self.pBackgroundView.backgroundColor = isAlreadySchedule ? UIColor.green : UIColor.clear
            }
        }
    }
    
    lazy var dotsView : UIView = {
        
        let frm = CGRect(x: 8.0, y: self.frame.size.width - 10.0 - 4.0, width: self.frame.size.width - 16.0, height: 8.0)
        let dv = UIView(frame: frm)
        
        
        return dv
        
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}
