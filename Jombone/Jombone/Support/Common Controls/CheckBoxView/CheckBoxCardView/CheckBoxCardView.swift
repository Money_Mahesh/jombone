//
//  CheckBoxCardView.swift
//  Jombone
//
//  Created by Money Mahesh on 04/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import UIKit

class CheckBoxCardView: GenericView<CheckBoxCardDataModel> {
    
    @IBOutlet weak var imgViewForIcon: UIImageView!
    @IBOutlet weak var lblFortitle: UILabel!
    
    override var viewModel: ViewModel<CheckBoxCardDataModel>? {
        didSet {
            self.imgViewForIcon.image = UIImage(named: ((viewModel?.data?.state ?? false) ? "checkBoxSelectedFilter" : "checkBoxFilter"))
            self.lblFortitle.text = viewModel?.data?.displayValue
        }
    }
}
