//
//  CheckBoxCardViewModel.swift
//  Jombone
//
//  Created by Money Mahesh on 04/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

protocol CheckBoxCardDataModel {
    var displayValue: String! {get set}
    var state: Bool! {get set}
}

import Foundation

class CheckBoxCardViewModel: ViewModel<CheckBoxCardDataModel> {
    
    override init(data: CheckBoxCardDataModel?, otherDetail: Any?) {
        super.init(data: data, otherDetail: otherDetail)
    }
}
