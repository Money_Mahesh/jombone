//
//  CheckBoxViewModel.swift
//  Jombone
//
//  Created by Money Mahesh on 04/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation

class CheckBoxViewModel: NSObject {
    var cardsDataModel: [CheckBoxCardDataModel]!
    
    init(data: [CheckBoxCardDataModel]!) {
        cardsDataModel = data
    }
}
