//
//  CheckBoxView.swift
//  Jombone
//
//  Created by Money Mahesh on 04/03/19.
//  Copyright © 2019 quarks. All rights reserved.
//

import Foundation
import UIKit

protocol CheckBoxViewProtocol: NSObjectProtocol {
    func optionTapped(_ identifier: String?, optionIndex: Int, state: Bool)
}

@IBDesignable class CheckBoxView: CustomView {
    
    @IBOutlet weak private var stackView: UIStackView!
    @IBOutlet weak var lblForTitle: UILabel!
    @IBInspectable var Identifier: String?

    @IBInspectable var checkBox: Bool = true
    @IBInspectable var title: String? {
        didSet {
            lblForTitle.text = title
        }
    }
    @IBInspectable var titleColor: UIColor! = UIColor(hexFromString: "#424242") {
        didSet {
            setStyle()
        }
    }
    private lazy var titleFont: UIFont! = UIFont.rubikMediumFontOfSize(18.0)
    

    weak var delegate: CheckBoxViewProtocol?
    var tableWidget: GenericTableViewComponent<CheckBoxCardDataModel, CheckBoxCardView>?
    var viewModel: CheckBoxViewModel?

    var selectedValue: [CheckBoxCardDataModel]? {
        get {
            return viewModel?.cardsDataModel.filter({return $0.state})
        }
    }
    
    override func xibLoaded() {
        setStyle()
    }
    
    func setTitleFont(_ titleFont: UIFont = UIFont.rubikRegularFontOfSize(12)) {
        self.titleFont = titleFont
        setStyle()
    }
    
    func setDetails(data: [CheckBoxCardDataModel], selectedData: [CheckBoxCardDataModel]?) {
        
        if let _selectedData = selectedData {

            let updatedData = data.map { (obj: CheckBoxCardDataModel) -> CheckBoxCardDataModel in
                var updatedObj = obj
                updatedObj.state = (_selectedData.filter({ (obj) -> Bool in
                    return (updatedObj.displayValue.lowercased() == obj.displayValue.lowercased())
                }).count > 0)
                return updatedObj
            }
            viewModel = CheckBoxViewModel(data: updatedData)
        }
        else {
            viewModel = CheckBoxViewModel(data: data)
        }
        setUpTable()
        setStyle()
    }
    
    private func setStyle() {
        lblForTitle.font = titleFont
        lblForTitle.textColor = titleColor
        lblForTitle.text = title
    }
    
    private func setUpTable() {
        
        let cardsDataModel = viewModel?.cardsDataModel
        
        if let _tableWidget = tableWidget {
            _tableWidget.updateTableRecord(objArray: cardsDataModel)
        }
        else {
            tableWidget = GenericTableViewComponent()
            tableWidget?.isHidden = false
            tableWidget?.genericTableViewModel = GenericTableViewModel(
                cellDataModel: cardsDataModel,
                cellOtherDetail: nil,
                tableType: .nonScrollable)
            tableWidget?.tableStyle(backGroundColor: UIColor.white, insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
            tableWidget?.delegate = self
            stackView.addArrangedSubview(tableWidget!)
        }
    }
}

extension CheckBoxView: GenericTableViewComponentDelegate {
    
    func genericTableDidSelectRowAt(_ indexPath: IndexPath, dataSource: inout [Any]) -> Bool {
        
        if checkBox {
            viewModel!.cardsDataModel[indexPath.row].state = !viewModel!.cardsDataModel[indexPath.row].state
        }
        else {
            let currentValue = (dataSource[indexPath.row] as! CheckBoxCardDataModel)
            viewModel?.cardsDataModel = viewModel?.cardsDataModel.map({ (obj: CheckBoxCardDataModel) -> CheckBoxCardDataModel in
                var _obj = obj
                _obj.state = (currentValue.displayValue == obj.displayValue) ? !(obj.state) : false
                
                return obj
            })
        }

        dataSource = viewModel!.cardsDataModel
        delegate?.optionTapped(Identifier, optionIndex: indexPath.row, state: viewModel!.cardsDataModel[indexPath.row].state)
        return true
    }
}
