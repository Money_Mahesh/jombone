//
//  LoadMoreView.swift
//  Jombone
//
//  Created by Money Mahesh on 13/08/18.
//  Copyright © 2018 Money Mahesh. All rights reserved.
//

import UIKit

class LoadMoreView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadViewFromXibWithFrame(frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private func loadViewFromXibWithFrame(_ frame : CGRect) {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "LoadMoreView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        view.frame = frame
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        self.addSubview(view)
    }
}
