//
//  LoaderView.swift
//  Jombone
//
//  Created by Money Mahesh on 13/08/18.
//  Copyright © 2015 tbsl. All rights reserved.
//

import UIKit


class LoaderView: UIView {
    
    @IBOutlet weak private var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak private var lblForMessage: UILabel!
    
    override private init(frame: CGRect) {
        super.init(frame: frame)
        self.loadViewFromXibWithFrame(frame)
    }

    required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
    }
    
    private func loadViewFromXibWithFrame(_ frame : CGRect) {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "LoaderView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        view.frame = frame
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        self.addSubview(view)
    }
    
    static var shared = LoaderView(frame: CGRect(x: 0, y: 0, width: WINDOW_WIDTH, height: WINDOW_HEIGHT))
    
    func showToView(_ mainView : UIView, frame : CGRect = CGRect(x: 0, y: 0, width: WINDOW_WIDTH, height: WINDOW_HEIGHT), message: String = "") {
        
        self.frame = CGRect(x: 0, y: 0, width: WINDOW_WIDTH, height: mainView.frame.size.height)
        activityIndicator.startAnimating()
        
        lblForMessage.isHidden = message.count == 0
        lblForMessage.text = message
        
        if !mainView.subviews.contains(self) {
            mainView.addSubview(self)
        }
    }
    
    func hide() {
        removeFromSuperview()
    }
}
