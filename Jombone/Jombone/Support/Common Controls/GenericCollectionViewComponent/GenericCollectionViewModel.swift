//
//  GenericCollectionViewModel.swift
//  Jombone
//
//  Created by Money Mahesh on 08/08/18.
//  Copyright © 2018 Money Mahesh. All rights reserved.
//

import Foundation
import UIKit

enum CollectionType {
    case nonScrollable
    case width(width: CGFloat)
    case expandable
}

class GenericCollectionViewModel: NSObject {
    
    var title: String?
    var _description: String?
    var showTickIcon: Bool = false
    var height: CGFloat!
    var cellExpandable: Bool = false
    var collectionType: CollectionType = .nonScrollable
    var identifier = String()
    var cellViewModel = [Any]()
    
    var numberOfSection: Int {
        return cellViewModel.count
    }
    
    override init() {
        self.collectionType = .nonScrollable
        self.cellViewModel = [Any]()
    }
    
    init(identifier: String,
         title: String? = nil,
         description: String? = nil,
         showTickIcon: Bool = false,
         cellViewModel: [Any]? = nil,
         height: CGFloat,
         cellExpandable: Bool = false,
         collectionType: CollectionType = .nonScrollable) {
        
        self.identifier = identifier
        self.title = title
        self._description = description
        self.showTickIcon = showTickIcon
        self.height = height
        self.collectionType = collectionType
        self.cellExpandable = cellExpandable
        self.cellViewModel = cellViewModel ?? [Any]()
    }
}
