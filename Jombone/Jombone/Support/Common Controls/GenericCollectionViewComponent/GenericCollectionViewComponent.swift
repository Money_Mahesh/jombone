//
//  GenericCollectionViewComponent.swift
//  Jombone
//
//  Created by Money Mahesh on 08/08/18.
//  Copyright © 2018 Money Mahesh. All rights reserved.
//

import UIKit

extension Notification.Name {
    static let CollectionCellSizeUpdated = Notification.Name("CollectionCell")
}

protocol GenericCollectionViewComponentDelegate: NSObjectProtocol {
    func genericCollectionDidSelectItemAt(_ indexPath: IndexPath, collectionView: UICollectionView, identifier: String, dataSource: inout [Any]) -> Bool
}
extension GenericCollectionViewComponentDelegate {
    func genericCollectionDidSelectItemAt(_ indexPath: IndexPath, collectionView: UICollectionView, identifier: String, dataSource: inout [Any]) -> Bool { return false }
}

class GenericCollectionViewComponent<V: Any, T: GenericView<V>>: CustomView, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak private var lblForTitle: UILabel?
    
    @IBOutlet weak private var constForLabelTop: NSLayoutConstraint?
    @IBOutlet weak private var constForLabelLeading: NSLayoutConstraint?
    @IBOutlet weak private var constForLabelTrailing: NSLayoutConstraint?
    @IBOutlet weak private var constForLabelBottom: NSLayoutConstraint?
    
    @IBOutlet weak private var lblForDescription: UILabel?
    @IBOutlet weak private var imageViewForIcon: UIImageView?
    @IBOutlet weak private var stackViewForDescription: UIStackView?

    @IBOutlet weak private var viewForShadow: UIView?
    @IBOutlet weak private var collectionView: UICollectionView?
    @IBOutlet weak private var constForCollectionHeight: NSLayoutConstraint?
    @IBOutlet weak private var constForCollectionWidth: NSLayoutConstraint?
    @IBOutlet weak private var constForTop: NSLayoutConstraint?
    @IBOutlet weak private var constForLeading: NSLayoutConstraint?
    @IBOutlet weak private var constForTrailing: NSLayoutConstraint?
    @IBOutlet weak private var constForBottom: NSLayoutConstraint?
    
    private var widthConstant: CGFloat?
    private var iscontentSizeObserverAdded = false
    var delegate: GenericCollectionViewComponentDelegate?
    var itemInterspacing: CGFloat = 0
    var identifier = String()

    override func loadNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "GenericCollectionViewComponent", bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as! UIView
    }
    
    var viewModel: GenericCollectionViewModel! = GenericCollectionViewModel() {
        didSet {
            
            if let title = viewModel.title {
                lblForTitle?.text = title
            }
            else {
                lblForTitle?.removeFromSuperview()
            }
            
            if let description = viewModel._description {
                lblForDescription?.text = description
                imageViewForIcon?.isHidden = !viewModel.showTickIcon
            }
            else {
                stackViewForDescription?.isHidden = true
            }

            identifier = viewModel.identifier
            constForCollectionHeight?.constant = viewModel.height
            
            switch viewModel.collectionType {
            case .nonScrollable:
                collectionView?.addObserver(self, forKeyPath: "contentSize", options: [NSKeyValueObservingOptions.new, NSKeyValueObservingOptions.old], context: nil)
                iscontentSizeObserverAdded = true
                collectionView?.isScrollEnabled = false
                constForCollectionWidth?.isActive = true

            case let .width(width):
                widthConstant = width
                constForCollectionWidth?.constant = width
                collectionView?.addObserver(self, forKeyPath: "contentSize", options: [NSKeyValueObservingOptions.new, NSKeyValueObservingOptions.old], context: nil)
                iscontentSizeObserverAdded = true
                collectionView?.isScrollEnabled = true
                constForCollectionWidth?.isActive = true

            case .expandable:
                constForCollectionWidth?.isActive = false
                collectionView?.isScrollEnabled = true
            }
            
            
            //Add observer
            if viewModel.cellExpandable {
                collectionView?.addObserver(self, selector: #selector(GenericCollectionViewComponent.valueUpdated(_:)), notificationsName: [Notification.Name.CollectionCellSizeUpdated])
            }
            if viewModel.cellViewModel.count == 0 {
                constForCollectionWidth?.isActive = false
                constForCollectionHeight?.constant = 0
            }
        }
    }
    
    func setLayout(estimatedItemSize: CGSize = CGSize(width: 1, height: 1), scrollDirection: UICollectionViewScrollDirection = .horizontal, sectionInset: UIEdgeInsets = UIEdgeInsets.zero, itemInterspacing: CGFloat = 0) {
        
        var layout = UICollectionViewFlowLayout()

        if let _layout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
            layout = _layout
        }

        layout.estimatedItemSize = estimatedItemSize
        layout.scrollDirection = scrollDirection
        layout.scrollDirection = scrollDirection
        layout.minimumInteritemSpacing = itemInterspacing
        layout.minimumLineSpacing = itemInterspacing
        layout.sectionInset = sectionInset
        layout.invalidateLayout()
        collectionView?.collectionViewLayout = layout
    }
    
    func style(backGroundColor: UIColor = UIColor.clear, collectionInsets: UIEdgeInsets, titleInsets: UIEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 6, right: 0)) {
        
        collectionView?.backgroundColor = backGroundColor
        viewForShadow?.backgroundColor = backGroundColor
        
        constForTop?.constant = collectionInsets.top
        constForBottom?.constant = collectionInsets.bottom
        constForLeading?.constant = collectionInsets.left
        constForTrailing?.constant = collectionInsets.right
        
        constForLabelTop?.constant = titleInsets.top
        constForLabelBottom?.constant = titleInsets.bottom
        constForLabelLeading?.constant = titleInsets.left
        constForLabelTrailing?.constant = titleInsets.right
    }
    
    func setDescription(showBullet: Bool = false, description: String?) {
        
        if let _description = description {
            lblForDescription?.text = _description
            imageViewForIcon?.isHidden = !showBullet
            stackViewForDescription?.isHidden = false
        }
        else {
            stackViewForDescription?.isHidden = true
        }
    }
    
    override func xibLoaded() {
        
        collectionView?.register(GenericCollectionViewCell<T>.self, forCellWithReuseIdentifier: "GenericCollectionViewCell")
        
        viewForShadow?.layer.addShadow(color: CARD_SHADOW_COLOR.cgColor, direction: .down, radius: 4.0, spread: 2)
        viewForShadow?.layer.cornerRadius = 10
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if let change = change, let oldvalue = change[NSKeyValueChangeKey.oldKey] as? CGSize,
            let newvalue = change[NSKeyValueChangeKey.newKey] as? CGSize,
            let collectionView = collectionView, oldvalue != newvalue {
                        
            var newWidth = collectionView.contentSize.width
            if let constWidth = widthConstant, newWidth >= constWidth {
                newWidth = constWidth
            }
            collectionView.layer.removeAllAnimations()
            constForCollectionWidth?.constant = newWidth
        }
    }
    
    deinit {
        if iscontentSizeObserverAdded {
            iscontentSizeObserverAdded = false
            collectionView?.removeObserver(self, forKeyPath: "contentSize")
        }
    }
    
    //MARK:- CollectionView DataSource & Delegate
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.cellViewModel.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GenericCollectionViewCell", for: indexPath) as! GenericCollectionViewCell<T>
        cell.view?.viewModel = viewModel.cellViewModel[indexPath.row] as? V
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let reloadData = delegate?.genericCollectionDidSelectItemAt(indexPath, collectionView: collectionView, identifier: identifier, dataSource: &viewModel.cellViewModel), reloadData {
            collectionView.reloadData()
        }
    }
    
    func reloadCollectionWithData(objArray: [Any]) {
        
        if viewModel.cellViewModel.count != objArray.count {
            viewModel.cellViewModel.removeAll()
            collectionView?.reloadData()
            collectionView?.collectionViewLayout.invalidateLayout()

            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
                self.viewModel.cellViewModel = objArray
                self.collectionView?.reloadData()
                self.collectionView?.collectionViewLayout.invalidateLayout()

                self.constForCollectionHeight?.constant = (self.viewModel.cellViewModel.count == 0) ? 0 : self.viewModel.height
            }
        }
        else {
            self.viewModel.cellViewModel = objArray
            self.collectionView?.reloadData()
            self.collectionView?.collectionViewLayout.invalidateLayout()
        }
        self.constForCollectionHeight?.constant = (self.viewModel.cellViewModel.count == 0) ? 0 : self.viewModel.height
    }
    
    //MARK:- Notification Observer Methods
    //handle notification
    @objc func valueUpdated(_ notification: NSNotification) {
        
        if notification.name == Notification.Name.CollectionCellSizeUpdated {
            
            if let indexPath = notification.userInfo?["indexPath"] as? IndexPath {
                collectionView?.reloadItems(at: [indexPath])
            }
            else {
                collectionView?.reloadData()
            }
        }
    }
}
