//
//  GenericCollectionViewCell.swift
//  Jombone
//
//  Created by Money Mahesh on 03/08/18.
//  Copyright © 2018 Money Mahesh. All rights reserved.
//

import UIKit

class GenericCollectionViewCell<T: CustomView>: UICollectionViewCell {
    
    var view: T?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.clear
        

        if view == nil {
            view = T()
            self.contentView.addSubview(view!)
            
            let topConstraint = NSLayoutConstraint(item: view!, attribute: .top, relatedBy: .equal, toItem: self.contentView, attribute: .top, multiplier: 1.0, constant: 1)
            topConstraint.isActive = true
            
            let bottomConstraint = NSLayoutConstraint(item: view!, attribute: .bottom, relatedBy: .equal, toItem: self.contentView, attribute: .bottom, multiplier: 1.0, constant: 1)
            bottomConstraint.isActive = true
            
            let leftConstraint = NSLayoutConstraint(item: view!, attribute: .left, relatedBy: .equal, toItem: self.contentView, attribute: .left, multiplier: 1.0, constant: 1)
            leftConstraint.isActive = true
            
            let rightConstraint = NSLayoutConstraint(item: view!, attribute: .right, relatedBy: .equal, toItem: self.contentView, attribute: .right, multiplier: 1.0, constant: 1)
            rightConstraint.isActive = true
            
            view!.translatesAutoresizingMaskIntoConstraints = false
            self.contentView.addConstraint(topConstraint)
            self.contentView.addConstraint(bottomConstraint)
            self.contentView.addConstraint(leftConstraint)
            self.contentView.addConstraint(rightConstraint)
        }
        else {
            view?.layoutIfNeeded()
        }
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
