//
//  ToolTip.swift
//  Jombone
//
//  Created by Money Mahesh on 21/11/18.
//  Copyright © 2018 tbsl. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class ToolTip: UIView {
    
    enum ArrowDirection {
        case topCenter
        case topLeft
        case topRight
        case downCenter
        case downLeft
        case downRight
    }
    
    @IBInspectable var arrowTopLeft: Bool = false
    @IBInspectable var arrowTopCenter: Bool = true
    @IBInspectable var arrowTopRight: Bool = false
    @IBInspectable var arrowBottomLeft: Bool = false
    @IBInspectable var arrowBottomCenter: Bool = false
    @IBInspectable var arrowBottomRight: Bool = false
    
    @IBInspectable var fillColor: UIColor = UIColor.green
    
//    @IBInspectable var borderColor: UIColor = UIColor(red:0, green:0, blue:0, alpha:0.05)
    @IBInspectable var borderRadius: CGFloat = 18
//    @IBInspectable var borderWidth: CGFloat = 1
    
    @IBInspectable var shadowColor: UIColor = UIColor(red:0, green:0, blue:0, alpha:0.14)
    @IBInspectable var shadowOffsetX: CGFloat = 0
    @IBInspectable var shadowOffsetY: CGFloat = 2
    @IBInspectable var shadowBlur: CGFloat = 10
    
    var arrowDirection: ArrowDirection! {
        didSet {
            
            arrowTopLeft = false
            arrowTopCenter = false
            arrowTopRight = false
            arrowBottomLeft = false
            arrowBottomCenter = false
            arrowBottomRight = false
            
            switch arrowDirection! {
            case .topCenter:
                arrowTopCenter = true
                
            case .topLeft:
                arrowTopLeft = true
                
            case .topRight:
                arrowTopRight = true

            case .downCenter:
                arrowBottomCenter = true

            case .downLeft:
                arrowBottomLeft = true

            case .downRight:
                arrowBottomRight = true
            }
        }
    }
    
    var tooltipWidth = 0
    var tooltipHeight = 0
    var customHorizontalForPointer: CGFloat?
//    var customVerticalForPointer: CGFloat?

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func draw(_ rect: CGRect) {
        drawTooltip()
    }
    
    //MARK: - Private Methods
    
    // Orientation methods
    
    private func topLeft(_ x: CGFloat, _ y: CGFloat) -> CGPoint {
        return CGPoint(x: x, y: y)
    }
    
    private func topRight(_ x: CGFloat, _ y: CGFloat) -> CGPoint {
        return CGPoint(x: CGFloat(tooltipWidth) - x, y: y)
    }
    
    private func bottomLeft(_ x: CGFloat, _ y: CGFloat) -> CGPoint {
        return CGPoint(x: x, y: CGFloat(tooltipHeight) - y)
    }
    
    private func bottomRight(_ x: CGFloat, _ y: CGFloat) -> CGPoint {
        return CGPoint(x: CGFloat(tooltipWidth) - x, y: CGFloat(tooltipHeight) - y)
    }
    
    // Draw methods
    private func drawTooltip() {
        
        tooltipWidth = Int(bounds.width)
        tooltipHeight = Int(bounds.height)
        
        if customHorizontalForPointer == nil {
            customHorizontalForPointer = CGFloat(tooltipWidth) / CGFloat(2.0)
        }
        // Define Bubble Shape
        let bubblePath = UIBezierPath()
        
        // Top left corner
        
        bubblePath.move(to: topLeft(0, borderRadius))
        bubblePath.addCurve(to: topLeft(borderRadius, 0), controlPoint1: topLeft(0, borderRadius / 2), controlPoint2: topLeft(borderRadius / 2, 0))
        
        // Top right corner
        
        bubblePath.addLine(to: topRight(borderRadius, 0))
        bubblePath.addCurve(to: topRight(0, borderRadius), controlPoint1: topRight(borderRadius / 2, 0), controlPoint2: topRight(0, borderRadius / 2))
        
        // Bottom right corner
        
        bubblePath.addLine(to: bottomRight(0, borderRadius))
        bubblePath.addCurve(to: bottomRight(borderRadius, 0), controlPoint1: bottomRight(0, borderRadius / 2), controlPoint2: bottomRight(borderRadius / 2, 0))
        
        // Bottom left corner
        
        bubblePath.addLine(to: bottomLeft(borderRadius, 0))
        bubblePath.addCurve(to: bottomLeft(0, borderRadius), controlPoint1: bottomLeft(borderRadius / 2, 0), controlPoint2: bottomLeft(0, borderRadius / 2))
        bubblePath.close()
        
        // Arrow
        
        if(arrowTopLeft) {
            bubblePath.move(to: topLeft(3, 10))
            bubblePath.addLine(to: topLeft(3, -4))
            bubblePath.addLine(to: topLeft(16, 2))
            bubblePath.close()
        }
        
        if(arrowTopCenter) {
            bubblePath.move(to: topLeft(CGFloat(customHorizontalForPointer! - 6), 0))
            bubblePath.addLine(to: topLeft(CGFloat(customHorizontalForPointer!), -6))
            bubblePath.addLine(to: topLeft(CGFloat(customHorizontalForPointer! + 6), 0))
            bubblePath.close()
        }
        
        if(arrowTopRight) {
            bubblePath.move(to: topRight(16, 2))
            bubblePath.addLine(to: topRight(3, -4))
            bubblePath.addLine(to: topRight(3, 10))
            bubblePath.close()
        }
        
        if(arrowBottomLeft) {
            bubblePath.move(to: bottomLeft(16, 2))
            bubblePath.addLine(to: bottomLeft(3, -4))
            bubblePath.addLine(to: bottomLeft(3, 10))
            bubblePath.close()
        }
        
        if(arrowBottomCenter) {
            bubblePath.move(to: bottomLeft(CGFloat(customHorizontalForPointer! - 6), 0))
            bubblePath.addLine(to: bottomLeft(CGFloat(customHorizontalForPointer!), -6))
            bubblePath.addLine(to: bottomLeft(CGFloat(customHorizontalForPointer! + 6), 0))
            bubblePath.close()
        }
        
        if(arrowBottomRight) {
            bubblePath.move(to: bottomRight(3, 10))
            bubblePath.addLine(to: bottomRight(3, -4))
            bubblePath.addLine(to: bottomRight(16, 2))
            bubblePath.close()
        }
        
        // Shadow Layer
        
        let shadowShape = CAShapeLayer()
        shadowShape.path = bubblePath.cgPath
        shadowShape.fillColor = fillColor.cgColor
        shadowShape.shadowColor = shadowColor.cgColor
        shadowShape.shadowOffset = CGSize(width: CGFloat(shadowOffsetX), height: CGFloat(shadowOffsetY))
        shadowShape.shadowRadius = CGFloat(shadowBlur)
        shadowShape.shadowOpacity = 0.8
        
        // Border Layer
        
        let borderShape = CAShapeLayer()
        borderShape.path = bubblePath.cgPath
        borderShape.fillColor = fillColor.cgColor
        borderShape.strokeColor = borderColor.cgColor
        borderShape.lineWidth = CGFloat(BorderWidth*2)
        
        // Fill Layer
        
        let fillShape = CAShapeLayer()
        fillShape.path = bubblePath.cgPath
        fillShape.fillColor = fillColor.cgColor
        
        // Add Sublayers
        self.layer.insertSublayer(shadowShape, at: 0)
        self.layer.insertSublayer(borderShape, at: 0)
        self.layer.insertSublayer(fillShape, at: 0)
        
    }
}
